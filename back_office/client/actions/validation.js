import {VALIDATION_ADD, VALIDATION_REMOVE} from "../constants/actions";

export function addValidationMessage(message) {
	return {
		type: VALIDATION_ADD,
		payload: message,
	};
}

export function delValidationMessage(message) {
	return {
		type: VALIDATION_REMOVE,
		payload: message,
	};
}
