import React, {Component} from "react";
import {Route, Switch} from "react-router";
import Header from "./components/partials/header";
import ErrorDialog from "./components/partials/error";

import Landing from "./components/landing/landing";

import Private from "./components/partials/private";
import Public from "./components/partials/public";

import Contacts from "./components/common/contacts";
import Disclaimer from "./components/common/disclaimer";
import Terms from "./components/common/terms";
import Privacy from "./components/common/privacy";
import Login from "./components/common/login";
import Message from "./components/common/message";
import NotFound from "./components/common/notfound";

import Dashboard from "./components/landing/dashboard";
import Profile from "./components/user/profile";
import Users from "./components/user/index";
import Post from "./components/post/index";

import GAListener from "./components/ga";


export default class App extends Component {
  render() {
    return (
      <GAListener trackingId="UA-89625580-1">
        <Header />
        <ErrorDialog />
        <Switch>
          <Route path="/" component={Landing} exact />

          <Private path="/dashboard" component={Dashboard} />

          <Private path="/profile" component={Profile} />
          <Private path="/users" component={Users} />
          <Private path="/posts" component={Post} />

          <Public path="/login" component={Login} />

          <Public path="/contacts" component={Contacts} />
          <Public path="/disclaimer" component={Disclaimer} />
          <Public path="/terms-of-service" component={Terms} />
          <Public path="/privacy-policy" component={Privacy} />

          <Public path="/message" component={Message} />

          <Public component={NotFound} />
        </Switch>
      </GAListener>
    );
  }
}
