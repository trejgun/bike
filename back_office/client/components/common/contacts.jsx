import React, {Component} from "react";
import {Form} from "react-bootstrap";
import {fullName, phoneNumber} from "../../constants/misc";
import InputStaticGroup from "../inputs/input.static.group";


export default class Contacts extends Component {
  render() {
    return (
      <div>
        <h1>Contacts</h1>
        <Form horizontal>
          <InputStaticGroup
            name="fullName"
            value={fullName}
          />
          <InputStaticGroup
            name="email"
            value="trejgun@purimotors.com"
          />
          <InputStaticGroup
            name="phoneNumber"
            value={phoneNumber}
          />
        </Form>
      </div>
    );
  }
}
