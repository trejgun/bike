import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {FormattedMessage} from "react-intl";
import {MESSAGE_REMOVE} from "../../constants/actions";


@connect(
	state => ({
		messages: state.messages,
	}),
)
export default class Message extends Component {
	static propTypes = {
		messages: PropTypes.array,
		dispatch: PropTypes.func,
	};

	static defaultProps = {
		messages: [],
	};

	componentWillUnmount() {
		this.props.messages.map(message =>
			this.props.dispatch({
				type: MESSAGE_REMOVE,
				data: message,
			}),
		);
	}

	renderDebugInfo() {
		if (process.env.NODE_ENV === "production") {
			return null;
		}
		return (
			<pre>
				DEBUG INFO: <br />
				{JSON.stringify(this.props.messages, null, "\t")}
			</pre>
		);
	}

	renderMessage(message) {
		if (message.status === 404 && message.name === "token") {
			return (
				<div>
					<FormattedMessage id="errors.token-expired" />{" "}
					<a href="/resend"><FormattedMessage id="errors.token-expired-link" /></a>
				</div>
			);
		}
		return <FormattedMessage id={`${message.status ? "errors" : "messages"}.${message.message}`} values={message}
			defaultMessage={message.message} />;
	}

	render() {
		return (
			<div>
				{this.props.messages.map((message, i) =>
					<p key={i}>{this.renderMessage(message)}</p>,
				)}
				{this.renderDebugInfo()}
			</div>
		);
	}
}
