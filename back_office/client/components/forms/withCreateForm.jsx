import React, {Component} from "react";
import PropTypes from "prop-types";
import withForm from "./withForm";
import withGuardianForm from "./withGuardianForm";


export default function withCreate(storeName, paramName) {
	return WrappedComponent => {
		// console.log("withCreate:WrappedComponent", WrappedComponent);
		class CreateForm extends Component {
			static propTypes = {
				onSubmit: PropTypes.func,
				storeName: PropTypes.string,
				paramName: PropTypes.string,

				history: PropTypes.shape({
					push: PropTypes.func.isRequired,
				}).isRequired,
			};

			componentDidUpdate() {
				const {storeName, paramName, history} = this.props;
				// console.log("CreateForm:componentWillReceiveProps", nextProps);
				if (!this.props[storeName].isLoading && this.props[storeName].success && this.props[storeName].name === "create") {
					history.push(`/${storeName}/${this.props[storeName].list[this.props[storeName].list.length - 1][paramName]}`);
				}
			}

			onSubmit(e) {
				// console.log("ListForm:onSubmit", this.props);
				this.props.onSubmit({
					preventDefault: e ? ::e.preventDefault : Function.prototype,
					target: {
						getAttribute: name => {
							return {
								action: (e && e.target.getAttribute(name)) || `/${this.props.storeName}`,
								name: (e && e.target.getAttribute(name)) || "create",
								method: (e && e.target.getAttribute(name)) || "POST",
							}[name];
						},
					},
				});
			}

			render() {
				// console.log("CreateForm:render", this.props);
				return (
					<WrappedComponent
						{...this.props}
						onSubmit={::this.onSubmit}
					/>
				);
			}
		}

		return withForm(storeName, paramName)(withGuardianForm()(CreateForm));
	};
}
