import React, {Component} from "react";
import PropTypes from "prop-types";
import withStore from "./withStore";


export default function withForm(storeName, paramName) {
	return WrappedComponent => {
		// console.log("withForm:WrappedComponent", WrappedComponent);

		class Form extends Component {
			static propTypes = {
				storeName: PropTypes.string,
				dispatch: PropTypes.func,
			};

			state = {
				// don't remove
			};

			onChange(e) {
				const {name, type, checked, value, files} = e.target;
				if (e.target.tagName === "SELECT") {
					if (type === "select-one") {
						switch (e.target.getAttribute("type")) {
							case "number":
								return this.setState({
									[name]: value === "" ? value : parseFloat(value),
								});
							default:
								return this.setState({
									[name]: value,
								});
						}
					} else { // select-multiple
						switch (e.target.getAttribute("type")) {
							case "number":
								return this.setState({
									[name]: [].slice.call(e.target.selectedOptions).map(a => parseFloat(a.value)),
								});
							default:
								return this.setState({
									[name]: [].slice.call(e.target.selectedOptions).map(a => a.value),
								});
						}
					}
				} else { // INPUT
					switch (type) {
						case "file":
							if (e.target.multiple) {
								return this.setState({
									[name]: files, // FileList
								});
							} else {
								return this.setState({
									[name]: files[0], // File
								});
							}
						case "number":
							return this.setState({
								[name]: value === "" ? value : parseFloat(value),
							});
						case "checkbox":
							return this.setState({
								[name]: checked,
							});
						case "text":
						case "email":
						case "password":
						default:
							return this.setState({
								[name]: value,
							});
					}
				}
			}

			onSubmit(e) {
				// console.log("Form:onSubmit", this.props, this.state);
				e.preventDefault();
				const {dispatch, storeName} = this.props;
				return dispatch({
					type: "FETCH_REQUESTED",
					data: this.state,
					storeName,
					//  = {action: "/", method: "GET", name: "view"}
					// can't rely on e.target.method it returns `get` if attribute is not set
					...["action", "method", "name"].reduce((memo, name) => Object.assign(memo, {[name]: e.target.getAttribute(name)}), {}),
				});
			}

			render() {
				// console.log("Form:render", this.props, this.state);
				return (
					<WrappedComponent
						{...this.props}
						formData={this.state}
						onChange={::this.onChange}
						setState={::this.setState}
						onSubmit={::this.onSubmit}
					/>
				);
			}
		}

		return withStore(storeName, paramName)(Form);
	};
}
