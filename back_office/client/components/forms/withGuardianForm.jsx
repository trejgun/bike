import React, {Component} from "react";
import PropTypes from "prop-types";
import {Prompt} from "react-router-dom";


export default function withGuardianForm() {
	return WrappedComponent => {
		// console.log("withGuardianForm:WrappedComponent", WrappedComponent);
		class GuardianForm extends Component {
			static propTypes = {
				storeName: PropTypes.string,
				onChange: PropTypes.func,
			};

			state = {
				isSaved: true,
			};

			static getDerivedStateFromProps(props) {
				// console.log("GuardianForm:getDerivedStateFromProps", props);
				const {storeName} = props;
				if (!props[storeName].isLoading && props[storeName].success) {
					if (props[storeName].name === "update" || props[storeName].name === "create" || props[storeName].name === "batch") {
						return {
							isSaved: true,
						};
					}
				}

				return null;
			}

			onChange(e) {
				// console.log("GuardianForm:onChange", e);
				e.persist();
				const {onChange} = this.props;
				if (this.state.isSaved) {
					this.setState({
						isSaved: false,
					}, () => {
						onChange(e);
					});
				} else {
					onChange(e);
				}
			}

			render() {
				// console.log("GuardianForm:render", this.props, this.state);
				return (
					<>
						<Prompt when={!this.state.isSaved} message="You have unsaved information, are you sure you want to leave this page?" />
						<WrappedComponent
							{...this.props}
							onChange={::this.onChange}
						/>
					</>
				);
			}
		}

		return GuardianForm;
	};
}
