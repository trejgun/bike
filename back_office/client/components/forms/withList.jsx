import React, {Component} from "react";
import PropTypes from "prop-types";
import querystring from "querystring";
import withForm from "./withForm";
import {readFromQueryString} from "../../utils/location";


export default function withListHelper(storeName) {
	return WrappedComponent => {
		class ListHelper extends Component {
			static propTypes = {
				onSubmit: PropTypes.func,
				onChange: PropTypes.func,
				setState: PropTypes.func,
				formData: PropTypes.shape({
					skip: PropTypes.number,
					limit: PropTypes.number,
					view: PropTypes.string,
				}),

				storeName: PropTypes.string,

				location: PropTypes.shape({
					search: PropTypes.string.isRequired,
					pathname: PropTypes.string.isRequired,
				}).isRequired,
				history: PropTypes.shape({
					push: PropTypes.func.isRequired,
				}).isRequired,
			};

			static defaultProps = {
				limit: 12,
				skip: 0,
				view: "list",
			};

			componentDidMount() {
				// console.log("ListForm:componentWillMount", this.props);
				const {setState, location, formData} = this.props;
				setState({
					skip: ~~readFromQueryString("skip", location.search) || formData.skip || ListHelper.defaultProps.skip,
					limit: ~~readFromQueryString("limit", location.search) || formData.limit || ListHelper.defaultProps.limit,
					view: readFromQueryString("view", location.search) || formData.view || ListHelper.defaultProps.view,
				});
			}

			componentDidUpdate() {
				// console.log("ListForm:componentDidUpdate", this.props);
				const {storeName} = this.props;
				// console.log("ListForm:componentDidUpdate", this.props);
				if (!this.props[storeName].isLoading && this.props[storeName].success && this.props[storeName].name === "list") {
					if (this.props[storeName].isLoading) {
						window.scrollTo(0, 0);
						this.updateQueryString(this.props);
					}
				}
			}

			onSubmit(e) {
				// console.log("ListForm:onSubmit", e);
				// e.persist();
				const {onSubmit, storeName} = this.props;
				onSubmit({
					preventDefault: e ? ::e.preventDefault : Function.prototype,
					target: {
						getAttribute: name => {
							return {
								action: `/${storeName}`,
								name: "list",
								method: "GET",
							}[name];
						},
					},
				});
			}

			updateQueryString({formData, location, history}) {
				// console.log("ListForm:updateQueryString");
				const state = Object.assign({}, formData);
				if (state.limit === ListHelper.defaultProps.limit) {
					state.limit = void 0;
				}
				if (state.skip === ListHelper.defaultProps.skip) {
					state.skip = void 0;
				}
				Object.keys(state).forEach(key => {
					if (state[key] === "") {
						state[key] = void 0;
					}
				});
				const search = querystring.stringify(Object.assign({}, querystring.parse(location.search.substring(1)), state));
				if (location.search.substring(1) !== search) {
					history.push({search});
				}
			}

			render() {
				// console.log("ListForm:render", this.props);
				return (
					<WrappedComponent
						{...this.props}
						onSubmit={::this.onSubmit}
					/>
				);
			}
		}

		return withForm(storeName)(ListHelper);
	};
}
