import React, {Component} from "react";


export default function withListForm() {
	return WrappedComponent => {
		// @withForm(storeName)
		// @withStore(storeName)
		return class ListForm extends Component {
			render() {
				// console.log("ListForm:render", this.props);
				return (
					<WrappedComponent
						{...this.props}
					/>
				);
			}
		};
	};
}
