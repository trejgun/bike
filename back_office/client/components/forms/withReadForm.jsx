import React, {Component} from "react";
import PropTypes from "prop-types";
import withForm from "./withForm";
import Loader from "../partials/loader";


export default function withReadForm(storeName, paramName) {
	return WrappedComponent => {
		class ReadForm extends Component {
			static propTypes = {
				setState: PropTypes.func,
				dispatch: PropTypes.func,
				paramName: PropTypes.string,
				storeName: PropTypes.string,

				match: PropTypes.shape({
					params: PropTypes.object.isRequired,
				}).isRequired,
			};

			state = {
				isNew: true,
				isReady: false,
			};

			componentDidMount() {
				// console.log("ReadForm:componentDidMount", this.props);
				const {storeName, paramName, match, dispatch} = this.props;
				const cached = this.getFromStore(this.props);
				if (!cached) {
					dispatch({
						type: "FETCH_REQUESTED",
						action: `/${storeName}/${match.params[paramName]}`,
						storeName: storeName,
						name: "view",
					});
				} else {
					this.props.setState(cached, () => {
						dispatch({
							type: `${storeName}_view_success`,
							name: "view",
							isLoading: false,
							success: true,
							data: cached,
						});
					});
				}
			}

			componentDidUpdate(prevProps, prevState) {
				// console.log("ReadForm:componentDidUpdate", prevProps, this.props);
				const {storeName, setState} = this.props;

				// console.log(this.props[storeName].isLoading, this.props[storeName].name === "view");
				if (this.props[storeName].isLoading && this.props[storeName].name === "view") {
					if (!(prevState.isNew && !prevState.isReady)) {
						this.setState({
							isNew: true,
							isReady: false,
						});
					}
				}

				// console.log(!this.props[storeName].isLoading, this.props[storeName].success, this.props[storeName].name === "view");
				if (!this.props[storeName].isLoading && this.props[storeName].success && this.props[storeName].name === "view") {
					if (this.state.isNew) {
						this.setState({
							isNew: false,
						}, () => {
							const cached = this.getFromStore(this.props);
							if (cached) {
								setState(cached, () => {
									this.setState({
										isReady: true,
									});
								});
							} else {
								this.setState({
									isReady: true,
								});
							}
						});
					}
				}
			}

			getFromStore(props) {
				// console.log("ReadForm:getFromStore", storeName, props);
				return props[props.storeName].list.find(item => item[props.paramName] === props.match.params[props.paramName]);
			}

			render() {
				// console.log("ReadForm:render", this.props, this.state);
				if (this.state.isReady) {
					return (
						<WrappedComponent
							{...this.props}
						/>
					);
				} else {
					return (
						<Loader />
					);
				}
			}
		}

		return withForm(storeName, paramName)(ReadForm);
	};
}
