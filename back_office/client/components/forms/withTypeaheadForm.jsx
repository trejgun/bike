import React, {Component} from "react";
import withTypeaheadProvider from "./withTypeaheadProvider";
import withLoader from "./withLoader";


export default function withTypeaheadForm(storeName, action) {
	return WrappedComponent => {
		class TypeaheadForm extends Component {
			render() {
				// console.log("TypeaheadForm:render", this.props);
				return (
					<WrappedComponent
						{...this.props}
						storeName={action}
					/>
				);
			}
		}

		return withTypeaheadProvider(storeName, action)(withLoader(action)(TypeaheadForm));
	};
}
