import React from "react";
import PropTypes from "prop-types";
import {FormControl, InputGroup} from "react-bootstrap";


export default class ColorButton extends React.Component {
	static propTypes = {
		value: PropTypes.string,
		colors: PropTypes.object,
		onClick: PropTypes.func,
		onChange: PropTypes.func,
	};

	onClick(e) {
		e.preventDefault();
		this.props.onClick(e);
	}

	onChange(e) {
		e.preventDefault();
	}

	render() {
		return (
			<InputGroup>
				<FormControl
					value={this.props.value}
					onClick={::this.onClick}
					onChange={::this.onChange}
				/>
				<InputGroup.Addon onClick={::this.onClick} style={{backgroundColor: this.props.colors[this.props.value]}}>
					&nbsp;&nbsp;
				</InputGroup.Addon>
			</InputGroup>

		);
	}
}
