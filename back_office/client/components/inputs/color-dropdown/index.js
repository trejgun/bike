import "./style.less";
import React, {Component} from "react";
import PropTypes from "prop-types";
import {Dropdown} from "react-bootstrap";
import ColorButton from "./button";
import ColorMenu from "./menu";
import ColorMenuItem from "./menu.item";


export default class InputColor extends Component {
	static propTypes = {
		value: PropTypes.oneOfType([
			PropTypes.string,
			PropTypes.number,
			PropTypes.bool,
		]),
		colors: PropTypes.object,
		onSelect: PropTypes.func,
	};

	render() {
		const {colors, value, onSelect} = this.props;
		return (
			<Dropdown id="colors" className="colors-dropdown" onSelect={onSelect}>
				<ColorButton
					bsRole="toggle"
					value={value}
					colors={colors}
				/>
				<ColorMenu
					bsRole="menu"
					className="colors-menu pull-right">
					{Object.keys(colors).map((color, i) =>
						<ColorMenuItem eventKey={i} key={color} style={{backgroundColor: colors[color], color: colors[color]}} />,
					)}
				</ColorMenu>
			</Dropdown>
		);
	}
}
