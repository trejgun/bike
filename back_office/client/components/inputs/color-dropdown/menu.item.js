import React from "react";
import classNames from "classnames";
import {MenuItem} from "react-bootstrap";
import createChainedFunction from "react-bootstrap/lib/utils/createChainedFunction";


export default class ColorMenuItem extends MenuItem {
	render() {
		const {
			active,
			disabled,
			onClick,
			className,
			style,
		} = this.props;

		return (
			<td
				onClick={createChainedFunction(onClick, this.handleClick)}
				role="menuitem"
				className={classNames(className, {active, disabled})}
				style={style}
			>
				{this.props.children}
			</td>
		);
	}
}
