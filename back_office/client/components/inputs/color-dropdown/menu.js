import React from "react";
import classNames from "classnames";
import {Dropdown} from "react-bootstrap";
import {getClassSet, prefix, splitBsPropsAndOmit} from "react-bootstrap/lib/utils/bootstrapUtils";
import ValidComponentChildren from "react-bootstrap/lib/utils/ValidComponentChildren";
import createChainedFunction from "react-bootstrap/lib/utils/createChainedFunction";
import RootCloseWrapper from "react-overlays/lib/RootCloseWrapper";
import {chunker} from "./utils";


export default class ColorMenu extends Dropdown.Menu {
	renderRow(children, i) {
		const {onSelect} = this.props;

		return (
			<tr key={i}>
				{ValidComponentChildren.map(children, child => (
					React.cloneElement(child, {
						onKeyDown: createChainedFunction(
							child.props.onKeyDown, this.handleKeyDown,
						),
						onSelect: createChainedFunction(
							child.props.onSelect, onSelect,
						),
					})
				))}
			</tr>
		);
	}

	render() {
		const {
			open,
			pullRight,
			labelledBy,
			className,
			rootCloseEvent,
			children,
			...props
		} = this.props;

		const [bsProps, elementProps] = splitBsPropsAndOmit(props, ["onClose"]);

		const classes = {
			...getClassSet(bsProps),
			[prefix(bsProps, "right")]: pullRight,
		};

		return (
			<RootCloseWrapper
				disabled={!open}
				onRootClose={this.handleRootClose}
				event={rootCloseEvent}
			>
				<table
					{...elementProps}
					role="menu"
					className={classNames(className, classes)}
					aria-labelledby={labelledBy}
				>
					<tbody>
						{chunker(children).map(::this.renderRow)}
					</tbody>
				</table>
			</RootCloseWrapper>
		);
	}
}
