export function objectKeyByValue(obj, val) {
	return Object.entries(obj).find(i => i[1] === val)[0];
}

export function chunker(arr) {
	const n = Math.floor(Math.sqrt(arr.length));
	return arr.reduce((p, cur, i) => {
		(p[i / n | 0] = p[i / n | 0] || []).push(cur);
		return p;
	}, []);
}
