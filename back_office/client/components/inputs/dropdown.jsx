import React, {Component} from "react";
import PropTypes from "prop-types";
import {DropdownButton} from "react-bootstrap";
import {omit} from "lodash";


export default class Dropdown extends Component {
	static propTypes = {
		name: PropTypes.string,
		value: PropTypes.string,
		onSelect: PropTypes.func,
	};

	static defaultProps = {
		value: "",
		// defaultValue: "",
		onSelect: Function.prototype,
	};

	onSelect(eventKey) {
		this.props.setState({
			[this.name]: eventKey,
		});
	}

	render() {
		const {name, value} = this.props;
		const props = omit(this.props, ["value", "onSelect"]);

		return (
			<DropdownButton
				id={name}
				title={value}
				onSelect={::this.onSelect}
				{...props}
			>
				{this.props.children}
			</DropdownButton>
		);
	}
}
