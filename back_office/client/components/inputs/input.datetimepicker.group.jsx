import InputDateTimePicker from "./input.datetimepicker";
import withGroup from "./withGroup";


export default withGroup(InputDateTimePicker);
