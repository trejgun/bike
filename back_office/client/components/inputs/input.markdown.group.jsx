import InputMarkdown from "./input.markdown";
import withGroup from "./withGroup";


export default withGroup(InputMarkdown);
