import InputMarkdownGroup from "./input.markdown.group";
import withValidation from "./withValidation";


export default withValidation(InputMarkdownGroup);
