import React, {Component} from "react";
import PropTypes from "prop-types";
import {FormControl} from "react-bootstrap";


export default class InputStatic extends Component {
	static propTypes = {
		name: PropTypes.string,
		value: PropTypes.oneOfType([
			PropTypes.string,
			PropTypes.number,
      PropTypes.element,
		]),
		defaultValue: PropTypes.oneOfType([
			PropTypes.string,
			PropTypes.number,
      PropTypes.element,
    ]),
	};

	render() {
		return (
			<FormControl.Static name={this.props.name}>
				{this.props.defaultValue || this.props.value}
			</FormControl.Static>
		);
	}
}
