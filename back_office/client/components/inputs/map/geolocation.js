export default (
	typeof window !== "undefined" && window.navigator && window.navigator.geolocation
		? window.navigator.geolocation
		: ({
			getCurrentPosition(success, failure) {
				failure(new Error("Your browser doesn't support geolocation."));
			},
		})
);
