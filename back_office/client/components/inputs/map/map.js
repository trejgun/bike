/* global google */
import React, {Component} from "react";
import PropTypes from "prop-types";
import {GoogleMap, Marker, withGoogleMap, withScriptjs} from "react-google-maps";


class Map extends Component {
	static propTypes = {
		onMapMounted: PropTypes.func,
		onBoundsChanged: PropTypes.func,
		onPlacesChanged: PropTypes.func,
		center: PropTypes.object,
	};

	componentDidMount() {
		const searchBox = new google.maps.places.SearchBox(document.querySelector("[name=\"address\"]"));
		searchBox.addListener("places_changed", () => {
			this.props.onPlacesChanged(searchBox.getPlaces());
		});
	}

	render() {
		return (
			<GoogleMap
				ref={this.props.onMapMounted}
				defaultZoom={12}
				center={this.props.center}
				onBoundsChanged={this.props.onBoundsChanged}
			>
				<Marker position={this.props.center} />
			</GoogleMap>
		);
	}
}

export default withScriptjs(withGoogleMap(Map));
