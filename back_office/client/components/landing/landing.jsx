import React, {Component} from "react";
import {Glyphicon} from "react-bootstrap";
import {companyName} from "../../constants/misc";
import LandingFooter from "./landing/footer";
import LandingBanner from "./landing/banner";
import LandingHeader from "./landing/header";
import PostList from "../post/list";
import "./landing.less";


export default class Landing extends Component {
  render() {
    return (
      <div>
        <LandingHeader>
          <h1>{companyName}</h1>
          <h3>Company blog</h3>
          <hr className="intro-divider" />
          <ul className="list-inline intro-social-buttons">
            <li>
              <a href={process.env.MODULE_VEHICLES_MARKET} className="btn btn-default btn-lg">
                <Glyphicon glyph="calendar" />&nbsp;
                <span className="network-name">Vessels Market</span>
              </a>
            </li>
            <li>
              <a href={process.env.MODULE_VEHICLES_OFFICE} className="btn btn-default btn-lg">
                <Glyphicon glyph="briefcase" />&nbsp;
                <span className="network-name">Vessels Office</span>
              </a>
            </li>
          </ul>
        </LandingHeader>

        <div className="container">
          <div className="row">
            <PostList {...this.props} />
          </div>
        </div>

        <LandingBanner>
          <h2>Old era of paper accounting is over!</h2>
        </LandingBanner>

        <LandingFooter />

      </div>
    );
  }
}
