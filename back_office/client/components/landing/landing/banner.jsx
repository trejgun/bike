import React, {Component} from "react";
import PropTypes from "prop-types";
import "./banner.less";


export default class LandingFooter extends Component {
	static propTypes = {
    children: PropTypes.element,
	};

	render() {
		return (
			<div className="banner">
				<div className="container">
					<div className="row">
						<div className="col-lg-6">
							{this.props.children}
						</div>
					</div>
				</div>
			</div>
		);
	}
}
