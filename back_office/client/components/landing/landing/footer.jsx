import React, {Component} from "react";
import {Link} from "react-router-dom";
import {companyName} from "../../../constants/misc";
import "./footer.less";


export default class LandingFooter extends Component {
  render() {
    return (
      <footer>
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <ul className="list-inline">
                <li>
                  <Link to="/contacts">
                    Contacts
                  </Link>
                </li>
                <li className="footer-menu-divider">&sdot;</li>
                <li>
                  <Link to="/disclaimer">
                    Disclaimer
                  </Link>
                </li>
                <li className="footer-menu-divider">&sdot;</li>
                <li>
                  <Link to="/privacy-policy">
                    Privacy policy
                  </Link>
                </li>
                <li className="footer-menu-divider">&sdot;</li>
                <li>
                  <Link to="/terms-of-service">
                    Terms of Service
                  </Link>
                </li>
              </ul>
              <p className="copyright text-muted small">Copyright &copy; {companyName} 2016. All Rights Reserved</p>
              <p className="copyright text-muted small">Design by <a href="http://startbootstrap.com/">Start
                Bootstrap</a></p>
            </div>
          </div>
        </div>
      </footer>
    );
  }
}
