import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, Modal} from "react-bootstrap";
import {connect} from "react-redux";
import {FormattedMessage} from "react-intl";
import {withRouter} from "react-router";
import {MESSAGE_REMOVE} from "../../constants/actions";


@withRouter
@connect(
	state => ({
		messages: state.messages,
	}),
)
export default class ErrorDialog extends Component {
	static propTypes = {
		messages: PropTypes.array,
		dispatch: PropTypes.func,
		location: PropTypes.shape({
			pathname: PropTypes.string.isRequired,
		}),
	};

	onHide(message) {
		return () => {
			this.props.dispatch({
				type: MESSAGE_REMOVE,
				data: message,
			});
		};
	}

	renderDialog(message, i) {
		return (
			<Modal key={i} show onHide={::this.onHide(message)}>
				<Modal.Header closeButton>
					<Modal.Title>
						{message.status ? "An Error Occurred" : "System Information"}
					</Modal.Title>
				</Modal.Header>
				<Modal.Body>
					<p><FormattedMessage id={`${message.status ? "errors" : "messages"}.${message.message}`} values={message}
						defaultMessage={message.message} /></p>
				</Modal.Body>
				<Modal.Footer>
					<Button onClick={::this.onHide(message)}>Close</Button>
				</Modal.Footer>
			</Modal>
		);
	}

	render() {
		if (this.props.location.pathname === "/message") {
			return null;
		}
		return (
			<div>
				{this.props.messages.map(::this.renderDialog)}
			</div>
		);
	}
}
