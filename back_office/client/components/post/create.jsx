import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, ButtonToolbar, Form} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import withCreate from "../forms/withCreateForm";
import {description, title} from "../../constants/placeholder";
import InputGroupValidation from "../inputs/input.group.validation";
import InputMarkdownGroupValidation from "../inputs/input.markdown.group.validation";
import LanguageGroup from "../groups/language";


@withCreate("posts", "postId")
export default class PostCreateForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
    formData: PropTypes.shape({
      title: PropTypes.string,
      description: PropTypes.string,
      postId: PropTypes.string,
      language: PropTypes.string,
    }),
  };

  render() {
    const {formData, onSubmit, onChange} = this.props;
    return (
      <Form horizontal onSubmit={onSubmit}>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.post.create" />
          </h4>
        </div>
        <InputGroupValidation
          name="postId"
          defaultValue={formData.postId}
          placeholder={""}
          onChange={onChange}
          required
        />
        <InputGroupValidation
          name="title"
          defaultValue={formData.title}
          placeholder={title}
          onChange={onChange}
          required
        />
        <InputMarkdownGroupValidation
          name="description"
          defaultValue={formData.description}
          placeholder={description}
          onChange={onChange}
          required
        />
        <LanguageGroup
          defaultValue={formData.language}
          onChange={onChange}
        />
        <ButtonToolbar>
          <Button type="submit" className="pull-right">
            <FormattedMessage id="components.buttons.save" />
          </Button>
        </ButtonToolbar>
      </Form>
    );
  }
}
