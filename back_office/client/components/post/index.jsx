import React, {Component} from "react";
import {Redirect, Route, Switch} from "react-router";
import PostUpdate from "./update";
import PostCreate from "./create";
import PostReadForm from "./read";


export default class Post extends Component {
	render() {
		return (
			<Switch>
				<Redirect from="/posts" to="/" exact />
				<Route path="/posts/create" component={PostCreate} />
				<Route path="/posts/:postId" component={PostReadForm} exact />
				<Route path="/posts/:postId/edit" component={PostUpdate} />
			</Switch>
		);
	}
}
