import {Component} from "react";
import PropTypes from "prop-types";
import {injectIntl, intlShape} from "react-intl";
import withListForm from "../forms/withListForm";


@injectIntl
@withListForm("posts")
export default class PostListForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    setState: PropTypes.func,
    formData: PropTypes.shape({
      language: PropTypes.string,
    }),

    intl: intlShape.isRequired,
  };

  componentDidMount() {
    // console.log("PostListForm:componentDidMount", this.props);
    const {setState, onSubmit, intl} = this.props;
    setState({
      language: intl.locale,
    }, onSubmit);
  }

  componentDidUpdate() {
    // console.log("PostListForm:componentDidUpdate", this.props);
    const {formData, setState, onSubmit, intl} = this.props;
    if (formData.language !== intl.locale) {
      setState({
        language: intl.locale,
      }, onSubmit);
    }
  }

  render() {
    return null;
  }
}
