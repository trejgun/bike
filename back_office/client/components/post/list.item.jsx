import React, {Component} from "react";
import PropTypes from "prop-types";
import {ListGroupItem} from "react-bootstrap";
import {Link} from "react-router-dom";
import withListItemHelper from "../forms/withListItem";


@withListItemHelper("posts")
export default class PostListItem extends Component {
  static propTypes = {
    formData: PropTypes.shape({
      postId: PropTypes.string,
      title: PropTypes.string,
      description: PropTypes.string,
    }),
  };

  render() {
    // console.log("PostListItem:render", this.props);
    const {formData} = this.props;
    return (
      <ListGroupItem>
        <div className="post-preview">
          <h2 className="post-title">
            <Link to={`/posts/${formData.postId}`}>
              {formData.title}
            </Link>
          </h2>
          <p className="post-meta">
            {formData.description}
          </p>
          <hr />
        </div>
      </ListGroupItem>
    );
  }
}
