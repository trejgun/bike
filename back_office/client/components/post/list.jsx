import React, {Component} from "react";
import Form from "./list.form";
import Item from "./list.item";
import Pagination from "../groups/pagination";
import withListHelper from "../forms/withList";
import "./list.less";


@withListHelper("posts")
export default class PostList extends Component {
  render() {
    // console.log("PostList:render", this.props);
    return (
      <div>
        <Form {...this.props} />
        <Item {...this.props} />
        <Pagination {...this.props} />
      </div>
    );
  }
}

