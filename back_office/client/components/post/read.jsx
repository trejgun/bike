import React, {Component} from "react";
import PropTypes from "prop-types";
import {Form} from "react-bootstrap";
import withDeleteForm from "../forms/withDeleteForm";
import "./read.less";


@withDeleteForm("posts", "postId")
export default class PostReadForm extends Component {
  static propTypes = {
    formData: PropTypes.shape({
      title: PropTypes.string,
      description: PropTypes.string,
    }),
  };

  render() {
    // console.log("PostReadForm:render", this.props);
    const {formData} = this.props;
    return (
      <Form horizontal>
        <div className="headline">
          <h4>
            {formData.title}
          </h4>
        </div>
        {formData.description}
      </Form>
    );
  }
}

