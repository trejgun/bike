import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, ButtonToolbar, Form} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import {description, title} from "../../constants/placeholder";
import withUpdateForm from "../forms/withUpdateForm";
import InputGroupValidation from "../inputs/input.group.validation";


@withUpdateForm("posts", "postId")
export default class PostUpdateForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
    formData: PropTypes.shape({
      title: PropTypes.string,
      description: PropTypes.string,
    }),
  };

  render() {
    const {formData, onSubmit, onChange} = this.props;
    return (
      <Form horizontal onSubmit={onSubmit}>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.post.update" />
          </h4>
        </div>
        <InputGroupValidation
          name="title"
          defaultValue={formData.title}
          placeholder={title}
          onChange={onChange}
          required
        />
        <InputGroupValidation
          name="description"
          defaultValue={formData.description}
          placeholder={description}
          onChange={onChange}
          required
        />
        <ButtonToolbar>
          <Button type="submit" className="pull-right">
            <FormattedMessage id="components.buttons.save" />
          </Button>
        </ButtonToolbar>
      </Form>
    );
  }
}
