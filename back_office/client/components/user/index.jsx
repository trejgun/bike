import React, {Component} from "react";
import {Route, Switch} from "react-router";
import UserUpdate from "./update";
import UserCreate from "./create";
import UserList from "./list";
import UserReadForm from "./read";


export default class User extends Component {
	render() {
		return (
			<Switch>
				<Route path="/users" component={UserList} exact />
				<Route path="/users/create" component={UserCreate} />
				<Route path="/users/:_id" component={UserReadForm} exact />
				<Route path="/users/:_id/edit" component={UserUpdate} />
			</Switch>
		);
	}
}
