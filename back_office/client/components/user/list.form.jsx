import React, {Component} from "react";
import PropTypes from "prop-types";
import ImageViewer from "../imageViewer";
import withListForm from "../forms/withListForm";


@withListForm("users")
export default class UserListForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
  };

  componentDidMount() {
    // console.log("VehicleListForm:componentDidMount", this.props);
    this.props.onSubmit();
  }

  render() {
    return (
      <div>
        <ImageViewer
          placeholder="organization"
          title=""
        />
      </div>
    );
  }
}

