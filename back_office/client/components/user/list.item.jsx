import React, {Component} from "react";
import PropTypes from "prop-types";
import {withRouter} from "react-router";
import {Button, Form, Image, ListGroupItem, Media} from "react-bootstrap";
import withListItemHelper from "../forms/withListItem";
import {LinkContainer} from "react-router-bootstrap";
import {listItemHeight, listItemWidth} from "../../constants/display";
import InputStaticGroup from "../inputs/input.static.group";


@withRouter
@withListItemHelper("users")
export default class UserListItem extends Component {
  static propTypes = {
    formData: PropTypes.shape({
      _id: PropTypes.string,
      image: PropTypes.string,
      fullName: PropTypes.string,
      email: PropTypes.string,
    }),
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
    user: PropTypes.object,
    storeName: PropTypes.string,
    dispatch: PropTypes.func,
  };

  componentDidUpdate() {
    const {storeName, history} = this.props;
    if (!this.props[storeName].isLoading && this.props[storeName].success && this.props[storeName].name === "impersonate") {
      history.push("/");
    }
  }

  impersonate() {
    const {dispatch, storeName, formData} = this.props;
    dispatch({
      type: "FETCH_REQUESTED",
      action: `/users/impersonate/${formData._id}`,
      method: "GET",
      storeName,
      name: "sync",
    });
  }

  render() {
    const {formData} = this.props;
    return (
      <LinkContainer key={formData._id} to={`/users/${formData._id}`}>
        <ListGroupItem key={formData._id}>
          <Media>
            <Media.Left className="hidden-xs">
              <Image
                width={listItemWidth}
                height={listItemHeight}
                src={formData.image || `${process.env.MODULE_CDN}/img/icons/user.png`}
                className="media-object img-thumbnail"
                alt="Image"
              />
            </Media.Left>
            <Media.Body>
              <Media.Heading>{formData.fullName}</Media.Heading>
              <Form horizontal>
                <InputStaticGroup
                  name="email"
                  value={formData.email}
                />
                <Button onClick={::this.impersonate}>supervise</Button>
              </Form>
            </Media.Body>
          </Media>
        </ListGroupItem>
      </LinkContainer>
    );
  }
}
