export default {
	black: "#000000",
	red: "#ff0000",
	orange: "#ff8c00",
	yellow: "#ffff00",
	green: "#00ff00",
	aqua: "#00ffff",
	blue: "#0000ff",
	magenta: "#ff00ff",
	white: "#ffffff",
};
