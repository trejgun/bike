import "../node_modules/bootstrap/less/bootstrap.less";
import "../node_modules/font-awesome/less/font-awesome.less";
import "./shared.less";
import "./market-office.less";
import "./styles.less";

import "../static/favicon.ico";
import "../server/configs/moment";
import App from "./app";
import hydrate from "./utils/render";
import configureStore from "./store";


const store = configureStore(window.__INITIAL_STATE__);

hydrate(App, store);

if (module.hot) {
  module.hot.accept("./app", () => {
    hydrate(App, store);
  });
}
