import {combineReducers} from "redux";
import {routerReducer as routing} from "react-router-redux";
import {intlReducer as intl} from "react-intl-redux";

import messages from "./messages";
import posts from "./posts";
import user from "./user";
import users from "./users";
import validations from "./validations";


export default combineReducers({
  intl,
  messages,
  posts,
  routing,
  validations,
  user,
  users,
});
