import {remove, replaceAll, replaceOrAppend} from "../utils/reducer";


const posts = {
  list: [],
  count: 0,
  isLoading: true,
  success: false,
  action: "",
};

export default function postsReducer(state = posts, action) {
  switch (action.type) {
    case "posts_view_start":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        name: action.name,
      });
    case "posts_view_success":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        list: replaceOrAppend(state, action.data),
        name: action.name,
      });
    case "posts_view_error":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        name: action.name,
      });
    case "posts_list_start":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        name: action.name,
      });
    case "posts_list_success":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        list: replaceAll(state, action.data.list),
        count: action.data.count,
        name: action.name,
      });
    case "posts_list_error":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        name: action.name,
      });
    case "posts_create_start":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        name: action.name,
      });
    case "posts_create_success":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        list: replaceOrAppend(state, action.data),
        name: action.name,
      });
    case "posts_create_error":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        name: action.name,
      });
    case "posts_update_start":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        name: action.name,
      });
    case "posts_update_success":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        list: replaceOrAppend(state, action.data),
        name: action.name,
      });
    case "posts_update_error":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        name: action.name,
      });
    case "posts_delete_start":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        name: action.name,
      });
    case "posts_delete_success":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        list: remove(state, action.data),
        name: action.name,
      });
    case "posts_delete_error":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        name: action.name,
      });
    default:
      return state;
  }
}
