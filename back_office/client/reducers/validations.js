import {VALIDATION_ADD, VALIDATION_ADD_ALL, VALIDATION_REMOVE} from "../constants/actions";


const validations = [];

export default function update(state = validations, action) {
	switch (action.type) {
		case VALIDATION_ADD:
			return [...state, action.data];
		case VALIDATION_ADD_ALL:
			return [...state, ...action.data];
		case VALIDATION_REMOVE:
			return state.filter(message => message !== action.data);
		default:
			return state;
	}
}
