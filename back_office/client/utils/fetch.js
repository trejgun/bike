import fetch from "isomorphic-fetch";
import {call, put} from "redux-saga/effects";
import {MESSAGE_ADD, MESSAGE_ADD_ALL, VALIDATION_ADD_ALL} from "../constants/actions";
import {readFromCookies} from "./location";


export default function * ({action, storeName, name, method = "GET", data = {}}) {
	// console.log("FETCH", action, storeName, method, name, data);

	// TODO CORS
	const url = new URL(`${document.location.protocol}//${document.location.host}/api${action}`);

	if (method === "GET") {
		Object.keys(data).forEach(key => url.searchParams.append(key, data[key]));
	}

	yield put({
		type: `${storeName}_${name}_start`,
		name,
		isLoading: true,
		success: false,
		data,
	});

	try {
		const response = yield call(fetch, url, {
			headers: {
				Accept: "application/json",
				"Content-Type": "application/json; charset=utf-8",
				"X-XSRF-TOKEN": readFromCookies("XSRF-TOKEN"),
			},
			credentials: "same-origin",
			method,
			body: method === "GET" ? void 0 : JSON.stringify(data),
		});
		const json = yield response.json();

		if (response.status === 200) {
			yield put({
				type: `${storeName}_${name}_success`,
				name,
				isLoading: false,
				success: true,
				data: json,
			});
		} else {
			yield put({
				type: response.status === 409 && response.status === 400 ? VALIDATION_ADD_ALL : MESSAGE_ADD_ALL,
				data: json.errors,
			});
			yield put({
				type: `${storeName}_${name}_error`,
				name,
				isLoading: false,
				success: false,
			});
		}
	} catch (error) {
		yield put({
			type: MESSAGE_ADD,
			data: error,
		});
		yield put({
			type: `${storeName}_${name}_error`,
			name,
			isLoading: false,
			success: false,
		});
	}
}
