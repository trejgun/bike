import React from "react";
import ReactDOM from "react-dom";
import {AppContainer} from "react-hot-loader";
import {Provider} from "react-intl-redux";
import {BrowserRouter} from "react-router-dom";
import MuiProvider from "./mui";


export default (App, store) => {
  ReactDOM.hydrate(
    <AppContainer>
      <Provider store={store}>
        <BrowserRouter>
          <MuiProvider>
            <App/>
          </MuiProvider>
        </BrowserRouter>
      </Provider>
    </AppContainer>,
    document.getElementById("app"),
  );
};
