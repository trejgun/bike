import React from "react";
import {Glyphicon} from "react-bootstrap";


export function uniq(value, index, self) {
	return self.indexOf(value) === index;
}

export function plateToText(plate) {
  return plate ? plate : "/!\\";
}

export function plateToHtml(plate) {
  return plate ? plate : <Glyphicon glyph="warning-sign" className="text-danger"/>;
}

export function vehicleToText({brand, model, variant, plate}, showPlate = false) {
	return [brand, model, variant, showPlate ? `${plateToText(plate)}` : ""].filter(String).join(" ");
}

export function vehicleToHtml({brand, model, variant, plate}, showPlate) {
	return (
		<span>
			<span>{brand}</span>&nbsp;
			<span>{model}</span>&nbsp;
			<span>{variant}</span>&nbsp;
			{showPlate ? <span>{plateToHtml(plate)}</span> : null}
		</span>
	);
}

export function priceToText({pricePerHour, pricePerDay, pricePerMonth}) {
	return `${pricePerHour || "-"}/${pricePerDay || "-"}/${pricePerMonth || "-"}`;
}

export function priceToHtml({pricePerHour, pricePerDay, pricePerMonth}) {
	if (!pricePerHour && !pricePerDay && !pricePerMonth) {
		return <Glyphicon glyph="warning-sign" className="text-danger" />;
	}
	return (
		<span>
			<span>{pricePerHour || "-"}</span>/
			<span>{pricePerDay || "-"}</span>/
			<span>{pricePerMonth || "-"}</span>
		</span>
	);
}
