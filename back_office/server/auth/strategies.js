import passport from "passport";
import {OAuth2Strategy as GoogleStrategy} from "passport-google-oauth";
import {Strategy as FacebookStrategy} from "passport-facebook";
import SystemStrategy from "./system-strategy";
import {getRandomString} from "../utils/misc";
import {makeError} from "../utils/error";

import UserController from "../controllers/impl/user";


function createUser(provider, profile) {
  const random = getRandomString(16);
  const userController = new UserController();
  return userController.insert({
    params: {
      isEmailVerified: true,
    },
    body: {
      email: profile.emails[0].value,
      fullName: profile.displayName || `${profile.name.givenName} ${profile.name.familyName}`,
      image: profile.photos[0].value,
      language: profile._json.language || profile._json.locale.split("_")[0],
      confirm: random,
      password: random,
      social: {
        [provider]: profile.id,
      },
    },
  });
}

function getUser(provider, profile) {
  const userController = new UserController();
  return userController.findOne({
    $or: [
      {[`social.${provider}`]: profile.id},
      {email: profile.emails[0].value},
    ],
  }, {lean: false})
    .then(user => {
      if (user) {
        if (user.social[provider] === profile.id) {
          return user;
        } else {
          Object.assign(user.social, {[provider]: profile.id});
          return userController.save(user);
        }
      } else if (process.env.AUTH_CREATEUSER_ON_LOGIN === "true") {
        return createUser(provider, profile);
      } else {
        throw makeError("access-denied", 403, {reason: "permissions"});
      }
    });
}

if (process.env.GOOGLE_CLIENT_ID) {
  passport.use(new GoogleStrategy({
      clientID: process.env.GOOGLE_CLIENT_ID,
      clientSecret: process.env.GOOGLE_CLIENT_SECRET,
      callbackURL: process.env.GOOGLE_CALLBACK_URL,
    },
    (accessToken, refreshToken, profile, callback) => {
      getUser("google", profile)
        .tap(user => {
          callback(null, user);
        })
        .catch(error => {
          callback(error, null);
        })
        .done();
    },
  ));
}

if (process.env.FACEBOOK_CLIENT_ID) {
  passport.use(new FacebookStrategy({
      clientID: process.env.FACEBOOK_CLIENT_ID,
      clientSecret: process.env.FACEBOOK_CLIENT_SECRET,
      callbackURL: process.env.FACEBOOK_CALLBACK_URL,
      profileFields: ["id", "birthday", "email", "gender", "link", "name", "locale", "picture"],
    },
    (accessToken, refreshToken, profile, callback) => {
      getUser("facebook", profile)
        .tap(user => {
          callback(null, user);
        })
        .catch(error => {
          callback(error, null);
        })
        .done();
    },
  ));
}

if (process.env.SYSTEM_CLIENT_ID) {
  passport.use(new SystemStrategy({
      clientID: process.env.SYSTEM_CLIENT_ID,
      clientSecret: process.env.SYSTEM_CLIENT_SECRET,
      callbackURL: process.env.SYSTEM_CALLBACK_URL,
      authorizationURL: `${process.env.MODULE_OAUTH2}/authorize`,
      tokenURL: `${process.env.MODULE_OAUTH2}/api/oauth/token`,
      profileURL: `${process.env.MODULE_OAUTH2}/api/userinfo`,
    },
    (accessToken, refreshToken, profile, callback) => {
      getUser("system", profile)
        .tap(user => {
          callback(null, user);
        })
        .catch(error => {
          callback(error, null);
        })
        .done();
    },
  ));
}

