import {InternalOAuthError, Strategy as OAuth2Strategy} from "passport-oauth2";

export default class SystemStrategy extends OAuth2Strategy {
  name = "system";

  userProfile(accessToken, callback) {
    this._oauth2.get(`${process.env.MODULE_OAUTH2}/api/userinfo`, accessToken, (err, body) => {
      if (err) {
        callback(new InternalOAuthError("failed to fetch user profile", err));
      } else {
        try {
          const json = JSON.parse(body);
          const profile = {
            provider: "system",
            id: json.id,
            displayName: json.name,
            emails: [{value: json.email}],
            photos: [{value: json.image}],
            _raw: body,
            _json: json,
          };
          callback(null, profile);
        } catch (e) {
          callback(e);
        }
      }
    });
  }
}
