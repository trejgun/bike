const moment = require("moment");
const range = require("moment-range");
require("moment-timezone");
require("moment-precise-range-plugin");

const DateRange = range.DateRange;
const extendMoment = range.extendMoment;

DateRange.prototype.contains = function contains(other, exclusive) {
	const start = this.start;
	const end = this.end;

	if (other instanceof DateRange) {
		return (start < other.start || (start.isSame(other.start) && !exclusive)) && (end > other.end || (end.isSame(other.end) && !exclusive));
	} else {
		return (start < other || (start.isSame(other) && !exclusive)) && (end > other || (end.isSame(other) && !exclusive));
	}
};

DateRange.prototype.intersect = function intersect(other, exclusive = true) {
	const start = this.start;
	const end = this.end;

	/*
	 [---range---]       | [---range---]
	 {---other---} |             {---other---}
	 */
	if ((start <= other.start) &&
		(other.start < end || (!exclusive && other.start.isSame(end))) &&
		(end < other.end)) {
		return new DateRange(other.start, end);
	} // eslint-disable-line brace-style
	/*
	 [---range---] |             [---range---]
	 {---other---}      | {---other---}
	 */
	else if ((other.start < start) &&
		(start < other.end || (!exclusive && start.isSame(other.end))) &&
		(other.end <= end)) {
		return new DateRange(start, other.end);
	} // eslint-disable-line brace-style
	/*
	 [---range---]
	 {-----other-----}
	 */
	else if ((other.start < start) && (start <= end) && (end < other.end)) {
		return this;
	} // eslint-disable-line brace-style
	/*
	 [-----range-----] | [---range---]
	 {---other---}   | {---other---}
	 */
	else if ((start <= other.start) && (other.start <= other.end) && (other.end <= end)) {
		return other;
	}

	return null;
};

DateRange.prototype.toString = function toString() {
	return `${this.start.toDate().toISOString()}/${this.end.toDate().toISOString()}`;
};

DateRange.prototype.toJSON = function toJSON() {
	return this.toString();
};

DateRange.prototype.toMoment = function toMoment() {
	return [this.start, this.end];
};

DateRange.prototype.format = function format(format, separator) {
	return this.start.format(format) + (separator || " - ") + this.end.format(format);
};

DateRange.prototype.isValid = function isValid() {
	return this.start.isValid() && this.end.isValid();
};

extendMoment(moment);
