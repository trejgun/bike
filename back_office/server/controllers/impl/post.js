import StatefulController from "../abstract/stateful";
import {paginate} from "../../utils/response";
import {getConnections} from "../../utils/mongoose";


export default class PostController extends StatefulController {
  static realm = "back_office";

  static param = "postId";

  list(request, response) {
    return paginate(request, response, this, {}, {
      populate: [{
        path: "organizations",
        model: getConnections().oauth2.model("Organization"),
      }],
    })
      .then(({count, list}) => ({
          count,
          list: list.map(post =>
            Object.assign(post, {
              created: post.createdAt,
              companyName: post.organizations[0].companyName,
            }),
          ),
        }),
      );
  }

  getById(request) {
    return this.getByUId(request)
      .then(post =>
        Object.assign(post, {
          created: post.createdAt,
          organization: post.organizations[0],
        }),
      );
  }
}
