import {Schema} from "mongoose";
import status from "../plugins/status";
import title from "../plugins/title";
import description from "../plugins/description";
import PostController from "../../controllers/impl/post";


const Post = new Schema({
  postId: {
    type: String,
    unique: true,
    required: [true, "required"],
  },
}, {
  toJSON: {
    transform(doc, ret) {
      ret.created = ret.createdAt;
      return ret;
    },
  },
});

Post.plugin(status, {controller: PostController});
Post.plugin(title);
Post.plugin(description);

export default Post;
