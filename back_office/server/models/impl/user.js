import bcrypt from "bcrypt-nodejs";
import {Schema} from "mongoose";
import zxcvbn from "zxcvbn";
import {difference} from "lodash";
import version from "@streethub/mongoose-version";

import {arrayGetter} from "../../utils/mongoose";
import permissions from "../../constants/permissions";

import email from "../plugins/email";
import fullName from "../plugins/fullName";
import phoneNumber from "../plugins/phoneNumber";
import status from "../plugins/status";
import country from "../plugins/country";
import language from "../plugins/language";


const User = new Schema({
  image: {
    type: String,
  },

  license: {
    type: String,
  },

  permissions: {
    type: Array,
    validate: {
      validator(p) {
        // we will actually never end up here because real validation is in user controller
        return !difference(p, permissions).length;
      },
      message: "invalid",
    },
    default: permissions,
    get: arrayGetter,
  },

  password: {
    type: String,
    select: false,
    required: [true, "required"],
    validate: [{
      validator() {
        return this.isModified("password") ? this.password === this.confirm : true;
      },
      msg: "mismatch",
    }, {
      validator() {
        return !!this.password;
      },
      msg: "required",
    }, {
      validator() {
        return zxcvbn(this.password).score >= 1;
      },
      msg: "weak",
    }],
  },

  social: {
    facebook: String,
    google: String,
    system: String,
  },
});

User.plugin(email);
User.plugin(fullName);
User.plugin(phoneNumber);
User.plugin(status);
User.plugin(country);
User.plugin(language);

User.plugin(version, {
  collection: "UserVersions",
  suppressVersionIncrement: false,
});

User.virtual("confirm")
  .set(function setConfirm(confirm) {
    this._confirm = confirm;
  })
  .get(function getConfirm() {
    return this._confirm;
  });

User.pre("save", function preSavePassword(next) {
  if (this.isModified("password")) {
    this.password = bcrypt.hashSync(this.password, bcrypt.genSaltSync(5));
  }
  next();
});

User.methods.verifyPassword = function verifyPassword(password) {
  return bcrypt.compareSync(password, this.password);
};

export default User;
