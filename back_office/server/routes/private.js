import fs from "fs";
import path from "path";
import {Router} from "express";
import {requiresLogin} from "../utils/middleware";

const router = Router(); // eslint-disable-line new-cap

router.use("/api", requiresLogin);

fs.readdirSync(path.join(__dirname, "./private")).forEach(file => {
  router.use("/api", require(path.join(__dirname, "./private", file)).default);
});

export default router;
