import {Router} from "express";
import PostController from "../../controllers/impl/post";
import {mongoId} from "../../utils/validator";
import crud from "../../utils/crud";


const object = "posts";
const router = Router(); // eslint-disable-line new-cap
const postController = new PostController();

router.param("_id", mongoId); // mongo id

crud(router, postController, {object});

export default router;
