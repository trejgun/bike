import {Router} from "express";
import crud from "../../utils/crud";
import {wrapJSON} from "../../utils/wrapper";
import {methodNotAllowed} from "../../utils/middleware";
import UserController from "../../controllers/impl/user";


const router = Router(); // eslint-disable-line new-cap
const userController = new UserController();

router.route("/sync")
  .get(wrapJSON(::userController.sync))
  .all(methodNotAllowed);

router.route("/users/impersonate/:_id")
  .get(wrapJSON(::userController.impersonate))
  .all(methodNotAllowed);

router.route("/users/depersonalize")
  .get(wrapJSON(::userController.depersonalize))
  .all(methodNotAllowed);

crud(router, userController, {object: "users"});

export default router;
