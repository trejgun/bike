import {Router} from "express";
import PostController from "../../controllers/impl/post";
import {wrapJSON} from "../../utils/wrapper";
import {mongoId} from "../../utils/validator";

const router = Router(); // eslint-disable-line new-cap
const postController = new PostController();

router.param("_id", mongoId); // mongo id

router.route("/posts")
  .get(wrapJSON(::postController.list));

router.route(`/posts/:${PostController.param}`)
  .get(wrapJSON(::postController.getById));

export default router;

