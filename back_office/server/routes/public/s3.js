import {Router} from "express";
import S3 from "../../connect/aws.s3";
import {methodNotAllowed} from "../../utils/middleware";


const router = Router(); // eslint-disable-line new-cap

router.route("/s3/sign")
  .get(::S3.putSignedUrl)
  .all(methodNotAllowed);

router.route("/s3/uploads")
  .get(::S3.getSignedUrl)
  .all(methodNotAllowed);

export default router;
