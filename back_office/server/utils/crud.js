import {wrapJSON} from "./wrapper";
import {checkPermissions, methodNotAllowed, params, validateParams} from "./middleware";
import {mongoId} from "./validator";


export default function crud(router, controller, {object}) {
	router.param("_id", mongoId); // mongo id

	router.route(`/${object}`)
		.get(validateParams([params.limit, params.skip], "query"), checkPermissions(`${object}:read`), wrapJSON(::controller.list))
		.post(checkPermissions(`${object}:create`), wrapJSON(::controller.insert))
		.all(methodNotAllowed);

	router.route(`/${object}/:${controller.constructor.param}`)
		.get(checkPermissions(`${object}:read`), wrapJSON(::controller.getById))
		.put(checkPermissions(`${object}:update`), wrapJSON(::controller.edit))
		.delete(checkPermissions(`${object}:delete`), wrapJSON(::controller.delete))
		.all(methodNotAllowed);
}
