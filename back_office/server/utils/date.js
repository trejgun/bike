import moment from "moment";

export function validateRange(range) {
	const {start, end} = moment.range(range);
	return !(!start.isValid() || !end.isValid() || start > end);
}
