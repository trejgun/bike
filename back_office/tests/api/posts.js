import assert from "power-assert";
import supertest from "../../../test-utils/supertest";
import {setUp, tearDown} from "../../../test-utils/flow";
import expect from "../../../test-utils/assert";
import PostController from "../../server/controllers/impl/post";
import {description, title} from "../../../test/constants";
import {getRandomString} from "../../../vehicles_office/server/utils/misc";


let data;

describe("Post", () => {
  const superapp = supertest("back_office");

  describe("GET /posts", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 2,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 2,
      }, {
        model: "Post",
        requires: {
          Organization: [0, 0, 1, 1],
        },
        count: 4,
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should get posts", () =>
      superapp
        .login(data.User[0])
        .get("/api/posts")
        .expect(200)
        .then(({body}) => {
          assert.equal(body.count, 4);
          assert.equal(body.list.length, 4);

          assert.equal(body.list[0].created, data.Post[0].createdAt.toISOString());
        }),
    );

    after(tearDown);
  });

  describe("POST /posts", () => {
    const postId = getRandomString(10);

    before(() =>
      setUp([{
        model: "Organization",
        count: 1,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 1,
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should create post", () =>
      superapp
        .login(data.User[0])
        .post("/api/posts")
        .type("application/json;charset=utf-8")
        .send({
          postId,
          title,
          description,
        })
        .expect(200)
        .then(({body}) => {
          assert.equal(body.postId, postId);
          assert.equal(body.title, title);
          assert.equal(body.description, description);
        }),
    );

    after(tearDown);
  });

  describe("GET /posts/:postId", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 1,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 1,
      }, {
        model: "Post",
        requires: {
          Organization: "o2o",
        },
        count: 1,
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should get post", () =>
      superapp
        .login(data.User[0])
        .get(`/api/posts/${data.Post[0].postId}`)
        .expect(200)
        .then(({body}) => {
          assert.equal(body.title, title);
          assert.equal(body.description, description);
          assert.equal(body.organization.companyName, data.Organization[0].companyName);
          assert.equal(body.created, data.Post[0].createdAt.toISOString());
        }),
    );

    after(tearDown);
  });

  describe("PUT /posts/:postId", () => {
    const newTitle = "new title";
    const newDescription = "new description".repeat(100);

    before(() =>
      setUp([{
        model: "Organization",
        count: 1,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 1,
      }, {
        model: "Post",
        requires: {
          Organization: "o2o",
        },
        count: 1,
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should edit post", () =>
      superapp
        .login(data.User[0])
        .put(`/api/posts/${data.Post[0].postId}`)
        .type("application/json;charset=utf-8")
        .send({
          title: newTitle,
          description: newDescription,
        })
        .expect(200)
        .then(({body}) => {
          assert.equal(body.title, newTitle);
          assert.equal(body.description, newDescription);
        }),
    );
  });

  describe("DELETE /posts/:postId", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 2,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 2,
      }, {
        model: "Post",
        requires: {
          Organization: "o2o",
        },
        count: 2,
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should reject post", () =>
      superapp
        .login(data.User[0])
        .delete(`/api/posts/${data.Post[0].postId}`)
        .expect(200)
        .then(({body}) => {
          // console.debug("body", body);
          assert.equal(body.status, PostController.statuses.inactive);
        }),
    );

    it("should throw `access-denied`", () =>
      superapp
        .login(data.User[0])
        .delete(`/api/posts/${data.Post[1].postId}`)
        .then(expect(403, [{reason: "organization"}])),
    );

    after(tearDown);
  });
});
