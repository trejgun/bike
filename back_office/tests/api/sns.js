import assert from "power-assert";
import supertest from "../../../test-utils/supertest";
import {tearDown} from "../../../test-utils/flow";
import {snsObject} from "../../../test-utils/objects";


describe("SNS", () => {
  const superapp = supertest("back_office");

  describe("POST /sns/bounces", () => {
    const subject = "my subject";

    it("should get posts", () =>
      superapp
        .post("/api/sns/bounces")
        .type("text/plain;charset=utf-8")
        .send(JSON.stringify(snsObject({Subject: subject, Message: JSON.stringify({Text: "text"})})))

        .then(({body}) => {
          assert.equal(body.Subject, subject);
        }),
    );

    after(tearDown);
  });
});
