import path from "path";
import webpack from "webpack";
import MiniCssExtractPlugin from "mini-css-extract-plugin";
import DotEnvPlugin from "dotenv-webpack";


const modules = ["vehicles_office", "vehicles_market", "back_office", "oauth2"];

export default modules.map(module => ({
  mode: process.env.NODE_ENV,
  // https://github.com/facebook/react/blob/master/docs/docs/cross-origin-errors.md
  devtool: "cheap-module-source-map",
  entry: {
    [module]: ["react-hot-loader/patch", `webpack-hot-middleware/client?path=${process.env.MODULE_CDN}/__webpack_hmr`, `../${module}/client`],
  },
  output: {
    path: path.join(__dirname, "..", "..", "..", "build", "bundle"),
    filename: "[name].js",
    sourceMapFilename: "[file].map",
    publicPath: `${process.env.MODULE_CDN}/bundle/`,
  },
  resolve: {
    extensions: [".json", ".jsx", ".js"],
    modules: [
      "node_modules",
    ],
  },
  module: {
    rules: [{
      test: /\.(le|c)ss$/,
      use: [{
        loader: MiniCssExtractPlugin.loader,
      }, {
        loader: "css-loader",
        options: {
          importLoaders: 1,
          sourceMap: true,
        },
      }, {
        loader: "postcss-loader",
        options: {
          sourceMap: true,
        },
      }, {
        loader: "less-loader",
        options: {
          sourceMap: true,
        },
      }],
    }, {
      test: /\.(ttf|woff|woff2|eot|svg|gif|png|ico)(\?.+)?$/,
      use: [{
        loader: "file-loader?name=[name].[ext]?[hash]",
      }],
    }, {
      test: /\.jsx?$/,
      exclude: [/node_modules/],
      use: [{
        loader: "babel-loader",
        options: {
          babelrc: false,
          presets: [
            [
              "@babel/env",
              {
                modules: false,
                targets: {
                  browsers: ["> 1%"],
                },
              },
            ],
            "@babel/react",
          ],
          plugins: [
            "react-hot-loader/babel",
            [
              "@babel/proposal-decorators",
              {
                "legacy": true,
              },
            ],
            "@babel/proposal-class-properties",
            "@babel/proposal-function-bind",
            "@babel/proposal-object-rest-spread",
            "@babel/transform-runtime",
            "lodash",
          ],
        },
      }],
    }],
  },
  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    new MiniCssExtractPlugin({
      filename: "[name].css",
    }),
    new DotEnvPlugin({
      path: `../${module}/.env.${process.env.NODE_ENV}`,
    }),
    new webpack.HotModuleReplacementPlugin(),
  ],
  performance: {
    hints: false,
  },
  optimization: {
    namedModules: true,
    splitChunks: {
      cacheGroups: {
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendors",
          chunks: "all",
        },
      },
    },
  },
}));
