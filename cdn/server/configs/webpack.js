import webpack from "webpack";
import {Router} from "express";
import webpackDevMiddleware from "webpack-dev-middleware";
import webpackHotMiddleware from "webpack-hot-middleware";
import configs from "./webpack.development";

const router = Router(); // eslint-disable-line new-cap

// Run Webpack dev server only in development mode
if (process.env.NODE_ENV === "development" || process.env.NODE_ENV === "test") {
  // NOTE https://github.com/webpack/webpack-dev-server/issues/641#issuecomment-444055379
  // NOTE https://github.com/webpack-contrib/webpack-hot-middleware/issues/270#issue-279238265
  configs.forEach(config => {
    const compiler = webpack(config);
    router
      .use(webpackDevMiddleware(compiler, {
        publicPath: `${process.env.MODULE_CDN}/bundle/`,
        watchOptions: {
          aggregateTimeout: 0,
        },
        stats: {
          assets: true,
          colors: false,
          timings: true,
          chunks: false,
          chunkModules: false,
          modules: false,
          children: false,
          version: true,
        },
      }))
      .use(webpackHotMiddleware(compiler));
  });
}

export default router;

