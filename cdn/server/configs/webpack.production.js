const path = require("path");
const webpack = require("webpack");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const ProgressBarPlugin = require("progress-bar-webpack-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const DotEnvPlugin = require("dotenv-webpack");


module.exports = {
  mode: process.env.NODE_ENV,
  devtool: "source-map",
  entry: {
    vehicles_office: ["./vehicles_office/client"],
    vehicles_market: ["./vehicles_market/client"],
    back_office: ["./back_office/client"],
    oauth2: ["./oauth2/client"],
  },
  output: {
    path: path.join(__dirname, "..", "..", "..", "..", "build", "bundle"),
    filename: "[name].js",
    sourceMapFilename: "[file].map",
    chunkFilename: "[name].js",
    publicPath: "/bundle/",
  },
  externals: {
    // require("jquery") is external and available
    // on the global var jQuery
    react: "React",
    "react-dom": "ReactDOM",
    // "react-bootstrap": "ReactBootstrap",
    // redux: "Redux",
    // "redux-thunk": "ReduxThunk",
    // "redux-logger": "reduxLogger",
    moment: "moment",
    // inlt: "IntlPolyfill",
    // "intl-locales-supported": "areIntlLocalesSupported",
    // "react-intl": "ReactIntl"
    lodash: "_",
  },
  resolve: {
    extensions: [".json", ".jsx", ".js"],
    modules: [
      "node_modules",
    ],
  },
  module: {
    rules: [{
      test: /\.(le|c)ss$/,
      use: [
        MiniCssExtractPlugin.loader,
        "css-loader",
        "postcss-loader",
        "less-loader",
      ],
    }, {
      test: /\.(ttf|woff|woff2|eot|svg|gif|png|ico)(\?.+)?$/,
      use: [{
        loader: "file-loader?name=[name].[ext]?[hash]",
      }],
    }, {
      test: /\.jsx?$/,
      exclude: [/node_modules/],
      use: [{
        loader: "babel-loader",
        options: {
          babelrc: false,
          presets: ["@babel/react", [
            "@babel/env",
            {
              targets: {
                browsers: ["> 1%"],
              },
            },
          ]],
          plugins: [
            [
              "@babel/proposal-decorators",
              {
                "legacy": true,
              },
            ],
            "@babel/proposal-class-properties",
            "@babel/proposal-function-bind",
            "@babel/proposal-object-rest-spread",
            "@babel/transform-runtime",
            "lodash",
          ],
        },
      }],
    }],
  },
  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    new MiniCssExtractPlugin({
      filename: "[name].css",
    }),
    new DotEnvPlugin({
      path: `.env.${process.env.NODE_ENV}`,
      systemvars: true,
    }),
    new ProgressBarPlugin(),
  ],
  performance: {
    hints: false,
  },
  optimization: {
    minimizer: [
      new UglifyJsPlugin({
        cache: true,
        parallel: true,
      }),
      new OptimizeCSSAssetsPlugin({}),
    ],
    splitChunks: {
      cacheGroups: {
        vendors: {
          test: /[\\/]node_modules[\\/]/,
          name: "vendors",
          chunks: "all",
        },
      },
    },
  },
};
