import "./configs/env";
import "./configs/moment";
import "./configs/winston";

import winston from "winston";
import app from "./configs/express";

import cors from "./routes/common/cors";
import assets from "./routes/common/assets";
import notFound from "./routes/common/404";
import {sendError} from "./utils/wrapper";


app.use(cors);
app.use(assets);
app.use(notFound);
app.use(sendError);

const server = app.listen(process.env.PORT, () => {
  winston.info(`Express server listening on port ${server.address().port}`);
});

process.on("unhandledRejection", winston.error);
process.on("uncaughtException", winston.error);

export default app;
