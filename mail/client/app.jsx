import React, {Component} from "react";
import {Route, Switch} from "react-router";
import Footer from "./components/partials/footer";

import Welcome from "./components/oauth2/welcome";

import NewBookingForCustomer from "./components/vehicles_market/newBookingForCustomer";
import NewBookingForRenter from "./components/vehicles_office/newBookingForRenter";
import BookingRejected from "./components/vehicles_market/bookingRejectedByRenter";
import BookingAccepted from "./components/vehicles_market/bookingAcceptedByRenter";
import BookingRevokedByCustomer from "./components/vehicles_office/bookingRevokedByCustomer";
import BookingCancelledByCustomer from "./components/vehicles_office/bookingCancelledByCustomer";
import BookingCancelledByRenter from "./components/vehicles_market/bookingCancelledByRenter";
import TimeToExtend from "./components/vehicles_market/timeToExtend";

import Verification from "./components/oauth2/verification";
import Forgot from "./components/oauth2/forgot";
import Missing from "./components/back_office/missing";


export default class App extends Component {
	render() {
		return (
			<div>
				<Switch>
					<Route path="/welcome" component={Welcome} />
					<Route path="/verification" component={Verification} />
					<Route path="/forgot" component={Forgot} />

					<Route path="/newBookingForCustomer" component={NewBookingForCustomer} />
					<Route path="/newBookingForRenter" component={NewBookingForRenter} />
					<Route path="/bookingRejectedByRenter" component={BookingRejected} />
					<Route path="/bookingAcceptedByRenter" component={BookingAccepted} />
					<Route path="/bookingRevokedByCustomer" component={BookingRevokedByCustomer} />
					<Route path="/bookingCancelledByCustomer" component={BookingCancelledByCustomer} />
					<Route path="/bookingCancelledByRenter" component={BookingCancelledByRenter} />
					<Route path="/timeToExtend" component={TimeToExtend} />

					<Route path="/missing" component={Missing} />
				</Switch>
				<Footer />
			</div>
		);
	}
}
