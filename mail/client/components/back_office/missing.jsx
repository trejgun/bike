import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {FormattedMessage} from "react-intl";


@connect(
	state => ({
		messages: state.messages,
	}),
)
export default class Missing extends Component {
	static propTypes = {
		messages: PropTypes.arrayOf(PropTypes.shape({
			data: PropTypes.shape({
				vehicle: PropTypes.object,
				user: PropTypes.shape({
          _id: PropTypes.string,
					fullName: PropTypes.string,
					organization: PropTypes.shape({
            _id: PropTypes.string,
						companyName: PropTypes.string,
					}),
				}),
			}),
		})),
	};

	render() {
		const {vehicle, user} = this.props.messages[0].data;
		return (
			<div>
				<h2>Missing Vehicle</h2>
				<table>
					<tbody>
						{Object.keys(vehicle).map(key => (
							<tr key={key}>
								<td>{key}</td>
								<td>{vehicle[key]}</td>
							</tr>
						))}
						<tr>
							<td><FormattedMessage id="fields.fullName.label" /></td>
              <td><a href={`${process.env.MODULE_BACK_OFFICE}/users/${user._id}`}> {user.fullName}</a></td>
						</tr>
						<tr>
							<td><FormattedMessage id="fields.companyName.label" /></td>
              <td><a
                href={`${process.env.MODULE_BACK_OFFICE}/organizations/${user.organization._id}`}> {user.organization.companyName}</a>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		);
	}
}
