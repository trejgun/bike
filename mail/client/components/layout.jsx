import React, {Component} from "react";
import PropTypes from "prop-types";
import Footer from "./partials/footer";


export default class Layout extends Component {
	static propTypes = {
		children: PropTypes.node,
	};

	render() {
		return (
			<div>
				{this.props.children}
				<Footer />
			</div>
		);
	}
}
