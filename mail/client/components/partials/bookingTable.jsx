import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import moment from "moment";
import {FormattedMessage} from "react-intl";
import {humanReadable} from "../../constants/date";
import {vehicleToText} from "../../utils/vehicle";

@connect(
  state => ({
    bookings: state.bookings,
  }),
)
export default class BookingTable extends Component {
  static propTypes = {
    bookings: PropTypes.object,
  };

  render() {
    const booking = this.props.bookings.list[0];
    return (
      <table>
        <tbody>
          <tr>
            <td><FormattedMessage id="fields.vehicle.label" /></td>
            <td>{vehicleToText(booking.vehicle)}</td>
          </tr>
          <tr>
            <td><FormattedMessage id="fields.range.label" /></td>
            <td>{moment.range(booking.range).format(humanReadable)}</td>
          </tr>
          <tr>
            <td><FormattedMessage id="fields.price.label" /></td>
            <td>{`${booking.price} 000,00Rp`}</td>
          </tr>
          <tr>
            <td><FormattedMessage id="fields.fullName.label" /></td>
            <td>{booking.fullName}</td>
          </tr>
          <tr>
            <td><FormattedMessage id="fields.phoneNumber.label" /></td>
            <td>{booking.phoneNumber}</td>
          </tr>
          <tr>
            <td><FormattedMessage id="fields.notes.label" /></td>
            <td>{booking.notes}</td>
          </tr>
        </tbody>
      </table>
    );
  }
}
