import React, {Component} from "react";
import BookingTable from "../partials/bookingTable";


export default class BookingAccepted extends Component {
	render() {
		return (
			<div>
				<h2>Booking was accepted</h2>
				<p>Vehicle owned accepted your booking:</p>
				<BookingTable />
        <p>Please manage you other <a href={`${process.env.MODULE_VEHICLES_MARKET}/bookings`}>bookings</a> manually</p>
			</div>
		);
	}
}
