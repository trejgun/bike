import React, {Component} from "react";
import BookingTable from "../partials/bookingTable";


export default class BookingRejected extends Component {
	render() {
		return (
			<div>
				<h2>Booking was rejected</h2>
				<p>Vehicle owner rejected your booking:</p>
				<BookingTable />
        <p>You can try to book another <a href={`${process.env.MODULE_VEHICLES_MARKET}/vehicles`}>vehicle</a></p>
			</div>
		);
	}
}
