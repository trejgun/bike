import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import BookingTable from "../partials/bookingTable";


@connect(
	state => ({
		bookings: state.bookings,
	}),
)
export default class NewBookingForCustomer extends Component {
	static propTypes = {
		bookings: PropTypes.object,
	};

	render() {
		const booking = this.props.bookings.list[0];
		return (
			<div>
				<h2>New Booking</h2>
				<p>You just made a new booking</p>
				<BookingTable />
        <p><a href={`${process.env.MODULE_VEHICLES_MARKET}/bookings/${booking._id}/`}>View on site</a></p>
			</div>
		);
	}
}
