import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import moment from "moment";
import {humanReadable} from "../../constants/date";
import {vehicleToText} from "../../utils/vehicle";


@connect(
  state => ({
    bookings: state.bookings,
  }),
)
export default class TimeToExtend extends Component {
  static propTypes = {
    bookings: PropTypes.shape({
      list: PropTypes.array,
    }),
  };

  render() {
    const booking = this.props.bookings.list[0];
    return (
      <div>
        <h2>Notification</h2>
        <p>Your booking {booking.bookingId} for {booking.vehicle.type} ({vehicleToText(booking.vehicle)}) will finish at
          soon
          ({moment(booking.end).format(humanReadable)})</p>
        <p>To quickly extend it you can follow this <a
          href={`${process.env.MODULE_VEHICLES_MARKET}/booking/${booking.bookingId}/extend`}>link</a>.</p>
      </div>
    );
  }
}
