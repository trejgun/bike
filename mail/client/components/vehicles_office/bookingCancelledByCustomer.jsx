import React, {Component} from "react";
import BookingTable from "../partials/bookingTable";


export default class BookingCancelled extends Component {
	render() {
		return (
			<div>
				<h2>Booking was cancelled</h2>
				<p>Vehicle owner cancelled your booking:</p>
				<BookingTable />
        <p>We are sorry for this experience. You can try to book another <a
          href={`${process.env.MODULE_VEHICLES_MARKET}/vehicles`}>vehicle</a></p>
			</div>
		);
	}
}
