import React, {Component} from "react";
import BookingTable from "../partials/bookingTable";


export default class BookingRevoked extends Component {
	render() {
		return (
			<div>
				<h2>Booking was revoked</h2>
				<p>Customer revoked his booking:</p>
				<BookingTable />
				<p>We are sorry for this experience.</p>
			</div>
		);
	}
}
