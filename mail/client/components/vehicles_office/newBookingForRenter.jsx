import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import BookingTable from "../partials/bookingTable";


@connect(
	state => ({
		bookings: state.bookings,
	}),
)
export default class NewBookingForRenter extends Component {
	static propTypes = {
		bookings: PropTypes.object,
	};

	render() {
		const booking = this.props.bookings.list[0];
		return (
			<div>
				<h2>New Booking</h2>
				<p>A customer just made a new booking:</p>
				<BookingTable />
				<p>
          <a href={`${process.env.MODULE_VEHICLES_OFFICE}/bookings/${booking._id}/accept`}>Accept</a>
          <a href={`${process.env.MODULE_VEHICLES_OFFICE}/bookings/${booking._id}/reject`}>Reject</a>
          <a href={`${process.env.MODULE_VEHICLES_OFFICE}/bookings/${booking._id}`}>View in calendar</a>
				</p>
			</div>
		);
	}
}
