export const date = new Date();

export const ISO_8601 = "YYYY-MM-DD\\THH:mm:ss.SSS\\Z";
export const humanReadable = "DD MMM YYYY HH:mm";
export const dateTimeFormat = "YYYY-MM-DD HH:mm";

export const checkOutTime = "09:00";
export const rangeSeparator = " - ";
