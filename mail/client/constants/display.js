export const listItemHeight = 150;
export const listItemWidth = 150;
export const viewItemHeight = 250;
export const viewItemWidth = 250;
export const listItemLabel = 2;
export const listItemValue = 4;
export const viewItemLabel = 3;
export const viewItemValue = 9;

export const imageOriginalHeight = 600;
export const imageOriginalWidth = 600;
export const imagePreviewHeight = 250;
export const imagePreviewWidth = 250;
