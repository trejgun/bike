import {enabledLanguages} from "../constants/language";
// here you bring in "intl" browser polyfill and language-specific polyfills
// (needed as safari doesn't have native intl: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Intl)
// as well as react-intl"s language-specific data
// be sure to use static imports for language or else every language will be included in your build (adds ~800 kb)
import {addLocaleData} from "react-intl";
// need Intl polyfill, Intl not supported in Safari
import Intl from "intl";
import areIntlLocalesSupported from "intl-locales-supported";
// bring in intl polyfill, react-intl, and app-specific language data
import en from "react-intl/locale-data/en";
import ru from "react-intl/locale-data/ru";

import ruData from "./ru";

import enData from "./en";


if (global.Intl) {
  // Determine if the built-in `Intl` has the locale data we need.
  if (!areIntlLocalesSupported(enabledLanguages)) {
    // `Intl` exists, but it doesn't have the data we need, so load the
    // polyfill and patch the constructors we need with the polyfill"s.
    global.Intl.NumberFormat = Intl.NumberFormat;
    global.Intl.DateTimeFormat = Intl.DateTimeFormat;
  }
} else {
  // No `Intl`, so use and load the polyfill.
  global.Intl = Intl;
}

// use this to allow nested messages, taken from docs:
// https://github.com/yahoo/react-intl/wiki/Upgrade-Guide#flatten-messages-object
function flattenMessages(nestedMessages = {}, prefix = "") {
  return Object.keys(nestedMessages).reduce((messages, key) => {
    const value = nestedMessages[key];
    const prefixedKey = prefix ? `${prefix}.${key}` : key;

    if (typeof value === "string") {
      messages[prefixedKey] = value; // eslint-disable-line no-param-reassign
    } else {
      Object.assign(messages, flattenMessages(value, prefixedKey));
    }

    return messages;
  }, {});
}


addLocaleData(en);
addLocaleData(ru);

const localization = {
  ru: {...ruData},
  en: {...enData},
};

export default function getLocalization(lang) {
  return {
    locale: lang,
    messages: Object.assign({},
      flattenMessages(localization.en),
      flattenMessages(localization[lang]),
    ),
  };
}
