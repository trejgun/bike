import {combineReducers} from "redux";
import {routerReducer as routing} from "react-router-redux";
import {intlReducer as intl} from "react-intl-redux";

import bookings from "./bookings";
import hash from "./hash";
import messages from "./messages";
import vehicles from "./vehicles";
import user from "./user";


export default combineReducers({
  bookings,
  intl,
  hash,
  messages,
  routing,
  vehicles,
  user,
});
