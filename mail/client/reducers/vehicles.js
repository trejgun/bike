import {remove, replaceAll, replaceOrAppend} from "../utils/reducer";


const vehicles = {
	list: [],
	count: 0,
	isLoading: true,
	success: false,
	name: "",
};

export default function vehiclesReducer(state = vehicles, action) {
	switch (action.type) {
		case "vehicles_view_start":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name,
			});
		case "vehicles_view_success":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				list: replaceOrAppend(state, action.data),
				count: 1,
				name: action.name,
			});
		case "vehicles_view_error":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name,
			});
		case "vehicles_list_start":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name,
			});
		case "vehicles_list_success":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				list: replaceAll(state, action.data.list),
				count: action.data.count,
				name: action.name,
			});
		case "vehicles_list_error":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name,
			});
		case "vehicles_batch_start":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				list: [],
				count: 0,
				name: action.name,
			});
		case "vehicles_batch_success":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				list: replaceAll(state, action.data.list),
				count: action.data.count,
				name: action.name,
			});
		case "vehicles_batch_error":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name,
			});
		case "vehicles_create_start":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name,
			});
		case "vehicles_create_success":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				list: replaceOrAppend(state, action.data),
				name: action.name,
			});
		case "vehicles_create_error":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name,
			});
		case "vehicles_update_start":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name,
			});
		case "vehicles_update_success":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				list: replaceOrAppend(state, action.data),
				name: action.name,
			});
		case "vehicles_update_error":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name,
			});
		case "vehicles_missing_start":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name,
			});
		case "vehicles_missing_success":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name,
			});
		case "vehicles_missing_error":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name,
			});
		case "vehicles_delete_start":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name,
			});
		case "vehicles_delete_success":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				list: remove(state, action.data),
				name: action.name,
			});
		case "vehicles_delete_error":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name,
			});
		default:
			return state;
	}
}
