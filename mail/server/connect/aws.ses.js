import AWS from "aws-sdk";
import bluebird from "bluebird";
import {email} from "../constants/misc";


export default new class SES {
  constructor() {
    AWS.config.setPromisesDependency(bluebird.Promise);
  }

  get client() {
    return new AWS.SES();
  }

  sendEmail(mail) {
    return bluebird.promisify(::this.client.sendEmail)({
      Source: email,
      Destination: {
        ToAddresses: mail.to,
      },
      Message: {
        Body: {
          Html: {
            Data: mail.html,
          },
        },
        Subject: {
          Data: mail.subject,
        },
      },
    });
  }
}();
