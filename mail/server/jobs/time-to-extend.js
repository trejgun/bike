import moment from "moment";
import bluebird from "bluebird";
import {date} from "../constants/date";
import {getConnections} from "../utils/mongoose";

import BookingController from "../../../vehicles_office/server/controllers/impl/booking";
import MailController from "../controllers/impl/mail";


export default function() {
  const connections = getConnections();
  const bookingController = new BookingController();
  const mailController = new MailController();
  return bookingController.find({
    "organizations.1": {$exists: true},
    end: {
      $gte: date,
      $lte: moment(date).add(1, "d").toDate(),
    },
  }, {
    lean: false,
    populate: [{
      path: "organizations",
      model: connections.oauth2.model("Organization"),
      populate: [{
        path: "users",
        model: connections.oauth2.model("User"),
      }],
    }, {
      path: "vehicle",
    }],
  })
    .then(bookings =>
      bluebird.all(bookings.map(booking =>
        mailController.compose("timeToExtend", booking.organizations[1].users[0].email, booking.organizations[1].users[0], {
          bookings: {list: [booking]}, // yeah i know this is ugly
        }),
      )),
    );
}
