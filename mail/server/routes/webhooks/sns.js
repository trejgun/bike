import {Router} from "express";
import {wrapJSON} from "../../utils/wrapper";
import {methodNotAllowed} from "../../utils/middleware";
import SNSController from "../../controllers/impl/sns";

const router = Router(); // eslint-disable-line new-cap
const snsController = new SNSController();

router.route("/sns/:type(bounces|complaints|deliveries)")
  .post(wrapJSON(request =>
    snsController.create(JSON.parse(request.body)),
  ))
  .all(methodNotAllowed);

export default router;
