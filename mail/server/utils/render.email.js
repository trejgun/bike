import bluebird from "bluebird";
import {makeError} from "./error";
import EML from "../../client/components/EML";
import {renderHTML, renderInitialMarkup} from "./render";

import App from "../../client/app";
import configureStore from "../../client/store";
import {SheetsRegistry} from "jss";

const sheetsRegistry = new SheetsRegistry();

export function renderEmailToString(view, data) {
  return new bluebird.Promise((resolve, reject) => {
    const store = configureStore(data, "email");
    const context = {};

    const initialMarkup = renderInitialMarkup(`/${view}`, store, context, sheetsRegistry, App);
    const initialStyles = sheetsRegistry.toString();

    if (context.url) {
      reject(makeError("email-not-found"));
    } else {
      resolve(renderHTML(initialMarkup, initialStyles, store, EML));
    }
  });
}
