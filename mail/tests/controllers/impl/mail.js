import assert from "power-assert";
import {email} from "../../../../test/constants";
import {defaultLanguage as language} from "../../../server/constants/language";
import getLocalization from "../../../client/intl/setup";
import {bookingObject, organizationObject, userObject, vehicleObject} from "../../../../test-utils/objects";
import {getNewId} from "../../../server/utils/mongoose";
import {tearDown} from "../../../../test-utils/flow";

import MailController from "../../../server/controllers/impl/mail";


describe("Mail", () => {
  const mailController = new MailController(true);

  describe("#sendMail", () => {
    it("send `newBookingForCustomer`", () =>
      mailController.compose("newBookingForCustomer", {to: email}, {language}, {
        bookings: {list: [bookingObject({_id: getNewId().toString()})]},
      })
        .then(mail => {
          assert.deepEqual(mail.to, [email]);
          assert.deepEqual(mail.subject, getLocalization("en").messages["email.types.newBookingForCustomer"]);
        }),
    );

    it("send `newBookingForRenter`", () =>
      mailController.compose("newBookingForRenter", {to: email}, {language}, {
        bookings: {list: [bookingObject({_id: getNewId().toString()})]},
      })
        .then(mail => {
          assert.deepEqual(mail.to, [email]);
          assert.deepEqual(mail.subject, getLocalization("en").messages["email.types.newBookingForRenter"]);
        }),
    );

    it("send `missing`", () =>
      mailController.compose("missing", {to: email}, {language}, {
        messages: [{
          data: {
            vehicle: vehicleObject({_id: getNewId().toString()}),
            user: userObject({
              _id: getNewId().toString(),
              organization: organizationObject({_id: getNewId().toString()}),
            }),
          },
        }],
      })
        .then(mail => {
          assert.deepEqual(mail.to, [email]);
          assert.deepEqual(mail.subject, localization.en.messages["email.types.missing"]);
        }),
    );

    after(tearDown);
  });
});
