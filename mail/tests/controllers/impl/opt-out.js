import assert from "power-assert";
import {getNewId} from "../../../server/utils/mongoose";
import {processValidationError} from "../../../server/utils/error";
import OptOutController from "../../../server/controllers/impl/opt-out";
import MailController from "../../../server/controllers/impl/mail";


describe("OptOut", () => {
  const optOutController = new OptOutController(true);

  describe("#create", () => {
    it("should opt out", () =>
      optOutController.create({
        user: getNewId(),
        type: MailController.types.oauth2.welcome,
      })
        .then(optOut => {
          assert.deepEqual(optOut.type, MailController.types.oauth2.welcome);
        }),
    );

    it("thould throw `invalid-param` (type)", () =>
      optOutController.create({
        user: getNewId(),
        type: "crap",
      })
        .then(assert.ifError)
        .catch(e => {
          const errors = processValidationError(e);
          assert.equal(errors.length, 1);
          assert.equal(errors[0].message, "invalid-param");
          assert.equal(errors[0].reason, "invalid");
          assert.equal(errors[0].name, "type");
        }),
    );
  });
});
