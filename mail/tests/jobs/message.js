import moment from "moment";
import assert from "power-assert";
import {date} from "../../../vehicles_office/server/constants/date";
import messageJob from "../../server/jobs/time-to-extend";
import {setUp, tearDown} from "../../../test-utils/flow";

import BookingController from "../../../vehicles_office/server/controllers/impl/booking";


describe("Mail", () => {
  describe("#Message", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 2,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 2,
      }, {
        model: "Vehicle",
        requires: {
          Organization: [0],
        },
        count: 1,
      }, {
        model: "Booking",
        requires: {
          Vehicle: [0],
          Organization: [[0, 1]],
        },
        data: [{
          status: BookingController.statuses.accepted,
          range: `${moment(date).toDate().toISOString()}/${moment(date).add(5, "h").toDate().toISOString()}`,
        }],
      }]),
    );

    it("send `timeToExtend`", () =>
      messageJob()
        .then(mails => {
          assert.equal(mails.length, 1);
          assert.equal(mails[0].type, "timeToExtend");
        }),
    );

    after(tearDown);
  });
});
