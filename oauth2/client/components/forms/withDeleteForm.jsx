import React, {Component} from "react";
import PropTypes from "prop-types";
import {FormattedMessage} from "react-intl";
import YesNoDialog from "../partials/yesno";
import withReadForm from "./withReadForm";


export default function withDeleteForm(storeName, paramName) {
  return WrappedComponent => {
    // console.log("withDeleteForm:WrappedComponent", WrappedComponent);
    class DeleteForm extends Component {
      static propTypes = {
        storeName: PropTypes.string,
        paramName: PropTypes.string,

        match: PropTypes.shape({
          params: PropTypes.object.isRequired,
        }).isRequired,
        history: PropTypes.shape({
          push: PropTypes.func.isRequired,
        }).isRequired,
      };

      state = {
        showConfirmationDialog: false,
      };

      componentDidUpdate() {
        const {storeName, history} = this.props;
        if (!this.props[storeName].isLoading && this.props[storeName].success && this.props[storeName].name === "delete") {
          history.push(`/${storeName}`);
        }
      }

      onDelete(e) {
        e.preventDefault();
        this.setState({
          showConfirmationDialog: true,
        });
      }

      onDeleteConfirmed() {
        this.props.dispatch({
          type: "FETCH_REQUESTED",
          action: `/${this.props.storeName}/${this.props.match.params[this.props.paramName]}`,
          method: "DELETE",
          storeName: this.props.storeName,
          name: "delete",
        });
      }

      onDialogClose() {
        this.setState({
          showConfirmationDialog: false,
        });
      }

      render() {
        const {storeName, formData} = this.props;
        return (
          <div>
            <YesNoDialog
              title={<FormattedMessage id="dialogs.confirmation" />}
              show={this.state.showConfirmationDialog}
              onConfirm={::this.onDeleteConfirmed}
              onCancel={::this.onDialogClose}
            >
              <FormattedMessage id={`dialogs.${storeName}-delete`} values={formData} />
            </YesNoDialog>
            <WrappedComponent
              {...this.props}
              onDelete={::this.onDelete}
            />
          </div>
        );
      }
    }

    return withReadForm(storeName, paramName)(DeleteForm);
  };
}
