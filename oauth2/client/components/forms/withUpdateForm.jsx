import React, {Component} from "react";
import PropTypes from "prop-types";
import withReadForm from "./withReadForm";
import withGuardianForm from "./withGuardianForm";


export default function withUpdateForm(storeName, paramName) {
  return WrappedComponent => {
    // console.log("withUpdateForm:WrappedComponent", WrappedComponent);
    class UpdateForm extends Component {
      static propTypes = {
        onSubmit: PropTypes.func,
        storeName: PropTypes.string,
        paramName: PropTypes.string,

        match: PropTypes.shape({
          params: PropTypes.object.isRequired,
        }).isRequired,
        history: PropTypes.shape({
          push: PropTypes.func.isRequired,
        }).isRequired,
      };

      componentDidUpdate() {
        // console.log("UpdateForm:componentDidUpdate", this.props);
        const {storeName, paramName, history, match} = this.props;
        if (!this.props[storeName].isLoading && this.props[storeName].success && this.props[storeName].name === "update") {
          history.push(`/${storeName}/${match.params[paramName]}`);
        }
      }

      onSubmit(e) {
        this.props.onSubmit({
          preventDefault: e ? ::e.preventDefault : Function.prototype,
          target: {
            getAttribute: name => {
              return {
                action: (e && e.target.getAttribute(name)) || `/${this.props.storeName}/${this.props.match.params[this.props.paramName]}`,
                name: (e && e.target.getAttribute(name)) || "update",
                method: (e && e.target.getAttribute(name)) || "PUT",
              }[name];
            },
          },
        });
      }

      render() {
        // console.log("UpdateForm:render", this.props, this.state);
        return (
          <WrappedComponent
            {...this.props}
            onSubmit={::this.onSubmit}
          />
        );
      }
    }

    return withReadForm(storeName, paramName)(withGuardianForm()(UpdateForm));
  };
}
