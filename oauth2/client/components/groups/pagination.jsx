import React, {Component} from "react";
import PropTypes from "prop-types";
import UltimatePagination from "./pagination.wrapper";


export default class AbstractPagination extends Component {
  static propTypes = {
    setState: PropTypes.func,
    onSubmit: PropTypes.func,
    formData: PropTypes.shape({
      limit: PropTypes.number,
      skip: PropTypes.number,
    }),
    storeName: PropTypes.string,
  };

  onSelect(page) {
    // console.log("AbstractPagination:onSelect", this.props, page);
    const {formData, onSubmit} = this.props;
    this.props.setState({
      limit: formData.limit,
      skip: formData.limit * (page - 1),
    }, onSubmit);
  }

  render() {
    // console.log("AbstractPagination:render", this.props);
    const {storeName, formData} = this.props;
    const store = this.props[storeName];
    if (store.list.length && formData.limit < store.count) {
      return (
        <UltimatePagination
          currentPage={Math.ceil(formData.skip / formData.limit) + 1}
          totalPages={formData.limit ? Math.ceil(store.count / formData.limit) : 1}
          boundaryPagesRange={0}
          siblingPagesRange={2}
          onChange={::this.onSelect}
          className="pagination-centered"
        />
      );
    } else {
      return null;
    }
  }
}
