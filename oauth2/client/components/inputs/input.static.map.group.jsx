import InputStaticMap from "./input.static.map";
import withGroup from "./withGroup";


export default withGroup(InputStaticMap);
