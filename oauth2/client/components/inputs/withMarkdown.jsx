import "./withMarkdown.less";
import React, {Component} from "react";
import PropTypes from "prop-types";
import {Tab, Tabs} from "react-bootstrap";
import marked from "marked";
import {FormattedMessage} from "react-intl";


export default function withMarkdown(Input) {
  return class InputMarkdown extends Component {
    static propTypes = {
      defaultValue: PropTypes.string,
    };

    state = {
      key: "editor",
    };

    handleSelect(key) {
      this.setState({key});
    }

    render() {
      return (
        <Tabs activeKey={this.state.key} onSelect={::this.handleSelect} id="tab">
          <Tab eventKey="editor" title={<FormattedMessage id="components.tabs.input.editor" />}>
            <Input
              {...this.props}
              componentClass="textarea"
            />
          </Tab>
          <Tab eventKey="preview" title={<FormattedMessage id="components.tabs.input.preview" />}>
            <div
              className="markdown"
              dangerouslySetInnerHTML={{__html: marked(this.props.defaultValue || "", {sanitize: true})}}
            />
          </Tab>
        </Tabs>
      );
    }
  };
}
