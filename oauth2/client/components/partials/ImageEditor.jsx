import "./ImageEditor.less";
import React, {Component} from "react";
import PropTypes from "prop-types";
import {Canvas} from "react-darkroom/lib/components";
import {Transform} from "react-darkroom/lib/utils";
import {Button, ButtonGroup, Glyphicon, ProgressBar} from "react-bootstrap";
import Dropzone from "react-dropzone";
import {imageOriginalHeight, imageOriginalWidth} from "../../constants/display";
import {connect} from "react-redux";
import {MESSAGE_ADD} from "../../constants/actions";


function fileController(thread = {source: null}, action) {
  switch (action.type) {
    case "SET_FILE":
      return Object.assign({}, thread, {
        source: action.file,
        angle: 0,
      });
    default:
      return thread;
  }
}

function imageController(thread = {crop: false, source: null, angle: 0}, action) {
  switch (action.type) {
    case "ROTATE_LEFT":
      return Object.assign({}, thread, {
        angle: thread.angle - 90,
      });
    case "ROTATE_RIGHT":
      return Object.assign({}, thread, {
        angle: thread.angle + 90,
      });
    case "START_CROPPING":
      return Object.assign({}, thread, {
        crop: true,
      });
    case "STOP_CROPPING":
      return Object.assign({}, thread, {
        crop: false,
      });
    case "CONFIRM_CROP":
      return Object.assign({}, thread, {
        crop: false,
        source: action.image,
      });
    default:
      return thread;
  }
}

function readFile(file, done) {
  const reader = new FileReader();
  reader.onload = e => done(e.target.result);
  reader.readAsDataURL(file);
}

@connect()
export default class ImageEditor extends Component {
  static propTypes = {
    dispatch: PropTypes.func,
    onChange: PropTypes.func,
    progress: PropTypes.number,
  };

  state = {
    step: 0,
    thread: [{
      crop: false,
      source: null,
      angle: 0,
    }],
  };

  onUndo() {
    this.update({type: "UNDO"});
  }

  onRedo() {
    this.update({type: "REDO"});
  }

  onRotateLeft() {
    this.update({type: "ROTATE_LEFT"});
  }

  onRotateRight() {
    this.update({type: "ROTATE_RIGHT"});
  }

  onCropStart() {
    this.update({type: "START_CROPPING"});
  }

  onCropCancel() {
    this.update({type: "STOP_CROPPING"});
  }

  onDrop(acceptedFiles, rejectedFiles) {
    // console.log("Accepted files: ", acceptedFiles);
    // console.log("Rejected files: ", rejectedFiles);

    if (rejectedFiles.length) {
      this.props.dispatch({
        type: MESSAGE_ADD,
        data: {
          type: "danger",
          message: `${rejectedFiles[0].name} was rejected`,
        },
      });
      return;
    }

    readFile(acceptedFiles[0], file => {
      this.update({type: "SET_FILE", file});
    });
  }

  onCropConfirm() {
    const {source} = this.state.thread[this.state.step];
    const {x, y, width, height} = this.refs.canvasWrapper.cropBox;

    Transform.cropImage(source, {x, y, width, height}, {width: imageOriginalWidth, height: imageOriginalHeight})
      .then(image => this.update({type: "CONFIRM_CROP", image}));
  }

  update(action) {
    const state = this.state;
    let nextThread;
    let nextStep = state.thread.length;
    let newThread;

    switch (action.type) {
      case "SET_FILE":
        nextThread = fileController(state.thread[state.step], action);
        break;

      case "UNDO":
        nextStep = state.step - 1;
        break;

      case "REDO":
        nextStep = state.step + 1;
        break;

      case "ROTATE_LEFT":
      case "ROTATE_RIGHT":
      case "START_CROPPING":
      case "STOP_CROPPING":
      case "CONFIRM_CROP":
        nextThread = imageController(state.thread[state.step], action);
        break;

      default:
        break;
    }

    if ((action.type !== "UNDO" && action.type !== "REDO") &&
      (state.step > 0 && state.step < state.thread.length - 1)) {
      newThread = [
        ...state.thread.slice(0, state.step),
        nextThread,
      ];
      nextStep = newThread.length - 1;
    } else {
      newThread = nextThread ? [...state.thread, nextThread] : [].concat(state.thread);
    }

    this.setState({
      step: nextStep,
      thread: newThread,
    }, () => {
      setTimeout(() => {
        this.preprocess(this.refs.canvasWrapper.cache);
      }, 200);
    });
  }

  preprocess(image) {
    // Resize the image
    const canvas = document.createElement("canvas");
    canvas.width = imageOriginalWidth;
    canvas.height = imageOriginalHeight;
    const context = canvas.getContext("2d");
    context.fillStyle = "#ffffff";
    context.fillRect(0, 0, imageOriginalWidth, imageOriginalHeight);
    context.drawImage(image, 0, 0, imageOriginalWidth, imageOriginalHeight);
    const dataUrl = canvas.toDataURL("image/jpeg");
    const resizedImage = this.dataURLToBlob(dataUrl);
    resizedImage.lastModifiedDate = new Date();
    resizedImage.name = "name.jpeg";
    this.props.onChange(resizedImage);
  }

  dataURLToBlob(dataURI) {
    const parts = dataURI.split(",");
    const mimeString = parts[0].split(":")[1].split(";")[0];
    const byteString = atob(parts[1]);
    // const byteString = Buffer.from(parts[1], "base64").toString("ascii");

    const rawLength = byteString.length;
    const uInt8Array = new Uint8Array(rawLength);

    for (let i = 0; i < rawLength; i++) {
      uInt8Array[i] = byteString.charCodeAt(i);
    }

    return new Blob([uInt8Array], {type: mimeString});
  }

  render() {
    // console.log("ImageEditor:render", this.props, this.state);

    const {angle, source, crop} = this.state.thread[this.state.step];

    return (
      <div className="darkroom">
        <div className="media-object img-thumbnail">
          <Canvas
            ref="canvasWrapper"
            crop={crop}
            source={source}
            angle={angle}
            width={imageOriginalWidth}
            height={imageOriginalHeight}
          >
            <Dropzone
              multiple={false}
              accept="image/*"
              maxSize={1024 * 1024 * 5}
              onDrop={::this.onDrop}
              className="placeholder"
            >
              <Glyphicon glyph="picture" />
              <div>Drop image here</div>
            </Dropzone>
            {this.props.progress !== null
              ? <div className="progress-bar-container">
                <ProgressBar active now={this.props.progress} />
              </div>
              : null}
          </Canvas>
        </div>
        <ButtonGroup>
          <Button
            onClick={::this.onUndo}
            disabled={this.state.step === 0}
            title="undo"
          >
            <Glyphicon glyph="step-backward" />
          </Button>
          <Button
            onClick={::this.onRedo}
            disabled={this.state.step === this.state.thread.length - 1}
            title="redo"
          >
            <Glyphicon glyph="step-forward" />
          </Button>
          <Button
            disabled={!source}
            onClick={::this.onRotateLeft}
            title="rotate left"
          >
            <Glyphicon glyph="repeat" className="gly-flip-horizontal" />
          </Button>
          <Button
            disabled={!source}
            onClick={::this.onRotateRight}
            title="rotate right"
          >
            <Glyphicon glyph="repeat" />
          </Button>
          {!crop
            ? <Button
              disabled={!source}
              onClick={::this.onCropStart}
              title="crop"
            >
              <Glyphicon glyph="resize-small" />
            </Button>
            : null}
          {crop
            ? <Button
              disabled={!source}
              onClick={::this.onCropConfirm}
              style={{color: "green"}}
              title="confirm"
            >
              <Glyphicon glyph="ok" />
            </Button>
            : null}
          {crop
            ? <Button
              disabled={!source}
              onClick={::this.onCropCancel}
              style={{color: "red"}}
              title="cancel"
            >
              <Glyphicon glyph="remove" />
            </Button>
            : null}
        </ButtonGroup>
      </div>
    );
  }
}
