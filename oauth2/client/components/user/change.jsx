import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, Col, Form, FormGroup} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import {confirm, password} from "../../constants/placeholder";
import InputGroupValidation from "../inputs/input.group.validation";
import withForm from "../forms/withForm";
import {viewItemLabel, viewItemValue} from "../../constants/display";
import {withRouter} from "react-router";
import {MESSAGE_ADD} from "../../constants/actions";


@withRouter
@withForm("users")
export default class Change extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
    setState: PropTypes.func,
    formData: PropTypes.shape({
      password: PropTypes.string,
      confirm: PropTypes.string,
    }),

    dispatch: PropTypes.func,

    storeName: PropTypes.string,

    match: PropTypes.shape({
      params: PropTypes.object,
    }).isRequired,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };

  componentDidMount() {
    const {setState, match} = this.props;
    setState({
      token: match.params.token,
    });
  }

  componentDidUpdate() {
    const {storeName, dispatch, history} = this.props;
    if (!this.props[storeName].isLoading && this.props[storeName].success && this.props[storeName].name === "change") {
      dispatch({
        type: MESSAGE_ADD,
        data: {
          type: "success",
          message: "change-successful",
        },
      });
      history.push("/message");
    }
  }

  render() {
    const {onChange, onSubmit, formData} = this.props;
    return (
      <Form horizontal autoComplete="off" onSubmit={onSubmit} name="change" action="/users/change" method="PUT">
        <InputGroupValidation
          type="password"
          name="password"
          defaultValue={formData.password}
          placeholder={password}
          onChange={onChange}
        />
        <InputGroupValidation
          type="password"
          name="confirm"
          defaultValue={formData.confirm}
          placeholder={confirm}
          onChange={onChange}
        />
        <FormGroup>
          <Col xsOffset={viewItemLabel} xs={viewItemValue}>
            <Button type="submit">
              <FormattedMessage id="components.buttons.change" />
            </Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}
