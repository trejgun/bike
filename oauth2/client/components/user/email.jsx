import React, {Component} from "react";
import PropTypes from "prop-types";
import {FormattedMessage} from "react-intl";
import {Button, Col, Form, FormGroup} from "react-bootstrap";
import {email} from "../../constants/placeholder";
import InputGroupValidation from "../inputs/input.group.validation";
import {viewItemLabel, viewItemValue} from "../../constants/display";
import {readFromQueryString} from "../../utils/location";
import withForm from "../forms/withForm";
import {withRouter} from "react-router";
import {MESSAGE_ADD} from "../../constants/actions";


@withRouter
@withForm("users")
export default class Email extends Component {
  static propTypes = {
    dispatch: PropTypes.func,

    onChange: PropTypes.func,
    onSubmit: PropTypes.func,
    formData: PropTypes.shape({
      email: PropTypes.string,
    }),

    storeName: PropTypes.string,

    location: PropTypes.shape({
      search: PropTypes.string.isRequired,
    }).isRequired,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };

  componentDidUpdate() {
    const {storeName, dispatch, history, location} = this.props;
    if (!this.props[storeName].isLoading && this.props[storeName].success && this.props[storeName].name === "email") {
      window.opener.postMessage({
        source: "oauth2",
        message: "",
      }, process.env[`MODULE_${readFromQueryString("module", location.search).toUpperCase()}`]);
      dispatch({
        type: MESSAGE_ADD,
        data: {
          type: "success",
          message: "email-successful",
        },
      });
      history.push("/message");
    }
  }

  render() {
    const {formData, onSubmit, onChange} = this.props;
    return (
      <Form horizontal autoComplete="off" onSubmit={onSubmit} name="email" action="/users/email" method="PUT">
        <InputGroupValidation
          type="email"
          name="email"
          defaultValue={formData.email}
          placeholder={email}
          onChange={onChange}
        />
        <FormGroup>
          <Col xsOffset={viewItemLabel} xs={viewItemValue}>
            <Button type="submit">
              <FormattedMessage id="components.buttons.change" />
            </Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}
