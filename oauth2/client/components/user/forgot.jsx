import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, Col, Form, FormGroup} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import {email} from "../../constants/placeholder";
import InputGroup from "../inputs/input.group";
import {viewItemLabel, viewItemValue} from "../../constants/display";
import withForm from "../forms/withForm";
import {withRouter} from "react-router";
import {MESSAGE_ADD} from "../../constants/actions";


@withRouter
@withForm("users")
export default class Forgot extends Component {
  static propTypes = {
    storeName: PropTypes.string,

    onChange: PropTypes.func,
    onSubmit: PropTypes.func,
    formData: PropTypes.shape({
      email: PropTypes.string,
    }),

    dispatch: PropTypes.func,

    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };

  componentDidUpdate() {
    const {storeName, dispatch, history} = this.props;
    if (!this.props[storeName].isLoading && this.props[storeName].success && this.props[storeName].name === "forgot") {
      dispatch({
        type: MESSAGE_ADD,
        data: {
          type: "success",
          message: "forgot-successful",
        },
      });
      history.push("/message");
    }
  }

  render() {
    const {formData, onSubmit, onChange} = this.props;
    return (
      <Form horizontal autoComplete="off" onSubmit={onSubmit} action="/users/forgot" name="forgot" method="POST">
        <InputGroup
          type="email"
          name="email"
          defaultValue={formData.email}
          placeholder={email}
          onChange={onChange}
        />
        <FormGroup>
          <Col xsOffset={viewItemLabel} xs={viewItemValue}>
            <Button type="submit">
              <FormattedMessage id="components.buttons.send" />
            </Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}
