import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, ButtonToolbar, Col, Form, FormGroup} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import {email, password} from "../../constants/placeholder";
import InputGroup from "../inputs/input.group";
import withForm from "../forms/withForm";
import {viewItemLabel, viewItemValue} from "../../constants/display";


@withForm("users")
export default class Login extends Component {
  static propTypes = {
    onChange: PropTypes.func,
    formData: PropTypes.shape({
      email: PropTypes.string,
      password: PropTypes.string,
    }),
  };

  render() {
    const {formData, onChange} = this.props;

    return (
      <Form horizontal autoComplete="off" name="login" action="/api/login" method="POST">
        <InputGroup
          type="email"
          name="email"
          defaultValue={formData.email}
          placeholder={email}
          onChange={onChange}
        />
        <InputGroup
          type="password"
          name="password"
          defaultValue={formData.password}
          placeholder={password}
          onChange={onChange}
        />
        <FormGroup>
          <Col xsOffset={viewItemLabel} xs={viewItemValue}>
            <ButtonToolbar>
              <Button type="submit">
                <FormattedMessage id="components.buttons.login" />
              </Button>
              <Button type="button" href="/forgot">
                <FormattedMessage id="components.buttons.forgot" />
              </Button>
              <Button type="button" href="/register">
                <FormattedMessage id="components.buttons.signup" />
              </Button>
            </ButtonToolbar>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}
