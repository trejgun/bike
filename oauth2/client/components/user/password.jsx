import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, Col, Form, FormGroup} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import {confirm, password} from "../../constants/placeholder";
import InputGroupValidation from "../inputs/input.group.validation";
import {viewItemLabel, viewItemValue} from "../../constants/display";
import {readFromQueryString} from "../../utils/location";
import withForm from "../forms/withForm";
import {withRouter} from "react-router";
import {MESSAGE_ADD} from "../../constants/actions";


@withRouter
@withForm("users")
export default class Password extends Component {
  static propTypes = {
    storeName: PropTypes.string,

    onChange: PropTypes.func,
    onSubmit: PropTypes.func,
    formData: PropTypes.shape({
      password: PropTypes.string,
      confirm: PropTypes.string,
    }),

    dispatch: PropTypes.func,

    location: PropTypes.shape({
      search: PropTypes.string.isRequired,
    }).isRequired,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };

  componentDidUpdate() {
    const {storeName, location, dispatch, history} = this.props;
    if (!this.props[storeName].isLoading && this.props[storeName].success && this.props[storeName].name === "password") {
      window.opener.postMessage({
        source: "oauth2",
        message: "",
      }, process.env[`MODULE_${readFromQueryString("module", location.search).toUpperCase()}`]);
      dispatch({
        type: MESSAGE_ADD,
        data: {
          type: "success",
          message: "change-successful",
        },
      });
      history.push("/message");
    }
  }

  render() {
    const {onChange, onSubmit, formData} = this.props;
    return (
      <Form horizontal autoComplete="off" onSubmit={onSubmit} action="/users/password" name="password" method="PUT">
        <InputGroupValidation
          type="password"
          name="password"
          defaultValue={formData.password}
          placeholder={password}
          onChange={onChange}
        />
        <InputGroupValidation
          type="password"
          name="confirm"
          defaultValue={formData.confirm}
          placeholder={confirm}
          onChange={onChange}
        />
        <FormGroup>
          <Col xsOffset={viewItemLabel} xs={viewItemValue}>
            <Button type="submit">
              <FormattedMessage id="components.buttons.change" />
            </Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}
