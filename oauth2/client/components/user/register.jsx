import React, {Component} from "react";
import PropTypes from "prop-types";
import {withRouter} from "react-router";
import {Button, ButtonToolbar, Col, Form, FormGroup} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import {confirm, email, fullName, password} from "../../constants/placeholder";
import InputGroupValidation from "../inputs/input.group.validation";
import withCreate from "../forms/withCreateForm";
import {viewItemLabel, viewItemValue} from "../../constants/display";
import LanguageGroup from "../groups/language";
import {MESSAGE_ADD} from "../../constants/actions";


@withRouter
@withCreate("users")
export default class CreateUserForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
    formData: PropTypes.shape({
      email: PropTypes.string,
      password: PropTypes.string,
      confirm: PropTypes.string,
      fullName: PropTypes.string,
      language: PropTypes.string,
    }),

    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,

    dispatch: PropTypes.func,
    storeName: PropTypes.string,
  };

  componentDidUpdate() {
    // console.log("CreateUserForm:componentDidUpdate", this.props);
    if (!this.props[this.props.storeName].isLoading && this.props[this.props.storeName].success && this.props[this.props.storeName].name === "create") {
      this.props.dispatch({
        type: MESSAGE_ADD,
        data: {
          type: "success",
          message: "registration-successful",
        },
      });
      // TODO this is ugly hack
      setTimeout(() => {
        this.props.history.push("/message");
      }, 100);
    }
  }

  render() {
    const {formData, onSubmit, onChange} = this.props;
    return (
      <Form horizontal autoComplete="off" onSubmit={onSubmit}>
        <InputGroupValidation
          type="email"
          name="email"
          defaultValue={formData.email}
          placeholder={email}
          onChange={onChange}
        />
        <InputGroupValidation
          type="password"
          name="password"
          defaultValue={formData.password}
          placeholder={password}
          onChange={onChange}
        />
        <InputGroupValidation
          type="password"
          name="confirm"
          defaultValue={formData.confirm}
          placeholder={confirm}
          onChange={onChange}
        />
        <InputGroupValidation
          name="fullName"
          defaultValue={formData.fullName}
          placeholder={fullName}
          onChange={onChange}
        />
        <LanguageGroup
          defaultValue={formData.language}
          onChange={onChange}
        />
        <FormGroup>
          <Col xsOffset={viewItemLabel} xs={viewItemValue}>
            <ButtonToolbar>
              <Button type="submit">
                <FormattedMessage id="components.buttons.signup" />
              </Button>
            </ButtonToolbar>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}
