import React, {Component} from "react";
import PropTypes from "prop-types";
import {FormattedMessage} from "react-intl";
import {Button, Col, Form, FormGroup} from "react-bootstrap";
import {email} from "../../constants/placeholder";
import InputGroupValidation from "../inputs/input.group.validation";
import {viewItemLabel, viewItemValue} from "../../constants/display";
import withForm from "../forms/withForm";
import {withRouter} from "react-router";
import {MESSAGE_ADD} from "../../constants/actions";


@withRouter
@withForm("users")
export default class Resend extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
    formData: PropTypes.shape({
      email: PropTypes.string,
    }),

    dispatch: PropTypes.func,
    storeName: PropTypes.string,
    location: PropTypes.shape({
      search: PropTypes.string.isRequired,
    }).isRequired,
    users: PropTypes.object,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };

  componentDidUpdate() {
    const {storeName, dispatch, history} = this.props;
    if (!this.props[storeName].isLoading && this.props[storeName].success && this.props[storeName].name === "email") {
      dispatch({
        type: MESSAGE_ADD,
        data: {
          type: "success",
          message: "resend-successful",
        },
      });
      history.push("/message");
    }
  }

  render() {
    const {onChange, onSubmit, formData} = this.props;
    return (
      <Form horizontal autoComplete="off" onSubmit={onSubmit} name="email" action="/users/resend" method="POST">
        <InputGroupValidation
          type="email"
          name="email"
          defaultValue={formData.email}
          placeholder={email}
          onChange={onChange}
        />
        <FormGroup>
          <Col xsOffset={viewItemLabel} xs={viewItemValue}>
            <Button type="submit">
              <FormattedMessage id="components.buttons.send" />
            </Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}
