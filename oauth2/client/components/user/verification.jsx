import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import Loader from "../partials/loader";
import {withRouter} from "react-router";
import {MESSAGE_ADD} from "../../constants/actions";


@withRouter
@connect(
  state => ({
    hash: state.hash,
    users: state.users,
  }),
)
export default class Verification extends Component {
  static propTypes = {
    users: PropTypes.object,
    success: PropTypes.bool,
    match: PropTypes.object,
    dispatch: PropTypes.func,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };

  componentDidMount() {
    this.props.dispatch({
      type: "FETCH_REQUESTED",
      data: this.props.match.params,
      storeName: "users",
      action: "/users/verify",
      name: "verify",
    });
  }

  componentDidUpdate() {
    if (!this.props.users.isLoading && this.props.users.name === "verify") {
      if (this.props.users.success) {
        this.props.dispatch({
          type: MESSAGE_ADD,
          data: {
            type: "success",
            message: "verification-successful",
          },
        });
      }
      this.props.history.push("/message");
    }
  }

  render() {
    // console.log("Verification:render", this.props);
    return (
      <Loader />
    );
  }
}
