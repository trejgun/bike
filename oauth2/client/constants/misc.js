export const phoneNumber = "+62 (812) 3919-8760";
export const systemId = "ffffffffffffffffffffffff";
export const fullName = "Trej Gun";
export const companyName = "Puri Motors";
export const domainName = "purimotors.com";
export const email = `no-reply@${domainName}`;
