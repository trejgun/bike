import {combineReducers} from "redux";
import {routerReducer as routing} from "react-router-redux";
import {intlReducer as intl} from "react-intl-redux";

import hash from "./hash";
import messages from "./messages";
import oauth2 from "./oauth2";
import user from "./user";
import users from "./users";
import validations from "./validations";


export default combineReducers({
  intl,
  hash,
  messages,
  oauth2,
  routing,
  validations,
  user,
  users,
});
