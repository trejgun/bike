import {takeEvery} from "redux-saga/effects";
import fetch from "../utils/fetch";


export default function* watchFetch() {
  yield takeEvery("FETCH_REQUESTED", fetch);
}
