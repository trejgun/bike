import moment from "moment";


export function calcPrice({pricePerHour, pricePerDay, pricePerMonth}, range) {
  let result = 0;

  const duration = moment.duration(range.end.diff(range.start));

  if (pricePerMonth) {
    const monthDiff = ~~duration.asMonths();
    if (monthDiff) {
      result += pricePerMonth * monthDiff;
      duration.subtract(monthDiff, "month");
    }
  }


  if (pricePerDay) {
    const dayDiff = ~~duration.asDays();
    if (dayDiff) {
      result += pricePerDay * dayDiff;
      duration.subtract(dayDiff, "day");
    }
  }


  if (pricePerHour) {
    const hourDiff = ~~duration.asHours();
    if (hourDiff) {
      result += pricePerHour * hourDiff;
      duration.subtract(hourDiff, "hour");
    }
  }

  return result;
}

export function getRangeByDates(start, end, of = "month") {
  return moment.range(moment(start).tz("UTC").startOf(of), moment(end).tz("UTC").endOf(of));
}

export function getRangeByDate(date, of) {
  return getRangeByDates(date, date, of);
}

