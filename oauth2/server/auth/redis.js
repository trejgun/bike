import winston from "winston";
import redis from "redis";


export default function() {

  const client = redis.createClient(process.env.REDIS_PORT, process.env.REDIS_HOST);

  client.on("error", error => {
    winston.info(error);
  });

  client.on("connect", () => {
    winston.info("connect on host", process.env.REDIS_HOST);
  });

  client.on("ready", () => {
    winston.info("redis is ready");
  });

  return client;
}
