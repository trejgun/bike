export default [
  "vehicles_office:vehicles:create",
  "vehicles_office:vehicles:read",
  "vehicles_office:vehicles:update",
  "vehicles_office:vehicles:delete",

  "vehicles_office:bookings:create",
  "vehicles_office:bookings:read",
  "vehicles_office:bookings:update",
  "vehicles_office:bookings:delete",

  "vehicles_office:users:create",
  "vehicles_office:users:read",
  "vehicles_office:users:update",
  "vehicles_office:users:delete",

  "vehicles_office:organizations:read",
  "vehicles_office:organizations:update",

  "vehicles_office:statistics:read",

  "vehicles_market:vehicles:create",
  "vehicles_market:vehicles:read",
  "vehicles_market:vehicles:update",
  "vehicles_market:vehicles:delete",

  "vehicles_market:bookings:create",
  "vehicles_market:bookings:read",
  "vehicles_market:bookings:update",
  "vehicles_market:bookings:delete",

  "back_office:posts:create",
  "back_office:posts:read",
  "back_office:posts:update",
  "back_office:posts:delete",

  "back_office:users:create",
  "back_office:users:read",
  "back_office:users:update",
  "back_office:users:delete",
];
