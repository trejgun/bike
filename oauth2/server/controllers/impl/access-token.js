import AbstractController from "../abstract/abstract";


export default class AccessTokenController extends AbstractController {
  static realm = "oauth2";
}
