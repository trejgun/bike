import AbstractController from "../abstract/abstract";


export default class AuthorizationCodeController extends AbstractController {
  static realm = "oauth2";
}
