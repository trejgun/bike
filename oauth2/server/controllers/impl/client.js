import AbstractController from "../abstract/abstract";


export default class ClientController extends AbstractController {
  static realm = "oauth2";
}
