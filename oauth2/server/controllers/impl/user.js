import bluebird from "bluebird";
import {difference, pick} from "lodash";
import {checkModel} from "../abstract/middleware";
import {makeError} from "../../utils/error";
import {paginate} from "../../utils/response";
import StatefulController from "../abstract/stateful";
import MailController from "./mail";
import TokenController from "./token";
import OrganizationController from "./organization";
import {getConnections} from "../../utils/mongoose";
import permissions from "../../constants/permissions";


function myCheckOrganization() {
  return function myCheckOrganizationInner(model, request) {
    if (!model.organizations.map(organization => organization.toString()).includes(request.user.organization._id.toString())) {
      throw makeError("access-denied", 403, {reason: "organization"});
    }
  };
}

export default class UserController extends StatefulController {
  static realm = "oauth2";

  static logout(request, response) {
    request.session.destroy();
    request.logout();
    response.clearCookie();
    return bluebird.resolve({success: true});
  }

  typeahead(request) {
    return this.find({organizations: request.user.organization._id}) // don't remove _id
      .then(users => ({typeahead: users.map(user => pick(user, "fullName"))}));
  }

  edit(request) {
    if (Array.isArray(request.body.permissions) && request.body.permissions.length) {
      // checking for bullshit
      const diff1 = difference(request.body.permissions, permissions);
      if (diff1.length) {
        throw makeError("invalid-param", 400, {name: "permissions", reason: "invalid", items: diff1});
      } else {
        // checking for user permissions
        const diff2 = difference(request.body.permissions, request.user.permissions);
        if (diff2.length) {
          throw makeError("access-denied", 403, {name: "permissions", reason: "insufficient", items: diff2});
        }
      }
    }

    return this.change(request, {}, [], ["country", "fullName", "phoneNumber", "image", "language", "license", "permissions"])
      .then(() => this.getByQuery({_id: request.params._id}));
  }

  getByUId(request, options = {}, conditions = []) {
    return this.findOne({
      [this.constructor.param]: request.params[this.constructor.param] || request.body[this.constructor.param],
    }, options)
      .then(this.conditions(request, [checkModel(), myCheckOrganization()].concat(conditions)));
  }

  getByQuery(query, {populate = [], ...options} = {}) {
    const connections = getConnections();
    Object.assign(options, {
      populate: [{
        path: "organizations",
        model: connections.oauth2.model("Organization"),
        populate: [{
          path: "settings",
          model: connections.vehicles_office.model("Settings"),
        }],
      }].concat(populate),
    });
    return this.findOne(query, options);
  }

  list(request, response) {
    const clean = {
      organizations: [request.user.organization._id],
    };
    return paginate(request, response, this, clean);
  }

  insert(request) {
    const clean = pick(request.body, ["country", "confirm", "email", "fullName", "image", "language", "password", "social"]);
    Object.assign(clean, {
      isEmailVerified: request.params.isEmailVerified,
    });
    return this.create(clean)
      .then(user => {
        const organizationController = new OrganizationController();
        return organizationController.create({
          email: user.email,
          companyName: user.fullName,
        })
          .then(organization =>
            this.save(Object.assign(user, {organization})),
          );
      })
      .tap(user => {
        Object.assign(request, {user}); // don't login
        if (request.params.isEmailVerified) {
          return this.sendWelcomeEmail(request);
        } else {
          return this.sendVerificationEmail(request);
        }
      });
  }

  sendWelcomeEmail(request) {
    const mailController = new MailController();
    return mailController.compose("welcome", {
      to: request.user.email,
    }, request.user.toObject());
  }

  sync(request) {
    return bluebird.resolve(request.isAuthenticated() ? request.user : null);
  }

  changePassword(request, response) {
    const tokenController = new TokenController();
    return tokenController.findOne({
      token: request.body.token,
      type: TokenController.types.password,
    })
      .tap(model => checkModel().bind(tokenController)(model, request))
      .then(hash =>
        this.findById(hash.user, {lean: false})
          .then(user => {
            Object.assign(request, {user});
            return this.editPassword(request, response);
          })
          .then(() =>
            this.deleteOne(hash),
          ),
      )
      .thenReturn({success: true});
  }

  sendVerificationEmail(request) {
    const tokenController = new TokenController();
    return tokenController.upsert({
      user: request.user._id,
      type: TokenController.types.email,
    }, {}, {}, {lean: true})
      .then(hash => {
        const mailController = new MailController();
        return mailController.compose("verification", {
          to: request.user.email,
        }, request.user.toObject(), {
          hash,
        });
      })
      .thenReturn({success: true});
  }

  verifyEmail(request) {
    const tokenController = new TokenController();
    return tokenController.findOneAndRemove({
      token: request.query.token,
      type: TokenController.types.email,
    })
      .tap(model => checkModel().bind(tokenController)(model, request))
      .then(hash =>
        this.findByIdAndUpdate(hash.user, {isEmailVerified: true}),
      )
      .thenReturn({success: true});
  }

  editEmail(request, response) {
    const clean = pick(request.body, ["email"]);
    Object.assign(request.user, clean);
    return this.save(request.user)
      .tap(() =>
        this.sendVerificationEmail(request),
      )
      .tap(() =>
        UserController.logout(request, response),
      )
      .thenReturn({success: true});
  }

  editPassword(request, response) {
    const clean = pick(request.body, ["password", "confirm"]);
    Object.assign(request.user, clean);
    return this.save(request.user)
      .tap(() =>
        UserController.logout(request, response),
      )
      .thenReturn({success: true});
  }

  forgot(request) {
    return this.getByQuery({email: request.body.email})
      .then(user => {
        if (user) {
          const tokenController = new TokenController();
          return tokenController.upsert({
            user,
            type: TokenController.types.password,
          }, {}, {}, {lean: true})
            .then(hash => {
              const mailController = new MailController();
              return mailController.compose("forgot", {to: user.email}, user, {
                hash,
              });
            });
        }
      })
      .thenReturn({success: true});
  }

  resendToken(request) {
    return this.getByQuery({email: request.body.email}, {lean: false})
      .then(user => {
        if (!user) {
          return {success: true}; // just lie
        }

        Object.assign(request, {user}); // don't login
        return this.sendVerificationEmail(request);
      });
  }

  impersonate(request) {
    return this.getByQuery({_id: request.params._id})
      .then(user => {
        user.supervisor = request.user._id.toString();
        request.session.supervisor = request.user._id.toString();
        return bluebird.promisify(::request.login)(user);
      })
      .thenReturn({success: true});
  }

  depersonalize(request) {
    return this.getByQuery({_id: request.session.supervisor})
      .then(user => {
        request.session.supervisor = null;
        return bluebird.promisify(::request.login)(user);
      })
      .thenReturn({success: true});
  }
}
