import "./configs/env";
import "./configs/moment";
import "./configs/winston";
import "./auth/oauth";

import winston from "winston";
import app from "./configs/express";

import session from "./auth/session";
import passport from "./auth/passport";
import cors from "./routes/common/cors";
import pre from "./routes/common/pre";
import main from "./routes/common/main";
import publicAPI from "./routes/public";
import privateAPI from "./routes/private";
import notFound from "./routes/common/404";
import authorize from "./routes/common/authorize";
import {sendError} from "./utils/wrapper";
import fe from "./routes/common/fe";


app.use(session);
app.use(passport);
app.use(cors);
app.use(pre);
app.use(main);
app.use(publicAPI);
app.use(privateAPI);
app.use(notFound);
app.use(sendError);
app.use(authorize);
app.use(fe);


const server = app.listen(process.env.PORT, () => {
  winston.info(`Express server listening on port ${server.address().port}`);
});

process.on("unhandledRejection", winston.error);
process.on("uncaughtException", winston.error);

export default app;
