import {Schema} from "mongoose";
import {getRandomString} from "../../utils/misc";
import ShortId from "mongoose-shortid-nodeps";


const AccessToken = new Schema({
  accessToken: {
    type: ShortId,
    len: 256,
    generator: (options, callback) => callback(null, getRandomString(options.len)),
  },
  accessTokenExpiresOn: Date,
  clientId: String,
  refreshToken: String,
  refreshTokenExpiresOn: Date,
  userId: String, // TODO use ObjectId
}, {
  timestamps: true,
  versionKey: false,
});

/*
// TODO use index
AccessToken.index({
	updatedAt: 1
}, {
	expireAfterSeconds: 3600
});
 */

export default AccessToken;
