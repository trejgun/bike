import {Schema} from "mongoose";
import {getRandomString} from "../../utils/misc";
import ShortId from "mongoose-shortid-nodeps";


const AuthorizationCode = new Schema({
  code: {
    type: ShortId,
    len: 16,
    generator: (options, callback) => callback(null, getRandomString(options.len)),
  },
  clientId: String,
  redirectURI: String,
  userId: String,
}, {
  timestamps: true,
  versionKey: false,
});

export default AuthorizationCode;
