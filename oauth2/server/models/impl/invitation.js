import {Schema} from "mongoose";
import email from "../plugins/email";
import status from "../plugins/status";


const Invitation = new Schema({});

Invitation.plugin(email);
Invitation.plugin(status);

Invitation.index({
  organizations: 1,
  email: 1,
}, {unique: true});

export default Invitation;
