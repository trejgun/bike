import {renderAppToString, renderEmptyString} from "../../utils/render.app";


export default function renderFE(request, response) {
  if (process.env.SSR === "on") {
    renderAppToString(request, response);
  } else { // client
    renderEmptyString(request, response);
  }
}
