import {isType} from "./misc";
import {validateRange} from "./date";
import {makeError} from "./error";
import {reRange} from "../constants/regexp";

const validator = {
  type(val, type) {
    return isType(val, type.name);
  },
  call(val, fn) {
    return fn(val);
  },
  min(val, min) {
    return val >= min;
  },
  max(val, max) {
    return val <= max;
  },
  minlength(val, min) {
    return val.length >= min;
  },
  maxlength(val, max) {
    return val.length <= max;
  },
  enum(val, options) {
    return options.includes(val);
  },
  required(val, flag) {
    return !flag || val !== void 0;
  },
  regexp(val, regexp) {
    return regexp.test(val);
  },
  name() {
    return true; // always true
  },
};


export function validateParams(rulesSet, source, code = 400) {
  return (request, response, next) => {
    try {
      rulesSet.forEach(rules => {
        if (!(rules.name in request[source])) {
          if (rules.required === true) {
            throw makeError("invalid-param", code, {name: rules.name, reason: "required"});
          }
        } else if (!Object.keys(rules).every(key => validator[key](request[source][rules.name], rules[key]))) {
          throw makeError("invalid-param", code, {name: rules.name, reason: "invalid"});
        }
      });
      return next();
    } catch (e) {
      return next(e);
    }
  };
}

export function methodNotAllowed(request, response, next) {
  return next(makeError("method-not-allowed", 405, {method: request.method, url: request.url}));
}

export function requiresLogin(request, response, next) {
  if (request.isUnauthenticated()) {
    return next(makeError("login", 401));
  }
  return next();
}

export function checkPermissions(permission) {
  return (request, response, next) => {
    if (!request.user.permissions.includes(`${process.env.MODULE}:${permission}`)) {
      return next(makeError("access-denied", 403, {reason: "permissions"}));
    }
    return next();
  };
}

export function checkFeatures(prefix, object) {
  return (request, response, next) => {
    if (object && !request.user[prefix].preferences.features[object]) {
      return next(makeError("feature-not-enabled", 401));
    }
    return next();
  };
}

export const params = {
  limit: {
    name: "limit",
    type: Number,
    min: 1,
    max: 100,
  },
  skip: {
    name: "skip",
    type: Number,
    min: 0,
    max: 1000,
  },
  range: {
    name: "range",
    type: String,
    regexp: reRange,
    call: validateRange,
    required: true,
  },
};
