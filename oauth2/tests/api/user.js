import winston from "winston";
import bluebird from "bluebird";
import assert from "power-assert";
import supertest from "../../../test-utils/supertest";
import {setUp, tearDown} from "../../../test-utils/flow";
import {confirm, email, fullName, password} from "../../../test/constants";
import {defaultLanguage as language} from "../../server/constants/language";
import expect from "../../../test-utils/assert";

import MailController from "../../../mail/server/controllers/impl/mail";
import TokenController from "../../../mail/server/controllers/impl/token";
import UserController from "../../server/controllers/impl/user";


let data;

describe("User (OAuth2)", () => {
  const superapp = supertest("oauth2");

  const mailController = new MailController();
  const userController = new UserController();

  afterEach(() =>
    mailController.deleteMany(),
  );

  describe("POST /register", () => {
    describe("@pass", () => {
      it("should register new user", () =>
        superapp
          .login(null)
          .post("/api/users")
          .type("application/json;charset=utf-8")
          .send({
            email,
            password,
            confirm,
            fullName,
            language,
          })
          .expect(200)
          .then(({body}) => {
            winston.debug("body", body);
            assert.equal(body.email, email);
            return mailController.find()
              .then(mails => {
                winston.debug("mails", mails);
                assert.deepEqual(mails.map(mail => mail.type).sort(), ["verification"]);
              });
          }),
      );

      after(tearDown);
    });

    describe("@throws", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 1,
        }, {
          model: "User",
          requires: {
            Organization: "o2o",
          },
          data: [{
            email,
          }],
        }])
          .then(result => {
            data = result;
          }),
      );

      it("should throw `email-duplicate`", () =>
        superapp
          .login(null)
          .post("/api/users")
          .type("application/json;charset=utf-8")
          .send({
            email,
            password,
            confirm,
            fullName,
            language,
          })
          .then(expect(409, [{reason: "duplicate", name: "email"}])),
      );

      after(tearDown);
    });
  });

  describe("GET /sendVerificationEmail", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 1,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        data: [{
          email,
        }],
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should send email", () =>
      superapp
        .login(data.User[0])
        .get("/api/sendVerificationEmail")
        .expect(200)
        .then(({body}) => {
          winston.debug("body", body);
          assert.equal(body.success, true);
          return mailController.find()
            .then(mails => {
              winston.debug("mails", mails);
              assert.deepEqual(mails.map(mail => mail.type).sort(), ["verification"]);
            });
        }),
    );

    after(tearDown);
  });

  describe("GET /verify/:token", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 1,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        data: [{
          email,
        }],
      }, {
        model: "Token",
        requires: {
          User: "o2o",
        },
        data: [{
          type: TokenController.types.email,
        }],
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should verify email", () =>
      superapp
        .login(data.User[0])
        .get("/api/users/verify")
        .query({
          token: data.Token[0].token,
        })
        .expect(200)
        .then(({body}) => {
          winston.debug("body", body);
          return userController.findById(data.User[0]._id)
            .then(user => {
              assert.equal(user.isEmailVerified, true);
            });
        }),
    );

    it("should throw `hash-not-found`", () =>
      superapp
        .login(data.User[0])
        .get("/api/users/verify")
        .query({
          token: "crap",
        })
        .then(expect(404, [{name: "token"}])),
    );

    after(tearDown);
  });

  describe("GET /forgot", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 1,
      }, {
        model: "User",
        requires: {
          Organization: "m2o",
        },
        count: 2,
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should send email", () =>
      superapp
        .login(data.User[0])
        .post("/api/users/forgot")
        .send({
          email: data.User[0].email,
        })
        .expect(200)
        .then(({body}) => {
          winston.debug("body", body);
          return mailController.find()
            .then(mails => {
              winston.debug("mails", mails);
              assert.equal(mails.length, 1);
              assert.deepEqual(mails.map(mail => mail.type).sort(), ["forgot"]);
            });
        }),
    );

    it("should throw `invalid-param` (email)[required]", () =>
      superapp
        .login(data.User[0])
        .post("/api/users/forgot")
        .send({
          email: "",
        })
        .then(expect(400, [{reason: "invalid", name: "email"}])),
    );

    it("should throw `invalid-param` (email)[required]", () =>
      superapp
        .login(data.User[0])
        .post("/api/users/forgot")
        .send({})
        .then(expect(400, [{reason: "required", name: "email"}])),
    );

    after(tearDown);
  });

  describe("GET /change", () => {
    const newPassword = "1q2w3e4r5t6y7u8i9o0p";

    before(() =>
      setUp([{
        model: "Organization",
        count: 1,
      }, {
        model: "User",
        requires: {
          Organization: "m2o",
        },
        count: 2,
      }, {
        model: "Token",
        requires: {
          User: "o2o",
        },
        data: [{
          type: TokenController.types.password,
        }],
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should change password", () =>
      superapp
        .login(null)
        .put("/api/users/change")
        .type("application/json;charset=utf-8")
        .send({
          token: data.Token[0].token,
          password: newPassword,
          confirm: newPassword,
        })
        .expect(200)
        .then(({body}) => {
          winston.debug("body", body);
          return userController.findById(data.User[0]._id, {
            lean: false,
            select: "+password",
          })
            .then(user => {
              winston.debug("user", user);
              assert.equal(user.verifyPassword(password), false);
              assert.equal(user.verifyPassword(newPassword), true);
            });
        }),
    );

    after(tearDown);
  });

  describe("GET /email", () => {
    const newEmail = "my.new.email@example.com";

    before(() =>
      setUp([{
        model: "Organization",
        count: 1,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 1,
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should change email", () =>
      superapp
        .login(data.User[0])
        .put("/api/users/email")
        .type("application/json;charset=utf-8")
        .send({
          email: newEmail,
        })
        .expect(200)
        .then(({body}) => {
          winston.debug("body", body);
          return bluebird.all([
            userController.findById(data.User[0]._id),
            mailController.find(),
          ])
            .spread((user, mails) => {
              winston.debug("user", user);
              assert.equal(user.email, newEmail);
              assert.equal(user.isEmailVerified, false);
              assert.deepEqual(mails.map(mail => mail.type).sort(), ["verification"]);
            });
        }),
    );

    after(tearDown);
  });

  describe("GET /logout", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 1,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 1,
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should logout user", () =>
      superapp
        .login(data.User[0])
        .get("/api/logout")
        .expect(302)
        .then(({body}) => {
          winston.debug("body", body);
        }),
    );

    it("should logout guest", () =>
      superapp
        .login(null)
        .get("/api/logout")
        .expect(302)
        .then(({body}) => {
          winston.debug("body", body);
        }),
    );

    after(tearDown);
  });
});
