#!/usr/bin/env bash

NODE_ENV=development MODULE=back_office PORT=65000 node_modules/.bin/babel-node server/back_office.js
NODE_ENV=development MODULE=vehicles_office PORT=55000 node_modules/.bin/babel-node server/vehicles_office.js
NODE_ENV=development MODULE=vehicles_market PORT=56000 node_modules/.bin/babel-node server/vehicles_market.js
NODE_ENV=development MODULE=oauth2 PORT=33000 node_modules/.bin/babel-node server/oauth2.js
NODE_ENV=development MODULE=cdn PORT=30000 node_modules/.bin/babel-node server/cdn.js
