import {getRandomFloat, getRandomString} from "../vehicles_office/server/utils/misc";
import populate from "./populate";
import {
  bookingObject,
  clientObject,
  mailObject,
  optOutObject,
  organizationObject,
  policyObject,
  postObject,
  settingsObject,
  snsObject,
  userObject,
  vehicleObject,
} from "./objects";

import ClientController from "../oauth2/server/controllers/impl/client";
import UserController from "../oauth2/server/controllers/impl/user";
import OrganizationController from "../oauth2/server/controllers/impl/organization";
import VehicleController from "../vehicles_office/server/controllers/impl/vehicle";
import BookingController from "../vehicles_office/server/controllers/impl/booking";
import SettingsController from "../vehicles_office/server/controllers/impl/settings";
import PolicyController from "../vehicles_office/server/controllers/impl/policy";
import TokenController from "../mail/server/controllers/impl/token";
import OptOutController from "../mail/server/controllers/impl/opt-out";
import MailController from "../mail/server/controllers/impl/mail";
import SnsController from "../mail/server/controllers/impl/sns";
import PostController from "../back_office/server/controllers/impl/post";


export function createVehicle(...args) {
  const vehicleController = new VehicleController();
  return vehicleController.create(populate(...args, (vehicle, nested) =>
    vehicleObject({
      organizations: nested.Organization,
    }, vehicle)));
}

export function createOrganization(...args) {
  const organizationController = new OrganizationController();
  return organizationController.create(populate(...args, (organization) =>
    organizationObject({
      companyName: getRandomString(getRandomFloat(5, 16), 2).toLowerCase(),
    }, organization)));
}

export function createUser(...args) {
  const userController = new UserController();
  return userController.create(populate(...args, (user, nested) =>
    userObject({
      organizations: [].concat(nested.Organization),
    }, user)));
}

export function createBooking(...args) {
  const bookingController = new BookingController();
  return bookingController.create(populate(...args, (booking, nested) =>
    bookingObject({
      vehicle: nested.Vehicle,
      organizations: [].concat(nested.Organization),
    }, booking)));
}

export function createToken(...args) {
  const tokenController = new TokenController();
  return tokenController.create(populate(...args, (booking, nested) =>
    bookingObject({
      user: nested.User,
      type: TokenController.types.email,
    }, booking)));
}

export function createOptOut(...args) {
  const optOutController = new OptOutController();
  return optOutController.create(populate(...args, (optouts, nested) =>
    optOutObject({
      user: nested.User,
    }, optouts)));
}

export function createPost(...args) {
  const postController = new PostController();
  return postController.create(populate(...args, (posts, nested) =>
    postObject({
      postId: getRandomString(10),
      organizations: [].concat(nested.Organization),
    }, posts)));
}

export function createSettings(...args) {
  const settingsController = new SettingsController();
  return settingsController.create(populate(...args, (settings, nested) =>
    settingsObject({
      organizations: [].concat(nested.Organization),
    }, settings)));
}

export function createPolicy(...args) {
  const policyController = new PolicyController();
  return policyController.create(populate(...args, (policy, nested) =>
    policyObject({
      organizations: [].concat(nested.Organization),
    }, policy)));
}

export function createMail(...args) {
  const mailController = new MailController();
  return mailController.create(populate(...args, mail =>
    mailObject({}, mail)));
}

export function createClient(...args) {
  const clientController = new ClientController();
  return clientController.create(populate(...args, client =>
    clientObject({}, client)));
}

export function createSns(...args) {
  const snsController = new SnsController();
  return snsController.create(populate(...args, client =>
    snsObject({}, client)));
}
