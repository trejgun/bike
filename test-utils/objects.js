import {getRange} from "./utils";
import {getRandomFloat, getRandomString} from "../vehicles_office/server/utils/misc";
import {
  brand,
  cc,
  companyName,
  confirm,
  country,
  description,
  domainName,
  email,
  fuel,
  fullName,
  model,
  notes,
  password,
  phoneNumber,
  price,
  pricePerDay,
  systemId,
  title,
  year,
} from "../test/constants";
import {checkOutTime, date} from "../vehicles_office/server/constants/date";
import {defaultLanguage as language} from "../vehicles_office/server/constants/language";
import colors from "../vehicles_office/client/constants/colors";
import getLocalization from "../vehicles_office/client/intl/setup";
import permissions from "../vehicles_office/server/constants/permissions";


export function makeTestEmail(type) {
	const parts = email.split("@");
	return `${parts[0]}+${type}+${getRandomString(10).toLowerCase()}@${parts[1]}`;
}

export function makeTestDomainName() {
	return `${getRandomString(10)}.${domainName}`;
}

export function vehicleObject(...data) {
	return Object.assign({
		type: "bike",
		plate: `DK${getRandomFloat(1, 9)}${getRandomString(3, 0)}${getRandomString(2, 1)}`,
		brand,
		model,
		fuel,
		cc,
		year,
		color: colors.black,
    image: `${process.env.MODULE_CDN}/img/bikes/honda-vario-125.jpg`,
		pricePerDay,
		discount: 10,
	}, ...data);
}

export function organizationObject(...data) {
	return Object.assign({
		companyName,
		domainName: makeTestDomainName(),
		phoneNumber,
		organizations: [systemId],
		email: makeTestEmail("organization"),
		delivery: true,
		address: "very long street 123",
		coordinates: [getRandomFloat(-180, 180, 5), getRandomFloat(-90, 90, 5)],
	}, ...data);
}

export function userObject(...data) {
	return Object.assign({
		password,
		confirm,
		fullName,
		phoneNumber,
		language,
		country,
		permissions,
		isEmailVerified: true,
		email: makeTestEmail("user"),
	}, ...data);
}

export function bookingObject(...data) {
	return Object.assign({
		fullName,
		phoneNumber,
		email: makeTestEmail("customer"),
		range: getRange(2, 5),
		price,
		country,
		notes,
		vehicle: vehicleObject(),
	}, ...data);
}

export function optOutObject(...data) {
	return Object.assign({
		type: "welcome",
	}, ...data);
}

export function postObject(...data) {
	return Object.assign({
		title,
		description,
	}, ...data);
}

export function settingsObject(...data) {
	return Object.assign({
		docs: false,
	}, ...data);
}

export function policyObject(...data) {
	return Object.assign({
		type: "return",
		description: "policy text".repeat(100),
		language,
		checkOutTime,
		allowed: "not",
	}, ...data);
}

export function mailObject(...data) {
	return Object.assign({
		to: email,
		cc: email,
		bcc: email,
		type: "welcome",
    subject: getLocalization(language).messages["email.types.welcome"],
		html: "Welcome my friend :)",
	}, ...data);
}

export function clientObject(...data) {
	return Object.assign({
		clientId: "purimotors-test-server",
		clientSecret: "01123581321345589144233377610",
		redirectURIs: [
			"http://localhost:9001/api/auth/system/callback",
			"http://localhost:8001/api/auth/system/callback",
			"http://localhost:7001/api/auth/system/callback",
		],
	}, ...data);
}


export function snsObject(...data) {
	return Object.assign({
		Type: "type",
		MessageId: "id",
		TopicArn: "arn",
		Subject: "suject",
		Message: {
			Text: "text",
		},
		Timestamp: date,
	}, ...data);
}
