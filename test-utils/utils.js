import "../vehicles_office/server/configs/moment";

import moment from "moment";
import {date} from "../vehicles_office/server/constants/date";


export function getRange(start, end) {
  return moment.range(moment(date).startOf("day").add(start, "d"), moment(date).endOf("day").add(end, "d")).toString();
}
