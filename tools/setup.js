import bluebird from "bluebird";
import {times} from "lodash";
import winston from "winston";
import "../vehicles_office/server/configs/env";
import "../vehicles_office/server/configs/winston";
import "../vehicles_office/server/configs/moment";
import {systemId} from "../vehicles_office/server/constants/misc";
import {checkOutTime} from "../vehicles_office/server/constants/date";
import {processValidationError} from "../vehicles_office/server/utils/error";
import {getRandomString} from "../vehicles_office/server/utils/misc";
import {bookingObject, organizationObject, postObject, userObject, vehicleObject} from "../test-utils/objects";
import {cleanUp} from "../test-utils/flow";
import {getRange} from "../test-utils/utils";
import {email} from "../test/constants";
import catalog from "../test/vehicles";
import permissions from "../vehicles_office/server/constants/permissions";

import ClientController from "../oauth2/server/controllers/impl/client";
import CatalogController from "../vehicles_office/server/controllers/impl/catalog";
import VehicleController from "../vehicles_office/server/controllers/impl/vehicle";
import OrganizationController from "../oauth2/server/controllers/impl/organization";
import UserController from "../oauth2/server/controllers/impl/user";
import BookingController from "../vehicles_office/server/controllers/impl/booking";
import MailController from "../mail/server/controllers/impl/mail";
import PostController from "../back_office/server/controllers/impl/post";
import PolicyController from "../vehicles_office/server/controllers/impl/policy";
import SettingsController from "../vehicles_office/server/controllers/impl/settings";


const clientController = new ClientController();
const organizationController = new OrganizationController();
const userController = new UserController();
const catalogController = new CatalogController();
const vehicleController = new VehicleController();
const bookingController = new BookingController();
const mailController = new MailController();
const postController = new PostController();
const policyController = new PolicyController();
const settingsController = new SettingsController();

cleanUp()
  .then(() =>
    bluebird.all([
      organizationController.create(organizationObject({
        _id: systemId,
        email,
      })),
      clientController.create({
        clientId: `purimotors-${process.env.NODE_ENV}-server`,
        clientSecret: "01123581321345589144233377610",
        redirectURIs: [
          `${process.env.MODULE_BACK_OFFICE}/api/auth/system/callback`,
          `${process.env.MODULE_VEHICLES_OFFICE}/api/auth/system/callback`,
          `${process.env.MODULE_VEHICLES_MARKET}/api/auth/system/callback`,
        ],
      }),
      mailController.create({
        to: [email],
        subject: "Test Email",
        html: "<h2>Lorem ipsum sit amet</h2>",
      }),
      catalogController.create(catalog),
    ])
      .spread(organization =>
        bluebird.all([
          userController.create(userObject({
            _id: systemId,
            organizations: [organization],
            email,
            permissions,
            social: {
              facebook: "369446300063594",
              google: "113789811157156505051",
            },
          })),
          userController.create(userObject({
            organizations: [organization],
            fullName: "CTAPbIu MABP",
            email: "ctapbiumabp@gmail.com",
            permissions,
            social: {
              facebook: "1364552366951980",
            },
          })),
          vehicleController.create(times(25, () => vehicleObject({
            organizations: [organization],
          }))),
          postController.create(times(15, i => postObject({
            organizations: [organization],
            postId: `post-${i}`,
          }))),
          policyController.create([{
            organizations: [organization],
            type: PolicyController.types.return,
            description: "You have to return vehicle in time, otherwise you must to pay a fine or keep it for the next day.",
            checkOutTime,
          }, {
            organizations: [organization],
            type: PolicyController.types.island,
            description: "You are not allowed to take vehicles of this rental shop to another island. In case you decided to do so you will be stopped by police at seaport and won't be allowed to use ferry.",
            allowed: "not",
          }, {
            organizations: [organization],
            type: PolicyController.types.delivery,
            description: "This rental shop provides vehicle delivery for a fee based on distance.",
            allowed: "fee",
          }]),
          settingsController.create({
            organizations: [organization],
            docs: true,
          }),
        ])
          .spread((user1, user2, vehicles) =>
            bluebird.all(vehicles.map(vehicle =>
              bookingController.create([bookingObject({
                organizations: [organization],
                vehicle,
                email,
                range: getRange(2, 5),
                fullName: getRandomString(8, 1),
              }), bookingObject({
                organizations: [organization],
                vehicle,
                email,
                range: getRange(7, 17),
                fullName: getRandomString(8, 1),
              })]),
            )),
          ),
      ),
  )
  .then(() => {
    winston.info("OK");
  })
  .catch(e => {
    winston.info("FAIL");
    if (e.name === "ValidationError") {
      winston.info(processValidationError(e));
    } else {
      winston.info(e);
    }
  })
  .done(() => {
    winston.info("DONE");
    process.exit(0);
  });
