import React, {Component} from "react";
import {Route, Switch} from "react-router";
import Header from "./components/partials/header";
import ErrorDialog from "./components/partials/error";

import Landing from "./components/landing/landing";

import Private from "./components/partials/private";
import Public from "./components/partials/public";

import Dashboard from "./components/landing/dashboard";

import Vehicle from "./components/vehicle/index";
import Bookings from "./components/booking/index";
import Profile from "./components/user/index";

import Contacts from "./components/common/contacts";
import Disclaimer from "./components/common/disclaimer";
import Terms from "./components/common/terms";
import Privacy from "./components/common/privacy";
import Login from "./components/common/login";
import Message from "./components/common/message";
import NotFound from "./components/common/notfound";

import GAListener from "./components/ga";


export default class App extends Component {
  render() {
    return (
      <GAListener trackingId="UA-89625580-3">
        <Header />
        <ErrorDialog />
        <Switch>
          <Route path="/" component={Landing} exact />

          <Private path="/dashboard" component={Dashboard} />

          <Private path="/vehicles" component={Vehicle} />
          <Private path="/bookings" component={Bookings} />
          <Private path="/profile" component={Profile} />

          <Public path="/login" component={Login} />

          <Public path="/contacts" component={Contacts} />
          <Public path="/disclaimer" component={Disclaimer} />
          <Public path="/terms-of-service" component={Terms} />
          <Public path="/privacy-policy" component={Privacy} />

          <Public path="/message" component={Message} />

          <Public component={NotFound} />
        </Switch>
      </GAListener>
    );
  }
}
