import React, {Component} from "react";
import PropTypes from "prop-types";
import {FormattedMessage} from "react-intl";
import {Link} from "react-router-dom";
import {Button, ButtonToolbar, Form, Media} from "react-bootstrap";
import {vehicleToHtml} from "../../utils/vehicle";
import withStore from "../forms/withStore";
import withReadForm from "../forms/withReadForm";
import withCreate from "../forms/withCreateForm";
import {notes, phoneNumber} from "../../constants/placeholder";
import InputMarkdownGroup from "../inputs/input.markdown.group";
import InputDateTimePickerGroup from "../inputs/input.datetimepicker.group";
import InputStaticGroup from "../inputs/input.static.group";
import InputGroupValidation from "../inputs/input.group.validation";
import ImageViewer from "../imageViewer";


@withStore("user")
@withReadForm("vehicles")
@withCreate("bookings")
export default class BookingCreateForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
    setState: PropTypes.func,
    formData: PropTypes.shape({
      fullName: PropTypes.string,
      email: PropTypes.string,
      range: PropTypes.string,
      phoneNumber: PropTypes.string,
      notes: PropTypes.string,
    }),

    user: PropTypes.object,
    vehicles: PropTypes.object,

    match: PropTypes.shape({
      params: PropTypes.object.isRequired,
    }).isRequired,
  };

  render() {
    // console.log("BookingCreateForm:render", this.props);

    const {formData, onSubmit, onChange, setState, vehicles, user, match} = this.props;

    const vehicle = vehicles.list.find(item => item._id === match.params._id);

    if (!vehicle) {
      return null;
    }

    return (
      <Form horizontal onSubmit={onSubmit}>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.booking.create" />
          </h4>
        </div>
        <Media>
          <Media.Left>
            <ImageViewer
              image={vehicle.image}
              placeholder="vehicle"
              title={vehicleToHtml(vehicle)}
            />
          </Media.Left>
          <Media.Body>
            <InputStaticGroup
              name="vehicle"
              value={<Link to={`/vehicles/${vehicle._id}`}>{vehicleToHtml(vehicle, false)}</Link>}
            />
            <InputStaticGroup
              name="fullName"
              value={user.fullName}
            />
            <InputStaticGroup
              name="email"
              value={user.email}
            />
            <InputDateTimePickerGroup
              name="range"
              defaultValue={formData.range}
              setState={setState}
            />
            <InputGroupValidation
              name="phoneNumber"
              defaultValue={formData.phoneNumber}
              placeholder={phoneNumber}
              onChange={onChange}
            />
            <InputMarkdownGroup
              componentClass="textarea"
              name="notes"
              defaultValue={formData.notes}
              placeholder={notes}
              onChange={onChange}
            />
          </Media.Body>
          <ButtonToolbar>
            <Button type="submit" className="pull-right">
              <FormattedMessage id="components.buttons.book" />
            </Button>
          </ButtonToolbar>
        </Media>
      </Form>
    );
  }
}
