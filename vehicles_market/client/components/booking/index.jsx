import React, {Component} from "react";
import PropTypes from "prop-types";
import {Route, Switch} from "react-router";
import BookingList from "./list";
import BookingCreate from "./create";
import BookingUpdate from "./update";
import BookingRead from "./read";
import {readFromQueryString} from "../../utils/location";


export default class Bookings extends Component {
  static propTypes = {
    location: PropTypes.shape({
      search: PropTypes.string.isRequired,
    }).isRequired,
  };

  subRender(component) {
    return props => {
      Object.assign(props.match.params, {_id: readFromQueryString("vehicle", this.props.location.search)});
      return React.createElement(component, props);
    };
  }

  render() {
    return (
      <Switch>
        <Route path="/bookings" component={BookingList} exact />
        <Route path="/bookings/create" render={this.subRender(BookingCreate)} />
        <Route path="/bookings/:_id" component={BookingRead} exact />
        <Route path="/bookings/:_id/edit" component={BookingUpdate} />
      </Switch>
    );
  }
}
