import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, Col, ControlLabel, Form, FormGroup} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import withStore from "../forms/withStore";
import withListForm from "../forms/withListForm";
import {date} from "../../constants/date";
import {getRangeByDate, getRangeByDates} from "../../utils/date";
import Input from "../inputs/input";
import LimitGroup from "../groups/limit";


@withStore("user")
@withListForm("bookings")
export default class BookingListForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
    formData: PropTypes.shape({
      skip: PropTypes.number,
      limit: PropTypes.number,
      range: PropTypes.string,
      status: PropTypes.string,
    }),

    user: PropTypes.object,

  };

  componentDidMount() {
    // console.log("BookingListForm:componentDidMount", this.props);
    this.props.onSubmit();
  }

  render() {
    // console.log("BookingListForm:render", this.props);
    const {formData, onSubmit, onChange, user} = this.props;
    return (
      <Form horizontal onSubmit={onSubmit}>
        <Col xs={12}>
          <FormGroup>
            <Col componentClass={ControlLabel}>
              <FormattedMessage id="fields.date.label" />
            </Col>
            <Input
              componentClass="select"
              name="range"
              defaultValue={formData.range}
              onChange={onChange}
            >
              {Array.from(getRangeByDates(user.organization.created, date, "M").by("month")).map((month, i) =>
                (<option
                  key={i}
                  value={getRangeByDate(month, "M").toString()}
                >
                  {month.format("MMM YYYY")}
                </option>),
              )}
            </Input>
          </FormGroup>
          <FormGroup>
            <Col componentClass={ControlLabel}>
              <FormattedMessage id="fields.status.label" />
            </Col>
            <Input
              componentClass="select"
              name="status"
              defaultValue={formData.status}
              onChange={onChange}
            >
              <option value="">all</option>
              <option value="new">new</option>
              <option value="accepted">accepted</option>
              <option value="rejected">rejected</option>
              <option value="revoked">revoked</option>
              <option value="cancelled">cancelled</option>
            </Input>
          </FormGroup>
          <LimitGroup
            defaultValue={formData.limit}
            onChange={onChange}
          />
          <FormGroup>
            <Button type="submit">
              <FormattedMessage id="components.buttons.search" />
            </Button>
          </FormGroup>
        </Col>
      </Form>
    );
  }
}
