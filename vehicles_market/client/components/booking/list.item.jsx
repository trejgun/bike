import React, {Component} from "react";
import PropTypes from "prop-types";
import moment from "moment";
import {LinkContainer} from "react-router-bootstrap";
import {Form, Image, ListGroupItem, Media} from "react-bootstrap";
import withListItemHelper from "../forms/withListItem";
import {listItemHeight, listItemWidth} from "../../constants/display";
import {humanReadable} from "../../constants/date";
import {vehicleToHtml} from "../../utils/vehicle";
import InputStaticGroup from "../inputs/input.static.group";


@withListItemHelper("bookings")
export default class BookingListItem extends Component {
  static propTypes = {
    formData: PropTypes.shape({
      _id: PropTypes.string,
      start: PropTypes.string,
      end: PropTypes.string,
      status: PropTypes.string,
      vehicle: PropTypes.object,
    }),
  };

  render() {
    const {formData} = this.props;
    return (
      <LinkContainer key={formData._id} to={`/bookings/${formData._id}`}>
        <ListGroupItem>
          <Media>
            <Media.Left className="hidden-xs">
              <Image
                width={listItemWidth}
                height={listItemHeight}
                src={formData.vehicle.image || `${process.env.MODULE_CDN}/img/icons/booking.png`}
                className="media-object img-thumbnail"
                alt="Image"
              />
            </Media.Left>
            <Media.Body>
              <Media.Heading>{vehicleToHtml(formData.vehicle)}</Media.Heading>
              <Form horizontal>
                <InputStaticGroup
                  name="date"
                  value={moment.range(formData.start, formData.end).format(humanReadable)}
                />
                <InputStaticGroup
                  name="status"
                  value={formData.status}
                />
              </Form>
            </Media.Body>
          </Media>
        </ListGroupItem>
      </LinkContainer>
    );
  }
}
