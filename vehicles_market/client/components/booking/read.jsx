import React, {Component} from "react";
import PropTypes from "prop-types";
import {Form, Glyphicon, Media} from "react-bootstrap";
import {Link} from "react-router-dom";
import moment from "moment";
import {FormattedMessage} from "react-intl";
import {vehicleToHtml} from "../../utils/vehicle";
import {humanReadable} from "../../constants/date";
import withDeleteForm from "../forms/withDeleteForm";
import ImageViewer from "../imageViewer";
import InputStaticGroup from "../inputs/input.static.group";


@withDeleteForm("bookings")
export default class BookingReadForm extends Component {
  static propTypes = {
    onDelete: PropTypes.func,
    formData: PropTypes.shape({
      _id: PropTypes.string,
      bookingId: PropTypes.string,
      vehicle: PropTypes.object,
      range: PropTypes.string,
      fullName: PropTypes.string,
      email: PropTypes.string,
      phoneNumber: PropTypes.string,
      status: PropTypes.string,
    }),
  };

  render() {
    // console.log("BookingReadForm:render", this.props);
    const {formData, onDelete} = this.props;
    return (
      <Form horizontal>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.booking.read" />
            {formData.status === "new" ? // any `new` can be edit
              <Link to={`/bookings/${formData._id}/edit`}>
                <Glyphicon glyph="edit" />
              </Link>
              : null}
            {formData.status === "new" ? // any `new` can be revoked
              <Link to={`/bookings/${formData._id}/delete`} onClick={onDelete}>
                <Glyphicon glyph="remove" />
              </Link>
              : null}
            {formData.status === "accepted" ? // any `accepted` can be cancelled
              <Link to={`/bookings/${formData._id}/delete`} onClick={onDelete}>
                <Glyphicon glyph="trash" />
              </Link>
              : null}
          </h4>
        </div>
        <Media className="view">
          <Media.Left>
            <ImageViewer
              image={formData.vehicle.image}
              placeholder="vehicle"
              title={vehicleToHtml(formData.vehicle, true)}
            />
          </Media.Left>
          <Media.Body>
            <InputStaticGroup
              name="bookingId"
              value={formData.bookingId}
            />
            <InputStaticGroup
              name="vehicle"
              value={<Link to={`/vehicles/${formData.vehicle._id}`}>{vehicleToHtml(formData.vehicle)}</Link>}
            />
            <InputStaticGroup
              name="range"
              value={moment.range(formData.range).format(humanReadable)}
            />
            <InputStaticGroup
              name="fullName"
              value={formData.fullName}
            />
            {formData.email ?
              <InputStaticGroup
                name="email"
                value={formData.email}
              /> : null}
            {formData.phoneNumber ?
              <InputStaticGroup
                name="phoneNumber"
                value={formData.phoneNumber}
              /> : null}
            <InputStaticGroup
              name="status"
              value={formData.status}
            />

          </Media.Body>
        </Media>
      </Form>
    );
  }
}

