import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, ButtonToolbar, Col, ControlLabel, Form, FormControl, FormGroup, Media} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import {Link} from "react-router-dom";
import {viewItemLabel, viewItemValue} from "../../constants/display";
import {vehicleToHtml} from "../../utils/vehicle";
import {email, fullName, notes, phoneNumber} from "../../constants/placeholder";
import withUpdateForm from "../forms/withUpdateForm";
import InputGroupValidation from "../inputs/input.group.validation";
import InputDateTimePickerGroup from "../inputs/input.datetimepicker.group";
import InputMarkdownGroup from "../inputs/input.markdown.group";
import ImageViewer from "../imageViewer";


@withUpdateForm("bookings")
export default class BookingUpdateForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
    setState: PropTypes.func,
    formData: PropTypes.shape({
      fullName: PropTypes.string,
      email: PropTypes.string,
      range: PropTypes.string,
      vehicle: PropTypes.shape({
        _id: PropTypes.string,
        image: PropTypes.string,
      }),
      phoneNumber: PropTypes.string,
      notes: PropTypes.string,
    }),
  };

  render() {
    const {formData, onSubmit, setState, onChange} = this.props;
    return (
      <Form horizontal onSubmit={onSubmit}>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.booking.update" />
          </h4>
        </div>
        <Media>
          <Media.Left>
            <ImageViewer
              image={formData.vehicle.image}
              placeholder="vehicle"
              title={vehicleToHtml(formData.vehicle, true)}
            />
          </Media.Left>
          <Media.Body>
            <FormGroup>
              <Col componentClass={ControlLabel} xs={viewItemLabel}>
                <FormattedMessage id="fields.vehicle.label" />
              </Col>
              <Col xs={viewItemValue}>
                <FormControl.Static>
                  {<Link to={`/vehicles/${formData.vehicle._id}`}>{vehicleToHtml(formData.vehicle, false)}</Link>}
                </FormControl.Static>
              </Col>
            </FormGroup>
            <InputDateTimePickerGroup
              name="range"
              defaultValue={formData.range}
              setState={setState}
            />
            <InputGroupValidation
              name="fullName"
              defaultValue={formData.fullName}
              placeholder={fullName}
              onChange={onChange}
              required
            />
            <InputGroupValidation
              type="email"
              name="email"
              defaultValue={formData.email}
              placeholder={email}
              onChange={onChange}
            />
            <InputGroupValidation
              name="phoneNumber"
              defaultValue={formData.phoneNumber}
              placeholder={phoneNumber}
              onChange={onChange}
            />
            <InputMarkdownGroup
              componentClass="textarea"
              name="notes"
              defaultValue={formData.notes}
              placeholder={notes}
              onChange={onChange}
            />
            <ButtonToolbar>
              <Button type="submit" className="pull-right">
                <FormattedMessage id="components.buttons.save" />
              </Button>
            </ButtonToolbar>
          </Media.Body>
        </Media>
      </Form>
    );
  }
}
