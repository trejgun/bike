import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";


export default function withTypeaheadProvider(storeName, action) {
  return WrappedComponent => {
    class TypeaheadProvider extends Component {
      static propTypes = {
        dispatch: PropTypes.func,
      };

      componentDidMount() {
        // console.log("TypeaheadProvider:componentDidMount", this.props);
        if (!this.props[action].list.length) {
          this.props.dispatch({
            type: "FETCH_REQUESTED",
            action: `/${storeName}/${action}`,
            name: action,
            storeName,
          });
        } else {
          this.props.dispatch({
            type: `${storeName}_${action}_success`,
            action,
            isLoading: false,
            success: true,
            data: this.props[action],
          });
        }
      }

      render() {
        // console.log("TypeaheadProvider:render", this.props);
        return (
          <WrappedComponent
            {...this.props}
          />
        );
      }
    }

    return connect(
      state => ({
        [action]: state[action],
      }),
    )(TypeaheadProvider);
  };
}
