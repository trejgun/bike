import React, {Component} from "react";
import PropTypes from "prop-types";
import {FormattedMessage} from "react-intl";
import {enabledLanguages} from "../../constants/language";
import InputGroupValidation from "../inputs/input.group.validation";


export default class LanguageGroup extends Component {
  static propTypes = {
    defaultValue: PropTypes.string,
    onChange: PropTypes.func,
  };

  render() {
    const {defaultValue, onChange} = this.props;
    return (
      <InputGroupValidation
        name="language"
        componentClass="select"
        defaultValue={defaultValue}
        onChange={onChange}
      >
        {enabledLanguages.map(language =>
          (<FormattedMessage key={language} id={`components.language.${language}`}>
            {formattedMessage => <option key={language} value={language}>{formattedMessage}</option>}
          </FormattedMessage>),
        )}
      </InputGroupValidation>
    );
  }
}
