import InputColorGroup from "./input.color.group";
import withValidation from "./withValidation";


export default withValidation(InputColorGroup);
