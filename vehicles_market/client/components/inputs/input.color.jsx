import React, {Component} from "react";
import PropTypes from "prop-types";
import ColorDropdown from "./color-dropdown/index";
import {objectKeyByValue} from "./color-dropdown/utils";


export default class InputColor extends Component {
  static propTypes = {
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.bool,
    ]),
    defaultValue: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.bool,
    ]),
    name: PropTypes.string,
    colors: PropTypes.object,
    onChange: PropTypes.func,
  };

  onSelect(e) {
    this.props.onChange({
      persist: Function,
      target: {
        type: "text",
        name: this.props.name,
        value: Object.values(this.props.colors)[e],
      },
    });
  }

  render() {
    return (
      <ColorDropdown
        onSelect={::this.onSelect}
        value={objectKeyByValue(this.props.colors, this.props.value || this.props.defaultValue || this.props.colors.black)}
        colors={this.props.colors}
      />
    );
  }
}
