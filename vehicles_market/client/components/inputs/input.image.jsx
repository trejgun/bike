import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {Button, ButtonToolbar, Glyphicon} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import ImageViewer from "../imageViewer";
import YesNoDialog from "../partials/yesno";
import ImageEditor from "../partials/ImageEditor";
import S3Upload from "react-s3-uploader/s3upload";
import {MESSAGE_ADD} from "../../constants/actions";


@connect(
  state => ({
    user: state.user,
  }),
)
export default class InputImage extends Component {
  static propTypes = {
    user: PropTypes.object,
    dispatch: PropTypes.func,

    onChange: PropTypes.func,
    folder: PropTypes.string.isRequired,
    image: PropTypes.string,
    name: PropTypes.string,
  };

  state = {
    showEditor: false,
    file: null,
    progress: null,
  };

  onError(message) {
    this.s3 = null;
    this.props.dispatch({
      type: MESSAGE_ADD,
      data: {
        type: "danger",
        message,
      },
    });
  }

  onFinish(e) {
    // console.log("InputImage:onFinish", e);
    this.props.onChange({
      persist: Function,
      target: {
        type: "text",
        name: this.props.name,
        value: e.publicUrl,
      },
    });
    this.s3 = null;
    this.setState({
      showEditor: false,
      file: null,
      progress: null,
    });
  }

  onDelete() {
    this.props.onChange({
      persist: Function,
      target: {
        type: "text",
        name: this.props.name,
        value: "",
      },
    });
  }

  onEdit() {
    // console.log("InputImage:onEdit");
    this.setState({
      showEditor: true,
    });
  }

  onProgress(progress) {
    this.setState({progress});
  }

  onConfirm() {
    // prevents double clicking
    if (this.s3) {
      return;
    }

    // fast return is no file
    if (!this.state.file) {
      this.setState({
        showEditor: false,
      });
      return;
    }

    this.s3 = new S3Upload({
      files: [this.state.file],
      disposition: "inline",
      signingUrl: "/api/s3/sign",
      onFinishS3Put: ::this.onFinish,
      onError: ::this.onError,
      onProgress: ::this.onProgress,
      signingUrlQueryParams: {folder: `${this.props.user.organization._id}/${this.props.folder}`},
      uploadRequestHeaders: {"x-amz-acl": "public-read"},
      contentDisposition: "auto",
      server: process.env[`MODULE_${process.env.MODULE.toUpperCase()}`],
    });
  }

  onCancel() {
    if (this.s3) {
      this.s3.abortUpload();
      this.s3 = null;
    }
    this.setState({
      showEditor: false,
    });
  }

  onChange(file) {
    // console.log("InputImage:onChange", file);
    this.setState({
      file,
    });
  }

  render() {
    // console.log("InputImage:render", this.props, this.state);
    return (
      <div className="img-uploader">
        <YesNoDialog
          backdrop="static"
          show={this.state.showEditor}
          onConfirm={::this.onConfirm}
          onCancel={::this.onCancel}
          animation={false}
          className="image-editor"
        >
          <ImageEditor onChange={::this.onChange} progress={this.state.progress} />
        </YesNoDialog>
        <ButtonToolbar className="btn-toolbar-image">
          <Button className="btn-file" onClick={::this.onEdit}>
            <Glyphicon glyph="folder-open" />
          </Button>
          <Button disabled={!this.props.image} onClick={::this.onDelete}>
            <Glyphicon glyph="trash" />
          </Button>
        </ButtonToolbar>
        <ImageViewer
          image={this.props.image}
          placeholder={this.props.folder}
          title={<FormattedMessage id={`components.image.${this.props.folder}`} />}
        />
      </div>
    );
  }
}
