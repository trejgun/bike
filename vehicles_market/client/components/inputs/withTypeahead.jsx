import React, {Component} from "react";
import PropTypes from "prop-types";
import {Typeahead} from "react-bootstrap-typeahead";
import withTypeaheadProvider from "../forms/withTypeaheadProvider";


export default function withTypeahead(storeName, action) {
  @withTypeaheadProvider(storeName, action)
  class TypeaheadInput extends Component {
    static propTypes = {
      labelKey: PropTypes.func,
      setState: PropTypes.func,
    };

    state = {
      isSaved: true,
    };

    constructor(props) {
      super(props);
      this._typeahead = React.createRef();
    }

    onChange([e]) {
      if (e) {
        this.setState({
          isSaved: true,
        }, () => {
          this.props.setState({
            type: e.type,
            brand: e.brand,
            model: e.model,
          });
        });
      }
    }

    onBlur() {
      const {isSaved} = this.state;
      this.setState({
        isSaved: false,
      }, () => {
        setTimeout(() => {
          if (!isSaved) {
            this._typeahead.current.getInstance().clear();
            this.props.setState({
              type: "",
              brand: "",
              model: "",
            });
          }
        }, 0);
      });
    }

    render() {
      return (
        <Typeahead
          {...this.props}
          options={this.props[action].list}
          renderMenuItemChildren={option => this.props.labelKey(option)}
          onChange={::this.onChange}
          onBlur={::this.onBlur}
          ref={this._typeahead}
        />
      );
    }
  }

  return TypeaheadInput;
}
