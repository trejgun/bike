import React, {Component} from "react";
import {Col, Row, Thumbnail} from "react-bootstrap";
import {Link} from "react-router-dom";
import withStore from "../forms/withStore";
import PropTypes from "prop-types";

@withStore("user")
export default class Dashboard extends Component {
  static propTypes = {
    user: PropTypes.object,
    storeName: PropTypes.string,
  };

  static renderItem(obj) {
    return (
      <Col xs={4} md={4} key={obj}>
        <Link to={`/${obj}`}>
          <Thumbnail src={`${process.env.MODULE_CDN}/img/dashboard/${obj}.png`} alt="Image" />
        </Link>
      </Col>
    );
  }

  render() {
    return (
      <Row>
        {["vehicles", "bookings", "users", "company", "statistics"].map(obj => {
          if (this.props[this.props.storeName].permissions.includes(`vehicles_market:${obj}:read`)) {
            return Dashboard.renderItem(obj);
          } else {
            return null;
          }
        })}
      </Row>
    );
  }
}
