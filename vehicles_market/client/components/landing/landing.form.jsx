import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, ButtonToolbar, Col, Form, FormGroup} from "react-bootstrap";
import querystring from "querystring";
import {FormattedMessage} from "react-intl";
import {vehicleToText} from "../../utils/vehicle";
import InputDateTimePicker from "../inputs/input.datetimepicker";
import withTypeahead from "../inputs/withTypeahead";
import {readFromQueryString} from "../../utils/location";


const InputTypeahead = withTypeahead("vehicles", "typeahead");

export default class LandingForm extends Component {
  static propTypes = {
    location: PropTypes.shape({
      search: PropTypes.string.isRequired,
    }).isRequired,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };

  state = {
    brand: "",
    model: "",
    cc: "",
    range: readFromQueryString("range", this.props.location.search),
  };

  onSubmit(e) {
    e.preventDefault();
    this.props.history.push({
      pathname: "/vehicles",
      search: querystring.stringify(this.state),
    });
  }

  render() {
    // console.log("LandingForm:render", this.state);
    return (
      <Form horizontal action="/vehicles" onSubmit={::this.onSubmit} className="search">
        <FormGroup>
          <Col sm={12}>
            <InputTypeahead
              labelKey={vehicleToText}
              setState={::this.setState}
            />
          </Col>
        </FormGroup>
        <FormGroup controlId="formHorizontalVehicle">
          <Col sm={12}>
            <InputDateTimePicker
              name="range"
              range={this.state.range}
              setState={::this.setState}
            />
          </Col>
        </FormGroup>
        <ButtonToolbar>
          <Button type="submit" className="pull-right" disabled={!this.state.range}>
            <FormattedMessage id="components.buttons.search" />
          </Button>
        </ButtonToolbar>
      </Form>
    );
  }
}
