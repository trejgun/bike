import React, {Component} from "react";
import {Grid} from "react-bootstrap";
import LandingForm from "./landing.form";


export default class Landing extends Component {
  render() {
    return (
      <Grid>
        <style
          type="text/css"
          dangerouslySetInnerHTML={{
            __html: `body {
							background-position: center;
							background-image: url("${process.env.MODULE_CDN}/img/market_vehicles/wallpaper.jpg");
						}`,
          }}
        />
        <LandingForm {...this.props} />
      </Grid>
    );
  }
}
