import React, {Component} from "react";
import PropTypes from "prop-types";
import {Form, Media} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import withUpdateForm from "../forms/withUpdateForm";
import InputStaticGroup from "../inputs/input.static.group";
import VehicleTabs from "../vehicle/tabs";
import ImageViewer from "../imageViewer";


@withUpdateForm("policies", "type")
export default class PolicyDelivery extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    formData: PropTypes.shape({
      description: PropTypes.string,
      allowed: PropTypes.string,
    }),
  };

  render() {
    // console.log("PolicyDelivery:render", this.props);
    const {formData, onSubmit} = this.props;
    return (
      <Form horizontal onSubmit={onSubmit}>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.policies.island" />
          </h4>

          <VehicleTabs />
        </div>
        <Media>
          <Media.Left>
            <ImageViewer
              placeholder="taxnservice"
              title="Policy"
            />
          </Media.Left>
          <Media.Body>
            <InputStaticGroup
              name="allowed"
              value={formData.allowed}
            />
            <InputStaticGroup
              name="description"
              value={formData.description}
            />
          </Media.Body>
        </Media>
      </Form>
    );
  }
}
