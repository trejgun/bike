import React, {Component} from "react";
import PropTypes from "prop-types";
import {Form, Media} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import withReadForm from "../forms/withReadForm";
import InputStaticGroup from "../inputs/input.static.group";
import VehicleTabs from "../vehicle/tabs";
import ImageViewer from "../imageViewer";
// import {timeZones} from "../../../constants/date";


@withReadForm("policies", "type")
export default class PolicyReturn extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    formData: PropTypes.shape({
      description: PropTypes.string,
      checkOutTime: PropTypes.string,
      // timeZone: PropTypes.string,
    }),
  };

  render() {
    // console.log("VehicleReturnPolicy:render", this.props);
    const {formData, onSubmit} = this.props;
    return (
      <Form horizontal onSubmit={onSubmit}>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.policies.return" />
          </h4>

          <VehicleTabs />
        </div>
        <Media>
          <Media.Left>
            <ImageViewer
              placeholder="taxnservice"
              title="Policy"
            />
          </Media.Left>
          <Media.Body>
            <InputStaticGroup
              name="checkOutTime"
              value={formData.checkOutTime}
            />
            <InputStaticGroup
              name="description"
              value={formData.description}
            />
            {/* <InputStaticGroup
							name="timeZone"
							value={this.props.timeZone}
						/> */}
          </Media.Body>
        </Media>
      </Form>
    );
  }
}
