import React, {Component} from "react";
import PropTypes from "prop-types";
import {Route, Switch} from "react-router";
import withStore from "../forms/withStore";

import UserUpdateForm from "./update";
import UserReadForm from "./read";
import UserSubscriptionsForm from "./subscriptions";


@withStore("user")
export default class UserProfile extends Component {
  static propTypes = {
    storeName: PropTypes.string,
  };

  subRender(component) {
    return props => {
      Object.assign(props.match.params, {_id: this.props[this.props.storeName]._id});
      return React.createElement(component, props);
    };
  }

  render() {
    return (
      <Switch>
        <Route path="/profile" render={this.subRender(UserReadForm)} exact />
        <Route path="/profile/edit" render={this.subRender(UserUpdateForm)} />
        <Route path="/profile/subscriptions" render={this.subRender(UserSubscriptionsForm)} />
      </Switch>
    );
  }
}
