import React, {Component} from "react";
import PropTypes from "prop-types";
import {Form, Glyphicon, Media} from "react-bootstrap";
import {Link} from "react-router-dom";
import {FormattedMessage} from "react-intl";
import withReadForm from "../forms/withReadForm";
import ImageViewer from "../imageViewer";
import InputStaticGroup from "../inputs/input.static.group";
import UserTabs from "./tabs";


@withReadForm("users")
export default class UserReadForm extends Component {
  static propTypes = {
    formData: PropTypes.shape({
      image: PropTypes.string,
      license: PropTypes.string,
      fullName: PropTypes.string,
      phoneNumber: PropTypes.string,
      email: PropTypes.string,
      country: PropTypes.string,
      language: PropTypes.string,
    }),
  };

  render() {
    const {formData} = this.props;
    return (
      <Form horizontal>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.profile.read" />
            <Link to="/profile/edit">
              <Glyphicon glyph="edit" />
            </Link>
          </h4>
          <UserTabs />
        </div>

        <Media className="view">
          <Media.Left>
            <ImageViewer
              image={formData.image}
              placeholder="user"
              title={<span>{formData.fullName}</span>}
            />
            <ImageViewer
              image={formData.license}
              placeholder="license"
              title={<span>{formData.fullName}</span>}
            />
          </Media.Left>
          <Media.Body>
            <InputStaticGroup
              name="email"
              value={formData.email}
            />
            <InputStaticGroup
              name="password"
              value={"********"}
            />
            <InputStaticGroup
              name="fullName"
              value={formData.fullName}
            />
            <InputStaticGroup
              name="phoneNumber"
              value={formData.phoneNumber}
            />
            {formData.country ?
              <InputStaticGroup
                name="country"
                value={<FormattedMessage id={`countries.${formData.country}`} />}
              /> : null}
            <InputStaticGroup
              name="language"
              value={<FormattedMessage id={`components.language.${formData.language}`} />}
            />
          </Media.Body>
        </Media>
      </Form>
    );
  }
}
