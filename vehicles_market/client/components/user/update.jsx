import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, ButtonToolbar, Form, Media} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import withStore from "../forms/withStore";
import withUpdateForm from "../forms/withUpdateForm";
import {fullName, phoneNumber} from "../../constants/placeholder";
import InputImage from "../inputs/input.image";
import InputGroupValidation from "../inputs/input.group.validation";
import InputStaticGroup from "../inputs/input.static.group";
import open from "../../utils/popup";
import countries from "../../intl/countries.en.json";
import LanguageGroup from "../groups/language";


@withStore("user")
@withUpdateForm("users")
export default class UserUpdateForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
    formData: PropTypes.shape({
      _id: PropTypes.string,
      image: PropTypes.string,
      fullName: PropTypes.string,
      email: PropTypes.string,
      country: PropTypes.string,
      language: PropTypes.string,
      phoneNumber: PropTypes.string,
      license: PropTypes.string,
    }),

    paramName: PropTypes.string,
    storeName: PropTypes.string,
    user: PropTypes.object,
    location: PropTypes.shape({
      pathname: PropTypes.string.isRequired,
    }).isRequired,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
    match: PropTypes.shape({
      params: PropTypes.object.isRequired,
    }).isRequired,

    dispatch: PropTypes.func,
  };

  componentDidMount() {
    // console.log("UserUpdateForm:componentDidMount");
    window.addEventListener("message", ::this.onMessage, false);
  }

  componentDidUpdate() {
    // console.log("UserUpdateForm:componentWillReceiveProps", nextProps);
    const {history, location, storeName, paramName, dispatch, match, user} = this.props;
    if (!user) {
      history.push("/login");
    }

    if (!this.props[storeName].isLoading && this.props[storeName].success && this.props[storeName].name === "update") {
      const parts = location.pathname.split("/");
      if (["profile"].includes(parts[1])) {
        dispatch({
          type: "FETCH_REQUESTED",
          method: "GET",
          action: "/sync",
          storeName: "user",
          name: "sync",
        });
        history.push(`/${parts[1]}`);
      } else {
        history.push(`/${storeName}/${match.params[paramName]}`);
      }
    }
  }

  componentWillUnmount() {
    // console.log("UserUpdateForm:componentWillUnmount");
    window.removeEventListener("message", ::this.onMessage);
  }

  onMessage(event) {
    // console.log("UserUpdateForm:onMessage", event);
    if (event.data.source === "oauth2") {
      this.props.dispatch({
        type: "FETCH_REQUESTED",
        method: "GET",
        action: "/sync",
        storeName: "user",
        name: "sync",
      });
    }
  }

  render() {
    const {formData, onSubmit, onChange, user} = this.props;
    const type = user._id === formData._id ? "profile" : "user";
    return (
      <Form horizontal onSubmit={onSubmit}>
        <div className="headline">
          <h4>
            <FormattedMessage id={`components.title.${type}.update`} />
          </h4>
        </div>
        <Media>
          <Media.Left>
            <InputImage
              folder="user"
              name="image"
              image={formData.image}
              onChange={onChange}
            />
            <InputImage
              folder="license"
              name="license"
              image={formData.license}
              onChange={onChange}
            />
          </Media.Left>
          <Media.Body>
            <InputStaticGroup
              name="email"
              value={<a href="#"
                        onClick={open(`${process.env.MODULE_OAUTH2}/email?module=${process.env.MODULE}&alert=change-email`)}>{formData.email}</a>}
            />
            <InputStaticGroup
              name="password"
              value={<a href="#"
                        onClick={open(`${process.env.MODULE_OAUTH2}/password?module=${process.env.MODULE}`)}>{"********"}</a>}
            />
            <InputGroupValidation
              name="fullName"
              defaultValue={formData.fullName}
              placeholder={fullName}
              onChange={onChange}
              required
            />
            <InputGroupValidation
              name="phoneNumber"
              defaultValue={formData.phoneNumber}
              placeholder={phoneNumber}
              onChange={onChange}
            />
            <InputGroupValidation
              name="country"
              componentClass="select"
              defaultValue={formData.country}
              onChange={onChange}
            >
              <option value="">other</option>
              {Object.keys(countries).map(key =>
                (<FormattedMessage key={key} id={`countries.${key}`}>
                  {formattedMessage => <option key={key} value={key}>{formattedMessage}</option>}
                </FormattedMessage>),
              )}
            </InputGroupValidation>
            <LanguageGroup
              defaultValue={formData.language}
              onChange={onChange}
            />
          </Media.Body>
        </Media>
        <ButtonToolbar>
          <Button type="submit" className="pull-right">
            <FormattedMessage id="components.buttons.save" />
          </Button>
        </ButtonToolbar>
      </Form>
    );
  }
}
