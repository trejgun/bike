import React, {Component} from "react";
import {Route, Switch} from "react-router";
import VehicleList from "./list";
import VehicleRead from "./read";


export default class Vehicles extends Component {
	render() {
		return (
			<Switch>
				<Route path="/vehicles" component={VehicleList} exact />
				<Route path="/vehicles/:_id" component={VehicleRead} />
			</Switch>
		);
	}
}
