import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, Col, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import withTypeaheadForm from "../forms/withTypeaheadForm";
import withListForm from "../forms/withListForm";
import {uniq} from "../../utils/vehicle";
import {getRangeByDate} from "../../utils/date";
import {readFromQueryString} from "../../utils/location";
import {date} from "../../constants/date";
import LimitGroup from "../groups/limit";
import moment from "moment";


@withListForm("vehicles")
@withTypeaheadForm("vehicles", "typeahead")
export default class VehicleListForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
    setState: PropTypes.func,
    formData: PropTypes.shape({
      type: PropTypes.string,
      brand: PropTypes.string,
      model: PropTypes.string,
      cc: PropTypes.string,
      skip: PropTypes.number,
      limit: PropTypes.number,
      view: PropTypes.string,
    }),

    typeahead: PropTypes.object,
    location: PropTypes.shape({
      search: PropTypes.string.isRequired,
    }).isRequired,
    children: PropTypes.node,
  };

  static defaultProps = {
    range: moment.range().toString(), // TODO TZ
    type: "bike",
  };

  componentDidMount() {
    // console.log("VehicleListForm:componentDidMount", this.props.location.search);
    const {setState, onSubmit, location} = this.props;
    setState({
      range: readFromQueryString("range", location.search) || getRangeByDate(date, "d").toString() || VehicleListForm.defaultProps.range,
      type: readFromQueryString("type", location.search) || VehicleListForm.defaultProps.type,
      brand: readFromQueryString("brand", location.search),
      model: readFromQueryString("model", location.search),
      cc: readFromQueryString("cc", location.search),
    }, onSubmit);
  }

  onChangeType(e) {
    this.props.setState({
      cc: "",
      model: "",
      brand: "",
    }, () => {
      this.props.onChange(e);
    });
  }

  onChangeBrand(e) {
    this.props.setState({
      cc: "",
      model: "",
    }, () => {
      this.props.onChange(e);
    });
  }

  onChangeModel(e) {
    this.props.setState({
      cc: "",
    }, () => {
      this.props.onChange(e);
    });
  }

  onChangeCC(e) {
    this.props.setState({}, () => {
      this.props.onChange(e);
    });
  }

  render() {
    // console.log("VehicleListForm:render", this.props);
    const {formData, onSubmit, onChange, typeahead} = this.props;

    if (!formData.type) {
      return null;
    }

    return (
      <Form horizontal onSubmit={onSubmit}>
        <Col xs={12}>
          <FormGroup>
            <Col componentClass={ControlLabel}>
              <FormattedMessage id="fields.type.label" />
            </Col>
            <FormControl
              componentClass="select"
              name="type"
              defaultValue={formData.type}
              onChange={::this.onChangeType}
            >
              <option value="">Select...</option>
              {typeahead.list
                .map(v => v.type)
                .filter(uniq)
                .map(type =>
                  (<FormattedMessage key={type} id={`components.vehicle.type.${type}`}>
                    {formattedMessage => <option key={type} value={type}>{formattedMessage}</option>}
                  </FormattedMessage>),
                )}
            </FormControl>
          </FormGroup>
          {formData.type ?
            <FormGroup>
              <Col componentClass={ControlLabel}>
                <FormattedMessage id="fields.brand.label" />
              </Col>
              <FormControl
                componentClass="select"
                name="brand"
                defaultValue={formData.brand}
                onChange={::this.onChangeBrand}
              >
                <option value="">Select...</option>
                {this.props.typeahead.list
                  .filter(v => v.type === formData.type)
                  .map(v => v.brand)
                  .filter(uniq)
                  .map(brand => <option value={brand} key={brand}>{brand}</option>)}
              </FormControl>
            </FormGroup> : null}
          {formData.brand ?
            <FormGroup>
              <Col componentClass={ControlLabel}>
                <FormattedMessage id="fields.model.label" />
              </Col>
              <FormControl
                componentClass="select"
                name="model"
                defaultValue={formData.model}
                onChange={::this.onChangeModel}
              >
                <option value="">Select...</option>
                {this.props.typeahead.list
                  .filter(v => v.type === formData.type)
                  .filter(v => v.brand === formData.brand)
                  .map(v => v.model)
                  .filter(uniq)
                  .map(model => <option value={model} key={model}>{model}</option>)}
              </FormControl>
            </FormGroup> : null}
          {formData.model ?
            <FormGroup>
              <Col componentClass={ControlLabel}>
                <FormattedMessage id="fields.cc.label" />
              </Col>
              <FormControl
                componentClass="select"
                name="cc"
                defaultValue={formData.cc}
                onChange={::this.onChangeCC}
              >
                {this.props.typeahead.list
                  .filter(v => v.type === formData.type)
                  .filter(v => v.brand === formData.brand)
                  .filter(v => v.model === formData.model)
                  .map(v => v.cc)
                  .filter(uniq)
                  .map(cc => <option value={cc} key={cc}>{cc}</option>)}
              </FormControl>
            </FormGroup> : null}
          <LimitGroup
            defaultValue={formData.limit}
            onChange={onChange}
          />
          <FormGroup>
            <Button type="submit">
              <FormattedMessage id="components.buttons.search" />
            </Button>
          </FormGroup>
        </Col>
      </Form>
    );
  }
}
