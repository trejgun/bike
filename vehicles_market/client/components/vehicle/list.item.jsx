import React, {Component} from "react";
import PropTypes from "prop-types";
import moment from "moment";
import querystring from "querystring";
import {FormattedMessage} from "react-intl";
import {withRouter} from "react-router";
import {Form, Image, ListGroupItem, Media} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import {listItemHeight, listItemWidth} from "../../constants/display";
import withListItemHelper from "../forms/withListItem";
import {vehicleToText} from "../../utils/vehicle";
import {calcPrice} from "../../utils/date";
import InputStaticGroup from "../inputs/input.static.group";
import {readFromQueryString} from "../../utils/location";


@withListItemHelper("vehicles")
@withRouter
export default class VehicleListItem extends Component {
  static propTypes = {
    formData: PropTypes.shape({
      image: PropTypes.string,
      _id: PropTypes.string,
      year: PropTypes.number,
      type: PropTypes.string,
    }),

    location: PropTypes.shape({
      search: PropTypes.string.isRequired,
    }).isRequired,
  };

  render() {
    // console.log("VehicleListItem:render", this.props);
    const {formData, location} = this.props;
    const range = readFromQueryString("range", location.search);
    return (
      <LinkContainer
        to={{
          pathname: "/bookings/create",
          search: querystring.stringify({range, vehicle: formData._id}),
        }}
        key={formData._id}
      >
        <ListGroupItem>
          <Media>
            <Media.Left className="hidden-xs">
              <Image
                width={listItemWidth}
                height={listItemHeight}
                src={formData.image || `${process.env.MODULE_CDN}/img/icons/vehicle.png`}
                className="media-object img-thumbnail"
                alt="Image"
              />
            </Media.Left>
            <Media.Body>
              <Media.Heading>{vehicleToText(formData)}</Media.Heading>
              <Form horizontal>
                <InputStaticGroup
                  name="price"
                  value={calcPrice(formData, moment.range(range))}
                />
                <InputStaticGroup
                  name="year"
                  value={formData.year}
                />
                <InputStaticGroup
                  name="license"
                  value={<FormattedMessage id={`components.license.${formData.type}`} />}
                />
              </Form>
            </Media.Body>
          </Media>
        </ListGroupItem>
      </LinkContainer>
    );
  }
}
