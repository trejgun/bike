import React, {Component} from "react";
import PropTypes from "prop-types";
import {Col, Row} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import Form from "./list.form";
import ListItem from "./list.item";
import TableItem from "./table.item";
import Pagination from "../groups/pagination";
import withListHelper from "../forms/withList";
import ViewGroup from "../groups/view";


@withListHelper("vehicles")
export default class VehicleList extends Component {
  static propTypes = {
    view: PropTypes.string,
  };

  render() {
    // console.log("VehicleList:render", this.props);
    return (
      <div>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.vehicle.list" />
          </h4>
        </div>

        <Row>
          <Col sm={3}>
            <ViewGroup{...this.props} />
            <Form {...this.props} />
          </Col>
          <Col sm={9}>
            {this.props.view === "list" ? <ListItem {...this.props} /> : <TableItem {...this.props} />}
            <Pagination {...this.props} />
          </Col>
        </Row>
      </div>
    );
  }
}
