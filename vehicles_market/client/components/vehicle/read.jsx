import React, {Component} from "react";
import PropTypes from "prop-types";
import {Form, Media} from "react-bootstrap";
import FacebookProvider, {Comments} from "react-facebook";
import {FormattedMessage} from "react-intl";
import {priceToHtml, vehicleToHtml} from "../../utils/vehicle";
import withReadForm from "../forms/withReadForm";
import InputStaticGroup from "../inputs/input.static.group";
import ImageViewer from "../imageViewer";
import VehicleTabs from "./tabs";


@withReadForm("vehicles")
export default class VehicleReadForm extends Component {
  static propTypes = {
    formData: PropTypes.shape({
      _id: PropTypes.string,
      image: PropTypes.string,
      type: PropTypes.string,
      brand: PropTypes.string,
      model: PropTypes.string,
      cc: PropTypes.number,
      year: PropTypes.number,
    }),

    location: PropTypes.shape({
      pathname: PropTypes.string.isRequired,
    }).isRequired,
  };

  render() {
    // console.log("VehicleReadForm:render", this.props);
    const {formData, location} = this.props;
    return (
      <Form horizontal>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.vehicle.read" />
          </h4>

          <VehicleTabs _id={formData._id} />
        </div>
        <Media className="view">
          <Media.Left>
            <ImageViewer
              image={formData.image}
              placeholder="vehicle"
              title={vehicleToHtml(formData, true)}
            />
          </Media.Left>
          <Media.Body>
            <InputStaticGroup
              name="brand"
              value={formData.brand}
            />
            <InputStaticGroup
              name="model"
              value={formData.model}
            />
            <InputStaticGroup
              name="cc"
              value={formData.cc}
            />
            <InputStaticGroup
              name="year"
              value={formData.year}
            />
            <InputStaticGroup
              name="price"
              value={priceToHtml(formData)}
            />
            <InputStaticGroup
              name="license"
              value={<FormattedMessage id={`components.license.${formData.type}`} />}
            />
          </Media.Body>
        </Media>
        <FacebookProvider messengerAppId="1157215174361176">
          <Comments href={`${process.env.MODULE_VEHICLES_MARKET}${location.pathname}`} width="100%" />
        </FacebookProvider>
      </Form>
    );
  }
}
