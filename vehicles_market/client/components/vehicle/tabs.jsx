import React, {Component} from "react";
import PropTypes from "prop-types";
import {MenuItem, Nav, NavDropdown, NavItem} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import {LinkContainer} from "react-router-bootstrap";


export default class VehicleTabs extends Component {
	static propTypes = {
    _id: PropTypes.string,
	};

	render() {
		const {_id} = this.props;

		return (
			<Nav bsStyle="tabs" id="uncontrolled-tab-example" className="nav-rtl">
				<LinkContainer to={`/vehicles/${_id}`} exact>
					<NavItem eventKey="1">
						<FormattedMessage id="components.tabs.vehicle.details" />
					</NavItem>
				</LinkContainer>
				<NavDropdown
					eventKey="3"
					title={<FormattedMessage id="components.tabs.vehicle.policies" />}
					id="nav-dropdown-within-tab"
				>
					<LinkContainer to={`/vehicles/${_id}/policies/return`}>
						<MenuItem eventKey="3.1">
							<FormattedMessage id="components.tabs.vehicle.return" />
						</MenuItem>
					</LinkContainer>
					<LinkContainer to={`/vehicles/${_id}/policies/island`}>
						<MenuItem eventKey="3.2">
							<FormattedMessage id="components.tabs.vehicle.island" />
						</MenuItem>
					</LinkContainer>
					<LinkContainer to={`/vehicles/${_id}/policies/delivery`}>
						<MenuItem eventKey="3.3">
							<FormattedMessage id="components.tabs.vehicle.delivery" />
						</MenuItem>
					</LinkContainer>
				</NavDropdown>
			</Nav>
		);
	}
}
