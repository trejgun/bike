import {combineReducers} from "redux";
import {routerReducer as routing} from "react-router-redux";
import {intlReducer as intl} from "react-intl-redux";
import bookings from "./bookings";
import messages from "./messages";
import policies from "./policies";
import subscription from "./subscription";
import typeahead from "./typeahead";
import user from "./user";
import users from "./users";
import validations from "./validations";
import vehicles from "./vehicles";


export default combineReducers({
  bookings,
  intl,
  messages,
  policies,
  routing,
  subscription,
  typeahead,
  validations,
  vehicles,
  user,
  users,
});
