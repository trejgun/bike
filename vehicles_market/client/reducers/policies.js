const policies = {
	list: [],
	count: 0,
	isLoading: true,
	success: false,
	name: "view",
};

export default function policiesReducer(state = policies, action) {
	switch (action.type) {
		case "policies_view_start":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				list: [],
				count: 0,
				name: action.name,
			});
		case "policies_view_success":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				list: [action.data],
				count: 1,
				name: action.name,
			});
		case "policies_view_error":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				list: [action.data],
				count: 1,
				name: action.name,
			});
		case "policies_update_start":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name,
			});
		case "policies_update_success":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				list: [action.data],
				count: 1,
				name: action.name,
			});
		case "policies_update_error":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name,
			});
		default:
			return state;
	}
}
