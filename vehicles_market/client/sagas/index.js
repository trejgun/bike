import {all} from "redux-saga/effects";
import watchFetch from "./fetch";


export default function* rootSaga() {
  yield all([
    watchFetch(),
  ]);
}
