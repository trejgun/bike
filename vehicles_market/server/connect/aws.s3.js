import AWS from "aws-sdk";
import bluebird from "bluebird";
import {getRandomString} from "../utils/misc";


export default new class S3 {
  constructor() {
    AWS.config.setPromisesDependency(bluebird.Promise);
  }

  get client() {
    return new AWS.S3();
  }

  getSignedUrl(request, response) {
    const params = {
      Bucket: process.env.AWS_S3_BUCKET,
      Key: request.query.pathname,
    };

    return response.redirect(this.client.getSignedUrl("getObject", params));
  }

  putSignedUrl(request, response) {
    const pathname = `${request.query.folder}/${getRandomString(20)}${request.query.objectName}`;
    const params = {
      Bucket: process.env.AWS_S3_BUCKET,
      Key: pathname,
      ContentType: request.query.contentType,
      ACL: "private",
      Expires: 60,
    };

    return response.json({
      signedUrl: this.client.getSignedUrl("putObject", params),
      publicUrl: `/api/s3/uploads?pathname=${pathname}`,
    });
  }
}();
