import bluebird from "bluebird";
import {pick} from "lodash";
import moment from "moment";
import {makeError} from "../../utils/error";
import StatefulController from "../abstract/stateful";
import VehicleController from "./vehicle";
import MailController from "./mail";
import {checkActive, checkModel} from "../abstract/middleware";
import {paginate} from "../../utils/response";
import {getConnections} from "../../utils/mongoose";
import {calcPrice} from "../../../client/utils/date";
import {setStatus} from "../../utils/request";


function checkIfEditable(item) {
  if (item.status === this.constructor.statuses.accepted) {
    throw makeError("booking-not-editable", 400);
  }
}

export default class BookingController extends StatefulController {
  static realm = "vehicles_office";

  static statuses = {
    new: "new",

    accepted: "accepted",
    rejected: "rejected",
    cancelled: "cancelled",
    revoked: "revoked",
  };

  // TODO send email to customer and all managers
  static sendEmailNotification(template, bookings) {
    const mailController = new MailController();
    return bluebird.all(bookings.map(booking =>
      mailController.compose(template, {
        // TODO check if user present
        to: booking.organizations[0].users[0].email,
        bcc: "trejgun+purimotors@gmail.com",
      }, booking.organizations[1] ? booking.organizations[1].users[0] : {}, {
        bookings: {list: [booking]},
      }),
    ));
  }

  getById(request) {
    return super.getByUId(request, {
      populate: ["vehicle"],
    });
  }

  edit(request) {
    const range = moment.range(request.body.range);

    // 1. check booking
    return this.getByUId(request, {lean: false})
      .tap(this.conditions(request, [checkActive([BookingController.statuses.accepted, BookingController.statuses.new]), checkIfEditable]))
      .then(booking => {
        const _id = request.body.vehicle ? (request.body.vehicle._id ? request.body.vehicle._id : request.body.vehicle) : booking.vehicle;

        // 2. check vehicle
        const vehicleController = new VehicleController();
        return vehicleController.findById(_id)
          .tap(vehicleController.conditions(request, [checkModel(), checkActive()]))
          .then(vehicle =>

            // 3. check if there is another `accepted` booking at this time
            this.checkActive(vehicle, range, [booking])
              .then(() => {
                const clean = pick(request.body, ["email", "fullName", "phoneNumber", "notes"]);
                Object.assign(booking, clean, {
                  price: calcPrice(vehicle, range),
                  country: request.user.country,
                  range,
                  vehicle,
                });

                // 4. update
                return this.save(booking)
                  .then(booking => Object.assign(booking.toJSON(), {vehicle}));
              }),
          );
      });
  }

  insert(request) {
    const range = moment.range(request.body.range);
    const connections = getConnections();

    const vehicleController = new VehicleController();
    return vehicleController.findOne({_id: request.body.vehicle}, {
      populate: [{
        path: "organizations",
        model: connections.oauth2.model("Organization"),
        populate: [{
          path: "users",
          model: connections.oauth2.model("User"),
        }],
      }],
    })
      .tap(vehicleController.conditions(request, [checkModel(), checkActive()]))
      .then(vehicle =>
        this.checkActive(vehicle, moment.range(request.body.range))
          .then(() => {
            const clean = pick(request.body, ["address", "delivery", "notes", "phoneNumber"]);
            Object.assign(clean, {
              vehicle,
              range,
              country: request.user.country,
              price: calcPrice(vehicle, range),
              email: request.user.email,
              fullName: request.user.fullName,
              organizations: [vehicle.organizations[0]._id, request.user.organization._id],
            });
            return this.create(clean)
              .then(booking => Object.assign(booking.toJSON(), {vehicle}))
              .tap(booking => {
                const mailController = new MailController();
                return bluebird.all([
                  mailController.compose("newBookingForCustomer", {
                    to: request.user.email,
                  }, request.user.toObject(), {bookings: {list: [booking]}}),
                  ...vehicle.organizations[0].users.map(user =>
                    mailController.compose("newBookingForRenter", {
                      to: user.email,
                    }, user, {bookings: {list: [booking]}}),
                  ),
                ].map(promise => promise.reflect()));
              });
          }),
      );
  }

  list(request, response) {
    const clean = {
      "organizations.1": request.user.organization._id,
    };
    setStatus(request, clean, BookingController);
    return paginate(request, response, this, clean, {
      populate: ["vehicle"],
      // sort: {createdAt: -1} // test fails because of this
    });
  }

  deactivate(request, options = {}, conditions = []) {
    const connections = getConnections();
    return super.getByUId(request, {
      lean: false,
      populate: [{
        path: "organizations",
        model: connections.oauth2.model("Organization"),
        populate: [{
          path: "users",
          model: connections.oauth2.model("User"),
        }],
      }],
    })
      .tap(this.conditions(request, [checkActive([BookingController.statuses.new, BookingController.statuses.accepted])].concat(conditions)))
      .then(booking => {
        const newStatus = booking.status === BookingController.statuses.new ? BookingController.statuses.revoked : BookingController.statuses.cancelled;
        const emailTemplate = booking.status === BookingController.statuses.new ? "bookingRevokedByCustomer" : "bookingCancelledByCustomer";
        booking.set("status", newStatus);
        return this.save(booking)
          .tap(() =>
            BookingController.sendEmailNotification(emailTemplate, [booking]),
          );
      });
  }

  checkActive(vehicle, range, bookings = []) {
    const clean = {
      vehicle,
      status: BookingController.statuses.accepted,
      $and: [
        {start: {$lte: range.end}},
        {end: {$gte: range.start}},
      ],
    };

    if (bookings.length > 1) {
      Object.assign(clean, {
        _id: {
          $nin: bookings.map(booking => booking._id),
        },
      });
    } else if (bookings.length === 1) {
      Object.assign(clean, {
        _id: {
          $ne: bookings[0]._id,
        },
      });
    }

    return this.findOne(clean)
      .then(acceptedBookings => {
        if (acceptedBookings) {
          throw makeError("conflict", 409, {name: "range", reason: "not-available"});
        }
      });
  }
}
