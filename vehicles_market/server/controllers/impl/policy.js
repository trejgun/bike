import {checkModel} from "../abstract/middleware";
import {systemId} from "../../constants/misc";
import AbstractController from "../abstract/abstract";
import VehicleController from "./vehicle";


export default class PolicyController extends AbstractController {
  static realm = "vehicles_office";

  static types = {
    return: "return",
  };

  getByVehicle(request) {
    const vehicleController = new VehicleController();
    return vehicleController.findById(request.params._id)
      .then(vehicle =>
        this.findOne({
          type: request.params.type,
          // language: request.user.language,
          $or: [{
            organizations: vehicle.organizations[0],
          }, {
            organizations: systemId,
          }],
        }, {sort: {organizations: 1}})
          .tap(this.conditions(request, [checkModel()])),
      );
  }
}
