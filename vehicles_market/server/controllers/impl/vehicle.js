import bluebird from "bluebird";
import {map, pick} from "lodash";
import {checkActive, checkModel} from "../abstract/middleware";
import {paginate} from "../../utils/response";
import {getConnections} from "../../utils/mongoose";
import moment from "moment";
import {makeError} from "../../utils/error";

import StatefulController from "../abstract/stateful";
import BookingController from "./booking";
import OrganizationController from "./organization";
import CatalogController from "./catalog";
import SearchController from "./search";


export default class VehicleController extends StatefulController {
  static realm = "vehicles_office";

  static statuses = {
    active: "active",
    inactive: "inactive",
    new: "new",
  };

  static types = {
    scooter: "scooter",
    bike: "bike",
    car: "car",
    bus: "bus",
    truck: "truck",
  };

  getById(request) {
    return this.findOne({
      [this.constructor.param]: request.params[this.constructor.param],
    }, {
      populate: [{
        path: "organizations",
        model: getConnections().oauth2.model("Organization"),
      }],
    })
      .tap(this.conditions(request, [checkModel(), checkActive()]));
  }

  typeahead() {
    const vehicleController = new VehicleController();
    return vehicleController.aggregate([{
      $match: {status: VehicleController.statuses.active},
    }, {
      $group: {_id: {type: "$type", brand: "$brand", model: "$model", cc: "$cc"}},
    }])
      .then(list => ({list: map(list, "_id")}));
  }

  list(request, response) {
    const range = moment.range(request.query.range);
    const clean = pick(request.query, ["type", "brand", "model", "fuel", "cc"]);

    const catalogController = new CatalogController();
    return catalogController.check(request.query, false)
      .then(count => {
        if (!count) {
          throw makeError("invalid-param", 400, {reason: "invalid-param-combination"});
        }

        const searchController = new SearchController();
        return searchController.create({
          user: request.user,
          start: range.start,
          end: range.end,
          range,
          ...clean,
        })
          .then(() => {
            const bookingController = new BookingController();
            const organizationController = new OrganizationController();
            return bluebird.all([
              bookingController.distinct("vehicle", {
                status: BookingController.statuses.accepted,
                $and: [
                  {start: {$lte: range.end}},
                  {end: {$gte: range.start}},
                ],
              }),
              organizationController.distinct("_id", {
                status: {
                  $in: [
                    OrganizationController.statuses.suspended,
                    OrganizationController.statuses.inactive,
                  ],
                },
              }),
            ])
              .spread((vehicles, organizations) => {
                Object.assign(clean, {
                  status: VehicleController.statuses.active,
                  _id: {$nin: vehicles},
                  organizations: {$nin: organizations},
                });
                return paginate(request, response, this, clean);
              });
          });
      });
  }
}
