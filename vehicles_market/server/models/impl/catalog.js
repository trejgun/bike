import {Schema} from "mongoose";

import CatalogController from "../../controllers/impl/catalog";

const types = Object.keys(CatalogController.types);
const fuels = Object.keys(CatalogController.fuels);

const Catalog = new Schema({
  model: String,
  type: {
    type: String,
    enum: {
      values: types,
      message: "invalid",
    },
  },
  brand: String,
  variant: {
    type: String,
  },
  cc: Number,
  fuel: {
    type: String,
    enum: {
      values: fuels,
      message: "invalid",
    },
  },
});

export default Catalog;
