import {Schema} from "mongoose";
import status from "../plugins/status";
import {arrayGetter} from "../../utils/mongoose";
import MailController from "../../controllers/impl/mail";


const Mail = new Schema({
  to: {
    type: [String],
    get: arrayGetter,
  },
  cc: {
    type: [String],
    get: arrayGetter,
  },
  bcc: {
    type: [String],
    get: arrayGetter,
  },
  type: String, // for tests
  subject: String,
  html: String,
});

Mail.plugin(status, {controller: MailController});

export default Mail;
