import {Schema} from "mongoose";
import status from "../plugins/status";
import type from "../plugins/type";

import VehicleController from "../../controllers/impl/vehicle";


const Search = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: "User",
  },

  brand: String,
  model: String,
  fuel: String,
  cc: Number,

  start: Date,
  end: Date,
  range: String,

  isQuick: {
    type: Boolean,
    default: false,
  },
});

Search.plugin(status);
Search.plugin(type, {controller: VehicleController});

export default Search;
