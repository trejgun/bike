import {Schema} from "mongoose";
import version from "@streethub/mongoose-version";

import status from "../plugins/status";
import type from "../plugins/type";
import {date} from "../../constants/date";
import {reHEXColor} from "../../constants/regexp";

import VehicleController from "../../controllers/impl/vehicle";
import CatalogController from "../../controllers/impl/catalog";


const fuels = Object.keys(CatalogController.fuels);

const Vehicle = new Schema({
  plate: {
    type: String,
    uppercase: true,
    trim: true,
  },
  year: {
    type: Number,
    required: "required",
    min: [2000, "min"],
    max: [date.getFullYear(), "max"],
  },
  brand: {
    type: String,
    required: "required",
  },
  model: {
    type: String,
    required: "required",
  },
  cc: {
    type: Number,
    required: "required",
    min: [50, "min"],
    max: [5000, "max"],
  },
  color: {
    type: String,
    match: [reHEXColor, "invalid"],
  },
  variant: {
    type: String,
  },
  fuel: {
    type: String,
    enum: {
      values: fuels,
      message: "invalid",
    },
    default: fuels[0],
  },

  image: {
    type: String,
  },
  taxnservice: {
    type: String,
  },
  bluebook: {
    type: String,
  },
  notes: {
    type: String,
  },

  pricePerHour: {
    type: Number,
    min: [0, "min"],
  },
  pricePerDay: {
    type: Number,
    min: [0, "min"],
  },
  pricePerMonth: {
    type: Number,
    min: [0, "min"],
  },
});

Vehicle.plugin(status, {controller: VehicleController});
Vehicle.plugin(type, {controller: CatalogController});

Vehicle.plugin(version, {
  collection: "VehicleVersions",
  suppressVersionIncrement: false,
});

Vehicle.virtual("bookings", {
  ref: "Booking",
  localField: "_id",
  foreignField: "vehicle",
});

Vehicle.pre("validate", function preValidateStatus(next) {
  // trying to activate, order does matter
  this.status = this.status === VehicleController.statuses.inactive ? VehicleController.statuses.inactive : VehicleController.statuses.active;
  next();
});

Vehicle.pre("validate", function preValidatePrice(next) {
  // TODO throw 409 instead of casting
  // trying to cast `integer`
  this.pricePerHour = this.pricePerHour ? this.pricePerHour : 0;
  this.pricePerDay = this.pricePerDay ? this.pricePerDay : 0;
  this.pricePerMonth = this.pricePerMonth ? this.pricePerMonth : 0;
  next();
});

Vehicle.pre("validate", function preValidateStatus(next) {
  if (!this.pricePerHour && !this.pricePerDay && !this.pricePerMonth) {
    this.status = VehicleController.statuses.new;
  }
  next();
});

Vehicle.pre("validate", function preValidateStatus(next) {
  if (!this.plate) {
    this.plate = void 0;
    this.status = VehicleController.statuses.new;
  }
  next();
});


Vehicle.index({
  plate: 1,
}, {
  unique: true,
  name: "plate-duplicate",
  partialFilterExpression: {
    // "unsupported expression in partial index: status $in [ \"active\" \"new\" ]\n",
    status: VehicleController.statuses.active,
  },
});

export default Vehicle;
