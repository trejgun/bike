import {Router} from "express";
import crud from "../../utils/crud";
import BookingController from "../../controllers/impl/booking";
import {params, validateParams} from "../../utils/middleware";


const object = "bookings";
const router = Router(); // eslint-disable-line new-cap
const bookingController = new BookingController();

// additional validation
router.route(`/${object}`)
  .post(validateParams([params.range], "body"));

router.route(`/${object}/:${BookingController.param}`)
  .put(validateParams([params.range], "body"));

crud(router, bookingController, {object});

export default router;
