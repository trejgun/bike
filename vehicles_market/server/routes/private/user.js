import {Router} from "express";
import {wrapJSON} from "../../utils/wrapper";
import {methodNotAllowed} from "../../utils/middleware";
import UserController from "../../controllers/impl/user";
import {mongoId} from "../../utils/validator";


const object = "users";
const router = Router(); // eslint-disable-line new-cap
const userController = new UserController();

router.param("_id", mongoId); // mongo id

router.route(`/${object}/:_id`)
  .get(wrapJSON(::userController.getById))
  .put(wrapJSON(::userController.edit))
  .all(methodNotAllowed);

export default router;
