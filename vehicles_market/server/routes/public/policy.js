import {Router} from "express";
import {wrapJSON} from "../../utils/wrapper";
import {methodNotAllowed} from "../../utils/middleware";
import PolicyController from "../../controllers/impl/policy";
import {mongoId} from "../../utils/validator";


const router = Router(); // eslint-disable-line new-cap
const policyController = new PolicyController();

router.param("_id", mongoId); // mongo id

router.route("/vehicles/:_id/policies/:type")
  .get(wrapJSON(::policyController.getByVehicle))
  .all(methodNotAllowed);

export default router;
