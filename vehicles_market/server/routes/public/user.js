import {Router} from "express";
import {wrapJSON} from "../../utils/wrapper";
import {methodNotAllowed} from "../../utils/middleware";
import UserController from "../../controllers/impl/user";

const router = Router(); // eslint-disable-line new-cap
const userController = new UserController();

router.route("/sync")
  .get(wrapJSON(::userController.sync))
  .all(methodNotAllowed);

export default router;
