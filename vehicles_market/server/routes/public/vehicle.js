import {Router} from "express";
import {wrapJSON} from "../../utils/wrapper";
import {methodNotAllowed, params, validateParams} from "../../utils/middleware";
import VehicleController from "../../controllers/impl/vehicle";
import {mongoId} from "../../utils/validator";


const router = Router(); // eslint-disable-line new-cap
const vehicleController = new VehicleController();

router.param("_id", mongoId); // mongo id

router.route("/vehicles/typeahead")
  .get(wrapJSON(::vehicleController.typeahead))
  .all(methodNotAllowed);

router.route("/vehicles")
  .get(validateParams([params.range], "query"), wrapJSON(::vehicleController.list))
  .all(methodNotAllowed);

router.route("/vehicles/:_id")
  .get(wrapJSON(::vehicleController.getById))
  .all(methodNotAllowed);

export default router;
