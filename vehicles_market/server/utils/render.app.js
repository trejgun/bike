import HTML from "../../client/components/HTML";
import getLocalization from "../../client/intl/setup";
import {defaultLanguage, enabledLanguages} from "../constants/language";
import {renderHTML, renderInitialMarkup} from "./render";
import {SheetsRegistry} from "jss";
import App from "../../client/app";
import configureStore from "../../client/store";

const sheetsRegistry = new SheetsRegistry();

export function renderAppToString(request, response) {
  const initLocale = request.user ? request.user.language : defaultLanguage;
  const preloadedState = {
    intl: {
      locale: initLocale,
      enabledLanguages,
      ...(getLocalization(initLocale) || {}),
    },
  };
  if (request.user) {
    Object.assign(preloadedState, {user: request.user.toJSON()});
  }
  if (request.oauth2) {
    Object.assign(preloadedState, {oauth2: request.oauth2});
  }

  const store = configureStore(preloadedState);

  const context = {};

  const initialMarkup = renderInitialMarkup(request.url, store, context, sheetsRegistry, App);
  // Grab the CSS from our sheetsRegistry.
  const initialStyles = sheetsRegistry.toString();

  // context.url will contain the URL to redirect to if a <Redirect> was used
  if (context.url) {
    response.redirect(302, context.url);
  } else {
    response.status(200).send(renderHTML(initialMarkup, initialStyles, store, HTML));
  }
}

export function renderEmptyString(request, response) {
  response.status(200).send(renderHTML("", "", {
    getState: () => ({
      intl: {
        locale: defaultLanguage,
        enabledLanguages,
        ...(getLocalization(defaultLanguage) || {}),
      },
    }),
  }, HTML));
}

