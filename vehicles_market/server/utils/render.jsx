import React from "react";
import {renderToStaticMarkup, renderToString} from "react-dom/server";
import {Provider} from "react-intl-redux";
import {StaticRouter} from "react-router";
import MuiProvider from "../../client/utils/mui";


export function renderInitialMarkup(url, store, context, sheetsRegistry, App) {
  return renderToString(
    <Provider store={store}>
      <StaticRouter location={url} context={context}>
        <MuiProvider sheetsRegistry={sheetsRegistry}>
          <App />
        </MuiProvider>
      </StaticRouter>
    </Provider>,
  );
}

export function renderHTML(initialMarkup, initialStyles, store, Wrapper) {
  return `<!doctype html>\n${renderToStaticMarkup(
    <Wrapper initialMarkup={initialMarkup} initialStyles={initialStyles} initialState={store.getState()} />,
  )}`;
}
