import {makeError} from "./error";
import {reMongoId} from "../constants/regexp";


function createRegExpParameter(re, name) {
  return (request, response, next, val) => {
    if (re.test(String(val))) {
      next();
    } else {
      next(makeError("invalid-param", 400, {name, reason: "invalid"}));
    }
  };
}

export const mongoId = createRegExpParameter(reMongoId, "_id");
