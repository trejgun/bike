import winston from "winston";
import assert from "power-assert";
import supertest from "../../../test-utils/supertest";
import {setUp, tearDown} from "../../../test-utils/flow";
import {country, fullName, phoneNumber, price, pricePerDay} from "../../../test/constants";
import {getRange} from "../../../test-utils/utils";
import {getNewId} from "../../../server/shared/utils/mongoose";
import expect from "../../../test-utils/assert";

import BookingController from "../../server/controllers/impl/booking";
import VehicleController from "../../server/controllers/impl/vehicle";
import MailController from "../../../mail/server/controllers/impl/mail";


let data;

describe("Booking (market)", () => {
  const superapp = supertest("vehicles_market");

  const mailController = new MailController();

  afterEach(() =>
    mailController.deleteMany(),
  );

  describe("GET /bookings", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 3,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 2,
      }, {
        model: "Vehicle",
        requires: {
          Organization: [0, 1],
        },
        count: 2,
      }, {
        model: "Booking",
        requires: {
          Vehicle: [0, 1, 2],
          Organization: [[0, 1], [0, 1], [1, 2]],
        },
        data: [{
          status: BookingController.statuses.accepted,
        }, {
          status: BookingController.statuses.rejected,
        }, {
          status: BookingController.statuses.new,
        }],
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should get bookings", () =>
      superapp
        .login(data.User[1])
        .get("/api/bookings")
        .expect(200)
        .then(({body}) => {
          winston.debug("body", body.list);
          assert.equal(body.count, 2);
          assert.equal(body.list[0].status, BookingController.statuses.accepted);
          assert.equal(body.list[0].vehicle._id, data.Vehicle[0]._id.toString());
          assert.equal(body.list[1].status, BookingController.statuses.rejected);
          assert.equal(body.list[1].vehicle._id, data.Vehicle[1]._id.toString());
        }),
    );

    after(tearDown);
  });

  describe("POST /bookings", () => {
    describe("@pass", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 2,
        }, {
          model: "User",
          requires: {
            Organization: "o2o",
          },
          count: 2,
        }, {
          model: "Vehicle",
          requires: {
            Organization: "o2o",
          },
          count: 2,
        }, {
          model: "Booking",
          requires: {
            Vehicle: [0],
            Organization: [0],
          },
          data: [{
            range: getRange(2, 5),
            status: BookingController.statuses.new,
          }],
        }])
          .then(result => {
            data = result;
          }),
      );

      // TODO not sure about this
      it("should create booking (same)", () =>
        superapp
          .login(data.User[0])
          .post("/api/bookings")
          .type("application/json;charset=utf-8")
          .send({
            vehicle: data.Vehicle[0]._id,
            fullName,
            phoneNumber,
            range: getRange(2, 5),
          })
          .expect(200)
          .then(({body}) => {
            winston.debug("body", body);
            assert.equal(body.status, BookingController.statuses.new);
            assert.equal(body.range, getRange(2, 5));
            assert.equal(body.vehicle._id, data.Vehicle[0]._id.toString());
            assert.equal(body.price, pricePerDay * 3);

            return mailController.find()
              .then(mails => {
                assert.deepEqual(mails.map(mail => mail.type).sort(), ["newBookingForCustomer", "newBookingForRenter"]);
              });
          }),
      );

      it("should create booking (other)", () =>
        superapp
          .login(data.User[1])
          .post("/api/bookings")
          .type("application/json;charset=utf-8")
          .send({
            vehicle: data.Vehicle[0]._id,
            fullName,
            phoneNumber,
            range: getRange(2, 5),
          })
          .expect(200)
          .then(({body}) => {
            winston.info("body", body);
            assert.equal(body.status, BookingController.statuses.new);
            assert.equal(body.range, getRange(2, 5));
            assert.equal(body.vehicle._id, data.Vehicle[0]._id.toString());
            assert.equal(body.price, pricePerDay * 3);
            assert.equal(body.country, country);

            return mailController.find()
              .then(mails => {
                assert.deepEqual(mails.map(mail => mail.type).sort(), ["newBookingForCustomer", "newBookingForRenter"]);
              });
          }),
      );

      after(tearDown);
    });

    describe("@pass (opt-out)", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 2,
        }, {
          model: "User",
          requires: {
            Organization: "o2o",
          },
          count: 2,
        }, {
          model: "Vehicle",
          requires: {
            Organization: "o2o",
          },
          count: 2,
        }, {
          model: "OptOut",
          requires: {
            User: "o2o",
          },
          data: [{
            type: MailController.types["vehicles_market"].newBookingForCustomer,
          }, {
            type: MailController.types["vehicles_office"].newBookingForRenter,
          }],
        }])
          .then(result => {
            data = result;
          }),
      );

      it("should create booking", () =>
        superapp
          .login(data.User[0])
          .post("/api/bookings")
          .type("application/json;charset=utf-8")
          .send({
            vehicle: data.Vehicle[1]._id,
            fullName,
            phoneNumber,
            range: getRange(2, 5),
          })
          .expect(200)
          .then(({body}) => {
            winston.info("body", body);

            return mailController.find()
              .then(mails => {
                assert.deepEqual(mails.map(mail => mail.type).sort(), []);
              });
          }),
      );

      after(tearDown);
    });

    describe("@throws", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 2,
        }, {
          model: "User",
          requires: {
            Organization: "o2o",
          },
          count: 2,
        }, {
          model: "Vehicle",
          requires: {
            Organization: "o2o",
          },
          count: 1,
        }, {
          model: "Booking",
          requires: {
            Vehicle: [0],
            Organization: [0],
          },
          data: [{
            range: getRange(10, 13),
            status: BookingController.statuses.accepted,
          }],
        }])
          .then(result => {
            data = result;
          }),
      );

      it("should throw `vehicle-not-found`", () =>
        superapp
          .login(data.User[1])
          .post("/api/bookings")
          .type("application/json;charset=utf-8")
          .send({
            vehicle: getNewId(),
            range: getRange(2, 5),
          })
          .then(expect(404, [{name: "vehicle"}])),
      );

      it("should throw `invalid-param` (range)[required]", () =>
        superapp
          .login(data.User[1])
          .post("/api/bookings")
          .type("application/json;charset=utf-8")
          .send({
            vehicle: data.Vehicle[0]._id,
          })
          .then(expect(400, [{reason: "required", name: "range"}])),
      );

      it("should throw `invalid-param` (range)[invalid]", () =>
        superapp
          .login(data.User[1])
          .post("/api/bookings")
          .type("application/json;charset=utf-8")
          .send({
            vehicle: data.Vehicle[0]._id,
            fullName,
            phoneNumber,
            range: "crap",
          })
          .then(expect(400, [{reason: "invalid", name: "range"}])),
      );

      it("should throw `conflict` (range)[not-available/start]", () =>
        superapp
          .login(data.User[1])
          .post("/api/bookings")
          .type("application/json;charset=utf-8")
          .send({
            vehicle: data.Vehicle[0]._id,
            fullName,
            phoneNumber,
            range: getRange(9, 11),
          })
          .then(expect(409, [{reason: "not-available", name: "range"}])),
      );

      it("should throw `conflict` (range)[not-available/in]", () =>
        superapp
          .login(data.User[1])
          .post("/api/bookings")
          .type("application/json;charset=utf-8")
          .send({
            vehicle: data.Vehicle[0]._id,
            fullName,
            phoneNumber,
            range: getRange(11, 12),
          })
          .then(expect(409, [{reason: "not-available", name: "range"}])),
      );

      it("should throw `conflict` (range)[not-available/out]", () =>
        superapp
          .login(data.User[1])
          .post("/api/bookings")
          .type("application/json;charset=utf-8")
          .send({
            vehicle: data.Vehicle[0]._id,
            fullName,
            phoneNumber,
            range: getRange(8, 14),
          })
          .then(expect(409, [{reason: "not-available", name: "range"}])),
      );

      it("should throw `invalid-param` (range)[not-available/end]", () =>
        superapp
          .login(data.User[1])
          .post("/api/bookings")
          .type("application/json;charset=utf-8")
          .send({
            vehicle: data.Vehicle[0]._id,
            fullName,
            phoneNumber,
            range: getRange(11, 13),
          })
          .then(expect(409, [{reason: "not-available", name: "range"}])),
      );

      after(tearDown);
    });
  });

  describe("GET /bookings/:_id", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 2,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 2,
      }, {
        model: "Vehicle",
        requires: {
          Organization: [0, 1],
        },
        count: 2,
      }, {
        model: "Booking",
        requires: {
          Vehicle: [0, 1, 1],
          Organization: [[0, 1], [0, 1], [1, 1]],
        },
        data: [{
          status: BookingController.statuses.accepted,
        }, {
          status: BookingController.statuses.rejected,
        }, {
          status: BookingController.statuses.accepted,
        }],
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should get booking (active)", () =>
      superapp
        .login(data.User[0])
        .get(`/api/bookings/${data.Booking[0]._id}`)
        .expect(200)
        .then(({body}) => {
          winston.debug("body", body);
          assert.equal(body.status, BookingController.statuses.accepted);
          assert.equal(body.price, price);
        }),
    );

    it("should get booking (inactive)", () =>
      superapp
        .login(data.User[0])
        .get(`/api/bookings/${data.Booking[1]._id}`)
        .expect(200)
        .then(({body}) => {
          winston.debug("body", body);
          assert.equal(body.status, BookingController.statuses.rejected);
        }),
    );

    it("should throw `access-denied`", () =>
      superapp
        .login(data.User[0])
        .get(`/api/bookings/${data.Booking[2]._id}`)
        .then(expect(403, [{reason: "organization"}])),
    );

    after(tearDown);
  });

  describe("PUT /bookings/:_id", () => {
    describe("@pass (booking)", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 2,
        }, {
          model: "User",
          requires: {
            Organization: "o2o",
          },
          count: 2,
        }, {
          model: "Vehicle",
          requires: {
            Organization: [0],
          },
          count: 1,
        }, {
          model: "Booking",
          requires: {
            Vehicle: [0, 0],
            Organization: [[0, 1], [0, 1]],
          },
          data: [{
            status: BookingController.statuses.new,
            range: getRange(3, 4),
          }, {
            status: BookingController.statuses.new,
            range: getRange(5, 6),
          }],
        }])
          .then(result => {
            data = result;
          }),
      );

      it("should edit booking", () =>
        superapp
          .login(data.User[0])
          .put(`/api/bookings/${data.Booking[0]._id}`)
          .type("application/json;charset=utf-8")
          .send({
            range: getRange(5, 6),
          })
          .expect(200)
          .then(({body}) => {
            winston.debug("body", body);
            assert.equal(body.range, getRange(5, 6));
            assert.equal(body.price, pricePerDay * 1);
          }),
      );
    });

    describe("@pass (vehicle)", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 2,
        }, {
          model: "User",
          requires: {
            Organization: "o2o",
          },
          count: 2,
        }, {
          model: "Vehicle",
          requires: {
            Organization: [0, 0, 1],
          },
          count: 3,
        }, {
          model: "Booking",
          requires: {
            Vehicle: [0, 0],
            Organization: [[0, 1], [0, 1]],
          },
          data: [{
            status: BookingController.statuses.new,
            range: getRange(3, 4),
          }, {
            status: BookingController.statuses.new,
            range: getRange(3, 4),
          }],
        }])
          .then(result => {
            data = result;
          }),
      );

      it("should edit booking (same company)", () =>
        superapp
          .login(data.User[0])
          .put(`/api/bookings/${data.Booking[0]._id}`)
          .type("application/json;charset=utf-8")
          .send({
            range: getRange(5, 6),
            vehicle: data.Vehicle[1]._id,
          })
          .expect(200)
          .then(({body}) => {
            winston.debug("body", body);
            assert.equal(body.range, getRange(5, 6));
            assert.equal(body.vehicle._id, data.Vehicle[1]._id.toString());
          }),
      );

      it("should edit booking (another company)", () =>
        superapp
          .login(data.User[0])
          .put(`/api/bookings/${data.Booking[1]._id}`)
          .type("application/json;charset=utf-8")
          .send({
            range: getRange(5, 6),
            vehicle: data.Vehicle[2]._id,
          })
          .expect(200)
          .then(({body}) => {
            winston.debug("body", body);
            assert.equal(body.range, getRange(5, 6));
            assert.equal(body.vehicle._id, data.Vehicle[2]._id.toString());
          }),
      );
    });

    describe("@throw (booking)", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 2,
        }, {
          model: "User",
          requires: {
            Organization: "o2o",
          },
          count: 2,
        }, {
          model: "Vehicle",
          requires: {
            Organization: [0, 0],
          },
          count: 2,
        }, {
          model: "Booking",
          requires: {
            Vehicle: [0, 0, 1, 1],
            Organization: [[0, 1], [0, 1], [0, 1], [1, 1]],
          },
          data: [{
            status: BookingController.statuses.new,
            range: getRange(3, 4),
          }, {
            status: BookingController.statuses.accepted,
            range: getRange(5, 6),
          }, {
            status: BookingController.statuses.accepted,
            range: getRange(3, 4),
          }, {
            status: BookingController.statuses.new,
            range: getRange(3, 4),
          }],
        }])
          .then(result => {
            data = result;
          }),
      );

      it("should throw `invalid-param` (range)[not-available]", () =>
        superapp
          .login(data.User[0])
          .put(`/api/bookings/${data.Booking[0]._id}`)
          .type("application/json;charset=utf-8")
          .send({
            range: getRange(5, 6),
          })
          .then(({body}) => {
            winston.debug("body", body);
            assert.equal(body.errors.length, 1);
            assert.equal(body.errors[0].message, "conflict");
          }),
      );

      it("should throw `booking-not-editable`", () =>
        superapp
          .login(data.User[0])
          .put(`/api/bookings/${data.Booking[2]._id}`)
          .type("application/json;charset=utf-8")
          .send({
            range: getRange(5, 6),
          })
          .then(({body}) => {
            winston.debug("body", body);
            assert.equal(body.errors.length, 1);
            assert.equal(body.errors[0].message, "booking-not-editable");
          }),
      );

      it("should throw `access-denied`", () =>
        superapp
          .login(data.User[0])
          .delete(`/api/bookings/${data.Booking[3]._id}`)
          .then(expect(403, [{reason: "organization"}])),
      );

      it("should throw `booking-not-found`", () =>
        superapp
          .login(data.User[0])
          .put(`/api/bookings/${getNewId()}`)
          .type("application/json;charset=utf-8")
          .send({
            range: getRange(5, 6),
          })
          .then(expect(404, [{name: "booking"}])),
      );

      after(tearDown);
    });

    describe("@throw (vehicle)", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 2,
        }, {
          model: "User",
          requires: {
            Organization: "o2o",
          },
          count: 2,
        }, {
          model: "Vehicle",
          requires: {
            Organization: [0, 1],
          },
          data: [{
            status: VehicleController.statuses.active,
          }, {
            plate: "", // to set status need to set this
            status: VehicleController.statuses.new,
          }],
        }, {
          model: "Booking",
          requires: {
            Vehicle: [0, 0],
            Organization: [[0], [0]],
          },
          data: [{
            status: BookingController.statuses.new,
            range: getRange(3, 4),
          }, {
            status: BookingController.statuses.new,
            range: getRange(3, 4),
          }],
        }])
          .then(result => {
            data = result;
          }),
      );

      it("should throw `not-active` (vehicle)", () =>
        superapp
          .login(data.User[0])
          .put(`/api/bookings/${data.Booking[0]._id}`)
          .type("application/json;charset=utf-8")
          .send({
            vehicle: data.Vehicle[1]._id,
            range: getRange(5, 6),
          })
          .then(expect(410, [{name: "vehicle"}])),
      );

      it("should throw `vehicle-not-found`", () =>
        superapp
          .login(data.User[0])
          .put(`/api/bookings/${data.Booking[1]._id}`)
          .type("application/json;charset=utf-8")
          .send({
            vehicle: getNewId(),
            range: getRange(5, 6),
          })
          .then(expect(404, [{name: "vehicle"}])),
      );

      after(tearDown);
    });
  });

  describe("DELETE /bookings/:_id", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 2,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 2,
      }, {
        model: "Vehicle",
        requires: {
          Organization: [0, 1],
        },
        count: 2,
      }, {
        model: "Booking",
        requires: {
          Vehicle: [0, 0, 0, 0, 1],
          Organization: [[0, 1], [0, 1], [0, 1], [0, 1], [1, 1]],
        },
        data: [{}, {
          status: BookingController.statuses.accepted,
        }, {
          status: BookingController.statuses.rejected,
        }, {
          status: BookingController.statuses.cancelled,
        }, {}],
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should revoke booking", () =>
      superapp
        .login(data.User[0])
        .delete(`/api/bookings/${data.Booking[0]._id}`)
        .expect(200)
        .then(({body}) => {
          winston.debug("body", body);
          assert.equal(body.status, BookingController.statuses.revoked);
          return mailController.find()
            .then(mails => {
              assert.deepEqual(mails.map(mail => mail.type).sort(), ["bookingRevokedByCustomer"]);
            });
        }),
    );

    it("should cancel booking", () =>
      superapp
        .login(data.User[0])
        .delete(`/api/bookings/${data.Booking[1]._id}`)
        .expect(200)
        .then(({body}) => {
          winston.debug("body", body);
          assert.equal(body.status, BookingController.statuses.cancelled);
          return mailController.find()
            .then(mails => {
              assert.deepEqual(mails.map(mail => mail.type).sort(), ["bookingCancelledByCustomer"]);
            });
        }),
    );

    it("should throw `not-active` (booking)[rejected]", () =>
      superapp
        .login(data.User[0])
        .delete(`/api/bookings/${data.Booking[2]._id}`)
        .then(expect(410, [{name: "booking"}])),
    );

    it("should throw `not-active` (booking)[cancelled]", () =>
      superapp
        .login(data.User[0])
        .delete(`/api/bookings/${data.Booking[3]._id}`)
        .then(expect(410, [{name: "booking"}])),
    );

    it("should throw `access-denied`", () =>
      superapp
        .login(data.User[0])
        .delete(`/api/bookings/${data.Booking[4]._id}`)
        .then(expect(403, [{reason: "organization"}])),
    );

    after(tearDown);
  });
});
