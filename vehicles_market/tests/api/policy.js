import winston from "winston";
import assert from "power-assert";
import supertest from "../../../test-utils/supertest";
import {setUp, tearDown} from "../../../test-utils/flow";
import {systemId} from "../../../test/constants";

let data;

describe("Policy", () => {
  const superapp = supertest("vehicles_market");

	describe.skip("GET /policies/:name/:_id", () => {
		before(() =>
			setUp([{
				model: "Organization",
				data: [{}, {}, {
          _id: systemId,
        }],
			}, {
				model: "User",
				requires: {
          Organization: "o2o",
				},
        count: 2,
			}, {
				model: "Policy",
				requires: {
          Organization: [1, 2],
				},
        count: 2,
			}])
				.then(result => {
					data = result;
        }),
		);

		it("should get default policy", () =>
			superapp
				.login(data.User[0])
				.get(`/api/policies/return/${data.Organization[0]._id}`)
				.expect(200)
				.then(({body}) => {
					winston.info("body", body);
					assert.equal(body._id, data.Policy[1]._id);
        }),
		);

		it("should get custom policy", () =>
			superapp
				.login(data.User[1])
				.get(`/api/policies/return/${data.Organization[1]._id}`)
				.expect(200)
				.then(({body}) => {
					winston.info("body", body);
					assert.equal(body._id, data.Policy[0]._id);
        }),
		);

		after(tearDown);
	});

	describe("GET /vehicles/:_id/policies/:name", () => {
		before(() =>
			setUp([{
				model: "Organization",
				data: [{}, {}, {
          _id: systemId,
        }],
			}, {
				model: "User",
				requires: {
          Organization: "o2o",
				},
        count: 2,
			}, {
				model: "Policy",
				requires: {
          Organization: [1, 2],
				},
        count: 2,
			}, {
				model: "Vehicle",
				requires: {
          Organization: "o2o",
				},
        count: 2,
			}])
				.then(result => {
					data = result;
        }),
		);

		it("should get default policy", () =>
			superapp
				.login(data.User[0])
				.get(`/api/vehicles/${data.Vehicle[0]._id}/policies/return`)
				.expect(200)
				.then(({body}) => {
					winston.info("body", body);
					assert.equal(body._id, data.Policy[1]._id.toString());
        }),
		);

		it("should get custom policy", () =>
			superapp
				.login(data.User[1])
				.get(`/api/vehicles/${data.Vehicle[1]._id}/policies/return`)
				.expect(200)
				.then(({body}) => {
					winston.info("body", body);
					assert.equal(body._id, data.Policy[0]._id.toString());
        }),
		);

		after(tearDown);
	});
});
