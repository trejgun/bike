import winston from "winston";
import assert from "power-assert";
import supertest from "../../../test-utils/supertest";
import {setUp, tearDown} from "../../../test-utils/flow";
import expect from "../../../test-utils/assert";


let data;

describe("User", () => {
  const superapp = supertest("vehicles_market");

	describe("GET /sync", () => {
		before(() =>
			setUp([{
				model: "User",
        count: 1,
			}])
				.then(result => {
					data = result;
        }),
		);

		it("should return nothing", () =>
			superapp
				.login(null)
				.get("/api/sync")
				.expect(200)
				.then(({body}) => {
					winston.info("body", body);
					assert.equal(body, null);
        }),
		);

		it("should return user", () =>
			superapp
				.login(data.User[0])
				.get("/api/sync")
				.expect(200)
				.then(({body}) => {
					winston.debug("body", body);
					assert.equal(body._id, data.User[0]._id.toString());
        }),
		);

		after(tearDown);
	});

	describe("PUT /users/:_id", () => {
		const fullName = "My New Full Name";
		const license = "/path/to/image.jpg";

		before(() =>
			setUp([{
				model: "Organization",
        count: 2,
			}, {
				model: "User",
				requires: {
          Organization: "o2o",
				},
        count: 2,
			}])
				.then(result => {
					data = result;
        }),
		);

		it("should edit user", () =>
			superapp
				.login(data.User[0])
				.put(`/api/users/${data.User[0]._id}`)
				.type("application/json;charset=utf-8")
				.send({
					fullName,
          license,
				})
				.expect(200)
				.then(({body}) => {
					assert.equal(body.fullName, fullName);
					assert.equal(body.license, license);
        }),
		);

		it("should throw `access-denied`", () =>
			superapp
				.login(data.User[0])
				.put(`/api/users/${data.User[1]._id}`)
				.type("application/json;charset=utf-8")
				.send({
          fullName,
				})
        .then(expect(403, [{reason: "organization"}])),
		);

		after(tearDown);
	});
});
