import winston from "winston";
import assert from "power-assert";
import supertest from "../../../test-utils/supertest";
import {setUp, tearDown} from "../../../test-utils/flow";
import {getRange} from "../../../test-utils/utils";
import {brand, cc, fuel, model, type} from "../../client/constants/placeholder";
import expect from "../../../test-utils/assert";

import VehicleController from "../../server/controllers/impl/vehicle";
import BookingController from "../../server/controllers/impl/booking";
import SearchController from "../../server/controllers/impl/search";
import OrganizationController from "../../../oauth2/server/controllers/impl/organization";
import CatalogController from "../../../vehicles_office/server/controllers/impl/catalog";
import catalog from "../../../test/vehicles";


const template = {
  type,
  brand,
  model,
  fuel,
  cc,
};

const plate0 = "DK1234AA";
const plate1 = null;
const plate2 = "DK1234CC";
const plate3 = "DK1234DD";
const plate4 = "DK1234EE";
const plate5 = "DK1234FF";

let data;

describe("Vehicles (market)", () => {
  const superapp = supertest("vehicles_market");

  const searchController = new SearchController();
  const catalogController = new CatalogController();

  describe("GET /vehicles", () => {
    afterEach(() =>
      searchController.deleteMany(),
    );

    describe.only("@pass (user)", () => {
      before(() =>
        setUp([{
          model: "Organization",
          data: [{}, {}, {}, {
            status: OrganizationController.statuses.suspended,
          }],
        }, {
          model: "User",
          requires: {
            Organization: [0],
          },
          count: 1,
        }, {
          model: "Vehicle",
          requires: {
            Organization: [1, 2, 2, 2, 2, 3],
          },
          data: [
            Object.assign({}, template, {
              plate: plate0,
              status: VehicleController.statuses.active,
            }),
            Object.assign({}, template, {
              plate: plate1,
              status: VehicleController.statuses.new,
            }),
            Object.assign({}, template, {
              plate: plate2,
              status: VehicleController.statuses.inactive,
            }),
            Object.assign({}, template, {
              plate: plate3,
              status: VehicleController.statuses.active,
            }),
            Object.assign({}, template, {
              plate: plate4,
              cc: 150,
              status: VehicleController.statuses.active,
            }),
            Object.assign({}, template, {
              plate: plate5,
              status: VehicleController.statuses.active,
            }),
          ],
        }])
          .then(result => {
            // console.log("result", result);
            data = result;
            return catalogController.create(catalog);
          }),
      );

      it("should get list of vehicles (no params)", () => {
        const range = getRange(0, 0);
        return superapp
          .login(data.User[0])
          .get("/api/vehicles")
          .query({
            range,
          })
          .expect(200)
          .then(({body}) => {
            winston.info("body", body);
            assert.equal(body.count, 3);
            assert.equal(body.list.length, 3);
            assert.equal(body.list[0].plate, plate0, "plate0");
            assert.equal(body.list[1].plate, plate3, "plate3");
            assert.equal(body.list[2].plate, plate4, "plate4");

            return searchController.find()
              .then(searches => {
                assert.equal(searches.length, 1);
                assert.equal(searches[0].user.toJSON(), data.User[0]._id.toString());
                assert.equal(searches[0].brand, void 0);
                assert.equal(searches[0].model, void 0);
                assert.equal(searches[0].fuel, void 0);
                assert.equal(searches[0].cc, void 0);
                assert.equal(searches[0].range, range.toString());
              });
          });
      });

      it.skip("should get list of vehicles (all params)", () => {
        const range = getRange(0, 0);
        return superapp
          .login(data.User[0])
          .get("/api/vehicles")
          .query({
            ...template,
            range,
          })
          .expect(200)
          .then(({body}) => {
            winston.info("body", body);
            assert.equal(body.count, 2);
            assert.equal(body.list.length, 2);
            assert.equal(body.list[0].plate, plate0);
            assert.equal(body.list[1].plate, plate3);

            return searchController.find()
              .then(searches => {
                assert.equal(searches.length, 1);
                assert.equal(searches[0].user.toJSON(), data.User[0]._id.toString());
                assert.equal(searches[0].brand, brand);
                assert.equal(searches[0].model, model);
                assert.equal(searches[0].fuel, fuel);
                assert.equal(searches[0].cc, cc);
                assert.equal(searches[0].range, range.toString());
              });
          });
      });

      it.skip("should get list of vehicles (range)", () =>
        superapp
          .login(data.User[0])
          .get("/api/vehicles")
          .query({
            ...template,
            range: getRange(3, 4),
          })
          .expect(200)
          .then(({body}) => {
            winston.info("body", body);
            assert.equal(body.count, 1);
            assert.equal(body.list.length, 1);
            assert.equal(body.list[0].plate, plate0);
          }),
      );

      after(tearDown);
    });

    describe("@pass (guest)", () => {
      before(() =>
        setUp([{
          model: "Organization",
          data: [{}, {
            delivery: true,
          }, {
            delivery: false,
          }, {
            status: OrganizationController.statuses.suspended,
          }],
        }, {
          model: "Vehicle",
          requires: {
            Organization: [1, 2, 2, 2, 2, 3],
          },
          data: [
            Object.assign({}, template, {
              plate: plate0,
              status: VehicleController.statuses.active,
            }),
            Object.assign({}, template, {
              plate: plate1,
              status: VehicleController.statuses.new,
            }),
            Object.assign({}, template, {
              plate: plate2,
              status: VehicleController.statuses.inactive,
            }),
            Object.assign({}, template, {
              plate: plate3,
              status: VehicleController.statuses.active,
            }),
            Object.assign({}, template, {
              plate: plate4,
              cc: 150,
              status: VehicleController.statuses.active,
            }),
            Object.assign({}, template, {
              plate: plate5,
              status: VehicleController.statuses.active,
            }),
          ],
        }, {
          model: "Booking",
          requires: {
            Vehicle: [1, 3],
            Organization: [2, 2],
          },
          data: [{
            status: BookingController.statuses.new,
          }, {
            status: BookingController.statuses.accepted,
          }],
        }])
          .then(result => {
            data = result;
            return catalogController.create(catalog);
          }),
      );

      it("should get list of vehicles", () => {
        const range = getRange(0, 0);
        return superapp
          .login(null)
          .get("/api/vehicles")
          .query({
            ...template,
            range,
          })
          .expect(200)
          .then(({body}) => {
            winston.info("body", body);
            assert.equal(body.count, 2);
            assert.equal(body.list.length, 2);
            assert.equal(body.list[0].plate, plate0);
            assert.equal(body.list[1].plate, plate3);

            return searchController.find()
              .then(searches => {
                assert.equal(searches.length, 1);
                assert.equal(searches[0].user, null);
                assert.equal(searches[0].brand, brand);
                assert.equal(searches[0].model, model);
                assert.equal(searches[0].cc, cc);
                assert.equal(searches[0].range, range.toString());
              });
          });
      });

      it("should get list of vehicles (range)", () =>
        superapp
          .login(null)
          .get("/api/vehicles")
          .query({
            ...template,
            range: getRange(3, 4),
          })
          .expect(200)
          .then(({body}) => {
            winston.info("body", body);
            assert.equal(body.count, 1);
            assert.equal(body.list.length, 1);
            assert.equal(body.list[0].plate, plate0);
          }),
      );

      after(tearDown);
    });

    describe("@throws (guest)", () => {
      before(() =>
        setUp([{
          model: "Organization",
          data: [{
            delivery: true,
          }, {
            delivery: false,
          }, {
            status: OrganizationController.statuses.suspended,
          }],
        }, {
          model: "Vehicle",
          requires: {
            Organization: [0, 1, 1, 1, 1, 2],
          },
          data: [
            Object.assign({}, template, {
              plate: plate0,
              status: VehicleController.statuses.active,
            }),
            Object.assign({}, template, {
              plate: plate1,
              status: VehicleController.statuses.new,
            }),
            Object.assign({}, template, {
              plate: plate2,
              status: VehicleController.statuses.inactive,
            }),
            Object.assign({}, template, {
              plate: plate3,
              status: VehicleController.statuses.active,
            }),
            Object.assign({}, template, {
              plate: plate4,
              cc: 150,
              status: VehicleController.statuses.active,
            }),
            Object.assign({}, template, {
              plate: plate5,
              status: VehicleController.statuses.active,
            }),
          ],
        }, {
          model: "Booking",
          requires: {
            Vehicle: [1, 3],
            Organization: [0],
          },
          data: [{
            status: BookingController.statuses.new,
          }, {
            status: BookingController.statuses.accepted,
          }],
        }])
          .then(result => {
            data = result;
            return catalogController.create(catalog);
          }),
      );

      it("should throw `invalid-param`", () =>
        superapp
          .login(null)
          .get("/api/vehicles")
          .then(expect(400, [{reason: "required", name: "range"}])),
      );

      it("should throw `invalid-param` (type)", () =>
        superapp
          .login(null)
          .get("/api/vehicles")
          .query({
            type: "crap",
            brand,
            model,
            fuel,
            cc,
            range: getRange(3, 4),
          })
          .then(expect(400, [{reason: "invalid", name: "type"}])),
      );

      it("should throw `invalid-param` (brand)[combination]", () =>
        superapp
          .login(null)
          .get("/api/vehicles")
          .query({
            type,
            brand: "Toyota",
            model,
            fuel,
            cc,
            range: getRange(3, 4),
          })
          .then(expect(400, [{reason: "combination", name: "brand"}])),
      );

      it("should throw `invalid-param` (brand)[invalid]", () =>
        superapp
          .login(null)
          .get("/api/vehicles")
          .query({
            type,
            brand: "crap",
            model,
            fuel,
            cc,
            range: getRange(3, 4),
          })
          .then(expect(400, [{reason: "invalid", name: "brand"}])),
      );

      it("should throw `invalid-param` (model)[combination]", () =>
        superapp
          .login(null)
          .get("/api/vehicles")
          .query({
            type,
            brand,
            model: "Ninja",
            fuel,
            cc,
            range: getRange(3, 4),
          })
          .then(expect(400, [{reason: "combination", name: "model"}])),
      );

      it("should throw `invalid-param` (model)[invalid]", () =>
        superapp
          .login(null)
          .get("/api/vehicles")
          .query({
            type,
            brand,
            model: "crap",
            fuel,
            cc,
            range: getRange(3, 4),
          })
          .then(expect(400, [{reason: "invalid", name: "model"}])),
      );

      it("should throw `invalid-param` (fuel)[combination]", () =>
        superapp
          .login(null)
          .get("/api/vehicles")
          .query({
            type,
            brand,
            model,
            fuel: "diesel",
            cc,
            range: getRange(3, 4),
          })
          .then(expect(400, [{reason: "combination", name: "fuel"}])),
      );

      it("should throw `invalid-param` (fuel)[invalid]", () =>
        superapp
          .login(null)
          .get("/api/vehicles")
          .query({
            type,
            brand,
            model,
            fuel: "crap",
            cc,
            range: getRange(3, 4),
          })
          .then(expect(400, [{reason: "invalid", name: "fuel"}])),
      );

      it("should throw `invalid-param` (cc)[combination]", () =>
        superapp
          .login(null)
          .get("/api/vehicles")
          .query({
            type,
            brand,
            model,
            fuel,
            cc: 1000,
            range: getRange(3, 4),
          })
          .then(expect(400, [{reason: "combination", name: "cc"}])),
      );

      it("should throw `invalid-param` (cc)[invalid]", () =>
        superapp
          .login(null)
          .get("/api/vehicles")
          .query({
            type,
            brand,
            model,
            fuel,
            cc: "crap",
            range: getRange(3, 4),
          })
          .then(expect(400, [{reason: "invalid", name: "cc"}])),
      );

      after(tearDown);
    });
  });

  describe("GET /vehicles/:_id", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 1,
      }, {
        model: "Vehicle",
        requires: {
          Organization: "m2o",
        },
        data: [
          Object.assign({}, template, {
            plate: plate0,
            status: VehicleController.statuses.active,
          }),
          Object.assign({}, template, {
            plate: plate1,
            status: VehicleController.statuses.new,
          }),
          Object.assign({}, template, {
            plate: plate2,
            status: VehicleController.statuses.inactive,
          }),
        ],
      }, {
        model: "Booking",
        requires: {
          Vehicle: [0],
          Organization: [0],
        },
        count: 1,
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should get vehicle", () =>
      superapp
        .get(`/api/vehicles/${data.Vehicle[0]._id}`)
        .expect(200)
        .then(({body}) => {
          winston.debug("body", body);
          assert.equal(body.plate, plate0);
          assert.equal(body.bookings, void 0);
        }),
    );

    it("should throw `not-active` (vehicle)[new]", () =>
      superapp
        .get(`/api/vehicles/${data.Vehicle[1]._id}`)
        .then(expect(410, [{name: "vehicle"}])),
    );

    it("should throw `not-active` (vehicle)[inactive]", () =>
      superapp
        .get(`/api/vehicles/${data.Vehicle[2]._id}`)
        .then(expect(410, [{name: "vehicle"}])),
    );

    after(tearDown);
  });
});
