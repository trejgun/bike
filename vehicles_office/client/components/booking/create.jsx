import React, {Component} from "react";
import PropTypes from "prop-types";
import {FormattedMessage} from "react-intl";
import {Button, ButtonToolbar, Form, Media} from "react-bootstrap";
import {vehicleToHtml, vehicleToText} from "../../utils/vehicle";
import countries from "../../intl/countries.en.json";
import withStore from "../forms/withStore";
import withCreate from "../forms/withCreateForm";
import withTypeaheadForm from "../forms/withTypeaheadForm";
import {email, fullName, notes, phoneNumber, price} from "../../constants/placeholder";
import InputGroup from "../inputs/input.group";
import InputGroupValidation from "../inputs/input.group.validation";
import InputDateTimePickerGroupValidation from "../inputs/input.datetimepicker.group.validation";
import InputMarkdownGroup from "../inputs/input.markdown.group";
import ImageViewer from "../imageViewer";
import {readFromQueryString} from "../../utils/location";


@withCreate("bookings")
@withTypeaheadForm("vehicles", "select")
@withStore("user")
export default class BookingCreateForm extends Component {
  static propTypes = {
    select: PropTypes.shape({
      list: PropTypes.array,
    }),
    user: PropTypes.shape({
      organization: PropTypes.object,
    }),

    match: PropTypes.shape({
      params: PropTypes.object.isRequired,
    }).isRequired,
    location: PropTypes.shape({
      search: PropTypes.string.isRequired,
    }).isRequired,

    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
    setState: PropTypes.func,
    formData: PropTypes.shape({
      fullName: PropTypes.string,
      email: PropTypes.string,
      range: PropTypes.string,
      price: PropTypes.number,
      country: PropTypes.string,
      vehicle: PropTypes.object,
      phoneNumber: PropTypes.string,
      notes: PropTypes.string,
    }),
    notes: PropTypes.string,
  };

  componentDidMount() {
    const {match, select, setState, location} = this.props;
    const vehicle = match.params._id ? select.list.find(item => item._id === match.params._id) : select.list[0];

    setState({
      range: readFromQueryString("range", location.search),
      vehicle,
    });
  }

  onChangeVehicle(e) {
    this.props.setState({
      [e.target.name]: this.props.select.list.find(item => item._id === e.target.value),
    });
  }

  render() {
    // console.log("BookingCreateForm:render", this.props);
    const {formData, onSubmit, onChange, setState, user, select} = this.props;

    // TODO FIX ME
    // this is ugly but works
    if (!formData.vehicle) {
      return null;
    }

    const vehicle = select.list.find(item => item._id === formData.vehicle._id);

    return (
      <Form horizontal onSubmit={onSubmit}>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.booking.create" />
          </h4>
        </div>
        <Media>
          <Media.Left>
            <ImageViewer
              image={vehicle.image}
              placeholder="vehicle"
              title={vehicleToHtml(vehicle, true)}
            />
          </Media.Left>
          <Media.Body>
            <InputGroupValidation
              componentClass="select"
              name="vehicle"
              defaultValue={vehicle._id}
              onChange={::this.onChangeVehicle}
            >
              <option value="">Select...</option>
              {select.list.map(item => <option value={item._id} key={item._id}>{vehicleToText(item, true)}</option>)}
            </InputGroupValidation>
            <InputDateTimePickerGroupValidation
              name="range"
              defaultValue={formData.range}
              onChange={onChange}
              setState={setState}
              checkOutTime={user.organization.checkOutTime}
            />
            <InputGroupValidation
              type="number"
              name="price"
              defaultValue={formData.price}
              placeholder={price}
              onChange={onChange}
              required
            />
            <InputGroupValidation
              name="fullName"
              defaultValue={formData.fullName}
              placeholder={fullName}
              onChange={onChange}
              required
            />
            <InputGroup
              name="country"
              componentClass="select"
              defaultValue={formData.country}
              onChange={onChange}
            >
              <option value="">other</option>
              {Object.keys(countries).map(key => (
                <FormattedMessage key={key} id={`countries.${key}`}>
                  {formattedMessage => <option key={key} value={key}>{formattedMessage}</option>}
                </FormattedMessage>
              ))}
            </InputGroup>
            <InputGroupValidation
              type="email"
              name="email"
              defaultValue={formData.email}
              placeholder={email}
              onChange={onChange}
            />
            <InputGroupValidation
              name="phoneNumber"
              defaultValue={formData.phoneNumber}
              placeholder={phoneNumber}
              onChange={onChange}
            />
            <InputMarkdownGroup
              componentClass="textarea"
              name="notes"
              defaultValue={formData.notes}
              placeholder={notes}
              onChange={onChange}
            />
            <ButtonToolbar>
              <Button type="submit" className="pull-right">
                <FormattedMessage id="components.buttons.save" />
              </Button>
            </ButtonToolbar>
          </Media.Body>
        </Media>
      </Form>
    );
  }
}
