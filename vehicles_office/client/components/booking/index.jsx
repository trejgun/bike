import React, {Component} from "react";
import {Route, Switch} from "react-router";
import BookingList from "./list";
import BookingCreate from "./create";
import BookingUpdate from "./update";
import BookingRead from "./read";
import BookingQuick from "./quick";


export default class Bookings extends Component {
	render() {
		return (
			<Switch>
				<Route path="/bookings" component={BookingList} exact />
				<Route path="/bookings/create" component={BookingCreate} />
				<Route path="/bookings/:_id" component={BookingRead} exact />
				<Route path="/bookings/:_id/edit" component={BookingUpdate} />
				<Route path="/bookings/:_id/:name" component={BookingQuick} />
			</Switch>
		);
	}
}
