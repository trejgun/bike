import React, {Component} from "react";
import PropTypes from "prop-types";
import moment from "moment";
import {Form, Image, ListGroupItem, Media} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import withListItemHelper from "../forms/withListItem";
import {listItemHeight, listItemWidth} from "../../constants/display";
import {humanReadable} from "../../constants/date";
import {vehicleToHtml} from "../../utils/vehicle";
import InputStaticGroup from "../inputs/input.static.group";


@withListItemHelper("bookings")
export default class BookingListItem extends Component {
  static propTypes = {
    formData: PropTypes.shape({
      _id: PropTypes.string,
      vehicle: PropTypes.shape({
        image: PropTypes.string,
      }),
      range: PropTypes.string,
      fullName: PropTypes.string,
      country: PropTypes.string,
      status: PropTypes.string,
    }),
  };

  render() {
    const {formData} = this.props;
    return (
      <LinkContainer key={formData._id} to={`/bookings/${formData._id}`}>
        <ListGroupItem>
          <Media>
            <Media.Left>
              <Image
                width={listItemWidth}
                height={listItemHeight}
                src={formData.vehicle.image || `${process.env.MODULE_CDN}/img/icons/booking.png`}
                className="media-object img-thumbnail"
                alt="Image"
              />
            </Media.Left>
            <Media.Body>
              <Media.Heading>{moment.range(formData.range).format(humanReadable)}</Media.Heading>
              <Form horizontal>
                <InputStaticGroup
                  name="vehicle"
                  value={vehicleToHtml(formData.vehicle, true)}
                />
                <InputStaticGroup
                  name="renter"
                  value={`${formData.fullName} ${formData.country ? `(${formData.country})` : ""}`}
                />
                <InputStaticGroup
                  name="status"
                  value={formData.status}
                />
              </Form>
            </Media.Body>
          </Media>
        </ListGroupItem>
      </LinkContainer>
    );
  }
}
