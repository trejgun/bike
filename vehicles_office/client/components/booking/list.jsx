import React, {Component} from "react";
import {Col, Glyphicon, Row} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import {Link} from "react-router-dom";
import Form from "./list.form";
import ListItem from "./list.item";
import Pagination from "../groups/pagination";
import withListHelper from "../forms/withList";


@withListHelper("bookings")
export default class BookingList extends Component {
  render() {
    return (
      <div>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.booking.list" />
            <Link to="/bookings/create">
              <Glyphicon glyph="plus" />
            </Link>
          </h4>
        </div>
        <Row>
          <Col sm={3}>
            <Form {...this.props} />
          </Col>
          <Col sm={9}>
            <ListItem {...this.props} />
            <Pagination {...this.props} />
          </Col>
        </Row>
      </div>
    );
  }
}
