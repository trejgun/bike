import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {injectIntl, intlShape} from "react-intl";
import Loader from "../partials/loader";
import {MESSAGE_ADD} from "../../constants/actions";


const storeName = "bookings";

@injectIntl
@connect(
  state => ({
    [storeName]: state[storeName],
  }),
)
export default class BookingQuickForm extends Component {
  static propTypes = {
    dispatch: PropTypes.func,

    match: PropTypes.shape({
      params: PropTypes.object.isRequired,
    }).isRequired,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
    intl: intlShape.isRequired,
  };

  componentDidMount() {
    this.props.dispatch({
      type: "FETCH_REQUESTED",
      action: `/bookings/${this.props.match.params._id}/${this.props.match.params.name}`,
      storeName,
      name: this.props.match.params.name,
    });
  }

  componentDidUpdate() {
    // console.log("BookingQuickForm:componentDidUpdate", nextProps);
    if (!this.props[storeName].isLoading && this.props[storeName].name === this.props.match.params.name) {
      if (this.props[storeName].success) {
        this.props.dispatch({
          type: MESSAGE_ADD,
          data: {
            type: "success",
            message: `bookind-successfully-${this.props.match.params.name}ed`,
          },
        });
      }
      this.props.history.push("/message");
    }
  }

  render() {
    return (
      <Loader />
    );
  }
}
