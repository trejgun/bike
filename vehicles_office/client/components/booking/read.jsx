import React, {Component} from "react";
import PropTypes from "prop-types";
import {Form, Glyphicon, Media} from "react-bootstrap";
import {Link} from "react-router-dom";
import moment from "moment";
import {FormattedMessage} from "react-intl";
import {humanReadable} from "../../constants/date";
import {vehicleToHtml} from "../../utils/vehicle";
import withDeleteForm from "../forms/withDeleteForm";
import ImageViewer from "../imageViewer";
import InputStaticGroup from "../inputs/input.static.group";


@withDeleteForm("bookings")
export default class BookingReadForm extends Component {
  static propTypes = {
    onDelete: PropTypes.func,

    formData: PropTypes.shape({
      _id: PropTypes.string,
      vehicle: PropTypes.object,
      bookingId: PropTypes.string,
      range: PropTypes.string,
      price: PropTypes.number,
      fullName: PropTypes.string,
      email: PropTypes.string,
      phoneNumber: PropTypes.string,
      status: PropTypes.string,
      isOffline: PropTypes.bool,
      country: PropTypes.string,
    }),
  };

  render() {
    // console.log("BookingReadForm:render", this.props);
    const {formData, onDelete} = this.props;
    return (
      <Form horizontal>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.booking.read" />
            {formData.status === "accepted" && formData.isOffline ? // offline `accepted` can be edited
              <Link to={`/bookings/${formData._id}/edit`}>
                <Glyphicon glyph="edit" />
              </Link>
              : null}
            {formData.status === "accepted" ? // any `accepted` can be cancelled
              <Link to={`/bookings/${formData._id}/delete`} onClick={onDelete}>
                <Glyphicon glyph="trash" />
              </Link>
              : null}
            {formData.status === "new" ? // any `new` can be accepted
              <Link to={`/bookings/${formData._id}/accept`}>
                <Glyphicon glyph="ok" />
              </Link>
              : null}
            {formData.status === "new" ? // any `new` can be rejected
              <Link to={`/bookings/${formData._id}/reject`} onClick={onDelete}>
                <Glyphicon glyph="remove" />
              </Link>
              : null}
          </h4>
        </div>
        <Media className="view">
          <Media.Left>
            <ImageViewer
              image={formData.vehicle.image}
              placeholder="vehicle"
              title={vehicleToHtml(formData.vehicle, false)}
            />
          </Media.Left>
          <Media.Body>
            <InputStaticGroup
              name="bookingId"
              value={formData.bookingId}
            />
            <InputStaticGroup
              name="vehicle"
              value={<Link to={`/vehicles/${formData.vehicle._id}`}>{vehicleToHtml(formData.vehicle, true)}</Link>}
            />
            <InputStaticGroup
              name="range"
              value={moment.range(formData.range).format(humanReadable)}
            />
            <InputStaticGroup
              name="price"
              value={`${formData.price} 000,00Rp`}
            />
            <InputStaticGroup
              name="fullName"
              value={formData.fullName}
            />
            <InputStaticGroup
              name="country"
              value={<FormattedMessage id={`countries.${formData.country}`} />}
            />
            {formData.email ?
              <InputStaticGroup
                name="email"
                value={formData.email}
              /> : null}
            {formData.phoneNumber ?
              <InputStaticGroup
                name="phoneNumber"
                value={formData.phoneNumber}
              /> : null}
            <InputStaticGroup
              name="status"
              value={formData.status}
            />
          </Media.Body>
        </Media>
      </Form>
    );
  }
}
