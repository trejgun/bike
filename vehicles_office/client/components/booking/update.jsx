import React, {Component} from "react";
import PropTypes from "prop-types";
import {FormattedMessage} from "react-intl";
import {Button, ButtonToolbar, Form, Media} from "react-bootstrap";
import {vehicleToHtml, vehicleToText} from "../../utils/vehicle";
import countries from "../../intl/countries.en.json";
import {email, fullName, notes, phoneNumber, price} from "../../constants/placeholder";
import withStore from "../forms/withStore";
import withUpdateForm from "../forms/withUpdateForm";
import withTypeaheadForm from "../forms/withTypeaheadForm";
import InputGroup from "../inputs/input.group";
import InputGroupValidation from "../inputs/input.group.validation";
import InputDateTimePickerGroup from "../inputs/input.datetimepicker.group";
import InputMarkdownGroup from "../inputs/input.markdown.group";
import ImageViewer from "../imageViewer";
import {MESSAGE_ADD} from "../../constants/actions";


@withUpdateForm("bookings")
@withTypeaheadForm("vehicles", "select")
@withStore("user")
export default class BookingUpdateForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
    setState: PropTypes.func,
    formData: PropTypes.shape({
      fullName: PropTypes.string,
      email: PropTypes.string,
      range: PropTypes.string,
      price: PropTypes.number,
      vehicle: PropTypes.object,
      country: PropTypes.string,
      phoneNumber: PropTypes.string,
      notes: PropTypes.string,
      isOffline: PropTypes.bool,
    }),

    select: PropTypes.object,
    user: PropTypes.object,

    dispatch: PropTypes.func,

    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,

  };

  componentDidMount() {
    const {formData, dispatch, history} = this.props;
    if (!formData.isOffline) {
      dispatch({
        type: MESSAGE_ADD,
        data: {
          type: "success",
          message: "booking-not-editable",
        },
      });
      history.push("/message");
    }
  }

  onChangeVehicle(e) {
    this.props.onChange({
      target: {
        name: e.target.name,
        value: this.props.select.list.find(item => item._id === e.target.value),
      },
    });
  }

  render() {
    const {formData, onSubmit, onChange, setState, user, select} = this.props;

    const vehicle = select.list.find(item => item._id === formData.vehicle._id);
    return (
      <Form horizontal onSubmit={onSubmit}>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.booking.update" />
          </h4>
        </div>
        <Media>
          <Media.Left>
            <ImageViewer
              image={vehicle.image}
              placeholder="vehicle"
              title={vehicleToHtml(vehicle, true)}
            />
          </Media.Left>
          <Media.Body>
            <InputGroupValidation
              componentClass="select"
              name="vehicle"
              defaultValue={vehicle._id}
              onChange={::this.onChangeVehicle}
            >
              <option value="">Select...</option>
              {select.list.map(item => <option value={item._id} key={item._id}>{vehicleToText(item, true)}</option>)}
            </InputGroupValidation>
            <InputDateTimePickerGroup
              name="range"
              defaultValue={formData.range}
              onChange={onChange}
              setState={setState}
              checkOutTime={user.organization.checkOutTime}
            />
            <InputGroupValidation
              type="number"
              name="price"
              defaultValue={formData.price}
              placeholder={price}
              onChange={onChange}
              required
            />
            <InputGroupValidation
              name="fullName"
              defaultValue={formData.fullName}
              placeholder={fullName}
              onChange={onChange}
              required
            />
            <InputGroup
              name="country"
              componentClass="select"
              defaultValue={formData.country}
              onChange={onChange}
            >
              <option value="">other</option>
              {Object.keys(countries).map(key =>
                (<FormattedMessage key={key} id={`countries.${key}`}>
                  {formattedMessage => <option key={key} value={key}>{formattedMessage}</option>}
                </FormattedMessage>),
              )}
            </InputGroup>
            <InputGroupValidation
              type="email"
              name="email"
              defaultValue={formData.email}
              placeholder={email}
              onChange={onChange}
            />
            <InputGroupValidation
              name="phoneNumber"
              defaultValue={formData.phoneNumber}
              placeholder={phoneNumber}
              onChange={onChange}
            />
            <InputMarkdownGroup
              componentClass="textarea"
              name="notes"
              defaultValue={formData.notes}
              placeholder={notes}
              onChange={onChange}
            />
          </Media.Body>
          <ButtonToolbar>
            <Button type="submit" className="pull-right">
              <FormattedMessage id="components.buttons.save" />
            </Button>
          </ButtonToolbar>
        </Media>
      </Form>
    );
  }
}
