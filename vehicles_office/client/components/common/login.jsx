import "./login.less";
import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {withRouter} from "react-router";
import open from "../../utils/popup";
import {MESSAGE_ADD} from "../../constants/actions";


@withRouter
@connect(
  state => ({
    user: state.user,
  }),
)
export default class Login extends Component {
  static propTypes = {
    user: PropTypes.object,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
    dispatch: PropTypes.func,
  };

  componentDidMount() {
    window.addEventListener("message", ::this.onMessage, false);
  }

  componentDidUpdate() {
    const {user, history} = this.props;
    if (user) {
      history.push("/dashboard");
    }
  }

  componentWillUnmount() {
    window.removeEventListener("message", ::this.onMessage);
  }

  onMessage(event) {
    if (event.data.source === "oauth2") {
      if (event.data.message) {
        this.props.dispatch({
          type: MESSAGE_ADD,
          data: {
            type: "danger",
            message: event.data.message,
          },
        });
      } else {
        this.onLogin();
      }
    }
  }

  onLogin() {
    this.props.dispatch({
      type: "FETCH_REQUESTED",
      name: "sync",
      storeName: "user",
      action: "/sync",
    });
  }

  render() {
    return (
      <div className="text-center">
        <br /><br /><br />
        <a className="social facebook" href="#" onClick={open("/api/auth/facebook")} />
        <a className="social google" href="#" onClick={open("/api/auth/google")} />
        <a className="social purimotors" href="#" onClick={open("/api/auth/system")} />
      </div>
    );
  }
}
