import React, {Component} from "react";
import PropTypes from "prop-types";
import {Route, Switch} from "react-router";
import withStore from "../forms/withStore";

import CompanyUpdate from "./update";
import CompanyRead from "./read";
import CompanySettings from "./settings";


@withStore("user")
export default class Company extends Component {
  static propTypes = {
    storeName: PropTypes.string,
  };

  subRender(component) {
    return props => {
      Object.assign(props.match.params, {_id: this.props[this.props.storeName] && this.props[this.props.storeName].organization && this.props[this.props.storeName].organization._id});
      return React.createElement(component, props);
    };
  }

  subRender2(component) {
    return props => {
      Object.assign(props.match.params, {_id: this.props[this.props.storeName] && this.props[this.props.storeName].organization && this.props[this.props.storeName].organization && this.props[this.props.storeName].organization.settings._id});
      return React.createElement(component, props);
    };
  }

  render() {
    return (
      <Switch>
        <Route path="/company" render={this.subRender(CompanyRead)} exact />
        <Route path="/company/edit" component={this.subRender(CompanyUpdate)} />
        <Route path="/company/settings" component={this.subRender2(CompanySettings)} />
      </Switch>
    );
  }
}
