import React from "react";
import {GoogleMap, Marker, withGoogleMap} from "react-google-maps";
import withScriptjs from "react-google-maps/lib/withScriptjs";


export default withScriptjs(withGoogleMap(props => {
	// console.log("GoogleMap", props);
	return (
		<GoogleMap
			defaultZoom={10}
			defaultCenter={props.defaultCenter}
		>
			<Marker position={props.defaultCenter} />
		</GoogleMap>
	);
}));
