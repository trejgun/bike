import React, {Component} from "react";
import PropTypes from "prop-types";
import {Form, Glyphicon, Media} from "react-bootstrap";
import {Link} from "react-router-dom";
import {FormattedMessage} from "react-intl";
import withReadForm from "../forms/withReadForm";
import ImageViewer from "../imageViewer";
import InputStaticGroup from "../inputs/input.static.group";
import InputStaticMapGroup from "../inputs/input.static.map.group";
import CompanyTabs from "./tabs";


@withReadForm("organizations")
export default class CompanyReadForm extends Component {
  static propTypes = {
    formData: PropTypes.shape({
      image: PropTypes.string,
      companyName: PropTypes.string,
      phoneNumber: PropTypes.string,
      email: PropTypes.string,
      domainName: PropTypes.string,
      address: PropTypes.string,
      coordinates: PropTypes.arrayOf(PropTypes.number),
    }),
  };

  render() {
    // console.log("CompanyReadForm:render", this.props);
    const {formData} = this.props;
    return (
      <Form horizontal>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.company.read" />
            <Link to="/company/edit">
              <Glyphicon glyph="edit" />
            </Link>
          </h4>

          <CompanyTabs />
        </div>
        <Media className="view">
          <Media.Left>
            <ImageViewer
              image={formData.image}
              placeholder="organization"
              title={<span>{formData.companyName}</span>}
            />
          </Media.Left>
          <Media.Body>

            <InputStaticGroup
              name="companyName"
              value={formData.companyName}
            />
            <InputStaticGroup
              name="email"
              value={formData.email}
            />
            <InputStaticGroup
              name="phoneNumber"
              value={formData.phoneNumber}
            />
            <InputStaticGroup
              name="domainName"
              value={<a href={`http://${formData.domainName}`}>{formData.domainName}</a>}
            />
            <InputStaticMapGroup
              name="address"
              defaultValue={formData.address}
              coordinates={formData.coordinates}
              onChange={() => {
              }}
            />
          </Media.Body>
        </Media>
      </Form>
    );
  }
}
