import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, ButtonToolbar, Checkbox, Col, Form, FormGroup, Media} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import withUpdateForm from "../forms/withUpdateForm";
import {viewItemLabel, viewItemValue} from "../../constants/display";
import CompanyTabs from "./tabs";


@withUpdateForm("settings")
export default class CompanySettingsForm extends Component {
  static propTypes = {
    storeName: PropTypes.string,

    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
    formData: PropTypes.shape({
      docs: PropTypes.bool,
    }),

    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };

  componentDidUpdate() {
    // console.log("CompanySettingsForm:componentWillReceiveProps", nextProps);
    const {storeName, history} = this.props;
    if (!this.props[storeName].isLoading && this.props[storeName].success && this.props[storeName].name === "update") {
      history.push("/company");
    }
  }

  render() {
    // console.log("CompanySettingsForm:render", this.props);
    const {onSubmit, formData, onChange} = this.props;
    return (
      <Form horizontal onSubmit={onSubmit}>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.company.settings" />
          </h4>

          <CompanyTabs />
        </div>

        <Media>
          <Media.Body>
            <FormGroup>
              <Col xsOffset={viewItemLabel} xs={viewItemValue}>
                <Checkbox name="docs" defaultChecked={formData.docs} onChange={onChange}>
                  <FormattedMessage id="components.settings.docs" />
                </Checkbox>
              </Col>
            </FormGroup>
            <ButtonToolbar>
              <Button type="submit" className="pull-right">
                <FormattedMessage id="components.buttons.save" />
              </Button>
            </ButtonToolbar>
          </Media.Body>
        </Media>
      </Form>
    );
  }
}
