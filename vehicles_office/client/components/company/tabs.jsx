import React, {Component} from "react";
import {MenuItem, Nav, NavDropdown, NavItem} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import {LinkContainer} from "react-router-bootstrap";


export default class CompanyTabs extends Component {
	render() {
		// console.log("CompanyTabs:render", this.props);
		return (
			<Nav bsStyle="tabs" id="uncontrolled-tab-example" className="nav-rtl">
				<LinkContainer to="/company" exact>
					<NavItem eventKey="1">
						<FormattedMessage id="components.tabs.company.general" />
					</NavItem>
				</LinkContainer>
				<LinkContainer to="/company/settings">
					<NavItem eventKey="2">
						<FormattedMessage id="components.tabs.company.settings" />
					</NavItem>
				</LinkContainer>
				<NavDropdown
					eventKey="3"
					title={<FormattedMessage id="components.tabs.company.policies" />}
					id="nav-dropdown-within-tab"
				>
					<LinkContainer to="/policies/delivery">
						<MenuItem eventKey="3.3">
							<FormattedMessage id="components.tabs.company.delivery" />
						</MenuItem>
					</LinkContainer>
					<LinkContainer to="/policies/return">
						<MenuItem eventKey="3.1">
							<FormattedMessage id="components.tabs.company.return" />
						</MenuItem>
					</LinkContainer>
					<LinkContainer to="/policies/island">
						<MenuItem eventKey="3.2">
							<FormattedMessage id="components.tabs.company.island" />
						</MenuItem>
					</LinkContainer>
				</NavDropdown>
			</Nav>
		);
	}
}
