import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, ButtonToolbar, Form, Media} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import withUpdateForm from "../forms/withUpdateForm";
import {address, companyName, domainName, email, phoneNumber} from "../../constants/placeholder";
import InputImage from "../inputs/input.image";
import InputMapGroup from "../inputs/input.map.group";
import InputGroupValidation from "../inputs/input.group.validation";
import InputAddonsGroupValidation from "../inputs/input.addons.group.validation";


@withUpdateForm("organizations")
export default class CompanyUpdateForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
    setState: PropTypes.func,
    formData: PropTypes.shape({
      email: PropTypes.string,
      companyName: PropTypes.string,
      domainName: PropTypes.string,
      address: PropTypes.string,
      image: PropTypes.string,
      phoneNumber: PropTypes.string,
      coordinates: PropTypes.arrayOf(PropTypes.number).isRequired,
    }),

    storeName: PropTypes.string,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
      replace: PropTypes.func.isRequired,
    }).isRequired,
  };

  componentDidUpdate() {
    // console.log("CompanySettingsForm:componentWillReceiveProps", nextProps);
    const {storeName, history} = this.props;
    if (!this.props[storeName].isLoading && this.props[storeName].success && this.props[storeName].name === "update") {
      history.push("/company");
    }
  }

  render() {
    // console.log("CompanyUpdateForm:render", this.props)
    const {onSubmit, onChange, formData, setState} = this.props;
    return (
      <Form horizontal onSubmit={onSubmit}>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.company.update" />
          </h4>
        </div>
        <Media>
          <Media.Left>
            <InputImage
              folder="organization"
              name="image"
              image={formData.image}
              onChange={onChange}
            />
          </Media.Left>
          <Media.Body>
            <InputGroupValidation
              name="companyName"
              defaultValue={formData.companyName}
              placeholder={companyName}
              onChange={onChange}
              required
            />
            <InputGroupValidation
              type="email"
              name="email"
              defaultValue={formData.email}
              placeholder={email}
              onChange={onChange}
            />
            <InputGroupValidation
              name="phoneNumber"
              defaultValue={formData.phoneNumber}
              placeholder={phoneNumber}
              onChange={onChange}
            />
            <InputAddonsGroupValidation
              name="domainName"
              defaultValue={formData.domainName}
              placeholder={domainName}
              onChange={onChange}
              before="http://"
            />
            <InputMapGroup
              name="address"
              defaultValue={formData.address}
              coordinates={formData.coordinates}
              placeholder={address}
              onChange={setState}
            />
            <ButtonToolbar>
              <Button type="submit" className="pull-right">
                <FormattedMessage id="components.buttons.save" />
              </Button>
            </ButtonToolbar>
          </Media.Body>
        </Media>
      </Form>
    );
  }
}
