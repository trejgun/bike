import React, {Component} from "react";
import PropTypes from "prop-types";
import {ListGroup, Row, Well} from "react-bootstrap";
import withLoader from "./withLoader";
import withStore from "./withStore";


export default function withListItemHelper(storeName) {
  return ListItem => {
    class ListItemHelper extends Component {
      static propTypes = {
        storeName: PropTypes.string,
        formData: PropTypes.shape({
          limit: PropTypes.number,
          view: PropTypes.string,
        }),
      };

      render() {
        // console.log("ListItemHelper:render", this.props);
        const {storeName, formData} = this.props;
        if (this.props[storeName].list.length) {
          const List = formData.view === "list" ? ListGroup : Row;
          return (
            <List className={`${storeName}-list`}>
              {this.props[storeName].list.slice(0, formData.limit).map((item, i) => (
                <ListItem
                  {...this.props}
                  key={i}
                  storeName={storeName}
                  formData={item}
                />
              ))}
            </List>
          );
        } else {
          return (
            <Well className="text-center">Nothing to display</Well>
          );
        }
      }
    }

    return withStore(storeName)(withLoader(storeName)(ListItemHelper));
  };
}

