import React, {Component} from "react";
import PropTypes from "prop-types";
import {Col, ControlLabel, FormControl, FormGroup} from "react-bootstrap";
import {FormattedMessage} from "react-intl";


export default class LimitGroup extends Component {
  static propTypes = {
    onChange: PropTypes.func,
    defaultValue: PropTypes.number,
  };

  render() {
    const {defaultValue, onChange} = this.props;
    return (
      <FormGroup>
        <Col componentClass={ControlLabel}>
          <FormattedMessage id="fields.limit.label" />
        </Col>
        <FormControl
          componentClass="select"
          name="limit"
          type="number"
          defaultValue={defaultValue}
          onChange={onChange}
        >
          <option value="12">12</option>
          <option value="24">24</option>
          <option value="48">48</option>
          <option value="96">96</option>
        </FormControl>
      </FormGroup>
    );
  }
}
