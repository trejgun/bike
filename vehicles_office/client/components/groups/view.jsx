import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, ButtonGroup, FormGroup, Glyphicon} from "react-bootstrap";
import {FormattedMessage} from "react-intl";


export default class ViewGroup extends Component {
  static propTypes = {
    view: PropTypes.string,
    setState: PropTypes.func,
  };

  onClick(e) {
    return this.props.setState({
      view: e.currentTarget.value,
    });
  }

  render() {
    // console.log("ViewGroup:render", this.props);
    return (
      <FormGroup>
        <ButtonGroup justified className="btn-group-view">
          <Button value="list" name="view" onClick={::this.onClick} active={this.props.view === "list"}>
            <Glyphicon glyph="th-list" />
            {" "}
            <FormattedMessage id="fields.list.label" />
          </Button>
          <Button value="table" name="view" onClick={::this.onClick} active={this.props.view === "table"}>
            <Glyphicon glyph="th" />
            {" "}
            <FormattedMessage id="fields.table.label" />
          </Button>
        </ButtonGroup>
      </FormGroup>
    );
  }
}
