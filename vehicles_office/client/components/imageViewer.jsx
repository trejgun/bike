import React, {Component} from "react";
import PropTypes from "prop-types";
import {Image, Modal} from "react-bootstrap";
import {Link} from "react-router-dom";
import "./imageViewer.less";
import {imagePreviewHeight, imagePreviewWidth} from "../constants/display";


export default class ImageViewer extends Component {
  static propTypes = {
    image: PropTypes.string,
    placeholder: PropTypes.string,
    title: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.element,
    ]),
  };

  state = {
    show: false,
  };

  onHide() {
    this.setState({show: false});
  }

  onShow(e) {
    e.preventDefault();
    if (this.props.image) {
      this.setState({show: true});
    }
  }

  render() {
    return (
      <div className="media-object img-thumbnail input-image">
        <Modal show={this.state.show} onHide={::this.onHide} dialogClassName="auto-modal">
          <Modal.Header closeButton>
            <Modal.Title>{this.props.title}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Image
              src={this.props.image}
              className="media-object img-thumbnail"
              alt="Image"
            />
          </Modal.Body>
        </Modal>
        <Link onClick={::this.onShow} to={this.props.image ? "#" : ""}>
          <Image
            style={this.props.image ? {cursor: "zoom-in"} : {}}
            width={imagePreviewWidth}
            height={imagePreviewHeight}
            src={this.props.image || `${process.env.MODULE_CDN}/img/icons/${this.props.placeholder}.png`}
            alt="Image"
          />
        </Link>
      </div>
    );
  }
}

