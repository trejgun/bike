import InputDateTimePickerGroup from "./input.datetimepicker.group";
import withValidation from "./withValidation";


export default withValidation(InputDateTimePickerGroup);
