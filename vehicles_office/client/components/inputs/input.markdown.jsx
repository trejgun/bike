import Input from "./input";
import withMarkdown from "./withMarkdown";


export default withMarkdown(Input);
