/* global google */
import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import geolocation from "./map/geolocation";
import Map from "./map/map";
import {MESSAGE_ADD} from "../../constants/actions";


export default function withMap(Input) {
  @connect()
  class InputMap extends Component {
    static propTypes = {
      onChange: PropTypes.func,
      name: PropTypes.string,
      defaultValue: PropTypes.string,
      coordinates: PropTypes.arrayOf(PropTypes.number),
      dispatch: PropTypes.func,
    };

    state = {
      address: this.props.defaultValue,
      coordinates: {
        lat: this.props.coordinates[1],
        lng: this.props.coordinates[0],
      },
    };

    componentDidMount() {
      // console.log("InputMap:componentDidMount")
      geolocation.getCurrentPosition(position => {
        this.setState({
          lat: position.coords.latitude,
          lng: position.coords.longitude,
        }, this.onSetState);
      }, error => {
        console.error(error);
        this.props.dispatch({
          type: MESSAGE_ADD,
          data: {
            type: "danger",
            message: `The Geolocation service failed (${error.message}).`,
          },
        });
      });
    }

    onSetState() {
      this.props.onChange({
        address: this.state.address,
        coordinates: [this.state.coordinates.lng, this.state.coordinates.lat],
      });
    }

    onPlacesChanged(places) {
      if (places.length) {
        this.setState({
          address: places[0].formatted_address,
          // address: places[0].address_components.map(component => component.long_name),
          coordinates: {
            lat: places[0].geometry.location.lat(),
            lng: places[0].geometry.location.lng(),
          },
        }, this.onSetState);
      }
    }

    onMapMounted(map) {
      this._map = map;
    }

    onBoundsChanged() {
      const latLng = this._map.getCenter();

      clearTimeout(this._timeout);
      this._timeout = setTimeout(() => {
        if (latLng.lat() === 0 && latLng.lng() === 0) {
          return;
        }
        const geocoder = new google.maps.Geocoder();
        geocoder.geocode({location: latLng}, (results, status) => {
          if (status === "OK") {
            if (results[0]) {
              this.setState({
                address: results[0].formatted_address,
              }, this.onSetState);
            } else {
              this.props.dispatch({
                type: MESSAGE_ADD,
                data: {
                  type: "danger",
                  message: "No results found",
                },
              });
            }
          } else {
            this.props.dispatch({
              type: MESSAGE_ADD,
              data: {
                type: "danger",
                message: `Geocoder failed due to: ${status}`,
              },
            });
          }
        });
      }, 500);

      this.setState({
        coordinates: {
          lat: latLng.lat(),
          lng: latLng.lng(),
        },
      }, this.onSetState);
    }

    onChange(e) {
      this.setState({
        address: e.target.value,
      }, this.onSetState);
    }

    render() {
      // TODO move google map key to config
      return (
        <div>
          <Input name={this.props.name} defaultValue={this.state.address} onChange={::this.onChange} />
          <Map
            googleMapURL="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDE04gHh_7770gDIvqX-euawlW7IWEtKYI&libraries=places"
            loadingElement={
              <div style={{height: "100%", width: "100%"}} />
            }
            containerElement={
              <div style={{height: "400px", width: "100%", marginTop: "10px"}} />
            }
            mapElement={
              <div style={{height: "400px", width: "100%"}} />
            }

            center={this.state.coordinates}

            onMapMounted={::this.onMapMounted}
            onBoundsChanged={::this.onBoundsChanged}
            onPlacesChanged={::this.onPlacesChanged}
          />
        </div>
      );
    }
  }

  return InputMap;
}
