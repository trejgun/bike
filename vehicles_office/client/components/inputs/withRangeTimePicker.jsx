import React, {Component} from "react";
import PropTypes from "prop-types";
import DateTimeRangePicker from "react-bootstrap-daterangepicker";
import moment from "moment";
import {Button, Glyphicon, InputGroup} from "react-bootstrap";
import {checkOutTime, dateTimeFormat, rangeSeparator} from "../../constants/date";


export default function withRangeTimePicker(Input) {
  return class DateAndTime extends Component {
    static propTypes = {
      setState: PropTypes.func,
      value: PropTypes.string,
      name: PropTypes.string,
      checkOutTime: PropTypes.string,
    };

    state = {
      startDate: null,
      endDate: null,
    };

    componentDidMount() {
      // console.log("DateAndTime:componentDidMount", this.props);
      const [checkOutHours, checkOutMinutes] = (this.props.checkOutTime || checkOutTime).split(":");
      const date = moment().startOf("day").add(checkOutHours, "h").add(checkOutMinutes, "m");
      const value = this.props.value ? moment.range(this.props.value) : moment.range(date, date.clone().add(1, "d"));
      this.setState({
        startDate: value.start,
        endDate: value.end,
      }, this.onSetState);
    }

    handleEvent(event, picker) {
      // console.log("DateAndTime:handleEvent")
      this.setState({
        startDate: picker.startDate,
        endDate: picker.endDate,
      }, this.onSetState);
    }

    onSetState() {
      // console.log("DateAndTime:onSetState", this.state);
      this.props.setState({
        [this.props.name]: moment.range(this.state.startDate, this.state.endDate).toString(),
      });
    }

    render() {
      // console.log("DateAndTime:render")
      const {startDate, endDate} = this.state;

      if (!startDate || !endDate) {
        return null;
      }

      const start = startDate.format(dateTimeFormat);
      const end = endDate.format(dateTimeFormat);

      const locale = {
        format: dateTimeFormat,
        separator: rangeSeparator,
        applyLabel: "Apply",
        cancelLabel: "Cancel",
        weekLabel: "W",
        customRangeLabel: "Custom Range",
        daysOfWeek: moment.weekdaysMin(),
        monthNames: moment.monthsShort(),
        firstDay: moment.localeData().firstDayOfWeek(),
      };

      return (
        <DateTimeRangePicker
          timePicker
          timePicker24Hour
          showDropdowns
          locale={locale}
          startDate={this.state.startDate}
          endDate={this.state.endDate}
          autoApply
          onEvent={::this.handleEvent}
          containerStyles={{}}
        >
          <InputGroup>
            <Input value={`${start}${rangeSeparator}${end}`} onChange={Function.prototype} />
            <InputGroup.Button>
              <Button className="default date-range-toggle">
                <Glyphicon glyph="calendar" />
              </Button>
            </InputGroup.Button>
          </InputGroup>
        </DateTimeRangePicker>
      );
    }
  };
}
