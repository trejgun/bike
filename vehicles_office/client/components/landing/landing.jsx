import React, {Component} from "react";
import PropTypes from "prop-types";
import {withStyles} from "@material-ui/core";

import {companyName} from "../../constants/misc";
import LandingFooter from "./landing/footer";
import LandingBanner from "./landing/banner";
import LandingHeader from "./landing/header";

import "./landing.less";
import styles from "./styles";

@withStyles(styles)
export default class Landing extends Component {
  static propTypes = {
    classes: PropTypes.object,
  };

  render() {
    const {classes} = this.props;
    return (
      <div className={classes.root}>
        <LandingHeader>
          <h1>{companyName}</h1>
          <h3>Accounting made simple</h3>
        </LandingHeader>

        <div className="content-section-a">
          <div className="container">
            <div className="row">
              <div className="col-lg-5 col-sm-6">
                <hr className="section-heading-spacer" />
                <div className="clearfix" />
                <h2 className="section-heading">Remove your fear of accounting</h2>
                <p className="lead">
                  Hear how people with a real fear of accounting and numbers were able to overcome
                  their mental block and start making better decisions on financial statements.
                </p>
              </div>
              <div className="col-lg-5 col-lg-offset-2 col-sm-6">
                <img className="img-responsive"
                     src={`${process.env.MODULE_CDN}/img/landing/macbook.png`}
                     alt="" />
              </div>
            </div>

          </div>
        </div>

        <div className="content-section-b">
          <div className="container">
            <div className="row">
              <div className="col-lg-5 col-lg-offset-1 col-sm-push-6  col-sm-6">
                <hr className="section-heading-spacer" />
                <div className="clearfix" />
                <h2 className="section-heading">Effective data visualization</h2>
                <p className="lead">
                  {companyName} is an all-in-one business dashboard app that helps you easily monitor
                  all your business data from one place
                </p>
              </div>
              <div className="col-lg-5 col-sm-pull-6  col-sm-6">
                <img className="img-responsive" src={`${process.env.MODULE_CDN}/img/landing/ipad.png`}
                     alt="" />
              </div>
            </div>
          </div>
        </div>

        <div className="content-section-a">
          <div className="container">
            <div className="row">
              <div className="col-lg-5 col-sm-6">
                <hr className="section-heading-spacer" />
                <div className="clearfix" />
                <h2 className="section-heading">Simple to use</h2>
                <p className="lead">
                  No installation required to use this app, it runs from any device
                  and works same good on computers, laptops, tablets and phones
                  due to responsive design.
                </p>
                <p className="lead">
                  No registration required, you can log in using your favourite social network
                </p>
              </div>
              <div className="col-lg-5 col-lg-offset-2 col-sm-6">
                <img className="img-responsive" src={`${process.env.MODULE_CDN}/img/landing/phones.png`}
                     alt="" />
              </div>
            </div>
          </div>
        </div>

        <LandingBanner>
          <h2>Old era of paper accounting is over!</h2>
        </LandingBanner>

        <LandingFooter />
      </div>
    );
  }
}
