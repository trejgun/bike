import React, {Component} from "react";
import PropTypes from "prop-types";
import "./header.less";


export default class LandingHeader extends Component {
  static propTypes = {
    children: PropTypes.array,
  };

  render() {
    return (
      <header className="intro-header"
              style={{backgroundImage: `url("${process.env.MODULE_CDN}/img/landing/intro-bg.jpg")`}}>
        <div className="container">
          <div className="row">
            <div className="col-lg-12">
              <div className="intro-message">
                {this.props.children}
              </div>
            </div>
          </div>
        </div>
      </header>
    );
  }
}
