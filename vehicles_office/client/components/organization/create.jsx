import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, ButtonToolbar, Form, Media} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import withCreate from "../forms/withCreateForm";
import {email, fullName, password, phoneNumber} from "../../constants/placeholder";
import InputImage from "../inputs/input.image";
import InputGroupValidation from "../inputs/input.group.validation";
import LanguageGroup from "../groups/language";


@withCreate("users")
export default class OrganizationCreateForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
    formData: PropTypes.shape({
      image: PropTypes.string,
      fullName: PropTypes.string,
      email: PropTypes.string,
      language: PropTypes.string,
      phoneNumber: PropTypes.string,
      password: PropTypes.string,
    }),
  };

  render() {
    // console.log("UserCreateForm:render", this.props);
    const {onSubmit, formData, onChange} = this.props;
    return (
      <Form horizontal autoComplete="false" onSubmit={onSubmit}>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.user.create" />
          </h4>
        </div>
        <Media>
          <Media.Left>
            <InputImage
              folder="user"
              name="image"
              image={formData.image}
              onChange={onChange}
            />
          </Media.Left>
          <Media.Body>
            <InputGroupValidation
              name="email"
              type="email"
              defaultValue={formData.email}
              placeholder={email}
              onChange={onChange}
              required
            />
            <InputGroupValidation
              name="password"
              type="password"
              defaultValue={formData.password}
              placeholder={password}
              onChange={onChange}
              required
              autoComplete="new-password"
            />
            <InputGroupValidation
              name="fullName"
              defaultValue={formData.fullName}
              placeholder={fullName}
              onChange={onChange}
              required
            />
            <InputGroupValidation
              name="phoneNumber"
              defaultValue={formData.phoneNumber}
              placeholder={phoneNumber}
              onChange={onChange}
            />
            <LanguageGroup
              defaultValue={formData.language}
              onChange={onChange}
            />
            <ButtonToolbar>
              <Button type="submit" className="pull-right">
                <FormattedMessage id="components.buttons.save" />
              </Button>
            </ButtonToolbar>
          </Media.Body>
        </Media>
      </Form>
    );
  }
}
