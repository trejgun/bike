import React, {Component} from "react";
import {Route, Switch} from "react-router";
import OrganizationUpdate from "../company/update";
import OrganizationCreate from "./create";
import OrganizationList from "./list";
import OrganizationRead from "../company/read";


export default class Organization extends Component {
	render() {
		return (
			<Switch>
				<Route path="/organizations" component={OrganizationList} exact />
				<Route path="/organizations/create" component={OrganizationCreate} />
				<Route path="/organizations/:_id" component={OrganizationRead} exact />
				<Route path="/organizations/:_id/edit" component={OrganizationUpdate} />
			</Switch>
		);
	}
}
