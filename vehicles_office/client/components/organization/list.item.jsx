import React, {Component} from "react";
import PropTypes from "prop-types";
import {Image, ListGroupItem, Media} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import {listItemHeight, listItemWidth} from "../../constants/display";
import withListItemHelper from "../forms/withListItem";
import withStore from "../forms/withStore";


@withListItemHelper("users")
@withStore("user")
export default class UserListItem extends Component {
  static propTypes = {
    formData: PropTypes.shape({
      _id: PropTypes.string,
      fullName: PropTypes.string,
      image: PropTypes.string,
    }),

    user: PropTypes.shape({
      _id: PropTypes.string,
    }),
  };

  render() {
    const {formData, user} = this.props;
    const type = user._id === formData._id ? "profile" : "user";
    return (
      <LinkContainer key={formData._id} to={type === "profile" ? "/profile/edit" : `/users/${formData._id}`}>
        <ListGroupItem>
          <Media>
            <Media.Left className="hidden-xs">
              <Image
                width={listItemWidth}
                height={listItemHeight}
                src={formData.image || `${process.env.MODULE_CDN}/img/icons/user.png`}
                className="media-object img-thumbnail"
                alt="Image"
              />
            </Media.Left>
            <Media.Body>
              <Media.Heading>{formData.fullName}</Media.Heading>
            </Media.Body>
          </Media>
        </ListGroupItem>
      </LinkContainer>
    );
  }
}
