import "./header.less";
import React, {Component} from "react";
import PropTypes from "prop-types";
import {connect} from "react-redux";
import {updateIntl} from "react-intl-redux";
import {Link} from "react-router-dom";
import {Image, MenuItem, Nav, Navbar, NavDropdown, NavItem} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import {FormattedMessage} from "react-intl";
import {withRouter} from "react-router";
import {companyName} from "../../constants/misc";
import {enabledLanguages} from "../../constants/language";
import getLocalization from "../../intl/setup";


@withRouter
@connect(
  state => ({
    user: state.user,
  }),
)
export default class Header extends Component {
  static propTypes = {
    user: PropTypes.object,

    dispatch: PropTypes.func,
  };

  onSelect(language) {
    const {dispatch} = this.props;
    dispatch(updateIntl(getLocalization(language)));
    document.documentElement.setAttribute("lang", language);
  }

  logout(e) {
    e.preventDefault();
    this.props.dispatch({
      type: "FETCH_REQUESTED",
      method: "GET",
      action: "/logout",
      storeName: "user",
      name: "logout",
    });
  }

  renderLogin() {
    return (
      <LinkContainer to="/login">
        <NavItem><FormattedMessage id="components.menu.login" /></NavItem>
      </LinkContainer>
    );
  }

  renderLinks() {
    const title = (
      <span>
				<Image src={this.props.user.image || `${process.env.MODULE_CDN}/img/icons/user.png`} circle height={25}
               width={25} />
				<span>{this.props.user.fullName}</span>
			</span>
    );
    switch (process.env.MODULE) {
      case "back_office":
        return (
          <NavDropdown title={title} id="main_menu">
            <LinkContainer to="/dashboard">
              <MenuItem><FormattedMessage id="components.menu.dashboard" /></MenuItem>
            </LinkContainer>
            <MenuItem divider />
            <MenuItem href={process.env.MODULE_VEHICLES_MARKET}>
              <FormattedMessage id="components.menu.vehicles_market" />
            </MenuItem>
            <MenuItem href={process.env.MODULE_VEHICLES_OFFICE}>
              <FormattedMessage id="components.menu.vehicles_office" />
            </MenuItem>
            <MenuItem divider />
            <LinkContainer onClick={::this.logout} to="/logout">
              <MenuItem><FormattedMessage id="components.menu.logout" /></MenuItem>
            </LinkContainer>
          </NavDropdown>
        );
      case "vehicles_office":
        return (
          <NavDropdown title={title} id="main_menu">
            <LinkContainer to="/dashboard" exact>
              <MenuItem><FormattedMessage id="components.menu.dashboard" /></MenuItem>
            </LinkContainer>
            <LinkContainer to="/profile" exact>
              <MenuItem><FormattedMessage id="components.menu.profile" /></MenuItem>
            </LinkContainer>
            <LinkContainer to="/company" exact>
              <MenuItem><FormattedMessage id="components.menu.organization" /></MenuItem>
            </LinkContainer>
            <MenuItem divider />
            <MenuItem href={process.env.MODULE_VEHICLES_MARKET}>
              <FormattedMessage id="components.menu.vehicles_market" />
            </MenuItem>
            <MenuItem divider />
            <LinkContainer onClick={::this.logout} to="/logout">
              <MenuItem><FormattedMessage id="components.menu.logout" /></MenuItem>
            </LinkContainer>
          </NavDropdown>
        );
      case "vehicles_market":
        return (
          <NavDropdown title={title} id="main_menu">
            <LinkContainer to="/dashboard">
              <MenuItem><FormattedMessage id="components.menu.dashboard" /></MenuItem>
            </LinkContainer>
            <LinkContainer to="/profile">
              <MenuItem><FormattedMessage id="components.menu.profile" /></MenuItem>
            </LinkContainer>
            <MenuItem divider />
            <LinkContainer onClick={::this.logout} to="/logout">
              <MenuItem><FormattedMessage id="components.menu.logout" /></MenuItem>
            </LinkContainer>
          </NavDropdown>
        );
      case "oauth2":
        return null;
      default:
        return null;
    }
  }

  renderLang() {
    return (
      <NavDropdown title={<FormattedMessage id="components.switchLanguage" />} id="lang_menu">
        {enabledLanguages.map(language =>
          (<MenuItem key={language} eventKey={language} onSelect={::this.onSelect}>
            <FormattedMessage id={`components.language.${language}`} />
          </MenuItem>),
        )}
      </NavDropdown>
    );
  }

  renderMenu() {
    return (
      <Nav navbar pullRight>
        {this.props.user ? null : this.renderLang()}
        {this.props.user ? this.renderLinks() : this.renderLogin()}
      </Nav>
    );
  }

  render() {
    return (
      <Navbar inverse>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to="/" className="navbar-brand">{companyName}</Link>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse href="#">
          {this.renderMenu()}
        </Navbar.Collapse>
      </Navbar>
    );
  }
}
