import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, ButtonToolbar, Form, Media} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import withUpdateForm from "../forms/withUpdateForm";
import InputGroupValidation from "../inputs/input.group.validation";
import InputMarkdownGroupValidation from "../inputs/input.markdown.group.validation";
import CompanyTabs from "../company/tabs";
import ImageViewer from "../imageViewer";


@withUpdateForm("policies", "type")
export default class PolicyDelivery extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
    formData: PropTypes.shape({
      description: PropTypes.string,
      allowed: PropTypes.string,
    }),
  };

  render() {
    const {onSubmit, formData, onChange} = this.props;
    return (
      <Form horizontal onSubmit={onSubmit}>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.policies.delivery" />
          </h4>

          <CompanyTabs />
        </div>
        <Media>
          <Media.Left>
            <ImageViewer
              placeholder="taxnservice"
              title="Policy"
            />
          </Media.Left>
          <Media.Body>
            <InputGroupValidation
              name="allowed"
              componentClass="select"
              defaultValue={formData.allowed}
              onChange={onChange}
            >
              <option value="free">For FREE</option>
              <option value="fee">For fee</option>
              <option value="not">No delivery</option>
            </InputGroupValidation>
            <InputMarkdownGroupValidation
              rows={10}
              componentClass="textarea"
              name="description"
              defaultValue={formData.description}
              placeholder={"..."}
              onChange={onChange}
            />
            <ButtonToolbar>
              <Button type="submit" className="pull-right">
                <FormattedMessage id="components.buttons.save" />
              </Button>
            </ButtonToolbar>
          </Media.Body>
        </Media>
      </Form>
    );
  }
}
