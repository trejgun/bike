import React, {Component} from "react";
import {Redirect, Route, Switch} from "react-router";

import PolicyDelivery from "./delivery";
import PolicyIsland from "./island";
import PolicyReturn from "./return";


export default class Policy extends Component {
	render() {
		return (
			<Switch>
				<Route path="/policies/:type/edit" render={props => {
					return React.createElement({
						delivery: PolicyDelivery,
						island: PolicyIsland,
						return: PolicyReturn,
					}[props.match.params.type], props);
				}} />
				<Route path="/policies/:type/" render={props => {
					return <Redirect to={`/policies/${props.match.params.type}/edit`} />;
				}} />
			</Switch>
		);
	}
}
