import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, ButtonToolbar, Form, Media} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import withUpdateForm from "../forms/withUpdateForm";
import InputGroup from "../inputs/input.group";
import InputMarkdownGroup from "../inputs/input.markdown.group";
import CompanyTabs from "../company/tabs";
import ImageViewer from "../imageViewer";


@withUpdateForm("policies", "type")
export default class PolicyIsland extends Component {
  static propTypes = {
    formData: PropTypes.shape({
      description: PropTypes.string,
      allowed: PropTypes.string,
    }),
    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
  };

  render() {
    const {onSubmit, formData, onChange} = this.props;
    return (
      <Form horizontal onSubmit={onSubmit}>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.policies.island" />
          </h4>

          <CompanyTabs />
        </div>
        <Media>
          <Media.Left>
            <ImageViewer
              placeholder="taxnservice"
              title="Policy"
            />
          </Media.Left>
          <Media.Body>
            <InputGroup
              name="allowed"
              componentClass="select"
              defaultValue={formData.allowed}
              onChange={onChange}
            >
              <option value="free">For FREE</option>
              <option value="fee">For fee</option>
              <option value="not">Not allowed</option>
            </InputGroup>
            <InputMarkdownGroup
              rows={10}
              componentClass="textarea"
              name="description"
              defaultValue={formData.description}
              placeholder={"..."}
              onChange={onChange}
            />
            <ButtonToolbar>
              <Button type="submit" className="pull-right">
                <FormattedMessage id="components.buttons.save" />
              </Button>
            </ButtonToolbar>
          </Media.Body>
        </Media>
      </Form>
    );
  }
}
