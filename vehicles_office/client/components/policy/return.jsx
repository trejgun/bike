import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, ButtonToolbar, Form, Media} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import withUpdateForm from "../forms/withUpdateForm";
import InputGroup from "../inputs/input.group";
import InputMarkdownGroup from "../inputs/input.markdown.group";
import CompanyTabs from "../company/tabs";
import ImageViewer from "../imageViewer";
// import {timeZones} from "../../../constants/date";


@withUpdateForm("policies", "type")
export default class PolicyReturn extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
    formData: PropTypes.shape({
      description: PropTypes.string,
      checkOutTime: PropTypes.string,
      // timeZone: PropTypes.string,
    }),
  };

  render() {
    // console.log("CompanyUpdateForm:render", this.props);
    const {onSubmit, formData, onChange} = this.props;
    return (
      <Form horizontal onSubmit={onSubmit}>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.policies.return" />
          </h4>

          <CompanyTabs />
        </div>
        <Media>
          <Media.Left>
            <ImageViewer
              placeholder="taxnservice"
              title="Policy"
            />
          </Media.Left>
          <Media.Body>
            <InputGroup
              name="checkOutTime"
              defaultValue={formData.checkOutTime}
              onChange={onChange}
              placeholder="22:00"
            />
            <InputMarkdownGroup
              rows={10}
              componentClass="textarea"
              name="description"
              defaultValue={formData.description}
              placeholder={"..."}
              onChange={onChange}
            />
            {/* <InputGroup
							name="timeZone"
							componentClass="select"
							value={this.props.timeZone}
							onChange={onChange}
						>
							{timeZones.map(timeZone =>
								<option key={timeZone} value={timeZone}>{timeZone}</option>
							)}
						</InputGroup> */}
            <ButtonToolbar>
              <Button type="submit" className="pull-right">
                <FormattedMessage id="components.buttons.save" />
              </Button>
            </ButtonToolbar>
          </Media.Body>
        </Media>
      </Form>
    );
  }
}
