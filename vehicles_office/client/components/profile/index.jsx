import React, {Component} from "react";
import PropTypes from "prop-types";
import {Route, Switch} from "react-router";
import withStore from "../forms/withStore";

import UserUpdate from "./update";
import UserRead from "./read";
import UserSubscriptions from "./subscriptions";


@withStore("user")
export default class UserProfile extends Component {
  static propTypes = {
    storeName: PropTypes.string,
  };

  subRender(component) {
    return props => {
      Object.assign(props.match.params, {_id: this.props[this.props.storeName]._id});
      return React.createElement(component, props);
    };
  }

  render() {
    return (
      <Switch>
        <Route path="/profile" exact render={this.subRender(UserRead)} />
        <Route path="/profile/edit" render={this.subRender(UserUpdate)} />
        <Route path="/profile/subscriptions" render={this.subRender(UserSubscriptions)} />
      </Switch>
    );
  }
}
