import React, {Component} from "react";
import PropTypes from "prop-types";
import {Form, Glyphicon, Media} from "react-bootstrap";
import {Link} from "react-router-dom";
import {FormattedMessage} from "react-intl";
import withReadForm from "../forms/withReadForm";
import withStore from "../forms/withStore";
import ImageViewer from "../imageViewer";
import InputStaticGroup from "../inputs/input.static.group";
import UserTabs from "../user/tabs.user";
import ProfileTabs from "../user/tabs.profile";


@withReadForm("users")
@withStore("user")
export default class UserReadForm extends Component {
  static propTypes = {
    formData: PropTypes.shape({
      _id: PropTypes.string,
      image: PropTypes.string,
      fullName: PropTypes.string,
      phoneNumber: PropTypes.string,
      email: PropTypes.string,
      language: PropTypes.string,
    }),

    user: PropTypes.shape({
      _id: PropTypes.string,
    }),
  };

  render() {
    const {user, formData} = this.props;
    const type = user._id === formData._id ? "profile" : "user";
    return (
      <Form horizontal>
        <div className="headline">
          <h4>
            <FormattedMessage id={`components.title.${type}.read`} />
            <Link to={type === "profile" ? "/profile/edit" : `/users/${formData._id}/edit`}>
              <Glyphicon glyph="edit" />
            </Link>
          </h4>
          {type === "profile" ? <ProfileTabs /> : <UserTabs _id={formData._id} />}
        </div>

        <Media className="view">
          <Media.Left>
            <ImageViewer
              image={formData.image}
              placeholder="user"
              title={<span>{formData.fullName}</span>}
            />
          </Media.Left>
          <Media.Body>
            <InputStaticGroup
              name="email"
              value={formData.email}
            />
            <InputStaticGroup
              name="password"
              value={"********"}
            />
            <InputStaticGroup
              name="fullName"
              value={formData.fullName}
            />
            <InputStaticGroup
              name="phoneNumber"
              value={formData.phoneNumber}
            />
            <InputStaticGroup
              name="language"
              value={<FormattedMessage id={`components.language.${formData.language}`} />}
            />
          </Media.Body>
        </Media>
      </Form>
    );
  }
}
