import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, ButtonToolbar, Checkbox, Col, Form, FormGroup, Media} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import withUpdateForm from "../forms/withUpdateForm";
import {viewItemLabel, viewItemValue} from "../../constants/display";
import ProfileTabs from "../user/tabs.profile";


const types = [
  "bookingCancelledByCustomer",
  "bookingRevokedByCustomer",
  "newBookingForRenter",
];

@withUpdateForm("subscription")
export default class UserSubscriptionsForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
    storeName: PropTypes.string,

    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };

  componentDidUpdate() {
    // console.log("UserSubscriptionsForm:componentWillReceiveProps", nextProps);
    const {storeName, history} = this.props;
    if (!this.props[storeName].isLoading && this.props[storeName].success && this.props[storeName].name === "update") {
      setTimeout(() => {
        history.replace("/profile");
      }, 0);
    }
  }

  render() {
    const {onSubmit, onChange} = this.props;
    return (
      <Form horizontal onSubmit={onSubmit}>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.profile.subscriptions" />
          </h4>
          <ProfileTabs />
        </div>

        <Media>
          <Media.Body>
            {types.map(type =>
              (<FormGroup key={type}>
                <Col xsOffset={viewItemLabel} xs={viewItemValue}>
                  <Checkbox name={type} defaultChecked={this.props[type]} onChange={onChange}>
                    <FormattedMessage id={`email.types.${type}`} />
                  </Checkbox>
                </Col>
              </FormGroup>),
            )}
          </Media.Body>
        </Media>
        <ButtonToolbar>
          <Button type="submit" className="pull-right">
            <FormattedMessage id="components.buttons.save" />
          </Button>
        </ButtonToolbar>
      </Form>
    );
  }
}
