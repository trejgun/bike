import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, ButtonToolbar, Form, Media} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import withUpdateForm from "../forms/withUpdateForm";
import withStore from "../forms/withStore";
import {fullName, phoneNumber} from "../../constants/placeholder";
import InputImage from "../inputs/input.image";
import InputGroupValidation from "../inputs/input.group.validation";
import InputStaticGroup from "../inputs/input.static.group";
import LanguageGroup from "../groups/language";
import open from "../../utils/popup";


@withStore("user")
@withUpdateForm("users")
export default class ProfileUpdateForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
    formData: PropTypes.shape({
      _id: PropTypes.string,
      image: PropTypes.string,
      fullName: PropTypes.string,
      email: PropTypes.string,
      language: PropTypes.string,
      phoneNumber: PropTypes.string,
    }),

    dispatch: PropTypes.func,
    paramName: PropTypes.string,
    storeName: PropTypes.string,
    user: PropTypes.object,
    users: PropTypes.object,

    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };

  componentDidMount() {
    // console.log("UserUpdateForm:componentDidMount");
    window.addEventListener("message", ::this.onMessage, false);
  }

  componentDidUpdate() {
    // console.log("ProfileUpdateForm:componentDidUpdate", this.props);
    const {history, dispatch, user} = this.props;

    if (!user) {
      history.push("/login");
    }

    if (!this.props["users"].isLoading && this.props["users"].success && this.props["users"].name === "update") {
      dispatch({
        type: "FETCH_REQUESTED",
        method: "GET",
        action: "/sync",
        storeName: "user",
        name: "sync",
      });
      setTimeout(() => {
        history.replace("/profile");
      }, 0);
    }
  }

  componentWillUnmount() {
    // console.log("UserUpdateForm:componentWillUnmount");
    window.removeEventListener("message", ::this.onMessage);
  }

  onMessage(event) {
    // console.log("UserUpdateForm:onMessage", event);
    if (event.data.source === "oauth2") {
      this.props.dispatch({
        type: "FETCH_REQUESTED",
        method: "GET",
        action: "/sync",
        storeName: "user",
        name: "sync",
      });
    }
  }

  render() {
    const {user, formData, onSubmit, onChange} = this.props;
    const type = user._id === formData._id ? "profile" : "user";

    return (
      <Form horizontal onSubmit={onSubmit}>
        <div className="headline">
          <h4>
            <FormattedMessage id={`components.title.${type}.update`} />
          </h4>
        </div>
        <Media>
          <Media.Left>
            <InputImage
              folder="user"
              name="image"
              image={formData.image}
              onChange={onChange}
            />
          </Media.Left>
          <Media.Body>
            {/* TODO hide links if not profile */}
            <InputStaticGroup
              name="email"
              value={<a href="#"
                        onClick={open(`${process.env.MODULE_OAUTH2}/email?module=${process.env.MODULE}&alert=change-email`)}>{formData.email}</a>}
            />
            <InputStaticGroup
              name="password"
              value={<a href="#"
                        onClick={open(`${process.env.MODULE_OAUTH2}/password?module=${process.env.MODULE}`)}>{"********"}</a>}
            />
            <InputGroupValidation
              name="fullName"
              defaultValue={formData.fullName}
              placeholder={fullName}
              onChange={onChange}
              required
            />
            <InputGroupValidation
              name="phoneNumber"
              defaultValue={formData.phoneNumber}
              placeholder={phoneNumber}
              onChange={onChange}
            />
            <LanguageGroup
              defaultValue={formData.language}
              onChange={onChange}
            />
            <ButtonToolbar>
              <Button type="submit" className="pull-right">
                <FormattedMessage id="components.buttons.save" />
              </Button>
            </ButtonToolbar>
          </Media.Body>
        </Media>
      </Form>
    );
  }
}
