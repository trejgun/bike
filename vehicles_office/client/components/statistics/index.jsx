import React, {Component} from "react";
import {Col, Row} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import OverviewPanel from "./overview.panel";


export default class Statistics extends Component {
	render() {
		return (
			<Row>
				<Col xs={12}>
					<div className="headline">
						<h4>
							<FormattedMessage id="components.title.statistics.overview" />
						</h4>
					</div>
				</Col>
				<Col xs={12} sm={6} lg={4}>
					<OverviewPanel />
				</Col>
			</Row>
		);
	}
}
