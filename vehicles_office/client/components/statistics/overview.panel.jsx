import React, {Component} from "react";
import PropTypes from "prop-types";
import moment from "moment";
import {connect} from "react-redux";
import {Col, ControlLabel, FormControl, FormGroup, Panel} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import {viewItemLabel, viewItemValue} from "../../constants/display";
import Loader from "../partials/loader";


@connect(
  state => ({
    statistics: state.statistics,
    user: state.user,
  }),
)
export default class OverviewPanel extends Component {
  static propTypes = {
    dispatch: PropTypes.func,
    user: PropTypes.object,
    statistics: PropTypes.object,
  };

  componentDidMount() {
    this.props.dispatch({
      type: "FETCH_REQUESTED",
      action: "/organizations/overview",
      storeName: "statistics",
      name: "overview",
    });
  }

  render() {
    const {statistics, user} = this.props;

    if (statistics.isLoading) {
      return (
        <Loader />
      );
    }

    return (
      <Panel>
        <Panel.Heading><FormattedMessage id="statistics.panels.title.overview" /></Panel.Heading>
        <Panel.Body>
          <FormGroup>
            <Col componentClass={ControlLabel} xs={viewItemValue}>
              <FormattedMessage id="statistics.panels.labels.joined" />
            </Col>
            <Col xs={viewItemLabel}>
              <FormControl.Static>
                {moment(user.organization.created).format("DD MMM YYYY")}
              </FormControl.Static>
            </Col>
          </FormGroup>
          <FormGroup>
            <Col componentClass={ControlLabel} xs={viewItemValue}>
              <FormattedMessage id="statistics.panels.labels.vehicles" />
            </Col>
            <Col xs={viewItemLabel}>
              <FormControl.Static>
                {statistics.overview.vehicles}
              </FormControl.Static>
            </Col>
          </FormGroup>
          <FormGroup>
            <Col componentClass={ControlLabel} xs={viewItemValue}>
              <FormattedMessage id="statistics.panels.labels.bookings" />
            </Col>
            <Col xs={viewItemLabel}>
              <FormControl.Static>
                {statistics.overview.bookings}
              </FormControl.Static>
            </Col>
          </FormGroup>
        </Panel.Body>
      </Panel>
    );
  }
}

