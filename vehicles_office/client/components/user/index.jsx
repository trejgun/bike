import React, {Component} from "react";
import {Route, Switch} from "react-router";
import UserUpdate from "../profile/update";
import UserCreate from "./create";
import UserList from "./list";
import UserRead from "../profile/read";
import UserSubscriptions from "../profile/subscriptions";
import UserPermissions from "./permissions";


export default class User extends Component {
	render() {
		return (
			<Switch>
				<Route path="/users" component={UserList} exact />
				<Route path="/users/create" component={UserCreate} />
				<Route path="/users/:_id" component={UserRead} exact />
				<Route path="/users/:_id/edit" component={UserUpdate} />
				<Route path="/users/:_id/subscription" component={UserSubscriptions} />
				<Route path="/users/:_id/permissions" component={UserPermissions} />
			</Switch>
		);
	}
}
