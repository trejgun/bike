import React, {Component} from "react";
import PropTypes from "prop-types";
import withListForm from "../forms/withListForm";


@withListForm("users")
export default class UserListForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
  };

  componentDidMount() {
    // console.log("UserListForm:componentDidMount", this.props);
    this.props.onSubmit();
  }

  render() {
    return (
      <div />
    );
  }
}
