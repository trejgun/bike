import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, ButtonToolbar, Checkbox, Col, Form, FormGroup, Media} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import withUpdateForm from "../forms/withUpdateForm";
import {viewItemLabel, viewItemValue} from "../../constants/display";
import UserTabs from "./tabs.user";
import permissions from "../../../server/constants/permissions";


@withUpdateForm("users")
export default class UserPermissionsForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
    formData: PropTypes.shape({
      _id: PropTypes.string,
      permissions: PropTypes.arrayOf(PropTypes.string),
    }),

    storeName: PropTypes.string,

    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };

  onChange(e) {
    const {formData, onChange} = this.props;
    onChange({
      persist: ::e.persist,
      target: {
        // type: "array",
        name: "permissions",
        value: e.target.checked
          ? formData.permissions.concat(e.target.name).sort()
          : formData.permissions.filter(p => p !== e.target.name),
      },
    });
  }

  render() {
    // console.log("UserPermissionsForm:render", this.props);
    const {formData, onSubmit} = this.props;
    return (
      <Form horizontal onSubmit={onSubmit}>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.user.permissions" />
          </h4>
          <UserTabs _id={formData._id} />
        </div>

        <Media>
          <Media.Body>
            {permissions.map(permission =>
              (<FormGroup key={permission}>
                <Col xsOffset={viewItemLabel} xs={viewItemValue}>
                  <Checkbox name={permission} defaultChecked={formData.permissions.includes(permission)}
                            onChange={::this.onChange}>
                    <FormattedMessage id={`permissions.${permission}`} />
                  </Checkbox>
                </Col>
              </FormGroup>),
            )}
            <ButtonToolbar>
              <Button type="submit" className="pull-right">
                <FormattedMessage id="components.buttons.save" />
              </Button>
            </ButtonToolbar>
          </Media.Body>
        </Media>
      </Form>
    );
  }
}
