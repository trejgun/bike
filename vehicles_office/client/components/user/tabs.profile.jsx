import React, {Component} from "react";
import {Nav, NavItem} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import {LinkContainer} from "react-router-bootstrap";


export default class ProfileTabs extends Component {
	render() {
		return (
			<Nav bsStyle="tabs" id="uncontrolled-tab-example" className="nav-rtl">
				<LinkContainer to="/profile" exact>
					<NavItem eventKey="1">
						<FormattedMessage id="components.tabs.profile.general" />
					</NavItem>
				</LinkContainer>
				<LinkContainer to="/profile/subscriptions">
					<NavItem eventKey="2">
						<FormattedMessage id="components.tabs.profile.subscriptions" />
					</NavItem>
				</LinkContainer>
			</Nav>
		);
	}
}
