import React, {Component} from "react";
import PropTypes from "prop-types";
import {Nav, NavItem} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import {LinkContainer} from "react-router-bootstrap";


export default class ProfileTabs extends Component {
	static propTypes = {
		_id: PropTypes.string,
	};

	render() {
		const {_id} = this.props;
		return (
			<Nav bsStyle="tabs" id="uncontrolled-tab-example" className="nav-rtl">
				<LinkContainer to={`/users/${_id}`} exact>
					<NavItem eventKey="1">
						<FormattedMessage id="components.tabs.user.general" />
					</NavItem>
				</LinkContainer>
				<LinkContainer to={`/users/${_id}/permissions`}>
					<NavItem eventKey="2">
						<FormattedMessage id="components.tabs.user.permissions" />
					</NavItem>
				</LinkContainer>
			</Nav>
		);
	}
}
