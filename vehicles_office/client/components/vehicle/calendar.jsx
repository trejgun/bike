import React, {Component} from "react";
import PropTypes from "prop-types";
import moment from "moment";
import querystring from "querystring";
import {Form} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import "./calendar.less";
import {checkOutTime, date} from "../../constants/date";
import BigCalendar from "./calendar/big";
import VehicleTabs from "./tabs";
import withStore from "../forms/withStore";
import withForm from "../forms/withForm";
import {MESSAGE_ADD} from "../../constants/actions";


function getRangeByDate(date) {
  return moment.range(
    moment(date).startOf("M"),
    moment(date).endOf("M"),
  );
}

@withStore("user")
@withForm("bookings")
export default class VehicleCalendarForm extends Component {
  static propTypes = {
    user: PropTypes.object,
    bookings: PropTypes.object,
    dispatch: PropTypes.func,
    storeName: PropTypes.string,
    paramName: PropTypes.string,

    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
    match: PropTypes.shape({
      params: PropTypes.object.isRequired,
    }).isRequired,
  };

  state = {
    date,
    range: getRangeByDate(date),
  };
  cache = [];

  componentDidMount() {
    this.onSubmit();
  }

  componentDidUpdate() {
    const {storeName} = this.props;
    if (!this.props[storeName].isLoading && this.props[storeName].success && this.props[storeName].name === "list") {
      this.cache.push(this.state.range.toString());
    }
  }

  onSubmit() {
    this.props.dispatch({
      type: "FETCH_REQUESTED",
      action: `/vehicles/${this.props.match.params._id}/bookings`,
      data: {range: this.state.range.toString()},
      storeName: this.props.storeName,
      name: "list",
    });
  }

  onSelectSlot(slot) {
    // console.log("VehicleCalendarForm:onSelectSlot", this.props);
    // TODO FIXME
    // const [checkOutHours, checkOutMinutes] = this.props.user.organization.checkOutTime.split(":");
    const [checkOutHours, checkOutMinutes] = checkOutTime.split(":");
    const range = moment.range(
      moment(slot.start).add(checkOutHours, "h").add(checkOutMinutes, "m"),
      moment(slot.end).add(checkOutHours, "h").add(checkOutMinutes, "m"),
    );
    if (!this.findBookingByRange(range)) {
      this.setState({
        range,
      }, () => {
        this.props.history.push({
          pathname: "/bookings/create",
          search: querystring.stringify({
            range: this.state.range.toString(),
            vehicle: this.props.match.params._id,
          }),
        });
      });
    } else {
      this.props.dispatch({
        type: MESSAGE_ADD,
        data: {
          type: "danger",
          message: "range-not-available",
        },
      });
    }
  }

  onSelectEvent(booking) {
    const {history} = this.props;
    history.push(`/bookings/${booking._id}`);
  }

  onNavigate(date) {
    const range = getRangeByDate(date);
    if (!this.cache.includes(range.toString())) {
      this.setState({
        date,
        range,
      }, this.onSubmit);
    }
  }

  findBookingByRange(range) {
    const {bookings} = this.props;
    return bookings.list
      .filter(booking => booking.status === "accepted")
      .find(booking => moment.range(booking.range).intersect(range, false));
  }

  render() {
    const {match, bookings, user} = this.props;
    return (
      <Form horizontal>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.vehicle.calendar" />
          </h4>
          <VehicleTabs _id={match.params._id} />
        </div>
        <BigCalendar
          storeName="bookings"
          bookings={bookings}
          defaultDate={this.state.date}
          culture={user.language}
          onSelectEvent={::this.onSelectEvent}
          onNavigate={::this.onNavigate}
          onSelectSlot={::this.onSelectSlot}
        />
      </Form>
    );
  }
}
