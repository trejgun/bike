import React, {Component} from "react";
import PropTypes from "prop-types";
import moment from "moment";
import BigCalendar from "react-big-calendar";
import withLoader from "../../forms/withLoader";
import EventWrapper from "./event.wrapper";


@withLoader("bookings")
export default class BigCalendarWrapper extends Component {
  static propTypes = {
    bookings: PropTypes.shape({
      list: PropTypes.array,
    }),
  };

  state = {
    view: "month",
  };

  onView(view) {
    this.setState({view});
  }

  render() {
    const events = this.props.bookings.list.map(booking => {
      const range = moment.range(booking.range);
      return {
        _id: booking._id,
        title: booking.fullName,
        status: booking.status,
        start: range.start.toDate(),
        end: range.end.toDate(),
        resourceId: booking.vehicle._id,
      };
    });

    return (
      <div className="rbc-calendar-container">
        <BigCalendar
          {...this.props}
          selectable
          views={["month", "day", "agenda"]}
          defaultView={this.state.view}
          onView={::this.onView}
          events={events}
          localizer={BigCalendar.momentLocalizer(moment)}
          defaultDate={moment().startOf(this.state.view === "day" ? "day" : "month").toDate()}
          components={{
            eventWrapper: EventWrapper,
          }}
        />
      </div>
    );
  }
}
