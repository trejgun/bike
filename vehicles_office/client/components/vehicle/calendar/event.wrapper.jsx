import React, {Component} from "react";
import PropTypes from "prop-types";


export default class EventWrapper extends Component {
	static propTypes = {
		children: PropTypes.element,
		event: PropTypes.object,
	};

	render() {
		return (
			<div className={this.props.event.status}>
				{this.props.children}
			</div>
		);
	}
}
