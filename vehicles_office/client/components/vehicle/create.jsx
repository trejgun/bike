import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, ButtonToolbar, Form, Media} from "react-bootstrap";
import {date} from "../../constants/date";
import {range} from "lodash";
import {uniq} from "../../utils/vehicle";
import {FormattedMessage} from "react-intl";
import withTypeaheadForm from "../forms/withTypeaheadForm";
import withCreate from "../forms/withCreateForm";
import withStore from "../forms/withStore";
import {brand, cc, model, notes, plate, variant} from "../../constants/placeholder";
import NotInList from "./missing.button";
import InputImage from "../inputs/input.image";
import InputGroupValidation from "../inputs/input.group.validation";
import InputAddonsGroupValidation from "../inputs/input.addons.group.validation";
import InputMarkdownGroup from "../inputs/input.markdown.group";
import InputColorGroupValidation from "../inputs/input.color.group.validation";
import colors from "../../constants/colors";


@withStore("user")
@withTypeaheadForm("vehicles", "catalog")
@withCreate("vehicles")
export default class VehicleCreateForm extends Component {
  static propTypes = {
    onChange: PropTypes.func,
    onSubmit: PropTypes.func,
    setState: PropTypes.func,
    formData: PropTypes.shape({
      bluebook: PropTypes.string,
      brand: PropTypes.string,
      cc: PropTypes.number,
      image: PropTypes.string,
      fuel: PropTypes.string,
      model: PropTypes.string,
      notes: PropTypes.string,
      plate: PropTypes.string,
      pricePerHour: PropTypes.string,
      pricePerDay: PropTypes.string,
      pricePerMonth: PropTypes.string,
      taxnservice: PropTypes.string,
      type: PropTypes.string,
      variant: PropTypes.string,
      year: PropTypes.number,
      color: PropTypes.string,
    }),

    dispatch: PropTypes.func,

    catalog: PropTypes.object,
    user: PropTypes.object,

    storeName: PropTypes.string,
    paramName: PropTypes.string,


    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,
  };

  state = {
    batch: false,
    count: 1,
  };

  componentDidMount() {
    this.props.setState({
      fuel: "petrol",
      year: date.getFullYear(),
    });
  }

  componentDidUpdate() {
    // console.log("VehicleCreateForm:componentWillReceiveProps", this.props[storeName]);
    const {storeName, paramName, history} = this.props;
    if (!this.props[storeName].isLoading && this.props[storeName].success) {
      if (this.props[storeName].name === "create") {
        history.push(`/${storeName}/${this.props[storeName].list[this.props[storeName].list.length - 1][paramName]}`);
      } else if (this.props[storeName].name === "batch") {
        history.push("/vehicles");
      }
    }
  }

  onModeChange() {
    this.setState({
      batch: !this.state.batch,
    });
  }

  render() {
    const docs = this.props.user.organization.settings.docs;
    const {onSubmit, onChange, formData, catalog} = this.props;
    return (
      <Form horizontal
            action={this.state.batch ? `/${this.props.storeName}/batch` : `/${this.props.storeName}`}
            name={this.state.batch ? "batch" : "create"}
            onSubmit={onSubmit}
      >
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.vehicle.create" />
          </h4>
        </div>
        <Media>
          <Media.Left>
            <InputImage
              folder="vehicle"
              name="image"
              image={formData.image}
              onChange={onChange}
            />
            {!this.state.batch && docs ?
              <InputImage
                folder="taxnservice"
                name="taxnservice"
                image={formData.taxnservice}
                onChange={onChange}
              /> : null}
            {!this.state.batch && docs ?
              <InputImage
                folder="bluebook"
                name="bluebook"
                image={formData.bluebook}
                onChange={onChange}
              /> : null}
            <ButtonToolbar>
              <Button type="button" onClick={::this.onModeChange}>
                Switch to {this.state.batch ? "single" : "batch"} mode
              </Button>
            </ButtonToolbar>
            <ButtonToolbar>
              <NotInList />
            </ButtonToolbar>
          </Media.Left>
          <Media.Body>
            {!this.state.batch ?
              <InputGroupValidation
                name="plate"
                defaultValue={formData.plate}
                placeholder={plate}
                onChange={onChange}
                required
              /> : <InputGroupValidation
                type="number"
                componentClass="select"
                name="count"
                defaultValue={this.state.count}
                onChange={onChange}
              >
                {range(1, 21).map(count =>
                  <option value={count} key={count}>{count}</option>,
                )}
              </InputGroupValidation>}
            <InputGroupValidation
              componentClass="select"
              name="type"
              defaultValue={formData.type}
              onChange={onChange}
            >
              <option value="">Select...</option>
              {this.props.catalog.list
                .map(v => v.type).filter(uniq)
                .map(type =>
                  (<FormattedMessage key={type} id={`components.vehicle.type.${type}`}>
                    {formattedMessage => <option key={type} value={type}>{formattedMessage}</option>}
                  </FormattedMessage>),
                )}
            </InputGroupValidation>
            {formData.type ?
              <InputGroupValidation
                componentClass="select"
                name="brand"
                defaultValue={formData.brand}
                placeholder={brand}
                onChange={onChange}
              >
                <option value="">Select...</option>
                {catalog.list
                  .filter(v => v.type === formData.type)
                  .map(v => v.brand)
                  .filter(uniq)
                  .map(brand => <option value={brand} key={brand}>{brand}</option>)}
              </InputGroupValidation> : null}
            {formData.brand ?
              <InputGroupValidation
                componentClass="select"
                name="model"
                defaultValue={formData.model}
                placeholder={model}
                onChange={onChange}
              >
                <option value="">Select...</option>
                {catalog.list
                  .filter(v => v.type === formData.type)
                  .filter(v => v.brand === formData.brand)
                  .map(v => v.model)
                  .filter(uniq)
                  .map(model => <option value={model} key={model}>{model}</option>)}
              </InputGroupValidation> : null}
            {formData.model ?
              <InputGroupValidation
                componentClass="select"
                name="cc"
                type="number"
                defaultValue={formData.cc}
                placeholder={cc}
                onChange={onChange}
              >
                <option value="">Select...</option>
                {catalog.list
                  .filter(v => v.type === formData.type)
                  .filter(v => v.brand === formData.brand)
                  .filter(v => v.model === formData.model)
                  .map(v => v.cc)
                  .filter(uniq)
                  .map(cc => <option value={cc} key={cc}>{cc}</option>)}
              </InputGroupValidation> : null}
            {formData.model ?
              <InputGroupValidation
                name="variant"
                defaultValue={formData.variant}
                placeholder={variant}
                onChange={onChange}
              /> : null}
            {formData.cc ?
              <InputGroupValidation
                componentClass="select"
                name="fuel"
                defaultValue={formData.fuel}
                onChange={onChange}
              >
                <option value="">Select...</option>
                {catalog.list
                  .filter(v => v.type === formData.type)
                  .filter(v => v.brand === formData.brand)
                  .filter(v => v.model === formData.model)
                  .filter(v => v.cc === formData.cc)
                  .map(v => v.fuel)
                  .filter(uniq)
                  .map(fuel =>
                    (<FormattedMessage key={fuel} id={`components.vehicle.fuel.${fuel}`}>
                      {formattedMessage => <option key={fuel} value={fuel}>{formattedMessage}</option>}
                    </FormattedMessage>),
                  )}
              </InputGroupValidation> : null}
            <InputGroupValidation
              componentClass="select"
              name="year"
              type="number"
              defaultValue={formData.year}
              onChange={onChange}
            >
              <option value="">Select...</option>
              {range(2000, date.getFullYear() + 1).reverse().map(year =>
                <option value={year} key={year}>{year}</option>,
              )}
            </InputGroupValidation>
            <InputColorGroupValidation
              name="color"
              colors={colors}
              defaultValue={formData.color}
              onChange={onChange}
            />
            <InputAddonsGroupValidation
              name="pricePerHour"
              defaultValue={formData.pricePerHour}
              placeholder="100"
              onChange={onChange}
              after=",000.00 Rp."
            />
            <InputAddonsGroupValidation
              name="pricePerDay"
              defaultValue={formData.pricePerDay}
              placeholder="100"
              onChange={onChange}
              after=",000.00 Rp."
            />
            <InputAddonsGroupValidation
              name="pricePerMonth"
              defaultValue={formData.pricePerMonth}
              placeholder="100"
              onChange={onChange}
              after=",000.00 Rp."
            />
            <InputMarkdownGroup
              componentClass="textarea"
              name="notes"
              defaultValue={formData.notes}
              placeholder={notes}
              onChange={onChange}
            />
            <ButtonToolbar>
              <Button type="submit" disabled={!formData.model} className="pull-right">
                <FormattedMessage id="components.buttons.save" />
              </Button>
            </ButtonToolbar>
          </Media.Body>
        </Media>
      </Form>
    );
  }
}
