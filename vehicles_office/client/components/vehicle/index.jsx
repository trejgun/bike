import React, {Component} from "react";
import {Route, Switch} from "react-router";
import VehicleList from "./list";
import VehicleCreate from "./create";
import VehicleUpdate from "./update";
import VehicleRead from "./read";
import VehicleCalendar from "./calendar";


export default class Vehicles extends Component {
	render() {
		return (
			<Switch>
				<Route path="/vehicles" component={VehicleList} exact />
				<Route path="/vehicles/create" component={VehicleCreate} />
				<Route path="/vehicles/:_id" component={VehicleRead} exact />
				<Route path="/vehicles/:_id/edit" component={VehicleUpdate} />
				<Route path="/vehicles/:_id/calendar" component={VehicleCalendar} />
			</Switch>
		);
	}
}
