import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, Checkbox, Col, ControlLabel, Form, FormControl, FormGroup} from "react-bootstrap";
import {FormattedMessage} from "react-intl";
import withTypeaheadForm from "../forms/withTypeaheadForm";
import withListForm from "../forms/withListForm";
import {uniq} from "../../utils/vehicle";
import {readFromQueryString} from "../../utils/location";
import LimitGroup from "../groups/limit";


@withTypeaheadForm("vehicles", "select")
@withListForm("vehicles")
export default class VehicleListForm extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    setState: PropTypes.func,
    onChange: PropTypes.func,
    formData: PropTypes.shape({
      availability: PropTypes.bool,
      type: PropTypes.string,
      brand: PropTypes.string,
      model: PropTypes.string,
      cc: PropTypes.string,
      limit: PropTypes.number,
    }),

    location: PropTypes.shape({
      search: PropTypes.string.isRequired,
      pathname: PropTypes.string.isRequired,
    }).isRequired,
    history: PropTypes.shape({
      push: PropTypes.func.isRequired,
    }).isRequired,

    select: PropTypes.object,
  };

  componentDidMount() {
    // console.log("VehicleListForm:componentDidMount", this.props);
    const {setState, onSubmit, location} = this.props;
    setState({
      type: readFromQueryString("type", location.search),
      brand: readFromQueryString("brand", location.search),
      model: readFromQueryString("model", location.search),
      cc: readFromQueryString("cc", location.search),
    }, onSubmit);
  }

  onChangeType(e) {
    // console.log("VehicleListForm:onChangeType", e, e.target.name, e.target.value);
    this.props.setState({
      type: e.target.value,
      brand: "",
      model: "",
      cc: "",
    });
  }

  onChangeBrand(e) {
    this.props.setState({
      brand: e.target.value,
      model: "",
      cc: "",
    });
  }

  onChangeModel(e) {
    this.props.setState({
      model: e.target.value,
      cc: "",
    });
  }

  onChangeCC(e) {
    this.props.setState({
      cc: parseFloat(e.target.value),
    });
  }

  render() {
    const {onSubmit, formData, onChange, select} = this.props;
    return (
      <Form horizontal onSubmit={onSubmit}>
        <Col xs={12}>
          <Checkbox
            name="availability"
            defaultValue={formData.availability}
            onChange={onChange}
          >
            <FormattedMessage id="fields.availability.label" />
          </Checkbox>
          <FormGroup>
            <Col componentClass={ControlLabel}>
              <FormattedMessage id="fields.type.label" />
            </Col>
            <FormControl
              componentClass="select"
              name="type"
              defaultValue={formData.type}
              onChange={::this.onChangeType}
            >
              <option value="">Select...</option>
              {select.list
                .map(v => v.type).filter(uniq)
                .map(type =>
                  (<FormattedMessage key={type} id={`components.vehicle.type.${type}`}>
                    {formattedMessage => <option key={type} value={type}>{formattedMessage}</option>}
                  </FormattedMessage>),
                )}
            </FormControl>
          </FormGroup>
          {formData.type ?
            <FormGroup>
              <Col componentClass={ControlLabel}>
                <FormattedMessage id="fields.brand.label" />
              </Col>
              <FormControl
                componentClass="select"
                name="brand"
                defaultValue={formData.brand}
                onChange={::this.onChangeBrand}
              >
                <option value="">Select...</option>
                {select.list
                  .filter(v => v.type === formData.type)
                  .map(v => v.brand)
                  .filter(uniq)
                  .map(brand => <option value={brand} key={brand}>{brand}</option>)}
              </FormControl>
            </FormGroup> : null}
          {formData.brand ?
            <FormGroup>
              <Col componentClass={ControlLabel}>
                <FormattedMessage id="fields.model.label" />
              </Col>
              <FormControl
                componentClass="select"
                name="model"
                defaultValue={formData.model}
                onChange={::this.onChangeModel}
              >
                <option value="">Select...</option>
                {select.list
                  .filter(v => v.type === formData.type)
                  .filter(v => v.brand === formData.brand)
                  .map(v => v.model)
                  .filter(uniq)
                  .map(model => <option value={model} key={model}>{model}</option>)}
              </FormControl>
            </FormGroup> : null}
          {formData.model ?
            <FormGroup>
              <Col componentClass={ControlLabel}>
                <FormattedMessage id="fields.cc.label" />
              </Col>
              <FormControl
                componentClass="select"
                name="cc"
                defaultValue={formData.cc}
                onChange={::this.onChangeCC}
              >
                {select.list
                  .filter(v => v.type === formData.type)
                  .filter(v => v.brand === formData.brand)
                  .filter(v => v.model === formData.model)
                  .map(v => v.cc)
                  .filter(uniq)
                  .map(cc => <option value={cc} key={cc}>{cc}</option>)}
              </FormControl>
            </FormGroup> : null}
          <LimitGroup
            defaultValue={formData.limit}
            onChange={onChange}
          />
          <FormGroup>
            <Button type="submit">
              <FormattedMessage id="components.buttons.search" />
            </Button>
          </FormGroup>
        </Col>
      </Form>
    );
  }
}
