import React, {Component} from "react";
import PropTypes from "prop-types";
import {Form, Image, ListGroupItem, Media} from "react-bootstrap";
import {LinkContainer} from "react-router-bootstrap";
import {FormattedMessage} from "react-intl";
import withListItemHelper from "../forms/withListItem";
import {listItemHeight, listItemWidth} from "../../constants/display";
import {priceToHtml, vehicleToHtml} from "../../utils/vehicle";
import InputStaticGroup from "../inputs/input.static.group";


@withListItemHelper("vehicles")
export default class VehicleListItem extends Component {
  static propTypes = {
    formData: PropTypes.shape({
      _id: PropTypes.string,
      image: PropTypes.string,
      isAvailable: PropTypes.bool,
    }),
  };

  render() {
    // console.log("VehicleListItem:render", this.props);
    const {formData} = this.props;
    return (
      <LinkContainer key={formData._id} to={`/vehicles/${formData._id}`}>
        <ListGroupItem>
          <Media>
            <Media.Left className="hidden-xs">
              <Image
                width={listItemWidth}
                height={listItemHeight}
                src={formData.image || `${process.env.MODULE_CDN}/img/icons/vehicle.png`}
                className="media-object img-thumbnail"
                alt="Image"
              />
            </Media.Left>
            <Media.Body>
              <Media.Heading>{vehicleToHtml(formData, true)}</Media.Heading>
              <Form horizontal>
                <InputStaticGroup
                  name="vehicle"
                  value={<FormattedMessage id={`components.vehicle.availability.${formData.isAvailable || "true"}`} />}
                />
                <InputStaticGroup
                  name="vehicle"
                  value={priceToHtml(formData)}
                />
              </Form>
            </Media.Body>
          </Media>
        </ListGroupItem>
      </LinkContainer>
    );
  }
}
