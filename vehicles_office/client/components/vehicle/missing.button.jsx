import React, {Component} from "react";
import PropTypes from "prop-types";
import {Button, Form} from "react-bootstrap";
import YesNoDialog from "../partials/yesno";
import withForm from "../forms/withForm";
import {FormattedMessage} from "react-intl";
import InputGroup from "../inputs/input.group";
import InputGroupValidation from "../inputs/input.group.validation";
import {brand, cc, model, notes, variant} from "../../constants/placeholder";
import {MESSAGE_ADD} from "../../constants/actions";


@withForm("vehicles")
export default class MissingButton extends Component {
  static propTypes = {
    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
    setState: PropTypes.func,
    formData: PropTypes.shape({
      type: PropTypes.string,
      brand: PropTypes.string,
      model: PropTypes.string,
      cc: PropTypes.string,
      variant: PropTypes.string,
      fuel: PropTypes.string,
      notes: PropTypes.string,
    }),

    dispatch: PropTypes.func,
    storeName: PropTypes.string,
  };

  state = {
    showDialog: false,
  };

  componentDidUpdate() {
    // console.log("MissingButton:componentDidUpdate");
    const {storeName, dispatch} = this.props;
    if (!this.props[storeName].isLoading && this.props[storeName].success && this.props[storeName].name === "missing") {
      dispatch({
        type: MESSAGE_ADD,
        data: {
          type: "success",
          message: "missing-successful",
        },
      });
      this.setState({
        showDialog: false,
      }, this.onClose);
    }
  }

  onClose() {
    this.props.setState({
      type: "bike",
      brand: "",
      model: "",
      cc: "",
      variant: "",
      fuel: "petrol",
      notes: "",
    });
  }

  onClick() {
    this.setState({
      showDialog: true,
    });
  }

  onCancel() {
    this.setState({
      showDialog: false,
    }, this.onClose);
  }

  onConfirm(e) {
    this.props.onSubmit({
      preventDefault: e ? ::e.preventDefault : Function.prototype,
      target: {
        getAttribute: name => {
          return {
            action: "/vehicles/missing",
            name: "missing",
            method: "POST",
          }[name];
        },
      },
    });
  }

  render() {
    // console.log("MissingButton:render", this.state);
    const {formData, onChange} = this.props;
    return (
      <>
        <YesNoDialog
          title={<FormattedMessage
            tagName="p"
            id="dialogs.vehicle-missing"
          />}
          show={this.state.showDialog}
          onConfirm={::this.onConfirm}
          onCancel={::this.onCancel}
        >
          <Form horizontal onSubmit={::this.onConfirm}>
            <InputGroupValidation
              componentClass="select"
              name="type"
              defaultValue={formData.type}
              onChange={onChange}
              required
            >
              {["bike", "car"].map(type =>
                (<FormattedMessage key={type} id={`components.vehicle.type.${type}`}>
                  {formattedMessage => <option key={type} value={type}>{formattedMessage}</option>}
                </FormattedMessage>),
              )}
            </InputGroupValidation>

            <InputGroupValidation
              name="brand"
              defaultValue={formData.brand}
              placeholder={brand}
              onChange={onChange}
              required
            />
            <InputGroupValidation
              name="model"
              defaultValue={formData.model}
              placeholder={model}
              onChange={onChange}
              required
            />
            <InputGroupValidation
              name="cc"
              defaultValue={formData.cc}
              placeholder={cc}
              onChange={onChange}
              required
            />
            <InputGroupValidation
              name="variant"
              defaultValue={formData.variant}
              placeholder={variant}
              onChange={onChange}
            />
            <InputGroupValidation
              componentClass="select"
              name="fuel"
              defaultValue={formData.fuel}
              onChange={onChange}
              required
            >
              {["petrol", "diesel", "electric"].map(fuel =>
                (<FormattedMessage key={fuel} id={`components.vehicle.fuel.${fuel}`}>
                  {formattedMessage => <option key={fuel} value={fuel}>{formattedMessage}</option>}
                </FormattedMessage>),
              )}
            </InputGroupValidation>
            <InputGroup
              componentClass="textarea"
              name="notes"
              defaultValue={formData.notes}
              placeholder={notes}
              onChange={onChange}
            />
          </Form>
        </YesNoDialog>
        <Button type="button" onClick={::this.onClick}>
          My vehicle is not in list
        </Button>
      </>
    );
  }
}
