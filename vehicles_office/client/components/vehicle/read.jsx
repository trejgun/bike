import React, {Component} from "react";
import PropTypes from "prop-types";
import {Form, Glyphicon, Media} from "react-bootstrap";
import {Link} from "react-router-dom";
import {FormattedMessage} from "react-intl";
import {plateToHtml, priceToHtml, vehicleToHtml} from "../../utils/vehicle";
import withDeleteForm from "../forms/withDeleteForm";
import ImageViewer from "../imageViewer";
import InputStaticGroup from "../inputs/input.static.group";
import VehicleTabs from "./tabs";


@withDeleteForm("vehicles")
export default class VehicleReadForm extends Component {
  static propTypes = {
    onDelete: PropTypes.func,
    formData: PropTypes.shape({
      _id: PropTypes.string,
      image: PropTypes.string,
      plate: PropTypes.string,
      type: PropTypes.string,
      brand: PropTypes.string,
      model: PropTypes.string,
      variant: PropTypes.string,
      fuel: PropTypes.string,
      cc: PropTypes.number,
      year: PropTypes.number,
      status: PropTypes.string,
    }),
  };

  render() {
    // console.log("VehicleReadForm:render", this.props);
    const {formData, onDelete} = this.props;
    return (
      <Form horizontal>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.vehicle.read" />
            <Link to={`/vehicles/${formData._id}/edit`}>
              <Glyphicon glyph="edit" />
            </Link>
            <Link to={`/vehicles/${formData._id}/delete`} onClick={onDelete}>
              <Glyphicon glyph="trash" />
            </Link>
          </h4>
          <VehicleTabs _id={formData._id} />
        </div>
        <Media className="view">
          <Media.Left>
            <ImageViewer
              image={formData.image}
              placeholder="vehicle"
              title={vehicleToHtml(this.props, true)}
            />
          </Media.Left>
          <Media.Body>
            <InputStaticGroup
              name="plate"
              value={plateToHtml(formData.plate)}
            />
            <InputStaticGroup
              name="type"
              value={formData.type}
            />
            <InputStaticGroup
              name="brand"
              value={formData.brand}
            />
            <InputStaticGroup
              name="model"
              value={formData.model}
            />
            <InputStaticGroup
              name="variant"
              value={formData.variant}
            />
            <InputStaticGroup
              name="cc"
              value={formData.cc}
            />
            <InputStaticGroup
              name="fuel"
              value={formData.fuel}
            />
            <InputStaticGroup
              name="year"
              value={formData.year}
            />
            <InputStaticGroup
              name="price"
              value={priceToHtml(this.props)}
            />
            <InputStaticGroup
              name="status"
              value={formData.status}
            />
          </Media.Body>
        </Media>
      </Form>
    );
  }
}
