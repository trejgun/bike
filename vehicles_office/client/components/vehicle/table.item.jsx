import React, {Component} from "react";
import PropTypes from "prop-types";
import {Col, Thumbnail} from "react-bootstrap";
import {Link} from "react-router-dom";
import withListItemHelper from "../forms/withListItem";
import {vehicleToHtml} from "../../utils/vehicle";


@withListItemHelper("vehicles")
export default class VehicleTableItem extends Component {
  static propTypes = {
    formData: PropTypes.shape({
      _id: PropTypes.string,
      image: PropTypes.string,
    }),
  };

  render() {
    // console.log("VehicleTableItem:render", this.props);
    const {formData} = this.props;
    return (
      <Col md={4} xs={6} key={formData._id}>
        <Link to={`/vehicles/${formData._id}`}>
          <Thumbnail src={formData.image || `${process.env.MODULE_CDN}/img/icons/vehicle.png`}>
            <h4>{vehicleToHtml(formData, true)}</h4>
          </Thumbnail>
        </Link>
      </Col>
    );
  }
}
