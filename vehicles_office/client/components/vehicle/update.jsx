import React, {Component} from "react";
import PropTypes from "prop-types";
import {range} from "lodash";
import {FormattedMessage} from "react-intl";
import {Button, ButtonToolbar, Form, Media} from "react-bootstrap";
import {date} from "../../constants/date";
import {uniq} from "../../utils/vehicle";
import withUpdateForm from "../forms/withUpdateForm";
import withTypeaheadForm from "../forms/withTypeaheadForm";
import withStore from "../forms/withStore";
import {brand, cc, model, plate, variant} from "../../constants/placeholder";
import InputImage from "../inputs/input.image";
import InputGroupValidation from "../inputs/input.group.validation";
import InputAddonsGroupValidation from "../inputs/input.addons.group.validation";
import InputColorGroupValidation from "../inputs/input.color.group.validation";
import colors from "../../constants/colors";


@withStore("user")
@withTypeaheadForm("vehicles", "catalog")
@withUpdateForm("vehicles")
export default class VehicleUpdateForm extends Component {
  static propTypes = {
    catalog: PropTypes.object,
    user: PropTypes.object,

    onSubmit: PropTypes.func,
    onChange: PropTypes.func,
    formData: PropTypes.shape({
      image: PropTypes.string,
      taxnservice: PropTypes.string,
      bluebook: PropTypes.string,
      plate: PropTypes.string,
      type: PropTypes.string,
      brand: PropTypes.string,
      model: PropTypes.string,
      variant: PropTypes.string,
      cc: PropTypes.number,
      fuel: PropTypes.string,
      year: PropTypes.number,
      color: PropTypes.string,
      pricePerHour: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]),
      pricePerDay: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]),
      pricePerMonth: PropTypes.oneOfType([
        PropTypes.string,
        PropTypes.number,
      ]),
      discount: PropTypes.number,
    }),
  };

  render() {
    // TODO create settings on sign up
    const {onSubmit, formData, onChange, user} = this.props;
    const docs = user.organization.settings.docs;
    return (
      <Form horizontal onSubmit={onSubmit}>
        <div className="headline">
          <h4>
            <FormattedMessage id="components.title.vehicle.update" />
          </h4>
        </div>
        <Media>
          <Media.Left>
            <InputImage
              folder="vehicle"
              name="image"
              image={formData.image}
              onChange={onChange}
            />
            {docs ?
              <InputImage
                folder="taxnservice"
                name="taxnservice"
                image={formData.taxnservice}
                onChange={onChange}
              /> : null}
            {docs ?
              <InputImage
                folder="bluebook"
                name="bluebook"
                image={formData.bluebook}
                onChange={onChange}
              /> : null}
          </Media.Left>
          <Media.Body>
            <InputGroupValidation
              name="plate"
              defaultValue={formData.plate}
              placeholder={plate}
              onChange={onChange}
              required
            />
            <InputGroupValidation
              componentClass="select"
              name="type"
              defaultValue={formData.type}
              onChange={onChange}
            >
              <option value="">Select...</option>
              {this.props.catalog.list
                .map(v => v.type).filter(uniq)
                .map(type => <option value={type} key={type}>{type}</option>)}
            </InputGroupValidation>

            {formData.type ?
              <InputGroupValidation
                componentClass="select"
                name="brand"
                defaultValue={formData.brand}
                placeholder={brand}
                onChange={onChange}
              >
                <option value="">Select...</option>
                {this.props.catalog.list
                  .filter(v => v.type === formData.type)
                  .map(v => v.brand)
                  .filter(uniq)
                  .map(brand => <option value={brand} key={brand}>{brand}</option>)}
              </InputGroupValidation> : null}
            {formData.brand ?
              <InputGroupValidation
                componentClass="select"
                name="model"
                defaultValue={formData.model}
                placeholder={model}
                onChange={onChange}
              >
                <option value="">Select...</option>
                {this.props.catalog.list
                  .filter(v => v.type === formData.type)
                  .filter(v => v.brand === formData.brand)
                  .map(v => v.model)
                  .filter(uniq)
                  .map(model => <option value={model} key={model}>{model}</option>)}
              </InputGroupValidation> : null}
            {formData.model ?
              <InputGroupValidation
                componentClass="select"
                name="cc"
                defaultValue={formData.cc}
                placeholder={cc}
                onChange={onChange}
              >
                <option value="">Select...</option>
                {this.props.catalog.list
                  .filter(v => v.type === formData.type)
                  .filter(v => v.brand === formData.brand)
                  .filter(v => v.model === formData.model)
                  .map(v => v.cc)
                  .filter(uniq)
                  .map(cc => <option value={cc} key={cc}>{cc}</option>)}
              </InputGroupValidation> : null}
            {formData.model ?
              <InputGroupValidation
                name="variant"
                defaultValue={formData.variant}
                placeholder={variant}
                onChange={onChange}
              /> : null}
            {formData.cc ?
              <InputGroupValidation
                componentClass="select"
                name="fuel"
                defaultValue={formData.fuel}
                onChange={onChange}
              >
                <option value="">Select...</option>
                {this.props.catalog.list
                  .filter(v => v.type === formData.type)
                  .filter(v => v.brand === formData.brand)
                  .filter(v => v.model === formData.model)
                  .filter(v => v.cc === formData.cc)
                  .map(v => v.fuel)
                  .filter(uniq)
                  .map(fuel => <option value={fuel} key={fuel}>{fuel}</option>)}
              </InputGroupValidation> : null}
            <InputGroupValidation
              componentClass="select"
              name="year"
              type="number"
              defaultValue={formData.year}
              onChange={onChange}
            >
              <option value="">Select...</option>
              {range(2000, date.getFullYear() + 1).reverse().map(year =>
                <option value={year} key={year}>{year}</option>,
              )}
            </InputGroupValidation>
            <InputColorGroupValidation
              name="color"
              colors={colors}
              defaultValue={formData.color}
              onChange={onChange}
            />
            <InputAddonsGroupValidation
              name="pricePerHour"
              defaultValue={formData.pricePerHour}
              placeholder="100"
              onChange={onChange}
              after=",000.00 Rp."
            />
            <InputAddonsGroupValidation
              name="pricePerDay"
              defaultValue={formData.pricePerDay}
              placeholder="100"
              onChange={onChange}
              after=",000.00 Rp."
            />
            <InputAddonsGroupValidation
              name="pricePerMonth"
              defaultValue={formData.pricePerMonth}
              placeholder="100"
              onChange={onChange}
              after=",000.00 Rp."
            />
            <ButtonToolbar>
              <Button type="submit" disabled={!formData.model} className="pull-right">
                <FormattedMessage id="components.buttons.save" />
              </Button>
            </ButtonToolbar>
          </Media.Body>
        </Media>
      </Form>
    );
  }
}
