import {remove, replaceAll, replaceOrAppend} from "../utils/reducer";

const bookings = {
  list: [],
  count: 0,
  isLoading: true,
  success: false,
  name: "",
};

export default function bookingsReducer(state = bookings, action) {
  switch (action.type) {
    case "bookings_view_start":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        name: action.name,
      });
    case "bookings_view_success":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        list: replaceOrAppend(state, action.data),
        count: 1,
        name: action.name,
      });
    case "bookings_view_error":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        name: action.name,
      });
    case "bookings_list_start":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        name: action.name,
      });
    case "bookings_list_success":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        list: replaceAll(state, action.data.list),
        count: action.data.count,
        name: action.name,
      });
    case "bookings_list_error":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        name: action.name,
      });
    case "bookings_create_start":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        name: action.name,
      });
    case "bookings_create_success":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        list: replaceOrAppend(state, action.data),
        name: action.name,
      });
    case "bookings_create_error":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        name: action.name,
      });
    case "bookings_update_start":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        name: action.name,
      });
    case "bookings_update_success":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        list: replaceOrAppend(state, action.data),
        name: action.name,
      });
    case "bookings_update_error":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        name: action.name,
      });
    case "bookings_delete_start":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        name: action.name,
      });
    case "bookings_delete_success":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        list: remove(state, action.data),
        name: action.name,
      });
    case "bookings_delete_error":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        name: action.name,
      });
    case "bookings_accept_start":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        name: action.name,
      });
    case "bookings_accept_success":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        list: remove(state, action.data),
        name: action.name,
      });
    case "bookings_accept_error":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        name: action.name,
      });
    case "bookings_reject_start":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        name: action.name,
      });
    case "bookings_reject_success":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        list: remove(state, action.data),
        name: action.name,
      });
    case "bookings_reject_error":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        name: action.name,
      });
    default:
      return state;
  }
}
