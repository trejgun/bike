const catalog = {
	list: [],
	isLoading: true,
	success: false,
	name: "",
};

export default function catalogReducer(state = catalog, action) {
	switch (action.type) {
		case "vehicles_catalog_start":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				list: [],
				name: action.name,
			});
		case "vehicles_catalog_success":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				list: action.data.list,
				name: action.name,
			});
		case "vehicles_catalog_error":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name,
			});
		default:
			return state;
	}
}
