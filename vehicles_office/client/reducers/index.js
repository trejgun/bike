import {combineReducers} from "redux";
import {routerReducer as routing} from "react-router-redux";
import {intlReducer as intl} from "react-intl-redux";
import bookings from "./bookings";
import catalog from "./catalog";
import messages from "./messages";
import organizations from "./organizations";
import policies from "./policies";
import select from "./select";
import settings from "./settings";
import statistics from "./statistics";
import subscription from "./subscription";
import typeahead from "./typeahead";
import user from "./user";
import users from "./users";
import vehicles from "./vehicles";
import validations from "./validations";


export default combineReducers({
  bookings,
  catalog,
  organizations,
  intl,
  messages,
  policies,
  routing,
  select,
  settings,
  statistics,
  subscription,
  typeahead,
  validations,
  vehicles,
  user,
  users,
});
