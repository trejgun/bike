const organizations = {
	list: [],
	count: 0,
	isLoading: true,
	success: false,
	name: "view",
};

export default function organizationsReducer(state = organizations, action) {
	switch (action.type) {
		case "organizations_view_start":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				list: [],
				count: 0,
				name: action.name,
			});
		case "organizations_view_success":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				list: [action.data],
				count: 1,
				name: action.name,
			});
		case "organizations_view_error":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				list: [action.data],
				count: 1,
				name: action.name,
			});
		case "organizations_update_start":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name,
			});
		case "organizations_update_success":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				list: [action.data],
				count: 1,
				name: action.name,
			});
		case "organizations_update_error":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name,
			});
		case "organizations_create_start":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name,
			});
		case "organizations_create_success":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				list: [action.data],
				count: 1,
				name: action.name,
			});
		case "organizations_create_error":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name,
			});
		default:
			return state;
	}
}
