import {replaceOrAppend} from "../utils/reducer";

const select = {
  list: [],
  isLoading: true,
  success: false,
  name: "",
};

export default function selectReducer(state = select, action) {
  switch (action.type) {
    case "vehicles_select_start":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        list: [],
        name: action.name,
      });
    case "vehicles_select_success":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        list: action.data.list,
        name: action.name,
      });
    case "vehicles_select_error":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        name: action.name,
      });

    case "vehicles_create_success":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        list: replaceOrAppend(state, action.data),
        name: action.name,
      });
    case "vehicles_update_success":
      return Object.assign({}, state, {
        isLoading: action.isLoading,
        success: action.success,
        list: replaceOrAppend(state, action.data),
        name: action.name,
      });
    default:
      return state;
  }
}
