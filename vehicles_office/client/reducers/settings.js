const settings = {
	list: [],
	count: 1,
	isLoading: true,
	success: false,
	name: "view",
};

export default function settingsReducer(state = settings, action) {
	switch (action.type) {
		case "settings_view_start":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				list: [],
				count: 0,
				name: action.name,
			});
		case "settings_view_success":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				list: [action.data],
				count: 1,
				name: action.name,
			});
		case "settings_view_error":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name,
			});
		case "settings_update_start":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				list: [],
				count: 0,
				name: action.name,
			});
		case "settings_update_success":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				list: [action.data],
				count: 1,
				name: action.name,
			});
		case "settings_update_error":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name,
			});
		default:
			return state;
	}
}
