const statistics = {
	isLoading: true,
	success: false,
	name: "overview",
	overview: {},
};

export default function statisticsReducer(state = statistics, action) {
	switch (action.type) {
		case "statistics_overview_start":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				overview: {},
				name: action.name,
			});
		case "statistics_overview_success":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				overview: action.data,
				name: action.name,
			});
		case "statistics_overview_error":
			return Object.assign({}, state, {
				isLoading: action.isLoading,
				success: action.success,
				name: action.name,
			});
		default:
			return state;
	}
}
