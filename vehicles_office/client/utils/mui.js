import React from "react";
import {createGenerateClassName, createMuiTheme, MuiThemeProvider} from "@material-ui/core/styles";
import JssProvider from "react-jss/lib/JssProvider";
import PropTypes from "prop-types";
import {SheetsRegistry} from "jss";


export default class MuiProvider extends React.Component {
  static propTypes = {
    children: PropTypes.node.isRequired,
    theme: PropTypes.object,
    generateClassName: PropTypes.func,
    sheetsRegistry: PropTypes.object,
    sheetsManager: PropTypes.object,
  };

  static defaultProps = {
    theme: createMuiTheme({
      typography: {
        useNextVariants: true,
      },
    }),
    // Create a new class name generator.
    generateClassName: createGenerateClassName(),
    // Create a sheetsRegistry instance.
    sheetsRegistry: new SheetsRegistry(),
    // Create a sheetsManager instance.
    sheetsManager: new Map(),
  };

  // Remove the server-side injected CSS.
  componentDidMount() {
    const jssStyles = document.getElementById("jss-server-side");
    if (jssStyles && jssStyles.parentNode) {
      jssStyles.parentNode.removeChild(jssStyles);
    }
  }

  render() {
    const {children, sheetsRegistry, generateClassName, theme, sheetsManager} = this.props;
    return (
      <JssProvider registry={sheetsRegistry} generateClassName={generateClassName}>
        <MuiThemeProvider theme={theme} sheetsManager={sheetsManager}>
          {children}
        </MuiThemeProvider>
      </JssProvider>
    );
  }
}

