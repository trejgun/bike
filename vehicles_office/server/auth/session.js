import {Router} from "express";
import session from "express-session";
import connectRedis from "connect-redis";
import connectMongo from "connect-mongo";
import redis from "./redis";
import {getConnections} from "../utils/mongoose";


const router = Router(); // eslint-disable-line new-cap

router.use(session(Object.assign({}, {
  proxy: false,
  secret: "keyboard_cat",
  saveUninitialized: true,
  resave: false,
  rolling: true,
  name: "purimotors.id",
  cookie: {
    path: "/",
    httpOnly: true,
    secure: false,
    maxAge: 24 * 60 * 60 * 1000,
    signed: false,
  },
}, {
  store: process.env.SESSION_STORE === "mongo"
    ? new (connectMongo(session))({
      mongooseConnection: getConnections().oauth2,
    })
    : new (connectRedis(session))({
      port: process.env.REDIS_PORT,
      host: process.env.REDIS_HOST,
      client: redis(),
    }),
})));

export default router;
