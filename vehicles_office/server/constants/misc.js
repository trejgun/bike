export const email = "no-reply@purimotors.com";

export const phoneNumber = "+62 (812) 3919-8760";

export const systemId = "ffffffffffffffffffffffff";

export const fullNameMinLength = 2;
export const fullNameMaxLength = 64;
export const fullName = "Trej Gun";

export const companyNameMinLength = 2;
export const companyNameMaxLength = 64;
export const companyName = "Puri Motors";

export const domainNameMinLength = 2;
export const domainNameMaxLength = 128;
export const domainName = "purimotors.com";

export const descriptionMinLength = 50;
export const descriptionMaxLength = 10000;

export const titleMinLength = 2;
export const titleMaxLength = 100;
