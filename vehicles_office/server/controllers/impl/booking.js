import bluebird from "bluebird";
import {pick} from "lodash";
import moment from "moment";
import {paginate} from "../../utils/response";
import {makeError} from "../../utils/error";
import {checkActive} from "../abstract/middleware";
import StatefulController from "../abstract/stateful";
import VehicleController from "./vehicle";
import MailController from "./mail";
import {getConnections} from "../../utils/mongoose";
import {setStatus} from "../../utils/request";


function checkIfEditable(item) {
  if (item.organizations.length === 2) {
    throw makeError("booking-not-editable", 400);
  }
}

export default class BookingController extends StatefulController {
  static realm = "vehicles_office";

  static statuses = {
    new: "new",

    accepted: "accepted",
    rejected: "rejected",
    cancelled: "cancelled",
    revoked: "revoked",
  };

  // TODO send email to customer and all managers
  static sendEmailNotification(template, bookings) {
    const mailController = new MailController();
    return bluebird.all(bookings
      .filter(booking => booking.organizations[1])
      .map(booking =>
        mailController.compose(template, {
          to: booking.email,
          bcc: "trejgun+purimotors@gmail.com",
        }, booking.organizations[1].users[0], {
          bookings: {list: [booking]},
        }),
      ),
    );
  }

  getById(request) {
    return super.getByUId(request, {
      populate: ["vehicle"],
    })
      .then(booking =>
        Object.assign(booking, {
          isOffline: booking.organizations.length === 1,
        }),
      );
  }

  insert(request) {
    const range = moment.range(request.body.range);

    // 1. check if vehicle is active
    const vehicleController = new VehicleController();
    return vehicleController.getById({params: {_id: request.body.vehicle}, body: {}, user: request.user})
      .then(vehicle =>
        // 2. check if there is another `accepted` booking at this time
        this.checkActive(vehicle, range) // TODO should throw range and fullName together
          .then(() => {
            // 3. create `accepted` booking
            const clean = pick(request.body, ["email", "fullName", "phoneNumber", "price", "notes", "country"]);
            Object.assign(clean, {
              status: BookingController.statuses.accepted,
              range,
              vehicle,
              organizations: [request.user.organization._id],
            });
            return this.create(clean)
              .then(booking => Object.assign(booking.toJSON(), {vehicle, isOffline: true}))
              .tap(booking =>
                this.killEmAll(booking.vehicle, range),
              );
          }),
      );
  }

  edit(request) {
    const range = moment.range(request.body.range);

    // 1. check booking
    return this.getByUId(request, {lean: false})
      .tap(this.conditions(request, [checkActive([BookingController.statuses.accepted, BookingController.statuses.new]), checkIfEditable]))
      .then(booking => {
        const _id = request.body.vehicle ? (request.body.vehicle._id ? request.body.vehicle._id : request.body.vehicle) : booking.vehicle;

        // 2. check vehicle
        const vehicleController = new VehicleController();
        return vehicleController.getByUId({params: {_id}, user: request.user}, {}, [checkActive()])
          .then(vehicle =>

            // 3. check if there is another `accepted` booking at this time
            this.checkActive(vehicle, range, [booking])
              .then(() => {
                const clean = pick(request.body, ["email", "fullName", "phoneNumber", "price", "notes", "country"]);
                Object.assign(booking, clean, {
                  range,
                  vehicle,
                });

                // 4. update
                return this.save(booking)
                  .then(booking => Object.assign(booking.toJSON(), {vehicle, isOffline: true}));
              }),
          );
      });
  }

  quickReject(request) {
    const connections = getConnections();
    return super.getByUId(request, {
      lean: false,
      populate: [{
        path: "vehicle",
      }, {
        path: "organizations",
        model: connections.oauth2.model("Organization"),
        populate: [{
          path: "users",
          model: connections.oauth2.model("User"),
        }],
      }],
    })
      .then(booking => {
        if (booking.status !== BookingController.statuses.new) {
          throw makeError("wrong-initial-status", 400);
        }

        booking.set("status", BookingController.statuses.rejected);
        return this.save(booking)
          .tap(() =>
            BookingController.sendEmailNotification("bookingRejectedByRenter", [booking]),
          );
      });
  }

  quickAccept(request) {
    const connections = getConnections();
    // 1. find booking
    return super.getByUId(request, {
      lean: false,
      populate: [{
        path: "organizations",
        model: connections.oauth2.model("Organization"),
        populate: [{
          path: "users",
          model: connections.oauth2.model("User"),
        }],
      }],
    })
      .then(booking => {
        if (booking.status !== BookingController.statuses.new) {
          throw makeError("wrong-initial-status", 400);
        }

        const range = moment.range(booking.range);

        // 2. check if there is another `accepted` booking at this time
        return this.checkActive(booking.vehicle, range)
          .then(() => {
            // 3. change booking status to `accepted`
            booking.set("status", BookingController.statuses.accepted);
            return this.save(booking)
              .tap(() =>
                BookingController.sendEmailNotification("bookingAcceptedByRenter", [booking]),
              )
              .tap(() =>
                // 4. kill all other bookings
                this.killEmAll(booking.vehicle, range),
              );
          });
      });
  }

  checkActive(vehicle, range, bookings = []) {
    const clean = {
      vehicle,
      status: BookingController.statuses.accepted,
      $and: [
        {start: {$lte: range.end}},
        {end: {$gte: range.start}},
      ],
    };

    if (bookings.length > 1) {
      Object.assign(clean, {
        _id: {
          $nin: bookings.map(booking => booking._id),
        },
      });
    } else if (bookings.length === 1) {
      Object.assign(clean, {
        _id: {
          $ne: bookings[0]._id,
        },
      });
    } // else bookings.length === 0

    return this.findOne(clean)
      .then(acceptedBookings => {
        if (acceptedBookings) {
          throw makeError("conflict", 409, {name: "range", reason: "not-available"});
        }
      });
  }

  killEmAll(vehicle, range) {
    const connections = getConnections();
    return this.find({
      vehicle,
      status: BookingController.statuses.new,
      $and: [
        {start: {$lte: range.end}},
        {end: {$gte: range.start}},
      ],
    }, {
      populate: [{
        path: "organizations",
        model: connections.oauth2.model("Organization"),
        populate: [{
          path: "users",
          model: connections.oauth2.model("User"),
        }],
      }],
    })
      .tap(newBookings =>
        this.update({
          _id: {
            $in: newBookings.map(b => b._id),
          },
        }, {status: BookingController.statuses.rejected}), // TODO check for $set
      )
      .tap(newBookings =>
        BookingController.sendEmailNotification("bookingRejectedByRenter", newBookings),
      );
  }

  list(request, response) {
    const clean = {
      "organizations.0": request.user.organization._id,
    };
    if (request.query.range) {
      const range = moment.range(request.query.range);
      Object.assign(clean, {
        $and: [
          {start: {$lte: range.end}},
          {end: {$gte: range.start}},
        ],
      });
    }
    setStatus(request, clean, BookingController);
    return paginate(request, response, this, clean, {
      populate: ["vehicle"],
      sort: {createdAt: -1},
    })
      .then(({count, list}) => ({
          count,
          list: list.map(item =>
            Object.assign(item, {
              isOffline: item.organizations.length === 1,
            }),
          ),
        }),
      );
  }

  deactivate(request, options = {}, conditions = []) {
    const connections = getConnections();
    return super.getByUId(request, {
      lean: false,
      populate: [{
        path: "organizations",
        model: connections.oauth2.model("Organization"),
        populate: [{
          path: "users",
          model: connections.oauth2.model("User"),
        }],
      }],
    })
      .tap(this.conditions(request, [checkActive([BookingController.statuses.new, BookingController.statuses.accepted])].concat(conditions)))
      .then(booking => {
        const newStatus = booking.status === BookingController.statuses.new ? BookingController.statuses.rejected : BookingController.statuses.cancelled;
        const emailTemplate = booking.status === BookingController.statuses.new ? "bookingRejectedByRenter" : "bookingCancelledByRenter";
        booking.set("status", newStatus);
        return this.save(booking)
          .tap(() =>
            BookingController.sendEmailNotification(emailTemplate, [booking]),
          );
      });
  }
}
