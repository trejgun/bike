import StatefulController from "../abstract/stateful";
import {difference} from "lodash";
import {makeError} from "../../utils/error";


function checkParam(clean, catalog, rest, name) {
  if (clean[name] === void 0) {
    return rest;
  }

  if (!catalog.find(vehicle => vehicle[name] === clean[name])) {
    throw makeError("invalid-param", 400, {name, reason: "invalid"});
  }

  const filtered = rest.filter(vehicle => vehicle[name] === clean[name]);

  if (!filtered.length) {
    throw makeError("invalid-param", 400, {name, reason: "combination"});
  }

  return filtered;
}

function checkInCatalog(clean, catalog, full = true) {
  let rest = catalog;

  const fields = ["type", "brand", "model", "fuel", "cc"];

  if (full) {
    const diff = difference(fields, Object.keys(clean));
    if (diff.length) {
      throw makeError("invalid-param", 400, {name: diff[0], reason: "required"});
    }
  }

  fields.forEach(name => {
    rest = checkParam(clean, catalog, rest, name);
  });

  return true;
}

export default class CatalogController extends StatefulController {
  static realm = "vehicles_office";

  static types = {
    scooter: "scooter",
    bike: "bike",
    car: "car",
    bus: "bus",
    truck: "truck",
    boat: "boat",
    plain: "plain",
  };

  static fuels = {
    petrol: "petrol",
    diesel: "diesel",
    electricity: "electricity",
  };

  check(data, full) {
    return this.find()
      .then(catalog => checkInCatalog(data, catalog, full));
  }

  list() {
    return this.find()
      .then(list => ({list, count: list.length}));
  }
}
