import {checkModel} from "../abstract/middleware";
import {renderEmailToString} from "../../utils/render.email";
import getLocalization from "../../../client/intl/setup";

import AbstractController from "../abstract/abstract";
import OptOutController from "./opt-out";


export default class MailController extends AbstractController {
  static realm = "mail";

  static statuses = {
    new: "new",

    cancelled: "cancelled",
    failed: "failed",
    sent: "sent",
  };

  static types = {
    vehicles_office: {
      bookingCancelledByCustomer: "bookingCancelledByCustomer",
      bookingRevokedByCustomer: "bookingRevokedByCustomer",
      newBookingForRenter: "newBookingForRenter",
    },
    vehicles_market: {
      bookingAcceptedByRenter: "bookingAcceptedByRenter",
      bookingCancelledByRenter: "bookingCancelledByRenter",
      bookingRejectedByRenter: "bookingRejectedByRenter",
      newBookingForCustomer: "newBookingForCustomer",
    },
    back_office: {
      missing: "missing",
    },
    oauth2: {
      forgot: "forgot",
      verification: "verification",
      welcome: "welcome",
    },
  };

  compose(type, address, user, data) {
    const optOutController = new OptOutController();
    return optOutController.findOne({
      user: user._id,
      type,
    })
      .tap(optout => checkModel().bind(optOutController)(!optout))
      .then(() =>
        renderEmailToString(type, data)
          .then(html =>
            this.create(Object.assign({
              html,
              type,
              subject: getLocalization(user.language).messages[`email.types.${type}`], // try to use real object
            }, address)),
          ),
      );
  }
}
