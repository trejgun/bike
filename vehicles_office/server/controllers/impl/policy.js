import {pick} from "lodash";
import {checkModel} from "../abstract/middleware";
import AbstractController from "../abstract/abstract";
import {systemId} from "../../constants/misc";


export default class PolicyController extends AbstractController {
  static realm = "vehicles_office";

  static types = {
    return: "return",
    island: "island",
    delivery: "delivery",
  };

  getByOrganization(request) {
    return this.findOne({
      type: request.params.type,
      // language: request.user.language,
      $or: [{
        organizations: request.user.organization._id,
      }, {
        organizations: systemId,
      }],
    }, {sort: {organizations: 1}})
      .tap(this.conditions(request, [checkModel()]));
  }

  edit(request) {
    const clean = pick(request.body, ["description", "allowed", "checkOutTime"]);
    return this.upsert({
      type: request.params.type,
      // language: request.body.language,
      organizations: request.user.organization._id,
    }, clean);
  }
}
