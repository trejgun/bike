import StatefulController from "../abstract/stateful";


export default class SettingsController extends StatefulController {
  static realm = "vehicles_office";

  edit(request) {
    return this.change(request, {}, [], ["docs"]);
  }
}
