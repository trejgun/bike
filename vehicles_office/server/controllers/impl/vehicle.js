import bluebird from "bluebird";
import {map, pick} from "lodash";
import moment from "moment";
import {setStatus} from "../../utils/request";
import {checkActive} from "../abstract/middleware";
import {date} from "../../constants/date";
import {systemId} from "../../constants/misc";
import {defaultLanguage} from "../../constants/language";
import StatefulController from "../abstract/stateful";
import {getConnections} from "../../utils/mongoose";

import CatalogController from "./catalog";
import BookingController from "./booking";
import MailController from "./mail";


export default class VehicleController extends StatefulController {
  static realm = "vehicles_office";

  static statuses = {
    active: "active",
    inactive: "inactive",
    new: "new",
  };

  typeahead(request) {
    return this.aggregate([{
      $match: {
        status: {
          $in: [
            VehicleController.statuses.active,
            VehicleController.statuses.new,
          ],
        },
        organizations: request.user.organization._id, // don't remove _id
      },
    }, {
      $group: {_id: {type: "$type", brand: "$brand", model: "$model", cc: "$cc"}},
    }])
      .then(list => ({list: map(list, "_id"), count: list.length}));
  }

  select(request) {
    return this.find({
      status: {
        $in: [
          VehicleController.statuses.active,
          VehicleController.statuses.new,
        ],
      },
      organizations: request.user.organization._id,
    }, {
      select: ["_id", "plate", "type", "brand", "model", "cc", "variant", "image"],
    })
      .then(list => ({list, count: list.length}));
  }

  catalog() {
    const catalogController = new CatalogController();
    return catalogController.list();
  }

  list(request, response) {
    const clean = pick(request.query, ["plate", "type", "brand", "model", "cc", "year"]);
    const catalogController = new CatalogController();
    return catalogController.check(clean, false)
      .then(() => {
        Object.assign(clean, {
          organizations: [request.user.organization._id],
          status: {
            $in: [
              VehicleController.statuses.new,
              VehicleController.statuses.active,
            ],
          },
        });
        setStatus(request, clean, VehicleController);
        const {skip = 0, limit = 10, availability} = request.query;
        return this.find(clean, {sort: {_id: 1}})
          .then(list =>
            this.getAvailability(list.map(item => item._id))
              .then(availabilities => availabilities.map(availability => availability.toString()))
              .then(availabilities => list.map(item =>
                  Object.assign(item, {
                    isAvailable: !availabilities.includes(item._id.toString()),
                  }),
                ),
              )
              .then(list => list.filter(item =>
                !availability || item.isAvailable),
              )
              .then(list => ({
                count: list.length,
                list: list.slice(skip, skip + limit),
              })),
          );
      });
  }

  getById(request) {
    return super.getByUId(request);
  }

  edit(request) {
    return this.getByUId(request, {lean: false})
      .then(vehicle => {
        const clean = pick(request.body, ["bluebook", "brand", "cc", "color", "discount", "fuel", "image", "model", "notes", "plate", "pricePerHour", "pricePerDay", "pricePerMonth", "taxnservice", "type", "variant", "year"]);
        const catalogController = new CatalogController();
        return catalogController.check(Object.assign({}, vehicle.toJSON(), clean))
          .then(() => {
            return this.save(Object.assign(vehicle, clean));
          });
      });
  }

  insert(request) {
    const clean = pick(request.body, ["bluebook", "brand", "cc", "color", "discount", "fuel", "image", "model", "notes", "plate", "pricePerHour", "pricePerDay", "pricePerMonth", "taxnservice", "type", "variant", "year"]);
    const catalogController = new CatalogController();
    return catalogController.check(clean)
      .then(() =>
        this.create(Object.assign(clean, {
          organization: request.user.organization._id,
        })),
      );
  }

  getAvailability(vehicles = [], start = date, end = date) {
    const bookingController = new BookingController();
    return bookingController.distinct("vehicle", {
      vehicle: {$in: vehicles},
      $and: [
        {start: {$lte: end}},
        {end: {$gte: start}},
      ],
    });
  }

  batch(request) {
    const {count} = request.body;
    const clean = pick(request.body, [/* "bluebook", */"brand", "cc", "discount", "fuel", "image", "model", /* "plate", */ "pricePerHour", "pricePerDay", "pricePerMonth", /* "taxnservice", */ "type", "year"]); // eslint-disable-line standard/array-bracket-even-spacing

    const catalogController = new CatalogController();
    return catalogController.check(clean)
      .then(() =>
        this.insertMany(new Array(count).fill(Object.assign(clean, {
          organization: request.user.organization._id,
        }))),
      )
      .then(list => ({list, count}));
  }

  deactivate(request) {
    return this.getByUId(request, {lean: false})
      .tap(this.conditions(request, [checkActive([VehicleController.statuses.new])]))
      .then(item => {
        item.set("status", VehicleController.statuses.inactive);
        return this.save(item);
      })
      .tap(vehicle => {
        const connections = getConnections();
        const bookingController = new BookingController();
        return bookingController.find({
          vehicle: vehicle._id,
          status: {
            $in: [
              BookingController.statuses.new,
              BookingController.statuses.accepted,
            ],
          },
        }, {
          populate: [{
            path: "vehicle",
          }, {
            path: "organizations",
            model: connections.oauth2.model("Organization"),
            populate: [{
              path: "users",
              model: connections.oauth2.model("User"),
            }],
          }],
        })
          .tap(bookings =>
            bluebird.all([
              this.update({
                _id: {
                  $in: bookings
                    .filter(b => b.status === BookingController.statuses.new)
                    .map(b => b._id),
                },
              }, {status: BookingController.statuses.rejected}) // TODO check for $set

                .then(() =>
                  BookingController.sendEmailNotification("bookingRejectedByRenter", bookings
                    .filter(b => b.status === BookingController.statuses.new)),
                ),
              this.update({
                _id: {
                  $in: bookings
                    .filter(b => b.status === BookingController.statuses.accepted)
                    .map(b => b._id),
                },
              }, {status: BookingController.statuses.cancelled}) // TODO check for $set
                .then(() =>
                  BookingController.sendEmailNotification("bookingCancelledByRenter", bookings
                    .filter(b => b.status === BookingController.statuses.accepted)),
                ),
            ]),
          );
      });
  }

  missing(request) {
    const mailController = new MailController();
    return mailController.compose("missing", {
      to: "trejgun+missing@gmail.com",
    }, {
      _id: systemId,
      organization: {_id: systemId},
      language: defaultLanguage,
    }, {
      messages: [{
        data: {
          vehicle: request.body,
          user: request.user.toJSON(),
        },
      }],
    })
      .thenReturn({success: true});
  }

  bookings(request) {
    return this.getByUId(request)
      .then(() => {
        const range = moment.range(request.query.range);
        const bookingController = new BookingController();
        return bookingController.find({
          vehicle: request.params._id,
          status: {
            $in: [
              BookingController.statuses.accepted,
              BookingController.statuses.new,
            ],
          },
          $and: [
            {start: {$lte: range.end}},
            {end: {$gte: range.start}},
          ],
        }, {
          populate: [{
            path: "vehicle",
          }],
        })
          .then(list => ({
            list: list.map(item =>
              Object.assign(item, {
                isOffline: item.organizations.length === 1,
              }),
            ),
          }));
      });
  }
}
