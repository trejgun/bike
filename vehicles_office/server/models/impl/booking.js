import {Schema} from "mongoose";
import moment from "moment";
import ShortId from "mongoose-shortid-nodeps";
import version from "@streethub/mongoose-version";

import {getRandomString} from "../../utils/misc";
import BookingController from "../../controllers/impl/booking";

import status from "../plugins/status";
import phoneNumber from "../plugins/phoneNumber";
import fullName from "../plugins/fullName";
import email from "../plugins/email";
import country from "../plugins/country";


const Booking = new Schema({
  vehicle: {
    type: Schema.Types.ObjectId,
    ref: "Vehicle",
  },

  bookingId: {
    type: ShortId,
    len: 8,
    generator: (options, callback) => callback(null, getRandomString(options.len, 2)),
  },

  notes: String,

  price: {
    type: Number,
    required: [true, "required"],
    min: [0, "min"],
  },

  start: {
    type: Date,
  },
  end: {
    type: Date,
  },
  range: {
    type: String,
    required: [true, "required"],
  },

}, {
  toJSON: {
    transform(doc, ret) {
      // ret.range = `${ret.start.toISOString()}/${ret.end.toISOString()}`;
      ret.isOffline = ret.organizations.length === 1;
      delete ret.start;
      delete ret.end;
      return ret;
    },
  },
});

Booking.plugin(fullName);
Booking.plugin(phoneNumber, {verified: false});
Booking.plugin(email, {unique: false, required: false, verified: false});
Booking.plugin(status, {controller: BookingController});
Booking.plugin(country);

Booking.plugin(version, {
  collection: "BookingVersions",
  suppressVersionIncrement: false,
});

Booking.pre("save", function preSaveRange(next) {
  const {start, end} = moment.range(this.range);
  Object.assign(this, {start, end});
  next();
});

export default Booking;
