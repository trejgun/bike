import {Schema} from "mongoose";
import {
  companyNameMaxLength,
  companyNameMinLength,
  domainNameMaxLength,
  domainNameMinLength,
} from "../../constants/misc";
import {arrayGetter} from "../../utils/mongoose";

import email from "../plugins/email";
import language from "../plugins/language";
import status from "../plugins/status";
import phoneNumber from "../plugins/phoneNumber";
import type from "../plugins/type";


import OrganizationController from "../../controllers/impl/organization";

const Organization = new Schema({
  companyName: {
    type: String,
    required: "required",
    minlength: [companyNameMinLength, "minlength"],
    maxlength: [companyNameMaxLength, "maxlength"],
  },
  domainName: {
    type: String,
    index: {
      unique: true,
      sparse: true,
      name: "domainName-duplicate",
    },
    minlength: [domainNameMinLength, "minlength"],
    maxlength: [domainNameMaxLength, "maxlength"],
  },
  address: {
    type: String,
  },
  image: {
    type: String,
  },

  location: {
    type: {
      type: String,
      enum: ["Point", "LineString", "Polygon"],
      default: "Point",
    },
    coordinates: {
      type: [Number],
      default: [0, 0],
      get: arrayGetter,
    },
  },
}, {
  toJSON: {
    transform(doc, ret) {
      ret.created = ret.createdAt;
      ret.users = ret.users || void 0;
      delete ret.address;
      delete ret.location;
      return ret;
    },
  },
});

Organization.plugin(status, {controller: OrganizationController});
Organization.plugin(type, {controller: OrganizationController});
Organization.plugin(email);
Organization.plugin(phoneNumber);
Organization.plugin(language);

Organization.virtual("users", {
  ref: "User",
  localField: "_id",
  foreignField: "organizations",
});

Organization.virtual("settings", {
  ref: "Settings",
  localField: "_id",
  foreignField: "organizations",
  justOne: true,
});

Organization
  .virtual("coordinates")
  // .get(function getCoordinates() {
  // 	return this.location && this.location.coordinates;
  // })
  .set(function setCoordinates(coordinates) {
    this.location.coordinates = coordinates;
  });

Organization.index({
  location: "2dsphere",
});

export default Organization;
