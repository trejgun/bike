import {Schema} from "mongoose";
import language from "../plugins/language";
import type from "../plugins/type";
import description from "../plugins/description";
import {reShortTime} from "../../constants/regexp";
import {checkOutTime} from "../../constants/date"; // eslint-disable-line standard/object-curly-even-spacing
import PolicyController from "../../controllers/impl/policy";


const Policy = new Schema({
  organizations: [{
    type: Schema.Types.ObjectId,
    ref: "Organization",
  }],

  checkOutTime: {
    type: String,
    default: checkOutTime,
    match: [reShortTime, "invalid"],
  },
  /* timeZone: {
    type: String,
    required: "time-zone-required",
    enum: timeZones
  }, */

  allowed: {
    type: String,
    default: "not",
    enum: {
      values: ["free", "fee", "not"],
      message: "invalid",
    },
  },
});

Policy.plugin(language);
Policy.plugin(type, {controller: PolicyController});
Policy.plugin(description);

export default Policy;
