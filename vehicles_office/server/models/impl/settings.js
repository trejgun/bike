import {Schema} from "mongoose";

const Settings = new Schema({
  organizations: [{
    type: Schema.Types.ObjectId,
    ref: "Organization",
  }],

  docs: {
    type: Boolean,
    default: false,
  },
});


export default Settings;
