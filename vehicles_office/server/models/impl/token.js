import {Schema} from "mongoose";
import {getRandomString} from "../../utils/misc";
import ShortId from "mongoose-shortid-nodeps";


const Token = new Schema({
  user: {
    type: Schema.Types.ObjectId,
  },
  token: {
    type: ShortId,
    len: 20,
    generator: (options, callback) => callback(null, getRandomString(options.len)),
  },
  type: {
    type: String,
  },
});

Token.index({
  user: 1,
  type: 1,
}, {unique: true});

Token.index({
  updatedAt: 1,
}, {
  expireAfterSeconds: 3600,
});

export default Token;
