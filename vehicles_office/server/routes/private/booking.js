import {Router} from "express";
import crud from "../../utils/crud";
import {checkPermissions, methodNotAllowed, params, validateParams} from "../../utils/middleware";
import {wrapJSON} from "../../utils/wrapper";
import BookingController from "../../controllers/impl/booking";

const object = "bookings";
const router = Router(); // eslint-disable-line new-cap
const bookingController = new BookingController();

router.route(`/${object}/:${BookingController.param}/accept`)
  .get(checkPermissions(`${object}:update`), wrapJSON(::bookingController.quickAccept))
  .all(methodNotAllowed);

router.route(`/${object}/:${BookingController.param}/reject`)
  .get(checkPermissions(`${object}:update`), wrapJSON(::bookingController.quickReject))
  .all(methodNotAllowed);

// additional validation
router.route(`/${object}`)
  .post(validateParams([params.range], "body"));

router.route(`/${object}/:${BookingController.param}`)
  .put(validateParams([params.range], "body"));

crud(router, bookingController, {object});

export default router;
