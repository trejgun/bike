import {Router} from "express";
import crud from "../../utils/crud";
import {wrapJSON} from "../../utils/wrapper";
import {methodNotAllowed} from "../../utils/middleware";
import OrganizationController from "../../controllers/impl/organization";


const object = "organizations";
const router = Router(); // eslint-disable-line new-cap
const organizationController = new OrganizationController();

router.route(`/${object}/overview`)
  .get(wrapJSON(::organizationController.overview))
  .all(methodNotAllowed);

crud(router, organizationController, {object});

export default router;
