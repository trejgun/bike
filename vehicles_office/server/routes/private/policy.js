import {Router} from "express";
import {wrapJSON} from "../../utils/wrapper";
import {checkPermissions, methodNotAllowed} from "../../utils/middleware";
import PolicyController from "../../controllers/impl/policy";


// TODO move this under organization
const object = "policies";
const object2 = "organizations";
const router = Router(); // eslint-disable-line new-cap
const policyController = new PolicyController();

router.route(`/${object}/:type`)
  .get(checkPermissions(`${object2}:read`), wrapJSON(::policyController.getByOrganization))
  .put(checkPermissions(`${object2}:update`), wrapJSON(::policyController.edit))
  .all(methodNotAllowed);

export default router;
