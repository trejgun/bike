import {Router} from "express";
import SettingsController from "../../controllers/impl/settings";
import {wrapJSON} from "../../utils/wrapper";
import {checkPermissions, methodNotAllowed} from "../../utils/middleware";
import {mongoId} from "../../utils/validator";

// TODO move this under organization
const object = "settings";
const object2 = "organizations";
const router = Router(); // eslint-disable-line new-cap
const settingsController = new SettingsController();

router.param("_id", mongoId); // mongo id

router.route(`/${object}/:${SettingsController.param}`)
  .get(checkPermissions(`${object2}:read`), wrapJSON(::settingsController.getById))
  .put(checkPermissions(`${object2}:update`), wrapJSON(::settingsController.edit))
  .all(methodNotAllowed);

export default router;
