import {Router} from "express";
import {wrapJSON} from "../../utils/wrapper";
import {methodNotAllowed} from "../../utils/middleware";
import OptOutController from "../../controllers/impl/opt-out";
import {mongoId} from "../../utils/validator";


// TODO move this under user
const object = "subscription";
const router = Router(); // eslint-disable-line new-cap
const optOutController = new OptOutController();

router.param("_id", mongoId); // mongo id

// there is no checkPermissions here because user always can edit himself
router.route(`/${object}/:${OptOutController.param}`)
  .get(wrapJSON(::optOutController.getByUser))
  .put(wrapJSON(::optOutController.replace))
  .all(methodNotAllowed);

export default router;
