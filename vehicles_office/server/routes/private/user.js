import {Router} from "express";
import {wrapJSON} from "../../utils/wrapper";
import {checkPermissions, methodNotAllowed} from "../../utils/middleware";
import UserController from "../../controllers/impl/user";
import {mongoId} from "../../utils/validator";
import crud from "../../utils/crud";


const object = "users";
const router = Router(); // eslint-disable-line new-cap
const userController = new UserController();

router.param("_id", mongoId); // mongo id

router.route("/users/typeahead")
  .get(checkPermissions(`${object}:read`), wrapJSON(::userController.typeahead))
  .all(methodNotAllowed);

crud(router, userController, {object});

export default router;
