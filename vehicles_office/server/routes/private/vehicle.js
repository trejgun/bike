import {Router} from "express";
import {wrapJSON} from "../../utils/wrapper";
import {checkPermissions, methodNotAllowed, params, validateParams} from "../../utils/middleware";
import crud from "../../utils/crud";
import VehicleController from "../../controllers/impl/vehicle";


const object = "vehicles";
const router = Router(); // eslint-disable-line new-cap
const vehicleController = new VehicleController();

router.route(`/${object}/typeahead`)
  .get(checkPermissions(`${object}:read`), wrapJSON(::vehicleController.typeahead))
  .all(methodNotAllowed);

router.route(`/${object}/select`)
  .get(checkPermissions(`${object}:read`), wrapJSON(::vehicleController.select))
  .all(methodNotAllowed);


// there is no checkPermissions here because catalog is public
router.route(`/${object}/catalog`)
  .get(wrapJSON(::vehicleController.catalog))
  .all(methodNotAllowed);

// there is no checkPermissions here because anyone can request to add new vehicle to catalog
router.route(`/${object}/missing`)
  .post(validateParams([].concat(["type", "brand", "model", "fuel"].map(name => ({
    name,
    type: String,
    required: true,
    minlength: 1,
  })), {name: "cc", type: Number, min: 50, required: true}), "body"), wrapJSON(::vehicleController.missing))
  .all(methodNotAllowed);

router.route(`/${object}/batch`)
  .post(checkPermissions(`${object}:create`), validateParams([{
    name: "count",
    type: Number,
    min: 1,
    max: 20,
  }], "body"), wrapJSON(::vehicleController.batch))
  .all(methodNotAllowed);

router.route(`/${object}/:_id/bookings`)
  .get(checkPermissions(`${object}:read`), checkPermissions("bookings:read"), validateParams([params.range], "query"), wrapJSON(::vehicleController.bookings))
  .all(methodNotAllowed);

crud(router, vehicleController, {object});

export default router;
