import bluebird from "bluebird";
import winston from "winston";
import assert from "power-assert";
import supertest from "../../../test-utils/supertest";
import {setUp, tearDown} from "../../../test-utils/flow";
import {fullName, notes, phoneNumber, price} from "../../../test/constants";
import {getRange} from "../../../test-utils/utils";
import {getNewId} from "../../../server/shared/utils/mongoose";
import expect from "../../../test-utils/assert";


import BookingController from "../../server/controllers/impl/booking";
import VehicleController from "../../server/controllers/impl/vehicle";
import MailController from "../../../mail/server/controllers/impl/mail";

let data;

describe("Booking (vehicles_office)", () => {
  const superapp = supertest("vehicles_office");

  const newPrice = 200;
  const newCountry = "UA";
  const newNotes = "my new notes".repeat(100);

  const bookingController = new BookingController();
  const mailController = new MailController();

  afterEach(() =>
    mailController.deleteMany(),
  );

  describe("GET /bookings", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 3,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 2,
      }, {
        model: "Vehicle",
        requires: {
          Organization: [0, 1],
        },
        count: 2,
      }, {
        model: "Booking",
        requires: {
          Vehicle: [0, 1, 2],
          Organization: [[0, 1], [0, 1], [1, 2]],
        },
        data: [{
          status: BookingController.statuses.accepted,
        }, {
          status: BookingController.statuses.rejected,
        }, {
          status: BookingController.statuses.new,
        }],
      }])
        .then(result => {
          data = result;
        }),
    );

    // TODO flickering, probably need to sort
    it.skip("should get bookings", () =>
      superapp
        .login(data.User[0])
        .get("/api/bookings")
        .expect(200)
        .then(({body}) => {
          console.log("body", body.list);
          assert.equal(body.count, 2);
          assert.equal(body.list[0].status, BookingController.statuses.accepted);
          assert.equal(body.list[0].vehicle._id, data.Vehicle[0]._id.toString());
          assert.equal(body.list[1].status, BookingController.statuses.rejected);
          assert.equal(body.list[1].vehicle._id, data.Vehicle[1]._id.toString());
        }),
    );

    after(tearDown);
  });

  describe("POST /bookings", () => {
    const plate1 = "DK2123BB";
    const plate2 = "DK2123CC";

    before(() =>
      setUp([{
        model: "Organization",
        count: 2,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 2,
      }, {
        model: "Vehicle",
        requires: {
          Organization: "o2o",
        },
        data: [{
          plate: plate1,
        }, {
          plate: plate2,
        }],
      }, {
        model: "Booking",
        requires: {
          Vehicle: [0, 0],
          Organization: [[0, 1], [0, 1]],
        },
        data: [{
          range: getRange(2, 5),
          status: BookingController.statuses.new,
        }, {
          range: getRange(10, 13),
          status: BookingController.statuses.accepted,
        }],
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should create booking", () =>
      superapp
        .login(data.User[0])
        .post("/api/bookings")
        .type("application/json;charset=utf-8")
        .send({
          vehicle: data.Vehicle[0]._id,
          fullName,
          phoneNumber,
          price,
          range: getRange(2, 5),
          notes,
          country: newCountry,
        })
        .expect(200)
        .then(({body}) => {
          winston.debug("body", body);
          assert.equal(body.status, BookingController.statuses.accepted);
          assert.equal(body.range, getRange(2, 5));
          assert.equal(body.vehicle.plate, plate1);
          assert.equal(body.price, price);
          assert.equal(body.notes, notes);
          assert.equal(body.country, newCountry);
        }),
    );

    it("should throw `vehicle-not-found`", () =>
      superapp
        .login(data.User[0])
        .post("/api/bookings")
        .type("application/json;charset=utf-8")
        .send({
          range: getRange(2, 5),
        })
        .then(expect(404, [{name: "vehicle"}])),
    );

    it("should throw `access-denied` (org)", () =>
      superapp
        .login(data.User[0])
        .post("/api/bookings")
        .type("application/json;charset=utf-8")
        .send({
          vehicle: data.Vehicle[1]._id,
          range: getRange(2, 5),
        })
        .then(expect(403, [{reason: "organization"}])),
    );

    it("should throw `invalid-param` (fullname, price)[required]", () =>
      superapp
        .login(data.User[0])
        .post("/api/bookings")
        .type("application/json;charset=utf-8")
        .send({
          vehicle: data.Vehicle[0]._id,
          range: getRange(6, 8),
        })
        .then(expect(400, [{reason: "required", name: "fullName"}, {reason: "required", name: "price"}])),
    );

    it("should throw `invalid-param` (range)[required]", () =>
      superapp
        .login(data.User[0])
        .post("/api/bookings")
        .type("application/json;charset=utf-8")
        .send({
          vehicle: data.Vehicle[0]._id,
        })
        .then(expect(400, [{reason: "required", name: "range"}])),
    );

    it("should throw `invalid-param` (range)[invalid]", () =>
      superapp
        .login(data.User[0])
        .post("/api/bookings")
        .type("application/json;charset=utf-8")
        .send({
          vehicle: data.Vehicle[0]._id,
          fullName,
          phoneNumber,
          range: "crap",
        })
        .then(expect(400, [{reason: "invalid", name: "range"}])),
    );

    it("should throw `conflict` (range)[not-available/start]", () =>
      superapp
        .login(data.User[0])
        .post("/api/bookings")
        .type("application/json;charset=utf-8")
        .send({
          vehicle: data.Vehicle[0]._id,
          fullName,
          phoneNumber,
          range: getRange(9, 11),
        })
        .then(expect(409, [{reason: "not-available", name: "range"}])),
    );

    it("should throw `conflict` (range)[not-available/in]", () =>
      superapp
        .login(data.User[0])
        .post("/api/bookings")
        .type("application/json;charset=utf-8")
        .send({
          vehicle: data.Vehicle[0]._id,
          fullName,
          phoneNumber,
          range: getRange(11, 12),
        })
        .then(expect(409, [{reason: "not-available", name: "range"}])),
    );

    it("should throw `conflict` (range)[not-available/out]", () =>
      superapp
        .login(data.User[0])
        .post("/api/bookings")
        .type("application/json;charset=utf-8")
        .send({
          vehicle: data.Vehicle[0]._id,
          fullName,
          phoneNumber,
          range: getRange(8, 14),
        })
        .then(expect(409, [{reason: "not-available", name: "range"}])),
    );

    it("should throw `conflict` (range)[not-available/end]", () =>
      superapp
        .login(data.User[0])
        .post("/api/bookings")
        .type("application/json;charset=utf-8")
        .send({
          vehicle: data.Vehicle[0]._id,
          fullName,
          phoneNumber,
          range: getRange(11, 13),
        })
        .then(expect(409, [{reason: "not-available", name: "range"}])),
    );

    it("should throw `invalid-param` (price)[min]", () =>
      superapp
        .login(data.User[0])
        .post("/api/bookings")
        .type("application/json;charset=utf-8")
        .send({
          vehicle: data.Vehicle[0]._id,
          fullName,
          price: -1,
          phoneNumber,
          range: getRange(15, 17),
        })
        .then(expect(400, [{reason: "min", name: "price"}])),
    );

    it("should throw `invalid-param` (country)", () =>
      superapp
        .login(data.User[0])
        .post("/api/bookings")
        .type("application/json;charset=utf-8")
        .send({
          vehicle: data.Vehicle[0]._id,
          fullName,
          country: "XX",
          phoneNumber,
          price,
          range: getRange(15, 17),
        })
        .then(expect(400, [{reason: "invalid", name: "country"}])),
    );

    after(tearDown);
  });

  describe("GET /bookings/:_id", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 2,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 2,
      }, {
        model: "Vehicle",
        requires: {
          Organization: [0, 1],
        },
        count: 2,
      }, {
        model: "Booking",
        requires: {
          Vehicle: [0, 1],
          Organization: [[0, 1], [0, 1], [1, 1]],
        },
        data: [{
          status: BookingController.statuses.accepted,
        }, {
          status: BookingController.statuses.rejected,
        }, {
          status: BookingController.statuses.accepted,
        }],
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should get booking (active)", () =>
      superapp
        .login(data.User[0])
        .get(`/api/bookings/${data.Booking[0]._id}`)
        .expect(200)
        .then(({body}) => {
          winston.debug("body", body);
          assert.equal(body.status, BookingController.statuses.accepted);
          assert.equal(body.price, price);
        }),
    );

    it("should get booking (inactive)", () =>
      superapp
        .login(data.User[0])
        .get(`/api/bookings/${data.Booking[1]._id}`)
        .expect(200)
        .then(({body}) => {
          winston.debug("body", body);
          assert.equal(body.status, BookingController.statuses.rejected);
        }),
    );

    it("should throw `access-denied`", () =>
      superapp
        .login(data.User[0])
        .get(`/api/bookings/${data.Booking[2]._id}`)
        .then(expect(403, [{reason: "organization"}])),
    );

    after(tearDown);
  });

  describe("PUT /bookings/:_id", () => {
    describe("@pass (booking)", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 2,
        }, {
          model: "User",
          requires: {
            Organization: "o2o",
          },
          count: 2,
        }, {
          model: "Vehicle",
          requires: {
            Organization: [0],
          },
          count: 1,
        }, {
          model: "Booking",
          requires: {
            Vehicle: [0, 0],
            Organization: [[0], [0, 1]],
          },
          data: [{
            status: BookingController.statuses.accepted,
            range: getRange(3, 4),
          }, {
            status: BookingController.statuses.new,
            range: getRange(5, 6),
          }],
        }])
          .then(result => {
            data = result;
          }),
      );

      it("should edit booking", () =>
        superapp
          .login(data.User[0])
          .put(`/api/bookings/${data.Booking[0]._id}`)
          .type("application/json;charset=utf-8")
          .send({
            range: getRange(5, 6),
            price: newPrice,
            notes: newNotes,
            country: newCountry,
          })
          .expect(200)
          .then(({body}) => {
            winston.debug("body", body);
            assert.equal(body.range, getRange(5, 6));
            assert.equal(body.price, newPrice);
            assert.equal(body.notes, newNotes);
            assert.equal(body.country, newCountry);
          }),
      );

      after(tearDown);
    });

    describe("@pass (vehicle)", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 2,
        }, {
          model: "User",
          requires: {
            Organization: "o2o",
          },
          count: 2,
        }, {
          model: "Vehicle",
          requires: {
            Organization: [0, 0],
          },
          count: 2,
        }, {
          model: "Booking",
          requires: {
            Vehicle: [0],
            Organization: [[0]],
          },
          data: [{
            status: BookingController.statuses.accepted,
            range: getRange(3, 4),
          }],
        }])
          .then(result => {
            data = result;
          }),
      );

      it("should edit booking", () =>
        superapp
          .login(data.User[0])
          .put(`/api/bookings/${data.Booking[0]._id}`)
          .type("application/json;charset=utf-8")
          .send({
            range: getRange(5, 6),
            vehicle: data.Vehicle[1]._id,
          })
          .expect(200)
          .then(({body}) => {
            winston.debug("body", body);
            assert.equal(body.range, getRange(5, 6));
            assert.equal(body.vehicle._id, data.Vehicle[1]._id.toString());
          }),
      );
    });

    describe("@throw (booking)", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 2,
        }, {
          model: "User",
          requires: {
            Organization: "o2o",
          },
          count: 2,
        }, {
          model: "Vehicle",
          requires: {
            Organization: [0, 0],
          },
          count: 2,
        }, {
          model: "Booking",
          requires: {
            Vehicle: [0, 0, 1, 1],
            Organization: [[0], [0, 1], [0, 1], [0, 1], [1]],
          },
          data: [{
            status: BookingController.statuses.accepted,
            range: getRange(3, 4),
          }, {
            status: BookingController.statuses.accepted,
            range: getRange(5, 6),
          }, {
            status: BookingController.statuses.accepted,
          }, {
            status: BookingController.statuses.rejected,
          }, {
            status: BookingController.statuses.new,
          }],
        }])
          .then(result => {
            data = result;
          }),
      );

      it("should throw `conflict` (range)[not-available]", () =>
        superapp
          .login(data.User[0])
          .put(`/api/bookings/${data.Booking[0]._id}`)
          .type("application/json;charset=utf-8")
          .send({
            range: getRange(5, 6),
          })
          .then(({body}) => {
            winston.debug("body", body);
            assert.equal(body.errors.length, 1);
            assert.equal(body.errors[0].message, "conflict");
            assert.equal(body.errors[0].reason, "not-available");
            assert.equal(body.errors[0].name, "range");
          }),
      );

      it("should throw `booking-not-editable`", () =>
        superapp
          .login(data.User[0])
          .put(`/api/bookings/${data.Booking[2]._id}`)
          .type("application/json;charset=utf-8")
          .send({
            range: getRange(5, 6),
          })
          .then(({body}) => {
            winston.debug("body", body);
            assert.equal(body.errors.length, 1);
            assert.equal(body.errors[0].message, "booking-not-editable");
          }),
      );

      it("should throw `not-active` (booking)", () =>
        superapp
          .login(data.User[0])
          .put(`/api/bookings/${data.Booking[3]._id}`)
          .type("application/json;charset=utf-8")
          .send({
            range: getRange(5, 6),
          })
          .then(expect(410, [{name: "booking"}])),
      );

      it("should throw `access-denied`", () =>
        superapp
          .login(data.User[0])
          .put(`/api/bookings/${data.Booking[4]._id}`)
          .type("application/json;charset=utf-8")
          .send({
            range: getRange(5, 6),
          })
          .then(expect(403, [{reason: "organization"}])),
      );

      it("should throw `booking-not-found`", () =>
        superapp
          .login(data.User[0])
          .put(`/api/bookings/${getNewId()}`)
          .type("application/json;charset=utf-8")
          .send({
            range: getRange(5, 6),
          })
          .then(expect(404, [{name: "booking"}])),
      );

      after(tearDown);
    });

    describe("@throw (vehicle)", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 2,
        }, {
          model: "User",
          requires: {
            Organization: "o2o",
          },
          count: 2,
        }, {
          model: "Vehicle",
          requires: {
            Organization: [0, 0, 1],
          },
          data: [{}, {
            plate: "",
            status: VehicleController.statuses.new,
          }, {}],
        }, {
          model: "Booking",
          requires: {
            Vehicle: [0, 0, 0],
            Organization: [[0], [0], [0]],
          },
          data: [{
            status: BookingController.statuses.new,
          }, {
            status: BookingController.statuses.new,
          }, {
            status: BookingController.statuses.new,
          }],
        }])
          .then(result => {
            data = result;
          }),
      );

      it("should throw `not-active` (vehicle)", () =>
        superapp
          .login(data.User[0])
          .put(`/api/bookings/${data.Booking[0]._id}`)
          .type("application/json;charset=utf-8")
          .send({
            vehicle: data.Vehicle[1]._id,
            range: getRange(5, 6),
          })
          .then(expect(410, [{name: "vehicle"}])),
      );

      it("should throw `access-denied`", () =>
        superapp
          .login(data.User[0])
          .put(`/api/bookings/${data.Booking[1]._id}`)
          .type("application/json;charset=utf-8")
          .send({
            vehicle: data.Vehicle[2],
            range: getRange(5, 6),
          })
          .then(expect(403, [{reason: "organization"}])),
      );

      it("should throw `vehicle-not-found`", () =>
        superapp
          .login(data.User[0])
          .put(`/api/bookings/${data.Booking[2]._id}`)
          .type("application/json;charset=utf-8")
          .send({
            vehicle: getNewId(),
            range: getRange(5, 6),
          })
          .then(expect(404, [{name: "vehicle"}])),
      );

      after(tearDown);
    });
  });

  describe("GET /bookings/:_id/accept", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 2,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 2,
      }, {
        model: "Vehicle",
        requires: {
          Organization: [0, 0, 0],
        },
        count: 3,
      }, {
        model: "Booking",
        requires: {
          Vehicle: [0, 1, 1, 2, 2, 2, 2, 2],
          Organization: [[0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1]],
        },
        data: [{
          range: getRange(2, 5),
          status: BookingController.statuses.cancelled,
        }, {
          range: getRange(2, 5),
        }, {
          range: getRange(3, 4),
          status: BookingController.statuses.accepted,
        }, {
          range: getRange(1, 3),
        }, {
          range: getRange(3, 4),
        }, {
          range: getRange(2, 5),
        }, {
          range: getRange(4, 6),
        }, {
          range: getRange(2, 5),
          status: BookingController.statuses.cancelled,
        }],
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should throw `wrong-initial-status`", () =>
      superapp
        .login(data.User[0])
        .get(`/api/bookings/${data.Booking[0]._id}/accept`)
        .then(({body}) => {
          winston.info("body", body);
          assert.equal(body.errors.length, 1);
          assert.equal(body.errors[0].message, "wrong-initial-status");
        }),
    );

    it("should throw `range-not-available`", () =>
      superapp
        .login(data.User[0])
        .get(`/api/bookings/${data.Booking[1]._id}/accept`)
        .then(({body}) => {
          winston.info("body", body);
          assert.equal(body.errors.length, 1);
          assert.equal(body.errors[0].message, "conflict");
          assert.equal(body.errors[0].reason, "not-available");
          assert.equal(body.errors[0].name, "range");
        }),
    );

    it("should accept booking", () =>
      superapp
        .login(data.User[0])
        .get(`/api/bookings/${data.Booking[5]._id}/accept`)
        .expect(200)
        .then(({body}) => {
          winston.debug("body", body);
          assert.equal(body.status, BookingController.statuses.accepted);
          return bluebird.all([
            bookingController.find({
              _id: {
                $in: [
                  data.Booking[3]._id,
                  data.Booking[4]._id,
                  data.Booking[5]._id,
                  data.Booking[6]._id,
                  data.Booking[7]._id,
                ],
              },
            }),
            mailController.find(),
          ])
            .spread((bookings, mails) => {
              assert.equal(bookings[0].status, BookingController.statuses.rejected);
              assert.equal(bookings[1].status, BookingController.statuses.rejected);
              assert.equal(bookings[2].status, BookingController.statuses.accepted);
              assert.equal(bookings[3].status, BookingController.statuses.rejected);
              assert.equal(bookings[4].status, BookingController.statuses.cancelled);

              assert.deepEqual(mails.map(mail => mail.type).sort(), ["bookingAcceptedByRenter", "bookingRejectedByRenter", "bookingRejectedByRenter", "bookingRejectedByRenter"]);
            });
        }),
    );

    after(tearDown);
  });

  describe("GET /bookings/:_id/reject", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 2,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 2,
      }, {
        model: "Vehicle",
        requires: {
          Organization: [0, 0, 0],
        },
        count: 3,
      }, {
        model: "Booking",
        requires: {
          Vehicle: [0, 1, 1, 2, 2, 2, 2, 2],
          Organization: [[0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1], [0, 1]],
        },
        data: [{
          range: getRange(2, 5),
          status: BookingController.statuses.cancelled,
        }, {
          range: getRange(2, 5),
        }, {
          range: getRange(3, 4),
          status: BookingController.statuses.accepted,
        }, {
          range: getRange(1, 3),
        }, {
          range: getRange(3, 4),
        }, {
          range: getRange(2, 5),
        }, {
          range: getRange(4, 6),
        }, {
          range: getRange(2, 5),
          status: BookingController.statuses.cancelled,
        }],
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should throw `wrong-initial-status`", () =>
      superapp
        .login(data.User[0])
        .get(`/api/bookings/${data.Booking[0]._id}/reject`)
        .then(({body}) => {
          winston.info("body", body);
          assert.equal(body.errors.length, 1);
          assert.equal(body.errors[0].message, "wrong-initial-status");
        }),
    );

    it("should reject booking", () =>
      superapp
        .login(data.User[0])
        .get(`/api/bookings/${data.Booking[5]._id}/reject`)
        .expect(200)
        .then(({body}) => {
          winston.debug("body", body);
          assert.equal(body.status, BookingController.statuses.rejected);
          return bluebird.all([
            bookingController.find({
              _id: {
                $in: [
                  data.Booking[3]._id,
                  data.Booking[4]._id,
                  data.Booking[5]._id,
                  data.Booking[6]._id,
                  data.Booking[7]._id,
                ],
              },
            }),
            mailController.find(),
          ])
            .spread((bookings, mails) => {
              assert.equal(bookings[0].status, BookingController.statuses.new);
              assert.equal(bookings[1].status, BookingController.statuses.new);
              assert.equal(bookings[2].status, BookingController.statuses.rejected);
              assert.equal(bookings[3].status, BookingController.statuses.new);
              assert.equal(bookings[4].status, BookingController.statuses.cancelled);

              assert.deepEqual(mails.map(mail => mail.type).sort(), ["bookingRejectedByRenter"]);
            });
        }),
    );

    after(tearDown);
  });

  describe("DELETE /bookings/:_id", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 2,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 2,
      }, {
        model: "Vehicle",
        requires: {
          Organization: [0],
        },
        count: 1,
      }, {
        model: "Booking",
        requires: {
          Vehicle: [0, 0, 0, 0, 1],
          Organization: [[0, 1], [0, 1], [0, 1], [0, 1], [1, 1]],
        },
        data: [{}, {
          status: BookingController.statuses.accepted,
        }, {
          status: BookingController.statuses.rejected,
        }, {
          status: BookingController.statuses.cancelled,
        }, {}],
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should reject booking", () =>
      superapp
        .login(data.User[0])
        .delete(`/api/bookings/${data.Booking[0]._id}`)
        .expect(200)
        .then(({body}) => {
          winston.debug("body", body);
          assert.equal(body.status, BookingController.statuses.rejected);
          return mailController.find()
            .then(mails => {
              assert.deepEqual(mails.map(mail => mail.type).sort(), ["bookingRejectedByRenter"]);
            });
        }),
    );

    it("should cancel booking", () =>
      superapp
        .login(data.User[0])
        .delete(`/api/bookings/${data.Booking[1]._id}`)
        .expect(200)
        .then(({body}) => {
          winston.debug("body", body);
          assert.equal(body.status, BookingController.statuses.cancelled);
          return mailController.find()
            .then(mails => {
              assert.deepEqual(mails.map(mail => mail.type).sort(), ["bookingCancelledByRenter"]);
            });
        }),
    );

    it("should throw `not-active` (booking)", () =>
      superapp
        .login(data.User[0])
        .delete(`/api/bookings/${data.Booking[2]._id}`)
        .then(expect(410, [{name: "booking"}])),
    );

    it("should throw `not-active` (booking)", () =>
      superapp
        .login(data.User[0])
        .delete(`/api/bookings/${data.Booking[3]._id}`)
        .then(expect(410, [{name: "booking"}])),
    );

    it("should throw `access-denied`", () =>
      superapp
        .login(data.User[0])
        .delete(`/api/bookings/${data.Booking[4]._id}`)
        .then(expect(403, [{reason: "organization"}])),
    );

    after(tearDown);
  });
});
