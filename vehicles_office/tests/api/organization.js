import assert from "power-assert";
import supertest from "../../../test-utils/supertest";
import {setUp, tearDown} from "../../../test-utils/flow";
import {companyName, coordinates, email} from "../../../test/constants";
import {
  companyNameMaxLength,
  companyNameMinLength,
  domainNameMaxLength,
  domainNameMinLength,
} from "../../server/constants/misc";
import expect from "../../../test-utils/assert";
import BookingController from "../../server/controllers/impl/booking";
import permissions from "../../server/constants/permissions";


let data;

describe("Organizations (vehicles_office)", () => {
  const superapp = supertest("vehicles_office");

  const newCheckOutTime = "09:00";
  const newCompanyName = "My new company name";
  const dupDomainName = "dup.com";
  const dupEmail = "dup@dup.com";
  const address = "123 very long street";

  describe("PUT /organizations/:_id", () => {
    before(() =>
      setUp([{
        model: "Organization",
        data: [{}, {}, {
          domainName: dupDomainName,
          email: dupEmail,
        }],
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 2,
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should edit company", () =>
      superapp
        .login(data.User[0])
        .put(`/api/organizations/${data.Organization[0]._id}`)
        .type("application/json;charset=utf-8")
        .send({
          // TODO FIXME
          checkOutTime: newCheckOutTime,
          companyName: newCompanyName,
          coordinates,
          address,
        })
        .expect(200)
        .then(({body}) => {
          assert.equal(body.companyName, newCompanyName);
          assert.equal(body.location, void 0);
          assert.deepEqual(body.coordinates, coordinates);
          assert.equal(body.address, address);
        }),
    );

    it("should throw `access-denied`", () =>
      superapp
        .login(data.User[0])
        .put(`/api/organizations/${data.Organization[1]._id}`)
        .type("application/json;charset=utf-8")
        .send({
          companyName,
        })
        .then(expect(403, [{reason: "organization"}])),
    );

    it.skip("should throw `invalid-param` (checkOutTime)", () =>
      superapp
        .login(data.User[1])
        .put(`/api/organizations/${data.Organization[1]._id}`)
        .type("application/json;charset=utf-8")
        .send({
          checkOutTime: "90:00",
        })
        .then(expect(400, [{reason: "invalid", name: "checkOutTime"}])),
    );

    it("should throw `invalid-param` (email)[invalid]", () =>
      superapp
        .login(data.User[1])
        .put(`/api/organizations/${data.Organization[1]._id}`)
        .type("application/json;charset=utf-8")
        .send({
          email: "crap",
        })
        .then(expect(400, [{reason: "invalid", name: "email"}])),
    );

    it("should throw `invalid-param` (email)[duplicate]", () =>
      superapp
        .login(data.User[1])
        .put(`/api/organizations/${data.Organization[1]._id}`)
        .type("application/json;charset=utf-8")
        .send({
          email: dupEmail,
        })
        .then(expect(409, [{reason: "duplicate", name: "email"}])),
    );

    it.skip("should throw `invalid-param` (delivery)", () =>
      superapp
        .login(data.User[1])
        .put(`/api/organizations/${data.Organization[1]._id}`)
        .type("application/json;charset=utf-8")
        .send({
          delivery: "crap",
        })
        .then(expect(400, [{reason: "invalid", name: "delivery"}])),
    );

    it("should throw `invalid-param` (companyName)[minlength]", () =>
      superapp
        .login(data.User[1])
        .put(`/api/organizations/${data.Organization[1]._id}`)
        .type("application/json;charset=utf-8")
        .send({
          companyName: "a".repeat(companyNameMinLength - 1),
        })
        .then(expect(400, [{reason: "minlength", name: "companyName"}])),
    );

    it("should throw `invalid-param` (companyName)[maxlength]", () =>
      superapp
        .login(data.User[1])
        .put(`/api/organizations/${data.Organization[1]._id}`)
        .type("application/json;charset=utf-8")
        .send({
          companyName: "a".repeat(companyNameMaxLength + 1),
        })
        .then(expect(400, [{reason: "maxlength", name: "companyName"}])),
    );

    it("should throw `invalid-param` (domainName)[minlength]", () =>
      superapp
        .login(data.User[1])
        .put(`/api/organizations/${data.Organization[1]._id}`)
        .type("application/json;charset=utf-8")
        .send({
          domainName: "a".repeat(domainNameMinLength - 1),
        })
        .then(expect(400, [{reason: "minlength", name: "domainName"}])),
    );

    it("should throw `invalid-param` (domainName)[maxlength]", () =>
      superapp
        .login(data.User[1])
        .put(`/api/organizations/${data.Organization[1]._id}`)
        .type("application/json;charset=utf-8")
        .send({
          domainName: "a".repeat(domainNameMaxLength + 1),
        })
        .then(expect(400, [{reason: "maxlength", name: "domainName"}])),
    );

    it("should throw `invalid-param` (domainName)[duplicate]", () =>
      superapp
        .login(data.User[1])
        .put(`/api/organizations/${data.Organization[1]._id}`)
        .type("application/json;charset=utf-8")
        .send({
          domainName: dupDomainName,
        })
        .then(expect(409, [{reason: "duplicate", name: "domainName"}])),
    );

    it("should throw `required`", () =>
      superapp
        .login(data.User[1])
        .put(`/api/organizations/${data.Organization[1]._id}`)
        .type("application/json;charset=utf-8")
        .send({
          email: "",
          companyName: "",
        })
        .then(expect(400, [{reason: "required", name: "companyName"}, {reason: "required", name: "email"}])),
    );

    after(tearDown);
  });

  describe("GET /organizations/overview", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 2,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 1,
      }, {
        model: "Vehicle",
        requires: {
          Organization: [0, 0, 1],
        },
        count: 3,
      }, {
        model: "Booking",
        requires: {
          Organization: [0, 0, 0, 0, 1],
          Vehicle: [0, 0, 1, 1, 2],
        },
        data: [{
          status: BookingController.statuses.accepted,
        }, {
          status: BookingController.statuses.new,
        }, {
          status: BookingController.statuses.accepted,
        }, {
          status: BookingController.statuses.rejected,
        }, {
          status: BookingController.statuses.accepted,
        }],
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should show overview", () =>
      superapp
        .login(data.User[0])
        .get("/api/organizations/overview")
        .expect(200)
        .then(({body}) => {
          assert.equal(body.vehicles, 2);
          assert.equal(body.bookings, 2);
        }),
    );

    after(tearDown);
  });

  describe("GET /organizations/:_id", () => {
    before(() =>
      setUp([{
        model: "Organization",
        data: [{
          companyName,
          coordinates,
          address,
        }, {}],
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 2,
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should get company", () =>
      superapp
        .login(data.User[0])
        .get(`/api/organizations/${data.Organization[0]._id}`)
        .expect(200)
        .then(({body}) => {
          assert.equal(body.companyName, companyName);
          assert.equal(body.address, address);
          assert.equal(body.location, void 0);
          assert.deepEqual(body.coordinates, coordinates);
        }),
    );

    it("should throw `access-denied`", () =>
      superapp
        .login(data.User[0])
        .get(`/api/organizations/${data.Organization[1]._id}`)
        .then(expect(403, [{reason: "organization"}])),
    );

    after(tearDown);
  });

  // TODO check me
  describe.skip("POST /organizations", () => {
    before(() =>
      setUp([{
        model: "User",
        data: [{
          permissions: [].concat(permissions, "office:organizations:create"),
        }, {
          permissions: [].concat(permissions, "office:organizations:create"),
        }],
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should create organization", () =>
      superapp
        .login(data.User[0])
        .post("/api/organizations")
        .type("application/json;charset=utf-8")
        .send({
          email,
          companyName,
          coordinates,
          address,
        })
        .expect(200)
        .then(({body}) => {
          assert.equal(body.email, email);
          assert.equal(body.companyName, companyName);
          assert.deepEqual(body.coordinates, coordinates);
          assert.equal(body.address, address);
        }),
    );

    it("should throw `required`", () =>
      superapp
        .login(data.User[1])
        .post("/api/organizations")
        .type("application/json;charset=utf-8")
        .send({})
        .then(expect(400, [{reason: "required", name: "email"}, {reason: "required", name: "companyName"}])),
    );

    after(tearDown);
  });
});
