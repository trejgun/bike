import assert from "power-assert";
import supertest from "../../../test-utils/supertest";
import {setUp, tearDown} from "../../../test-utils/flow";
import {systemId} from "../../../test/constants";
import {defaultLanguage as language} from "../../server/constants/language";

import PolicyController from "../../server/controllers/impl/policy";

let data;

describe("Policy (vehicles_office)", () => {
  const superapp = supertest("vehicles_office");

  describe("GET /policies/:type", () => {
    before(() =>
      setUp([{
        model: "Organization",
        data: [{}, {}, {
          _id: systemId,
        }],
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 2,
      }, {
        model: "Policy",
        requires: {
          Organization: [1, 2],
        },
        count: 2,
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should get default policy", () =>
      superapp
        .login(data.User[0])
        .get("/api/policies/return")
        .expect(200)
        .then(({body}) => {
          assert.equal(body._id, data.Policy[1]._id);
        }),
    );

    it("should get custom policy", () =>
      superapp
        .login(data.User[1])
        .get("/api/policies/return")
        .expect(200)
        .then(({body}) => {
          assert.equal(body._id, data.Policy[0]._id);
        }),
    );

    after(tearDown);
  });

  describe("PUT /policy/:type", () => {
    const newDescription = "my new policy".repeat(100);
    const newCheckOutTime = "09:00";
    const newAllowed = "fee";

    before(() =>
      setUp([{
        model: "Organization",
        data: [{}, {}, {
          _id: systemId,
        }],
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 2,
      }, {
        model: "Policy",
        requires: {
          Organization: [1, 2],
        },
        count: 2,
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should edit default policy", () =>
      superapp
        .login(data.User[0])
        .put("/api/policies/return")
        .type("application/json;charset=utf-8")
        .send({
          language,
          type: PolicyController.types.return,
          description: newDescription,
          checkOutTime: newCheckOutTime,
          allowed: newAllowed,
        })
        .expect(200)
        .then(({body}) => {
          assert.notEqual(body._id, data.Policy[1]._id.toString());
          assert.equal(body.type, PolicyController.types.return);
          assert.equal(body.description, newDescription);
          assert.equal(body.checkOutTime, newCheckOutTime);
          assert.equal(body.allowed, newAllowed);
        }),
    );

    it("should edit custom policy", () =>
      superapp
        .login(data.User[1])
        .put("/api/policies/return")
        .type("application/json;charset=utf-8")
        .send({
          language,
          type: PolicyController.types.return,
          description: newDescription,
          checkOutTime: newCheckOutTime,
          allowed: newAllowed,
        })
        .expect(200)
        .then(({body}) => {
          assert.equal(body._id, data.Policy[0]._id);
          assert.equal(body.type, PolicyController.types.return);
          assert.equal(body.description, newDescription);
          assert.equal(body.checkOutTime, newCheckOutTime);
          assert.equal(body.allowed, newAllowed);
        }),
    );

    after(tearDown);
  });
});
