import winston from "winston";
import assert from "power-assert";
import supertest from "../../../test-utils/supertest";
import {setUp, tearDown} from "../../../test-utils/flow";
import {companyName} from "../../../test/constants";
import expect from "../../../test-utils/assert";


let data;

describe("Settings (vehicles_office)", () => {
  const superapp = supertest("vehicles_office");

	describe("GET /settings/:_id", () => {
		before(() =>
			setUp([{
				model: "Organization",
        count: 2,
			}, {
				model: "Settings",
				requires: {
          Organization: "o2o",
				},
        count: 2,
			}, {
				model: "User",
				requires: {
          Organization: "o2o",
				},
        count: 2,
			}])
				.then(result => {
					data = result;
        }),
		);

		it("should get settings", () =>
			superapp
				.login(data.User[0])
				.get(`/api/settings/${data.Settings[0]._id}`)
				.expect(200)
				.then(({body}) => {
					winston.info("body", body);
					assert.equal(body.docs, false);
        }),
		);

		it("should throw `access-denied`", () =>
			superapp
				.login(data.User[0])
				.get(`/api/settings/${data.Settings[1]._id}`)
        .then(expect(403, [{reason: "organization"}])),
		);

		after(tearDown);
	});

	describe("PUT /settings/:_id", () => {
		before(() =>
			setUp([{
				model: "Organization",
        count: 2,
			}, {
				model: "Settings",
				requires: {
          Organization: "o2o",
				},
        count: 2,
			}, {
				model: "User",
				requires: {
          Organization: "o2o",
				},
        count: 2,
			}])
				.then(result => {
					data = result;
        }),
		);

		it("should edit settings", () =>
			superapp
				.login(data.User[0])
				.put(`/api/settings/${data.Settings[0]._id}`)
				.type("application/json;charset=utf-8")
				.send({
          docs: true,
				})
				.expect(200)
				.then(({body}) => {
					winston.info("body", body);
					assert.equal(body.docs, true);
        }),
		);

		it("should throw `access-denied`", () =>
			superapp
				.login(data.User[0])
				.get(`/api/settings/${data.Settings[1]._id}`)
				.type("application/json;charset=utf-8")
				.send({
          companyName,
				})
        .then(expect(403, [{reason: "organization"}])),
		);

		after(tearDown);
	});
});
