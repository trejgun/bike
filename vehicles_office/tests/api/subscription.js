import winston from "winston";
import assert from "power-assert";
import supertest from "../../../test-utils/supertest";
import {setUp, tearDown} from "../../../test-utils/flow";
import expect from "../../../test-utils/assert";

import MailController from "../../../mail/server/controllers/impl/mail";


let data;

describe("Subscription (vehicles_office)", () => {
  const superapp = supertest("vehicles_office");

  describe("GET /subscription/:_id", () => {
    before(() =>
      setUp([{
        model: "User",
        count: 2,
      }, {
        model: "OptOut",
        requires: {
          User: [0],
        },
        data: [{
          type: MailController.types[process.env.MODULE].newBookingForRenter,
        }],
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should return subscriptions", () =>
      superapp
        .login(data.User[0])
        .get(`/api/subscription/${data.User[0]._id}`)
        .expect(200)
        .then(({body}) => {
          winston.info("body", body);
          Object.keys(MailController.types[process.env.MODULE]).forEach(key => {
            assert.equal(body[key], key !== MailController.types[process.env.MODULE].newBookingForRenter);
          });
        }),
    );

    it("should throw `access-denied`", () =>
      superapp
        .login(data.User[1])
        .get(`/api/subscription/${data.User[0]._id}`)
        .then(expect(403, [{reason: "user"}])),
    );

    after(tearDown);
  });

  describe("PUT /subscription/:_id", () => {
    before(() =>
      setUp([{
        model: "User",
        count: 2,
      }, {
        model: "OptOut",
        requires: {
          User: [0],
        },
        data: [{
          type: MailController.types.vehicles_office.newBookingForRenter,
        }],
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should edit subscriptions", () =>
      superapp
        .login(data.User[0])
        .put(`/api/subscription/${data.User[0]._id}`)
        .type("application/json;charset=utf-8")
        .send({
          [MailController.types.vehicles_office.bookingRevokedByCustomer]: false,
        })
        .expect(200)
        .then(({body}) => {
          const excluded = [
            MailController.types.vehicles_office.newBookingForRenter,
            MailController.types.vehicles_office.bookingRevokedByCustomer,
          ];
          Object.keys(MailController.types[process.env.MODULE]).forEach(key => {
            assert.equal(body[key], !excluded.includes(key));
          });
        }),
    );

    it("should throw `invalid-param`", () =>
      superapp
        .login(data.User[0])
        .put(`/api/subscription/${data.User[0]._id}`)
        .type("application/json;charset=utf-8")
        .send({
          crap: false,
        })
        .then(expect(400, [{reason: "unrecognized", name: "crap"}])),
    );

    it("should throw `access-denied`", () =>
      superapp
        .login(data.User[1])
        .put(`/api/subscription/${data.User[0]._id}`)
        .type("application/json;charset=utf-8")
        .send({
          [MailController.types["vehicles_market"].bookingCancelledByRenter]: true,
        })
        .then(expect(403, [{reason: "user"}])),
    );

    after(tearDown);
  });
});
