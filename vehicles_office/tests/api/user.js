import winston from "winston";
import assert from "power-assert";
import supertest from "../../../test-utils/supertest";
import {setUp, tearDown} from "../../../test-utils/flow";
import expect from "../../../test-utils/assert";
import permissions from "../../server/constants/permissions";
import UserController from "../../../oauth2/server/controllers/impl/user";


let data;

describe("User (vehicles_office)", () => {
  const superapp = supertest("vehicles_office");

  describe("GET /sync", () => {
    before(() =>
      setUp([{
        model: "User",
        count: 1,
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should return nothing", () =>
      superapp
        .login(null)
        .get("/api/sync")
        .expect(200)
        .then(({body}) => {
          winston.info("body", body);
          assert.equal(body, null);
        }),
    );

    it("should return user", () =>
      superapp
        .login(data.User[0])
        .get("/api/sync")
        .expect(200)
        .then(({body}) => {
          winston.debug("body", body);
          assert.equal(body._id, data.User[0]._id.toString());
        }),
    );

    after(tearDown);
  });

  describe("GET /users", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 2,
      }, {
        model: "User",
        requires: {
          Organization: [1, 1, 1, 2, 2],
        },
        count: 5,
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should get list of users", () =>
      superapp
        .login(data.User[0])
        .get("/api/users")
        .expect(200)
        .then(({body}) => {
          winston.debug("body", body);
          assert.equal(body.list.length, 3);
        }),
    );

    after(tearDown);
  });

  describe("GET /users/typeahead", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 2,
      }, {
        model: "User",
        requires: {
          Organization: [1, 1, 1, 2, 2],
        },
        count: 5,
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should get list of users", () =>
      superapp
        .login(data.User[0])
        .get("/api/users/typeahead")
        .expect(200)
        .then(({body}) => {
          winston.info("body", body);
          assert.equal(body.typeahead.length, 3);
        }),
    );

    after(tearDown);
  });

  describe("PUT /users/:_id", () => {
    describe("@pass (simple)", () => {
      const fullName = "My New Full Name";

      before(() =>
        setUp([{
          model: "Organization",
          count: 1,
        }, {
          model: "User",
          requires: {
            Organization: "m2o",
          },
          count: 2,
        }])
          .then(result => {
            data = result;
          }),
      );

      it("should edit self", () =>
        superapp
          .login(data.User[0])
          .put(`/api/users/${data.User[0]._id}`)
          .type("application/json;charset=utf-8")
          .send({
            fullName,
          })
          .expect(200)
          .then(({body}) => {
            winston.debug("body", body);
            assert.equal(body.fullName, fullName);
          }),
      );

      it("should edit user", () =>
        superapp
          .login(data.User[0])
          .put(`/api/users/${data.User[1]._id}`)
          .type("application/json;charset=utf-8")
          .send({
            fullName,
          })
          .expect(200)
          .then(({body}) => {
            winston.debug("body", body);
            assert.equal(body.fullName, fullName);
          }),
      );

      after(tearDown);
    });

    describe("@throw (simple)", () => {
      const fullName = "My New Full Name";

      before(() =>
        setUp([{
          model: "Organization",
          count: 2,
        }, {
          model: "User",
          requires: {
            Organization: [0, 0, 0, 1],
          },
          data: [{
            permissions: [],
          }, {}, {}, {}],
        }])
          .then(result => {
            data = result;
          }),
      );

      it("should throw `access-denied` (permissions)", () =>
        superapp
          .login(data.User[0])
          .put(`/api/users/${data.User[1]._id}`)
          .type("application/json;charset=utf-8")
          .send({
            fullName,
          })
          .then(expect(403, [{reason: "permissions"}])),
      );

      it("should throw `access-denied` (organization)", () =>
        superapp
          .login(data.User[2])
          .put(`/api/users/${data.User[3]._id}`)
          .type("application/json;charset=utf-8")
          .send({
            fullName,
          })
          .then(expect(403, [{reason: "organization"}])),
      );

      after(tearDown);
    });

    describe("@throw (permissions)", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 1,
        }, {
          model: "User",
          requires: {
            Organization: "m2o",
          },
          data: [{
            permissions: ["vehicles_office:users:update"],
          }, {}, {}],
        }])
          .then(result => {
            data = result;
          }),
      );

      it("should not set permissions which user don't have", () =>
        superapp
          .login(data.User[0])
          .put(`/api/users/${data.User[1]._id}`)
          .type("application/json;charset=utf-8")
          .send({
            permissions: [permissions[0]],
          })
          .then(expect(403, [{name: "permissions", reason: "insufficient", items: [permissions[0]]}])),
      );

      it("should not set permissions which are not exist", () =>
        superapp
          .login(data.User[0])
          .put(`/api/users/${data.User[2]._id}`)
          .type("application/json;charset=utf-8")
          .send({
            permissions: ["crap"],
          })
          .then(expect(400, [{name: "permissions", reason: "invalid"}])),
      );

      after(tearDown);
    });
  });

  describe("DELETE /users/:_id", () => {
    describe("@pass", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 1,
        }, {
          model: "User",
          requires: {
            Organization: "m2o",
          },
          count: 2,
        }])
          .then(result => {
            data = result;
          }),
      );

      it("should delete user", () =>
        superapp
          .login(data.User[0])
          .delete(`/api/users/${data.User[1]._id}`)
          .expect(200)
          .then(({body}) => {
            winston.debug("body", body);
            assert.equal(body.status, UserController.statuses.inactive);
          }),
      );

      after(tearDown);
    });

    describe("@throw", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 2,
        }, {
          model: "User",
          requires: {
            Organization: [0, 0, 0, 1],
          },
          data: [{
            permissions: [],
          }, {}, {}, {}],
        }])
          .then(result => {
            data = result;
          }),
      );

      it("should throw `access-denied` (permissions)", () =>
        superapp
          .login(data.User[0])
          .delete(`/api/users/${data.User[1]._id}`)
          .then(expect(403, [{reason: "permissions"}])),
      );

      it("should throw `access-denied` (organization)", () =>
        superapp
          .login(data.User[2])
          .delete(`/api/users/${data.User[3]._id}`)
          .then(expect(403, [{reason: "organization"}])),
      );

      after(tearDown);
    });
  });
});
