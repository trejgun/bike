import assert from "power-assert";
import supertest from "../../../test-utils/supertest";
import expect from "../../../test-utils/assert";
import {setUp, tearDown} from "../../../test-utils/flow";
import {getNewId} from "../../server/utils/mongoose";
import {date} from "../../server/constants/date";
import {brand, cc, fuel, model, pricePerDay, type, year} from "../../../test/constants";
import {getRange} from "../../../test-utils/utils";
import {vehicleObject} from "../../../test-utils/objects";
import BookingController from "../../server/controllers/impl/booking";
import VehicleController from "../../server/controllers/impl/vehicle";
import MailController from "../../../mail/server/controllers/impl/mail";
import CatalogController from "../../server/controllers/impl/catalog";
import catalog from "../../../test/vehicles";


let data;

describe("Vehicle (vehicles_office)", () => {
  const superapp = supertest("vehicles_office");

  const mailController = new MailController();
  const catalogController = new CatalogController();

  afterEach(() =>
    mailController.deleteMany(),
  );

  const plate = "DK2123AA";

  function myVehicleObject(obj) {
    return vehicleObject({
      type,
      plate,
      brand,
      cc,
      model,
      year,
      pricePerDay,
    }, obj);
  }

  describe("GET /vehicles", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 1,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 1,
      }, {
        model: "Vehicle",
        requires: {
          Organization: "m2o",
        },
        data: [{
          plate,
        }, {
          plate: "DK0001AA",
        }, {
          plate: "DK0001AB",
        }, {
          plate: "BP0001AA",
        }, {
          plate: "AE0001AA",
          status: VehicleController.statuses.inactive,
        }],
      }, {
        model: "Booking",
        requires: {
          Organization: "m2o",
          Vehicle: "o2o",
        },
        data: [{
          range: getRange(2, 4),
        }, {
          range: getRange(-2, 2),
        }],
      }])
        .then(result => {
          data = result;
          return catalogController.create(catalog);
        }),
    );

    it("should get list of vehicles", () =>
      superapp
        .login(data.User[0])
        .get("/api/vehicles")
        .expect(200)
        .then(({body}) => {
          assert.equal(body.count, 4);
          assert.equal(body.list.length, 4);
          assert.equal(body.list[0].isAvailable, true);
          assert.equal(body.list[1].isAvailable, false);
          assert.equal(body.list[2].isAvailable, true);
          assert.equal(body.list[3].isAvailable, true);
        }),
    );

    it("should respect `availability`", () =>
      superapp
        .login(data.User[0])
        .get("/api/vehicles")
        .query({
          availability: true,
        })
        .expect(200)
        .then(({body}) => {
          assert.equal(body.count, 3);
          assert.equal(body.list.length, 3);
          assert.equal(body.list[0].isAvailable, true);
          assert.equal(body.list[1].isAvailable, true);
          assert.equal(body.list[2].isAvailable, true);
        }),
    );

    it("should respect `limit`", () =>
      superapp
        .login(data.User[0])
        .get("/api/vehicles")
        .query({
          limit: 2,
        })
        .expect(200)
        .then(({body}) => {
          assert.equal(body.count, 4);
          assert.equal(body.list.length, 2);
        }),
    );

    it("should respect `skip`", () =>
      superapp
        .login(data.User[0])
        .get("/api/vehicles")
        .query({
          skip: 2,
        })
        .expect(200)
        .then(({body}) => {
          assert.equal(body.count, 4);
          assert.equal(body.list.length, 2);
        }),
    );

    it("should respect `plate`", () =>
      superapp
        .login(data.User[0])
        .get("/api/vehicles")
        .query({
          plate,
        })
        .expect(200)
        .then(({body}) => {
          assert.equal(body.count, 1);
          assert.equal(body.list.length, 1);
        }),
    );

    it("should respect `status`", () =>
      superapp
        .login(data.User[0])
        .get("/api/vehicles")
        .query({
          status: VehicleController.statuses.active,
        })
        .expect(200)
        .then(({body}) => {
          assert.equal(body.count, 4);
          assert.equal(body.list.length, 4);
        }),
    );

    it("should return 0 results", () =>
      superapp
        .login(data.User[0])
        .get("/api/vehicles")
        .query({
          plate: "DK0000XX",
        })
        .expect(200)
        .then(({body}) => {
          assert.equal(body.count, 0);
          assert.equal(body.list.length, 0);
        }),
    );

    after(tearDown);
  });

  describe("POST /vehicles", () => {
    describe("@pass", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 1,
        }, {
          model: "User",
          requires: {
            Organization: "o2o",
          },
          count: 1,
        }])
          .then(result => {
            data = result;
            return catalogController.create(catalog);
          }),
      );

      it("should create vehicle (active)", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({pricePerMonth: ""}))
          .expect(200)
          .then(({body}) => {
            assert.equal(body.plate, plate);
            assert.equal(body.pricePerDay, pricePerDay);
            assert.equal(body.pricePerMonth, 0);
            assert.equal(body.status, VehicleController.statuses.active);
          }),
      );

      it("should create vehicle (new/plate)", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({plate: void 0}))
          .expect(200)
          .then(({body}) => {
            assert.equal(body.plate, void 0);
            assert.equal(body.status, VehicleController.statuses.new);
          }),
      );

      it("should create vehicle (new/price)", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({pricePerDay: void 0}))
          .expect(200)
          .then(({body}) => {
            assert.equal(body.pricePerDay, 0);
            assert.equal(body.status, VehicleController.statuses.new);
          }),
      );

      after(tearDown);
    });

    describe("@throw", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 1,
        }, {
          model: "User",
          requires: {
            Organization: "o2o",
          },
          count: 1,
        }])
          .then(result => {
            data = result;
            return catalogController.create(catalog);
          }),
      );

      it.skip("should throw `invalid-param` (plate)[invalid]", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({plate: "crap"}))
          .then(expect(400, [{reason: "invalid", name: "plate"}])),
      );

      it("should throw `invalid-param` (year)[max]", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({year: 1}))
          .then(expect(400, [{reason: "min", name: "year"}])),
      );

      it("should throw `invalid-param` (year)[max]", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({year: date.getFullYear() + 1}))
          .then(expect(400, [{reason: "max", name: "year"}])),
      );

      it("should throw `invalid-param` (color)[invalid]", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({color: "black"}))
          .then(expect(400, [{reason: "invalid", name: "color"}])),
      );

      it("should throw `invalid-param` (price-per-x)[min]", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({
            pricePerHour: -1,
            pricePerDay: -1,
            pricePerMonth: -1,
          }))
          .then(expect(400, [{
            reason: "min",
            name: "pricePerHour",
          }, {
            reason: "min",
            name: "pricePerDay",
          }, {
            reason: "min",
            name: "pricePerMonth",
          }])),
      );

      it.skip("should throw `price-per-x-lt-min`", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({
            pricePerHour: "",
            pricePerDay: null,
            pricePerMonth: [1],
          }))
          .then(({body}) => {
            assert.equal(body.errors.length, 3);
            assert.equal(body.errors[0].reason, "price-per-month-not-integer");
            assert.equal(body.errors[1].reason, "price-per-day-not-integer");
            assert.equal(body.errors[2].reason, "price-per-hour-not-integer");
          }),
      );

      after(tearDown);
    });

    describe("@throw (catalog)", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 1,
        }, {
          model: "User",
          requires: {
            Organization: "o2o",
          },
          count: 1,
        }])
          .then(result => {
            data = result;
            return catalogController.create(catalog);
          }),
      );

      it("should throw `invalid-param` (type)[required]", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({type: void 0}))
          .then(expect(400, [{reason: "required", name: "type"}])),
      );

      it("should throw `invalid-param` (type)[invalid]", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({type: "crap"}))
          .then(expect(400, [{reason: "invalid", name: "type"}])),
      );

      it("should throw `invalid-param` (brand)[required]", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({brand: void 0}))
          .then(expect(400, [{reason: "required", name: "brand"}])),
      );

      it("should throw `invalid-param` (brand)[invalid]", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({brand: "crap"}))
          .then(expect(400, [{reason: "invalid", name: "brand"}])),
      );

      it("should throw `invalid-param` (brand)[combination]", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({type: "bike", brand: "Toyota"}))
          .then(expect(400, [{reason: "combination", name: "brand"}])),
      );

      it("should throw `invalid-param` (model)[required]", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({model: void 0}))
          .then(expect(400, [{reason: "required", name: "model"}])),
      );

      it("should throw `invalid-param` (model)[invalid]", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({model: "crap"}))
          .then(expect(400, [{reason: "invalid", name: "model"}])),
      );

      it("should throw `invalid-param` (model)[combination]", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({brand: "Kawasaki", model: "Vario"}))
          .then(expect(400, [{reason: "combination", name: "model"}])),
      );

      it("should throw `invalid-param` (fuel)[required]", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({fuel: void 0}))
          .then(expect(400, [{reason: "required", name: "fuel"}])),
      );

      it("should throw `invalid-param` (fuel)[invalid]", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({fuel: "crap"}))
          .then(expect(400, [{reason: "invalid", name: "fuel"}])),
      );

      it("should throw `invalid-param` (fuel)[combination]", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({model: "Vario", fuel: "diesel"}))
          .then(expect(400, [{reason: "combination", name: "fuel"}])),
      );

      it("should throw `invalid-param` (cc)[required]", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({cc: void 0}))
          .then(expect(400, [{reason: "required", name: "cc"}])),
      );

      it("should throw `invalid-param` (cc)[invalid]", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({cc: "crap"}))
          .then(expect(400, [{reason: "invalid", name: "cc"}])),
      );

      it("should throw `invalid-param` (cc)[combination]", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({type: "car", brand: "Suzuki", model: "Swift", fuel: "diesel", cc: 1500}))
          .then(expect(400, [{reason: "combination", name: "cc"}])),
      );

      after(tearDown);
    });
  });

  describe("PUT /vehicles/:_id", () => {
    describe("@pass (plate)", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 1,
        }, {
          model: "User",
          requires: {
            Organization: "o2o",
          },
          count: 1,
        }, {
          model: "Vehicle",
          requires: {
            Organization: [0, 0, 0, 0],
          },
          data: [{
            plate: "DK1234AA",
          }, {
            plate: "",
            status: VehicleController.statuses.new,
          }, {
            plate: "",
            pricePerDay: 0,
            status: VehicleController.statuses.new,
          }, {
            plate: "DK1234BB",
          }],
        }])
          .then(result => {
            data = result;
            return catalogController.create(catalog);
          }),
      );

      it("should edit vehicle (active -> active)", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[0]._id}`)
          .type("application/json;charset=utf-8")
          .send({plate: "DK4321AA"})
          .expect(200)
          .then(({body}) => {
            assert.equal(body.plate, "DK4321AA");
            assert.equal(body.status, VehicleController.statuses.active);
          }),
      );

      it("should edit vehicle (new -> active)", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[1]._id}`)
          .type("application/json;charset=utf-8")
          .send({plate: "DK4321BB"})
          .expect(200)
          .then(({body}) => {
            assert.equal(body.plate, "DK4321BB");
            assert.equal(body.status, VehicleController.statuses.active);
          }),
      );

      it("should edit vehicle (new -> new)", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[2]._id}`)
          .type("application/json;charset=utf-8")
          .send({plate: "DK4321CC"})
          .expect(200)
          .then(({body}) => {
            assert.equal(body.plate, "DK4321CC");
            assert.equal(body.status, VehicleController.statuses.new);
          }),
      );

      it("should edit vehicle (active -> new)", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[3]._id}`)
          .type("application/json;charset=utf-8")
          .send({plate: ""})
          .expect(200)
          .then(({body}) => {
            assert.equal(body.plate, void 0);
            assert.equal(body.status, VehicleController.statuses.new);
          }),
      );

      after(tearDown);
    });

    describe("@throw (catalog)", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 1,
        }, {
          model: "User",
          requires: {
            Organization: "o2o",
          },
          count: 1,
        }, {
          model: "Vehicle",
          requires: {
            Organization: "m2o",
          },
          data: [{
            // type
          }, {
            // brand
          }, {
            model: "Vario", // -> Scoopy
            cc: 110,
          }, {
            type: "car",
            brand: "Suzuki",
            model: "Swift",
            fuel: "petrol", // -> diesel
            cc: 1300,
          }, {
            // cc 125 -> 150
          }],
        }])
          .then(result => {
            data = result;
            return catalogController.create(catalog);
          }),
      );

      it.skip("should replace type", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[0]._id}`)
          .type("application/json;charset=utf-8")
          .send({type: ""})
          .expect(200)
          .then(({body}) => {
            // winston.info("body", body);
          }),
      );

      it.skip("should replace brand", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[1]._id}`)
          .type("application/json;charset=utf-8")
          .send({brand: ""})
          .expect(200)
          .then(({body}) => {
            // winston.info("body", body);
          }),
      );

      it("should replace model", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[2]._id}`)
          .type("application/json;charset=utf-8")
          .send({model: "Scoopy"})
          .expect(200)
          .then(({body}) => {
            assert.equal(body.model, "Scoopy");
          }),
      );

      it("should replace fuel", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[3]._id}`)
          .type("application/json;charset=utf-8")
          .send({fuel: "diesel"})
          .expect(200)
          .then(({body}) => {
            assert.equal(body.fuel, "diesel");
          }),
      );

      it("should replace cc", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[4]._id}`)
          .type("application/json;charset=utf-8")
          .send({cc: 150})
          .expect(200)
          .then(({body}) => {
            assert.equal(body.cc, 150);
          }),
      );
    });

    describe("@price", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 1,
        }, {
          model: "User",
          requires: {
            Organization: "o2o",
          },
          count: 1,
        }, {
          model: "Vehicle",
          requires: {
            Organization: [0, 0, 0, 0],
          },
          data: [{
            pricePerDay: 100,
          }, {
            pricePerDay: 0,
            status: VehicleController.statuses.new,
          }, {
            plate: "",
            pricePerDay: 0,
            status: VehicleController.statuses.new,
          }, {
            pricePerDay: 100,
          }],
        }])
          .then(result => {
            data = result;
            return catalogController.create(catalog);
          }),
      );

      it("should edit vehicle (active -> active)", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[0]._id}`)
          .type("application/json;charset=utf-8")
          .send({pricePerDay: 200})
          .expect(200)
          .then(({body}) => {
            assert.equal(body.pricePerDay, 200);
            assert.equal(body.status, VehicleController.statuses.active);
          }),
      );

      it("should edit vehicle (new -> active)", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[1]._id}`)
          .type("application/json;charset=utf-8")
          .send({pricePerDay: 200})
          .expect(200)
          .then(({body}) => {
            assert.equal(body.pricePerDay, 200);
            assert.equal(body.status, VehicleController.statuses.active);
          }),
      );

      it("should edit vehicle (new -> new)", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[2]._id}`)
          .type("application/json;charset=utf-8")
          .send({pricePerDay: 200})
          .expect(200)
          .then(({body}) => {
            assert.equal(body.pricePerDay, 200);
            assert.equal(body.status, VehicleController.statuses.new);
          }),
      );

      it("should edit vehicle (active -> new)", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[3]._id}`)
          .type("application/json;charset=utf-8")
          .send({pricePerDay: 0})
          .expect(200)
          .then(({body}) => {
            assert.equal(body.pricePerDay, 0);
            assert.equal(body.status, VehicleController.statuses.new);
          }),
      );

      after(tearDown);
    });

    describe("@throw ()", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 1,
        }, {
          model: "User",
          requires: {
            Organization: "o2o",
          },
          count: 1,
        }, {
          model: "Vehicle",
          requires: {
            Organization: [0],
          },
          count: 1,
        }])
          .then(result => {
            data = result;
            return catalogController.create(catalog);
          }),
      );

      it.skip("should throw `invalid-param` (plate)[invalid]", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[0]._id}`)
          .type("application/json;charset=utf-8")
          .send({plate: "crap"})
          .then(expect(400, [{reason: "invalid", name: "plate"}])),
      );

      it("should throw `invalid-param` (year)[min]", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[0]._id}`)
          .type("application/json;charset=utf-8")
          .send({year: 1})
          .then(expect(400, [{reason: "min", name: "year"}])),
      );

      it("should throw `invalid-param` (year)[max]", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[0]._id}`)
          .type("application/json;charset=utf-8")
          .send({year: date.getFullYear() + 1})
          .then(expect(400, [{reason: "max", name: "year"}])),
      );

      it("should throw `invalid-param` (color)[invalid]", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[0]._id}`)
          .type("application/json;charset=utf-8")
          .send({color: "black"})
          .then(expect(400, [{reason: "invalid", name: "color"}])),
      );

      it("should throw `invalid-param` (price-per-x)[min]", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[0]._id}`)
          .type("application/json;charset=utf-8")
          .send({
            pricePerHour: -1,
            pricePerDay: -1,
            pricePerMonth: -1,
          })
          .then(expect(400, [{
            reason: "min",
            name: "pricePerHour",
          }, {
            reason: "min",
            name: "pricePerDay",
          }, {
            reason: "min",
            name: "pricePerMonth",
          }])),
      );

      after(tearDown);
    });

    describe("@throw (catalog)", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 1,
        }, {
          model: "User",
          requires: {
            Organization: "o2o",
          },
          count: 1,
        }, {
          model: "Vehicle",
          requires: {
            Organization: [0],
          },
          count: 1,
        }])
          .then(result => {
            data = result;
            return catalogController.create(catalog);
          }),
      );

      it("should throw `invalid-param` (type)", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[0]._id}`)
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({type: "crap"}))
          .then(expect(400, [{reason: "invalid", name: "type"}])),
      );

      it("should throw `invalid-param` (brand)", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[0]._id}`)
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({brand: "crap"}))
          .then(expect(400, [{reason: "invalid", name: "brand"}])),
      );

      it("should throw `invalid-param` (brand)", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[0]._id}`)
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({type: "bike", brand: "Toyota"}))
          .then(expect(400, [{reason: "combination", name: "brand"}])),
      );

      it("should throw `invalid-param` (model)", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[0]._id}`)
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({model: "crap"}))
          .then(expect(400, [{reason: "invalid", name: "model"}])),
      );

      it("should throw `invalid-param` (model)", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[0]._id}`)
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({brand: "Kawasaki", model: "Vario"}))
          .then(expect(400, [{reason: "combination", name: "model"}])),
      );

      it("should throw `invalid-param` (fuel)", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[0]._id}`)
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({fuel: "crap"}))
          .then(expect(400, [{reason: "invalid", name: "fuel"}])),
      );

      it("should throw `invalid-param` (fuel)", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[0]._id}`)
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({model: "Vario", fuel: "diesel"}))
          .then(expect(400, [{reason: "combination", name: "fuel"}])),
      );

      it("should throw `invalid-param` (cc)", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[0]._id}`)
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({cc: "crap"}))
          .then(expect(400, [{reason: "invalid", name: "cc"}])),
      );

      it("should throw `invalid-param` (cc)", () =>
        superapp
          .login(data.User[0])
          .put(`/api/vehicles/${data.Vehicle[0]._id}`)
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({type: "car", brand: "Suzuki", model: "Swift", fuel: "diesel", cc: 1500}))
          .then(expect(400, [{reason: "combination", name: "cc"}])),
      );

      after(tearDown);
    });
  });

  describe("GET /vehicles/:_id", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 2,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 2,
      }, {
        model: "Vehicle",
        requires: {
          Organization: [0, 1, 0],
        },
        data: [{
          plate: "DK1234GA",
        }, {
          plate: "DK2345GA",
        }, {
          plate: "DK3456GA",
          status: VehicleController.statuses.inactive,
        }],
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should get vehicle", () =>
      superapp
        .login(data.User[0])
        .get(`/api/vehicles/${data.Vehicle[0]._id}`)
        .expect(200)
        .then(({body}) => {
          assert.equal(body.plate, data.Vehicle[0].plate);
        }),
    );

    it("should throw `access-denied`", () =>
      superapp
        .login(data.User[0])
        .get(`/api/vehicles/${data.Vehicle[1]._id}`)
        .then(expect(403, [{reason: "organization"}])),
    );

    it.skip("should throw `not-active` (vehicle)", () =>
      superapp
        .login(data.User[0])
        .get(`/api/vehicles/${data.Vehicle[2]._id}`)
        .then(expect(410, [{name: "vehicle"}])),
    );

    it("should throw `vehicle-not-found`", () =>
      superapp
        .login(data.User[0])
        .get(`/api/vehicles/${getNewId()}`)
        .then(expect(404, [{name: "vehicle"}])),
    );

    it("should throw `invalid-param`", () =>
      superapp
        .login(data.User[0])
        .get(`/api/vehicles/${"crap"}`)
        .then(expect(400, [{reason: "invalid", name: "_id"}])),
    );

    after(tearDown);
  });

  describe("DELETE /vehicles/:_id", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 2,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 2,
      }, {
        model: "Vehicle",
        requires: {
          Organization: [0, 1, 0],
        },
        data: [{
          plate: "DK1234GA",
        }, {
          plate: "DK2345GA",
        }, {
          plate: "DK3456GA",
          status: VehicleController.statuses.inactive,
        }],
      }, {
        model: "Booking",
        requires: {
          Vehicle: [0, 0, 0],
          Organization: [[0, 1], [0, 1], [0, 1]],
        },
        data: [{
          status: BookingController.statuses.new,
        }, {
          status: BookingController.statuses.accepted,
        }, {
          status: BookingController.statuses.cancelled,
        }],
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should delete vehicle", () =>
      superapp
        .login(data.User[0])
        .delete(`/api/vehicles/${data.Vehicle[0]._id}`)
        .expect(200)
        .then(({body}) => {
          assert.equal(body.plate, data.Vehicle[0].plate);
          assert.equal(body.status, VehicleController.statuses.inactive);
          return mailController.find()
            .then(mails => {
              assert.deepEqual(mails.map(mail => mail.type).sort(), ["bookingCancelledByRenter", "bookingRejectedByRenter"]);
            });
        }),
    );

    it("should throw `access-denied`", () =>
      superapp
        .login(data.User[0])
        .delete(`/api/vehicles/${data.Vehicle[1]._id}`)
        .then(expect(403, [{reason: "organization"}])),
    );

    it("should throw `not-active` (vehicle)", () =>
      superapp
        .login(data.User[0])
        .delete(`/api/vehicles/${data.Vehicle[2]._id}`)
        .then(expect(410, [{name: "vehicle"}])),
    );

    it("should throw `vehicle-not-found`", () =>
      superapp
        .login(data.User[0])
        .delete(`/api/vehicles/${getNewId()}`)
        .then(expect(404, [{name: "vehicle"}])),
    );

    it("should throw `invalid-param`", () =>
      superapp
        .login(data.User[0])
        .delete(`/api/vehicles/${"crap"}`)
        .then(expect(400, [{reason: "invalid", name: "_id"}])),
    );

    after(tearDown);
  });

  describe("GET /vehicles/:_id/bookings", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 2,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 2,
      }, {
        model: "Vehicle",
        requires: {
          Organization: [0, 1],
        },
        count: 2,
      }, {
        model: "Booking",
        requires: {
          Vehicle: [0, 1],
          Organization: [[0, 1], [0, 1], [1, 1]],
        },
        data: [{
          status: BookingController.statuses.accepted,
        }, {
          status: BookingController.statuses.rejected,
        }, {
          status: BookingController.statuses.accepted,
        }],
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should get bookings", () =>
      superapp
        .login(data.User[0])
        .get(`/api/vehicles/${data.Vehicle[0]._id}/bookings`)
        .query({
          range: getRange(-10, +10),
        })
        .expect(200)
        .then(({body}) => {
          assert.equal(body.list.length, 1);
          assert.equal(body.list[0]._id, data.Booking[0]._id.toString());
        }),
    );

    it("should throw `required`", () =>
      superapp
        .login(data.User[0])
        .get(`/api/vehicles/${data.Vehicle[0]._id}/bookings`)
        .query({})
        .then(expect(400, [{reason: "required", name: "range"}])),
    );

    it("should throw `invalid-param`", () =>
      superapp
        .login(data.User[0])
        .get(`/api/vehicles/${data.Vehicle[0]._id}/bookings`)
        .query({
          range: "crap",
        })
        .then(expect(400, [{reason: "invalid", name: "range"}])),
    );

    it("should throw `access-denied`", () =>
      superapp
        .login(data.User[0])
        .get(`/api/vehicles/${data.Vehicle[1]._id}/bookings`)
        .query({
          range: getRange(-10, +10),
        })
        .then(expect(403, [{reason: "organization"}])),
    );

    after(tearDown);
  });

  describe("GET /vehicles/typeahead", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 1,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 2,
      }, {
        model: "Vehicle",
        requires: {
          Organization: [0, 0, 0, 1, 1],
        },
        data: [{
          brand,
          model,
          cc: 110,
        }, {
          brand,
          model,
          cc: 125,
        }, {
          brand,
          model: "NMax",
          cc: 150,
        }, {
          brand,
          model: "NMax",
          cc: 150,
        }, {
          brand,
          model,
          cc: 110,
        }, {
          brand,
          model: "NMax",
          cc: 110,
        }],
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should get data", () =>
      superapp
        .login(data.User[0])
        .get("/api/vehicles/typeahead")
        .expect(200)
        .then(({body}) => {
          assert.equal(body.list.length, 3);
        }),
    );

    after(tearDown);
  });

  describe("GET /vehicles/select", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 1,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 2,
      }, {
        model: "Vehicle",
        requires: {
          Organization: [0, 0, 0, 1, 1],
        },
        data: [{
          brand,
          model,
          cc: 110,
        }, {
          brand,
          model,
          cc: 125,
        }, {
          brand,
          model,
          cc: 150,
        }, {
          brand,
          model,
          cc: 110,
        }, {
          brand,
          model: "NMax",
          cc: 110,
        }],
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should get data", () =>
      superapp
        .login(data.User[0])
        .get("/api/vehicles/select")
        .expect(200)
        .then(({body}) => {
          assert.equal(body.list.length, 3);
        }),
    );

    after(tearDown);
  });

  describe("POST /vehicles/missing", () => {
    before(() =>
      setUp([{
        model: "Organization",
        count: 1,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 1,
      }])
        .then(result => {
          data = result;
          return catalogController.create(catalog);
        }),
    );

    it("should add vehicle", () =>
      superapp
        .login(data.User[0])
        .post("/api/vehicles/missing")
        .type("application/json;charset=utf-8")
        .send({
          type: "bike",
          brand: "Honda",
          model: "Scorpio",
          variant: "GTX",
          cc: 1000,
          fuel: "petrol",
        })
        .expect(200)
        .then(({body}) => {
          return mailController.find()
            .then(mails => {
              assert.deepEqual(mails.map(mail => mail.type).sort(), [MailController.types["back_office"].missing]);
            });
        }),
    );

    it("should throw `invalid-param` (type)", () =>
      superapp
        .login(data.User[0])
        .post("/api/vehicles/missing")
        .type("application/json;charset=utf-8")
        .send({})
        .then(expect(400, [{reason: "required", name: "type"}])),
    );

    it("should throw `invalid-param` (type)", () =>
      superapp
        .login(data.User[0])
        .post("/api/vehicles/missing")
        .type("application/json;charset=utf-8")
        .send({
          type: "",
        })
        .then(expect(400, [{reason: "invalid", name: "type"}])),
    );

    it("should throw `invalid-param` (brand)", () =>
      superapp
        .login(data.User[0])
        .post("/api/vehicles/missing")
        .type("application/json;charset=utf-8")
        .send({
          type,
        })
        .then(expect(400, [{reason: "required", name: "brand"}])),
    );

    it("should throw `invalid-param` (brand)", () =>
      superapp
        .login(data.User[0])
        .post("/api/vehicles/missing")
        .type("application/json;charset=utf-8")
        .send({
          type,
          brand: "",
        })
        .then(expect(400, [{reason: "invalid", name: "brand"}])),
    );

    it("should throw `invalid-param` (model)", () =>
      superapp
        .login(data.User[0])
        .post("/api/vehicles/missing")
        .type("application/json;charset=utf-8")
        .send({
          type,
          brand,
        })
        .then(expect(400, [{reason: "required", name: "model"}])),
    );

    it("should throw `invalid-param` (model)", () =>
      superapp
        .login(data.User[0])
        .post("/api/vehicles/missing")
        .type("application/json;charset=utf-8")
        .send({
          type,
          brand,
          model: "",
        })
        .then(expect(400, [{reason: "invalid", name: "model"}])),
    );

    it("should throw `invalid-param` (fuel)", () =>
      superapp
        .login(data.User[0])
        .post("/api/vehicles/missing")
        .type("application/json;charset=utf-8")
        .send({
          type,
          brand,
          model,
        })
        .then(expect(400, [{reason: "required", name: "fuel"}])),
    );

    it("should throw `invalid-param` (fuel)", () =>
      superapp
        .login(data.User[0])
        .post("/api/vehicles/missing")
        .type("application/json;charset=utf-8")
        .send({
          type,
          brand,
          model,
          fuel: "",
        })
        .then(expect(400, [{reason: "invalid", name: "fuel"}])),
    );

    it("should throw `invalid-param` (cc)[required]", () =>
      superapp
        .login(data.User[0])
        .post("/api/vehicles/missing")
        .type("application/json;charset=utf-8")
        .send({
          type,
          brand,
          model,
          fuel,
        })
        .then(expect(400, [{reason: "required", name: "cc"}])),
    );

    it("should throw `invalid-param` (cc)[invalid]", () =>
      superapp
        .login(data.User[0])
        .post("/api/vehicles/missing")
        .type("application/json;charset=utf-8")
        .send({
          type,
          brand,
          model,
          fuel,
          cc: "",
        })
        .then(expect(400, [{reason: "invalid", name: "cc"}])),
    );

    it("should throw `invalid-param` (cc)[min]", () =>
      superapp
        .login(data.User[0])
        .post("/api/vehicles/missing")
        .type("application/json;charset=utf-8")
        .send({
          type,
          brand,
          model,
          fuel,
          cc: 0,
        })
        .then(expect(400, [{reason: "invalid", name: "cc"}])),
    );

    after(tearDown);
  });

  describe("POST /vehicles/batch", () => {
    describe("@pass", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 1,
        }, {
          model: "User",
          requires: {
            Organization: "o2o",
          },
          count: 1,
        }])
          .then(result => {
            data = result;
            return catalogController.create(catalog);
          }),
      );

      it("should create vehicles", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles/batch")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({count: 3}))
          .expect(200)
          .then(({body}) => {
            assert.equal(body.count, 3);
            assert.equal(body.list.length, 3);
            assert.equal(body.list[0].status, VehicleController.statuses.new);
            assert.equal(body.list[1].status, VehicleController.statuses.new);
            assert.equal(body.list[2].status, VehicleController.statuses.new);
          }),
      );

      after(tearDown);
    });

    describe("@throw (batch)", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 1,
        }, {
          model: "User",
          requires: {
            Organization: "o2o",
          },
          count: 1,
        }])
          .then(result => {
            data = result;
          }),
      );

      it("should throw `invalid-param` (count)[min]", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles/batch")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({count: 0}))
          .then(expect(400, [{reason: "invalid", name: "count"}])),
      );

      it("should throw `invalid-param` (count)[max]", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles/batch")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({count: 25}))
          .then(expect(400, [{reason: "invalid", name: "count"}])),
      );
    });

    describe("@throw (catalog)", () => {
      before(() =>
        setUp([{
          model: "Organization",
          count: 1,
        }, {
          model: "User",
          requires: {
            Organization: "o2o",
          },
          count: 1,
        }])
          .then(result => {
            data = result;
            return catalogController.create(catalog);
          }),
      );

      it("should throw `required` [type](required)", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles/batch")
          .type("application/json;charset=utf-8")
          .send({count: 1})
          .then(expect(400, [{reason: "required", name: "type"}])),
      );

      it("should throw `invalid-param` [type](invalid)", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles/batch")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({count: 1, type: "crap"}))
          .then(expect(400, [{reason: "invalid", name: "type"}])),
      );

      it("should throw `invalid-param` [model](combination)", () =>
        superapp
          .login(data.User[0])
          .post("/api/vehicles/batch")
          .type("application/json;charset=utf-8")
          .send(myVehicleObject({count: 1, brand: "Kawasaki", model: "Vario"}))
          .then(expect(400, [{reason: "combination", name: "model"}])),
      );

      after(tearDown);
    });
  });
});
