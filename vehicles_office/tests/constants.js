import {date} from "../vehicles_office/server/constants/date";

export const timeZone = "Pacific/Chatham"; // UTC + 12:45
export const email = "trejgun@gmail.com";
export const password = "My5up3r5tr0ngP@55w0rd";
export const confirm = password;
export const systemId = "ffffffffffffffffffffffff";
export const phoneNumber = "+62 (812) 3919-8760";
export const fullName = "Trej Gun";
export const companyName = "Test Company Name";
export const domainName = "test-domain.com";
export const coordinates = [14, 88];
export const price = 100;
export const pricePerDay = 25;
export const notes = "some notes ".repeat(100);
export const country = "AU";

export const type = "bike";
export const brand = "Honda";
export const model = "Vario";
export const fuel = "petrol";
export const cc = 125;
export const year = date.getFullYear();

export const title = "title";
export const description = "description ".repeat(100);
