import assert from "power-assert";
import moment from "moment";
import {setUp, tearDown} from "../../../../test-utils/flow";
import {getRange} from "../../../../test-utils/utils";
import {date} from "../../../server/constants/date";

import VehicleController from "../../../server/controllers/impl/vehicle";

let data;

describe("Vehicle", () => {
  describe("#getAvailability", () => {
    const vehicleController = new VehicleController();

    before(() =>
      setUp([{
        model: "Organization",
        count: 1,
      }, {
        model: "User",
        requires: {
          Organization: "o2o",
        },
        count: 1,
      }, {
        model: "Vehicle",
        requires: {
          Organization: "m2o",
        },
        count: 2,
      }, {
        model: "Booking",
        requires: {
          Organization: "m2o",
          Vehicle: "o2o",
        },
        data: [{
          range: getRange(2, 4),
        }, {
          range: getRange(-2, 2),
        }],
      }])
        .then(result => {
          data = result;
        }),
    );

    it("should get availability for one date", () =>
      vehicleController.getAvailability(data.Vehicle.map(vehicle => vehicle._id), date, date)
        .then(availability => {
          assert.deepEqual(availability.map(_id => _id.toString()), [data.Vehicle[1]._id.toString()]);
        }),
    );

    // TODO flickering
    it("should get availability for date range", () =>
      vehicleController.getAvailability(data.Vehicle.map(vehicle => vehicle._id), moment(date).add(1, "d").toDate(), moment(date).add(3, "d").toDate())
        .then(availability => {
          assert.deepEqual(availability.map(_id => _id.toString()), [data.Vehicle[0]._id.toString(), data.Vehicle[1]._id.toString()]);
        }),
    );

    after(tearDown);
  });
});
