import assert from "power-assert";
import moment from "moment";
import {calcPrice} from "../../client/utils/date";


describe("Date", () => {
  describe("#calcPrice", () => {
    describe("40 days", () => {
      const range = moment.range("2000-01-01T00:00:00.000Z/2000-02-10T00:00:00.000Z");

      it("pricePerDay", () => {
        const price = calcPrice({pricePerDay: 100}, range);
        assert.equal(price, 100 * 40);
      });

      it("pricePerMonth + pricePerDay", () => {
        const price = calcPrice({pricePerMonth: 2500, pricePerDay: 100}, range);
        assert.equal(price, (1 * 2500) + (10 * 100));
      });
    });
  });
});
