import assert from "power-assert";
import {makeError} from "../../server/utils/error";

describe("Error", () => {
  describe("#makeError", () => {
    it("makeError name", () => {
      const error = makeError("invalid-param");
      assert.equal(error.message, "invalid-param");
      assert.equal(error.status, 400);
    });

    it("makeError name/code", () => {
      const error = makeError("invalid-param", 418);
      assert.equal(error.message, "invalid-param");
      assert.equal(error.status, 418);
    });

    it("makeError name/code/data", () => {
      const url = "/";
      const error = makeError("page-not-found", 404, {url});
      assert.equal(error.message, "page-not-found");
      assert.equal(error.status, 404);
      assert.equal(error.url, url);
    });
  });
});
