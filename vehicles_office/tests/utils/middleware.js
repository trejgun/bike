import assert from "power-assert";
import {checkActive, checkModel, checkOrganization} from "../../server/controllers/abstract/middleware";
import {getNewId} from "../../server/shared/utils/mongoose";


describe("Middleware", () => {
  class TestController {
    static realm = "realm";
    static statuses = {
      active: "active",
      semiactive: "semiactive",
      inactive: "inactive",
    };
    static displayName = "Test";
  }

  const testController = new TestController();

  describe("#checkActive", () => {
    it("active", () => {
      const model = {
        status: TestController.statuses.active,
      };
      const user = {};
      assert.doesNotThrow(() => {
        checkActive().bind(testController)(model, {user});
      });
    });

    it("inactive", () => {
      const model = {
        status: TestController.statuses.inactive,
      };
      const user = {};
      assert.throws(() => {
        checkActive().bind(testController)(model, {user});
      }, e => e.message === "not-active");
    });
  });

  describe("#checkModel", () => {
    const model = {};
    const user = {};

    it("checkModel with valid user and model", () => {
      assert.doesNotThrow(() => {
        checkModel().bind(testController)(model, {user});
      });
    });

    it("checkModel without user", () => {
      assert.doesNotThrow(() => {
        checkModel().bind(testController)(model);
      });
    });

    it("checkModel without model", () => {
      assert.throws(() => {
        checkModel().bind(testController)(null, {user});
      }, e => e.message === "not-found");
    });
  });

  describe("#checkOrganization", () => {
    const _id = getNewId();
    const model = {
      organizations: [{_id}],
    };

    it("checkOrganization same", () => {
      const user = {
        organizations: [{_id}],
      };
      assert.doesNotThrow(() => {
        checkOrganization().bind(testController)(model, {user});
      });
    });

    it("checkOrganization different", () => {
      const user = {
        organizations: [{_id: getNewId()}],
      };
      assert.throws(() => {
        checkOrganization().bind(testController)(model, {user});
      }, e => e.message === "access-denied");
    });
  });
});
