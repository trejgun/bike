import assert from "power-assert";
import {
  getObject,
  getRandomColor,
  getRandomElement,
  getRandomFloat,
  getRandomString,
  isType,
  toDollars,
  toTitleCase,
} from "../../server/utils/misc";


describe("Misc", () => {
  describe("#getObject", () => {
    const object = {a: {b: {c: "d"}}};

    it("should get object", () => {
      assert.equal(getObject("a.b.c", object), "d");
    });

    it("should get object (delimeter)", () => {
      assert.equal(getObject("a/b/c", object, "/"), "d");
    });
  });

  describe("#getRandomColor", () => {
    it("random color", () => {
      const pattern = /^#[A-Z0-9]{6}$/i;
      const str = getRandomColor();
      assert.equal(pattern.test(str), true);
    });
  });

  describe("#getRandomElement", () => {
    it("should get random element", () => {
      const array = [0, 1, 2, 3, 4];
      assert.ok(array.includes(getRandomElement(array)));
    });

    it("should get random element", () => {
      assert.equal(getRandomElement(), void 0);
    });
  });

  describe("#getRandomString", () => {
    const patterns = [/[0-9]/, /[A-Z]/, /[A-Z0-9]/, /[A-Z0-9]/i];
    patterns.forEach((pattern, i) => {
      it(`random string for a type ${i}`, () => {
        const length = Math.ceil((Math.random() * 100));
        const str = getRandomString(length, i);
        assert.equal(patterns[i].test(str), true);
        assert.equal(str.length, length);
      });
    });

    it("random string for default settings", () => {
      const pattern = /[A-Z0-9]/i;
      const str = getRandomString();
      assert.equal(pattern.test(str), true);
      assert.equal(str.length, 64);
    });
  });

  describe("#getRandomFloat", () => {
    const patterns1 = [[-20, -10], [-10, 10], [0, 0], [10, -10], [10, 20]];
    patterns1.forEach(pattern => {
      it(`random return int (${pattern[0]}, ${pattern[1]})`, () => {
        const int = getRandomFloat(pattern[0], pattern[1]);
        assert.ok(int >= Math.min(pattern[0], pattern[1]));
        assert.ok(int <= Math.max(pattern[0], pattern[1]));
        assert.equal(int % 1, 0);
      });
    });

    const patterns2 = [[Math.E, Math.PI]];
    patterns2.forEach(pattern => {
      it(`should return float (${pattern[0]}, ${pattern[1]})`, () => {
        const int = getRandomFloat(pattern[0], pattern[1], 3);
        assert.ok(int >= Math.min(pattern[0], pattern[1]));
        assert.ok(int <= Math.max(pattern[0], pattern[1]));
        assert.ok(int % 1 > 0);
      });
    });

    const patterns3 = [[-Infinity, Infinity], [Number.NaN, 0], [Number.NaN, Infinity]];
    patterns3.forEach(pattern => {
      it(`should return NaN (${pattern[0]}, ${pattern[1]})`, () => {
        const int = getRandomFloat(pattern[0], pattern[1]);
        assert.ok(Number.isNaN(int));
      });
    });
  });

  describe("#toDollars", () => {
    it("should format number", () => {
      assert.equal(toDollars(Math.PI * 100), "$3.14");
    });

    it("should format 0", () => {
      assert.equal(toDollars(), "$0.00");
    });
  });

  describe("#toTitleCase", () => {
    it("should convert file name", () => {
      assert.equal(toTitleCase("controller.js"), "Controller");
    });

    it("should convert file name with dash", () => {
      assert.equal(toTitleCase("abstract-controller.js"), "AbstractController");
    });
  });

  describe("#isType", () => {
    const variables = [{}, [], void 0, null, new Date(), "string", 1, new Error(), new RegExp()]; // eslint-disable-line no-new-object, no-array-constructor
    const types = ["Object", "Array", "Undefined", "Null", "Date", "String", "Number", "Error", "RegExp"];
    variables.forEach((variable, i) => {
      it(`check a type of ${variable}`, () => {
        const result = isType(variable, types[i]);
        assert.equal(result, true);
      });
    });
  });
});
