import assert from "power-assert";
import {paramToArray, setRegExp, setStatus} from "../../server/utils/request";


describe("Request", () => {
  describe("#setStatus", () => {
    class TestController {
      static displayName = "test2";
      static statuses = {
        active: "active",
        inactive: "inactive",
      };
    }

    const error = "invalid-param";

    it("setStatus status type is String `active`", () => {
      const query = {status: "active"};
      const clean = {};
      setStatus({query}, clean, TestController);
      assert.deepEqual(clean, {status: "active"});
    });

    it("setStatus status type is String `crap`", () => {
      const query = {status: "crap"};
      const clean = {};
      assert.throws(() => {
        setStatus({query}, clean, TestController);
      }, e => e.message === error);
    });

    it("setStatus status type is Array `[]`", () => {
      const query = {status: []};
      const clean = {};
      setStatus({query}, clean, TestController);
      assert.deepEqual(clean, {});
    });

    it("setStatus status type is Array `[active]`", () => {
      const query = {status: ["active"]};
      const clean = {};
      setStatus({query}, clean, TestController);
      assert.deepEqual(clean, {status: "active"});
    });

    it("setStatus status type is Array `[active, inactive]`", () => {
      const query = {status: ["active", "inactive"]};
      const clean = {};
      setStatus({query}, clean, TestController);
      assert.deepEqual(clean, {status: {$in: ["active", "inactive"]}});
    });

    it("setStatus status type is Array `[active, crap]`", () => {
      const query = {status: ["active", "crap"]};
      const clean = {};
      assert.throws(() => {
        setStatus({query}, clean, TestController);
      }, e => e.message === error);
    });

    it("setStatus status type is Array `[crap]`", () => {
      const query = {status: ["crap"]};
      const clean = {};
      assert.throws(() => {
        setStatus({query}, clean, TestController);
      }, e => e.message === error);
    });
  });

  describe("#paramToArray", () => {
    const value = "!@#$%";
    const request = key => ({body: {key}});

    it("should return (empty)", () => {
      assert.deepEqual(paramToArray(request(value), "body", "non-existing-key"), []);
    });

    it("should return (default)", () => {
      assert.deepEqual(paramToArray(request(value), "body", "key"), [value]);
    });

    it("should return (string)", () => {
      assert.deepEqual(paramToArray(request(value), "body", "key", String), [value]);
    });

    it("should return (number)", () => {
      const value2 = 123;
      assert.deepEqual(paramToArray(request(value2), "body", "key", Number), [value2]);
    });

    it("should throw `invalid-param`", () => {
      assert.throws(() => {
        paramToArray(request(value), "body", "key", Number);
      }, e => e.message === "invalid-param");
    });
  });

  describe("#setRegExp", () => {
    it("setRegExp with an array of queries", () => {
      const clean = {};
      const query = {
        boolean: true,
        number: 12345,
        string: "trejgun@gmail.com",
        array: [1, 2, 3],
        object: {x: 1},
        specialchars: "|\\[]{}()^$+*?.-",
        crap: "crap",
      };
      const fields = ["number", "string", "array", "object", "not-existing", "specialchars"];
      setRegExp({query}, clean, fields);
      assert.deepEqual(clean, {
        number: {$regex: "12345", $options: "i"},
        string: {$regex: "trejgun@gmail\\.com", $options: "i"},
        array: {$regex: "1,2,3", $options: "i"},
        object: {$regex: "\\[object Object\\]", $options: "i"},
        specialchars: {$regex: "\\|\\\\\\[\\]\\{\\}\\(\\)\\^\\$\\+\\*\\?\\.-", $options: "i"},
      });
    });

    it("setRegExp with empty query and fields", () => {
      const clean = {};
      const query = {};
      setRegExp({query}, clean, []);
      assert.deepEqual(clean, {});
    });
  });
});
